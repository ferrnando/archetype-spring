<h1> Entendimento negocio</h1>

<b>PlantUml</b>

https://plantuml.com/

<hr />
```
@startuml
title Fornecedor Layout de Relatorio

(*) --> "Recebe codigo FAP"
--> "Pesquisa tab. 'aol.de_para_os_dasa'"

if "Encontrou ?" then
->[SIM] "Busca solicitacao 'aol.solicitacao'"

if "Encontrou ?" then
->[SIM] "Busca Entidade 'labor.entidade'"
--> "Busca 'MarcaEntidade'"

if "Encontrou ?" then
-->[SIM] "Busca laudo marca 'labor.laudo_marca'"
--> "Busca Marca 'labor.marca'" as marca
else
-->[NÃO] "Busca tab. 'labor.parametro_config' default"
--> marca
endif
--> "Recupera imagem do header conforme marca"
--> "Monta Base64"
--> "Busca nome Instituicao 'internet.internet_laudo_conf'"
if "Encontrou e tem nome da instituicao?" then
--> [SIM] "Pega nome instituicao"
--> "Monta DTO - Devolve no response" as rel
else
--> [NAO] "Pega nome da entidade 'labor.entidade'"
--> rel
endif
-->(*)
else
-->[NÃO] ===FIM===
endif
else
-->[NÃO] ===FIM===
endif


--> "Fim do Fluxo"
-->(*)
@enduml
```
