#!/usr/bin/env bash
echo '################### CLEAN PACKAGE ######################'
cd ..
mvn clean package -DskipTests

echo '################### CLEANING CONTAINERS ######################'
cd docker
docker-compose down
docker system prune -f

echo '################### STARTING MICROSERVICE  ######################'
docker-compose up -d --build api

echo