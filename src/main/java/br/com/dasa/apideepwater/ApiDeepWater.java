package br.com.dasa.apideepwater;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ApiDeepWater {

	public static void main(String[] args) {
		SpringApplication.run(ApiDeepWater.class, args);
	}

}
