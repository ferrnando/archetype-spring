package br.com.dasa.apideepwater.configs;

import br.com.dasa.apideepwater.exceptions.dtos.ErrorResponseDTO;
import com.fasterxml.classmate.TypeResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.builders.*;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Response;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

@EnableWebMvc
@Configuration
public class SwaggerConfig {

    public static final Tag REPORT_TAG = new Tag("Report", "Operations about report");

    @Bean
    public Docket api() {
        var typeResolver = new TypeResolver();
        return new Docket(DocumentationType.OAS_30)
                .groupName("V1")
                .select()
                .apis(RequestHandlerSelectors.basePackage("br.com.dasa.apideepwater.controllers"))
                .paths(PathSelectors.any())
                .build()
                .useDefaultResponseMessages(false)
                .globalResponses(HttpMethod.GET, globalGetResponse())
                .additionalModels(typeResolver.resolve(ErrorResponseDTO.class))
                .apiInfo(apiInfo())
                .tags(REPORT_TAG);
    }

    private List<Response> globalGetResponse() {
        return Arrays.asList(
                new ResponseBuilder()
                        .code(String.valueOf(HttpStatus.NOT_FOUND.value()))
                        .description("Resource not found")
                        .representation(MediaType.APPLICATION_JSON)
                        .apply(getModelError())
                        .build(),
                new ResponseBuilder()
                        .code(String.valueOf(HttpStatus.BAD_REQUEST.value()))
                        .description("Bad request")
                        .representation(MediaType.APPLICATION_JSON)
                        .apply(getModelError())
                        .build());
    }
    private Consumer<RepresentationBuilder> getModelError() {
        return r -> r.model(m -> m.name("ErrorResponseDTO")
                .referenceModel(ref -> ref.key(k -> k.qualifiedModelName(
                        q -> q.name("Error Response").namespace("br.com.dasa.apideepwater.exceptions.dtos")))));
    }

    public ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Custumer Report Template API")
                .description("Provides report template for use")
                .version("1.0")
                .contact(new Contact("Dasa", "www.dasa.com.br", "dasa@email.com"))
                .build();
    }

}
