package br.com.dasa.apideepwater.services;

import br.com.dasa.apideepwater.entities.Entidade;
import br.com.dasa.apideepwater.entities.InternetLaudoConf;
import br.com.dasa.apideepwater.repositories.InternetLaudoConfRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class GetInstitutionNameService {
    private final InternetLaudoConfRepository internetLaudoConfRepository;

    public String execute(Entidade entidade) {
        return getInstitutionName(entidade);
    }

    private String getInstitutionName(Entidade entity) {
        String institutionName;

        final var internetReportConf = getByIdEntity(entity.getId());

        if (internetReportConf.isEmpty() || internetReportConf.get().getInstituicaoImp().isEmpty()) {
            institutionName = entity.getNome(); // entidade.getEntFantasia()?
        } else {
            institutionName = internetReportConf.get().getInstituicaoImp();
        }
        return institutionName;
    }

    private Optional<InternetLaudoConf> getByIdEntity(Long id) {
        return internetLaudoConfRepository.findByIdEntidade(id);
    }
}
