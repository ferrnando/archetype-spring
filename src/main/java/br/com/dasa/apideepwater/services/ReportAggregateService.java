package br.com.dasa.apideepwater.services;

import br.com.dasa.apideepwater.exceptions.SolicitacaoNotFoundException;
import br.com.dasa.apideepwater.repositories.EntidadeRepository;
import br.com.dasa.apideepwater.repositories.SolicitacaoRepository;
import br.com.dasa.apideepwater.entities.DeParaOsDasa;
import br.com.dasa.apideepwater.entities.Entidade;
import br.com.dasa.apideepwater.entities.Solicitacao;
import br.com.dasa.apideepwater.exceptions.DeParaOsDasaNotFoundException;
import br.com.dasa.apideepwater.exceptions.EntidadeNotFoundException;
import br.com.dasa.apideepwater.exceptions.PacienteNotFoundException;
import br.com.dasa.apideepwater.repositories.DeParaOsDasaRepository;
import br.com.dasa.apideepwater.repositories.PacienteRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class ReportAggregateService {

    private final DeParaOsDasaRepository deParaOsDasaRepository;
    private final SolicitacaoRepository solicitacaoRepository;
    private final EntidadeRepository entidadeRepository;
    private final PacienteRepository pacienteRepository;

    protected DeParaOsDasa getFromToOrderDasa(String fapTeste) {
        log.info("Getting obj DePara - Fap: {}", fapTeste);
        return deParaOsDasaRepository.findByIdOs(fapTeste)
                .orElseThrow(DeParaOsDasaNotFoundException::new);
    }

    protected Solicitacao getRequest(DeParaOsDasa deParaOsDasa) {
        log.info("Getting obj Solicitacao - deparaOsDasa Id: {}",deParaOsDasa.getId());

        return solicitacaoRepository.findByIdOrdemServico(deParaOsDasa.getIdOrdemServico())
                .orElseThrow(SolicitacaoNotFoundException::new);
    }

    protected Entidade getEntity(Solicitacao solicitacao) {
        log.info("Getting ob Entidade - solicitacao id: {}", solicitacao.getId());

        return entidadeRepository.findById(solicitacao.getIdEntidade())
                .orElseThrow(EntidadeNotFoundException::new);
    }

    protected String getRegistrationLis(Solicitacao solicitacao) {
        log.info("Getting RegistrationLis - solicitacao id: {}", solicitacao.getId());

        final var paciente = pacienteRepository.findByIdLabor(solicitacao.getIdPaciente())
                .orElseThrow(PacienteNotFoundException::new);
        return paciente.getIdLocal();
    }

}
