package br.com.dasa.apideepwater.services;

import br.com.dasa.apideepwater.entities.Marca;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import java.io.IOException;
import java.util.Base64;

@Slf4j
@Service
@AllArgsConstructor
public class GenerateBinaryService {
    protected final static String HEADER_LOGO = "%s/cabecalho_%s_logo.png";
    protected final static String HEADER_AWARD = "%s/cabecalho_%s_premio.png";
    private final String PATH_IMAGES = "/images/";


    protected String execute(Marca brand, String headerLogo) {
        log.info("Generate base64 image Marca: {} - Header: {}",brand.getNome(),headerLogo);
        return getImageBase64(brand, headerLogo);
    }

    private String getImageBase64(Marca brand, String pathImage) {

        final var pathString = getPathString(brand, pathImage);

        return getImageBase64(pathString);
    }

    private String getImageBase64(String pathResource) {
        try {
            var premioBytes = FileCopyUtils.copyToByteArray(getClass().getResourceAsStream(pathResource));
            return Base64.getEncoder().encodeToString(premioBytes);

        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new RuntimeException("Could not convert image Base64");
    }

    private String getPathString(Marca brand, String format) {
        return PATH_IMAGES +
                String.format(
                        format,
                        brand.getNome().toLowerCase(),
                        brand.getNome().toLowerCase()
                );
    }

}
