package br.com.dasa.apideepwater.services;

import br.com.dasa.apideepwater.entities.Entidade;
import br.com.dasa.apideepwater.services.vos.CustomReportTemplateVO;
import br.com.dasa.apideepwater.util.CommonsUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
@AllArgsConstructor
public class GetCustomReportTemplate {

    private final GenerateBinaryService generateBinaryService;
    private final ReportAggregateService reportAggregateService;
    private final GetBrandService getBrandService;
    private final GetInstitutionNameService getInstitutionNameService;

    public CustomReportTemplateVO execute(final String fap) {

        Assert.notNull(fap, "code fap cannot be null");
        final var fromToOrderDasa = reportAggregateService.getFromToOrderDasa(fap);
        final var request = reportAggregateService.getRequest(fromToOrderDasa);
        final var entity = reportAggregateService.getEntity(request);
        final var brand = getBrandService.execute(entity);

        final var logoBase64 = generateBinaryService.execute(brand, GenerateBinaryService.HEADER_LOGO);
        final var certificateBase64 = generateBinaryService.execute(brand, GenerateBinaryService.HEADER_AWARD);
        final var issueDate = CommonsUtil.getDateTimeNow();
        final var registrationDate = CommonsUtil.formatDateTime(request.getDataSolicitacao());
        final var institutionName = getInstitutionNameService.execute(entity);
        final var registrationLis = reportAggregateService.getRegistrationLis(request);

        return getCustomReportTemplate(entity, logoBase64, certificateBase64, issueDate, registrationDate, institutionName, registrationLis);
    }

    private CustomReportTemplateVO getCustomReportTemplate(Entidade entity, String logoBase64, String certificateBase64, String issueDate, String registrationDate, String institutionName, String registrationLis) {
        return CustomReportTemplateVO.builder().
                logo(logoBase64)
                .certificate(certificateBase64)
                .issueDate(issueDate)
                .registrationDate(registrationDate)
                .institutionName(institutionName)
                .customerCity(entity.getCidade())
                .registrationLis(registrationLis)
                .build();
    }

}
