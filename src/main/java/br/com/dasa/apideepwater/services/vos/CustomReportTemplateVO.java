package br.com.dasa.apideepwater.services.vos;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class CustomReportTemplateVO {

    private final String logo;
    private final String certificate;
    private final String issueDate;
    private final String registrationDate;
    private final String institutionName;
    private final String customerCity;
    private final String registrationLis;
}
