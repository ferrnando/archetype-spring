package br.com.dasa.apideepwater.services;

import br.com.dasa.apideepwater.entities.*;
import br.com.dasa.apideepwater.exceptions.LaudoMarcaNotFoundException;
import br.com.dasa.apideepwater.exceptions.MarcaNotFoundException;
import br.com.dasa.apideepwater.exceptions.ParametroConfigNotFoundException;
import br.com.dasa.apideepwater.repositories.LaudoMarcaRepository;
import br.com.dasa.apideepwater.repositories.MarcaEntidadeRepository;
import br.com.dasa.apideepwater.repositories.MarcaRepository;
import br.com.dasa.apideepwater.repositories.ParametroConfigRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
@AllArgsConstructor
public class GetBrandService {

    private final String REPORT_HEADER_DEFAULT = "LAUDO_CABECALHO";

    private final ParametroConfigRepository parametroConfigRepository;
    private final MarcaEntidadeRepository marcaEntidadeRepository;
    private final LaudoMarcaRepository laudoMarcaRepository;
    private final MarcaRepository marcaRepository;


    protected Marca execute(Entidade entity) {
        return getBrand(entity);
    }

    private Marca getBrand(Entidade entity) {
        LaudoMarca reportBrand;
        final Optional<MarcaEntidade> brandEntity = getBrandEntity(entity);

        if (brandEntity.isPresent()) {
            reportBrand = getReportBrand(brandEntity.get().getIdMarca());
        } else {
            final var parameterConfig = getParameterConfig();
            reportBrand = getReportBrandByHeader(parameterConfig.getParametroUm());
        }
        return marcaRepository.findById(reportBrand.getIdMarca())
                .orElseThrow(MarcaNotFoundException::new);
    }

    private Optional<MarcaEntidade> getBrandEntity(Entidade entity) {
        return marcaEntidadeRepository.findByIdEntidade(entity.getId());
    }

    private LaudoMarca getReportBrand(Long id) {
        return laudoMarcaRepository.findById(id)
                .orElseThrow(ParametroConfigNotFoundException::new);
    }

    private ParametroConfig getParameterConfig() {
        return parametroConfigRepository.findByCodChamadaLike(REPORT_HEADER_DEFAULT)
                .orElseThrow(ParametroConfigNotFoundException::new);
    }

    private LaudoMarca getReportBrandByHeader(String header) {
        return laudoMarcaRepository.findByCabecalho(header)
                .orElseThrow(LaudoMarcaNotFoundException::new);
    }

}
