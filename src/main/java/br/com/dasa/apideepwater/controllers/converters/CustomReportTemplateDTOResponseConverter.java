package br.com.dasa.apideepwater.controllers.converters;

import br.com.dasa.apideepwater.controllers.dtos.responses.CustomReportTemplateDTOResponse;
import br.com.dasa.apideepwater.controllers.dtos.responses.CustomReportTemplateFooterDTOResponse;
import br.com.dasa.apideepwater.controllers.dtos.responses.CustomReportTemplateFooterLabDTOResponse;
import br.com.dasa.apideepwater.controllers.dtos.responses.CustomReportTemplateHeaderDTOResponse;
import br.com.dasa.apideepwater.services.vos.CustomReportTemplateVO;

public abstract class CustomReportTemplateDTOResponseConverter {

    private static final String DESCRIPTION = "Amostra coletada, identificada e enviada pelo lab. associado";
    private static final String RESPONSIBLE_PERSON_DESCRIPTION = "Sob a responsabilidade do Dr. Gustavo Aguiar Campana CRM 112.181";
    private static final String LAB_DESCRIPTION = "Laboratório registrado no CRM/SP sob o número 900128";
    private static final String LAB_ADDRESS = "Av. Juruá, 434, Barueri - SP";
    private static final String LAB_PHONE = "Telefone: 0800-643-8100";
    private static final String LAB_SITE = "www.alvaroapoio.com.br";

    public static CustomReportTemplateDTOResponse convert(CustomReportTemplateVO customReportTemplate) {
        return CustomReportTemplateDTOResponse
                .builder()
                .header(CustomReportTemplateHeaderDTOResponse
                        .builder()
                        .logo(customReportTemplate.getLogo())
                        .certificate(customReportTemplate.getCertificate())
                        .issueDate(customReportTemplate.getIssueDate())
                        .registrationDate(customReportTemplate.getRegistrationDate())
                        .institutionName(customReportTemplate.getInstitutionName())
                        .customerCity(customReportTemplate.getCustomerCity())
                        .description(DESCRIPTION)
                        .registrationLis(customReportTemplate.getRegistrationLis())
                        .build())
                .footer(CustomReportTemplateFooterDTOResponse
                        .builder()
                        .responsiblePersonDescription(RESPONSIBLE_PERSON_DESCRIPTION)
                        .lab(CustomReportTemplateFooterLabDTOResponse
                                .builder()
                                .description(LAB_DESCRIPTION)
                                .address(LAB_ADDRESS)
                                .phoneNumber(LAB_PHONE)
                                .site(LAB_SITE)
                                .build())
                        .build())
                .build();
    }

}