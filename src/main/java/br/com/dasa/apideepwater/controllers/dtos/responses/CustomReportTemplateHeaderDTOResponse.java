package br.com.dasa.apideepwater.controllers.dtos.responses;

import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Getter;

@ApiModel(value = "Custom Report Template Header Response", description = "Return header fields of custom template")
@Getter
@Builder
public class CustomReportTemplateHeaderDTOResponse {

    private String logo;
    private String certificate;
    private String issueDate;
    private String registrationDate;
    private String institutionName;
    private String customerCity;
    private String description;
    private String registrationLis;

}
