package br.com.dasa.apideepwater.controllers;

import br.com.dasa.apideepwater.controllers.assembly.ReportSwaggerOpenApi;
import br.com.dasa.apideepwater.controllers.converters.CustomReportTemplateDTOResponseConverter;
import br.com.dasa.apideepwater.controllers.dtos.responses.CustomReportTemplateDTOResponse;
import br.com.dasa.apideepwater.services.GetCustomReportTemplate;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@AllArgsConstructor
@RestController
@RequestMapping(value = "/v1/report", produces = MediaType.APPLICATION_JSON_VALUE)
public class ReportController implements ReportSwaggerOpenApi {

    private final GetCustomReportTemplate customService;

    @Override
    @GetMapping("/template/{fap}")
    public ResponseEntity<CustomReportTemplateDTOResponse> getCustomTemplate(final Long fap) {

        final var responseDTO = CustomReportTemplateDTOResponseConverter.convert(customService.execute(fap.toString()));

        return ResponseEntity.ok(responseDTO);
    }

}
