package br.com.dasa.apideepwater.controllers.dtos.responses;

import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Getter;

@ApiModel(value = "Custom Report Template Footer Lab Response", description = "Return lab fields")
@Getter
@Builder
public class CustomReportTemplateFooterLabDTOResponse {

    private String description;
    private String address;
    private String phoneNumber;
    private String site;

}
