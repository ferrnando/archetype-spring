package br.com.dasa.apideepwater.controllers.dtos.responses;

import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Getter;

@ApiModel(value = "Custom Report Template Footer Response", description = "Return lab and responsible")
@Getter
@Builder
public class CustomReportTemplateFooterDTOResponse {

    private String responsiblePersonDescription;
    private CustomReportTemplateFooterLabDTOResponse lab;

}
