package br.com.dasa.apideepwater.controllers.dtos.responses;

import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Getter;

@ApiModel(value = "Custom Report Template Response", description = "Return header and footer")
@Getter
@Builder
public class CustomReportTemplateDTOResponse {

    private CustomReportTemplateHeaderDTOResponse header;
    private CustomReportTemplateFooterDTOResponse footer;

}
