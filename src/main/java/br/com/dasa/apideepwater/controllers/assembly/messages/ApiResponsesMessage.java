package br.com.dasa.apideepwater.controllers.assembly.messages;

public class ApiResponsesMessage {

    public static final String HTTP_200 = "Return custom template";

    public static final String HTTP_404 = "Return custom header and custom footer from DB";

    public static final String DE_PARA_OS_NOT_FOUND = "1 - De/Para nao encontrado";
    public static final String ENTIDADE_NOT_FOUND = "1 - Entidade nao encontrado";
    public static final String LAUDO_MARCA_NOT_FOUND = "1 - LaudoMarca nao encontrado";
    public static final String MARCA_NOT_FOUND = "1 - Marca nao encontrado";
    public static final String PACIENTE_NOT_FOUND = "1 - Paciente nao encontrado";
    public static final String PARAMETRO_CONFIG_NOT_FOUND = "1 - ParametroConfig nao encontrado";
    public static final String SOLICITACAO_NOT_FOUND = "1 - Solicitacao nao encontrado";


    public static Integer getCode(String message) {
        return Integer.valueOf(message.substring(0, message.indexOf("-") - 1));
    }

    public static String getMessage(String message) {
        return message.substring(message.indexOf("-") + 2);
    }

}