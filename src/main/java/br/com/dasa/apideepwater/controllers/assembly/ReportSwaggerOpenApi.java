package br.com.dasa.apideepwater.controllers.assembly;

import br.com.dasa.apideepwater.controllers.assembly.messages.ApiResponsesMessage;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

@Api(tags = "Report")
public interface ReportSwaggerOpenApi {

    @ApiOperation(value = "Return custom template",
            notes = "Return custom header and custom footer from DB")
    @ApiResponses(value = { @ApiResponse(code = 200, message = ApiResponsesMessage.HTTP_200),
            @ApiResponse(code = 404, message = ApiResponsesMessage.HTTP_404)})

    ResponseEntity<?> getCustomTemplate(@PathVariable @ApiParam(required = true, value = "FAP (Ordem servico no Motion)", defaultValue = "542315098006") Long fap);

}
