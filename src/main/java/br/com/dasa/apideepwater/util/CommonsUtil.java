package br.com.dasa.apideepwater.util;

import java.time.LocalDateTime;

public abstract class CommonsUtil {

    public static String getDateTimeNow() {
        return LocalDateTime.now().toString();
    }

    public static String formatDateTime(LocalDateTime dataSolicitacao) {
        return dataSolicitacao.toString();
    }
}
