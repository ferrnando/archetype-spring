package br.com.dasa.apideepwater.repositories;

import br.com.dasa.apideepwater.entities.LaudoMarca;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LaudoMarcaRepository extends JpaRepository<LaudoMarca,Long> {
    Optional<LaudoMarca> findByCabecalho(String parametroUm);
}
