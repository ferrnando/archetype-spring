package br.com.dasa.apideepwater.repositories;

import br.com.dasa.apideepwater.entities.Solicitacao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SolicitacaoRepository extends JpaRepository<Solicitacao,Long> {
    Optional<Solicitacao> findByIdOrdemServico(Long idOrdemServico);
}
