package br.com.dasa.apideepwater.repositories;

import br.com.dasa.apideepwater.entities.Paciente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface PacienteRepository extends JpaRepository<Paciente,Long> {

    Optional<Paciente> findByIdLabor(Long idPaciente);
}

