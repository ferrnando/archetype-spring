package br.com.dasa.apideepwater.repositories;

import br.com.dasa.apideepwater.entities.ParametroConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ParametroConfigRepository extends JpaRepository<ParametroConfig,Long> {
    Optional<ParametroConfig> findByCodChamadaLike(String name);
}
