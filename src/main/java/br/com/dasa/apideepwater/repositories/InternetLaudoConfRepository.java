package br.com.dasa.apideepwater.repositories;

import br.com.dasa.apideepwater.entities.InternetLaudoConf;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface InternetLaudoConfRepository extends JpaRepository<InternetLaudoConf,Long> {
    Optional<InternetLaudoConf> findByIdEntidade(Long idEntidade);
}
