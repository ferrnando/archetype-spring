package br.com.dasa.apideepwater.repositories;

import br.com.dasa.apideepwater.entities.DeParaOsDasa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DeParaOsDasaRepository extends JpaRepository<DeParaOsDasa,Long> {
    Optional<DeParaOsDasa> findByIdOs(String fap);
}
