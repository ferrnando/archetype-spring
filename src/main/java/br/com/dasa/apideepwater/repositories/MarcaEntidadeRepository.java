package br.com.dasa.apideepwater.repositories;

import br.com.dasa.apideepwater.entities.MarcaEntidade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MarcaEntidadeRepository extends JpaRepository<MarcaEntidade,Long> {
    Optional<MarcaEntidade> findByIdEntidade(Long idEntidade);
}
