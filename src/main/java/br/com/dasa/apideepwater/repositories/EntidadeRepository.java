package br.com.dasa.apideepwater.repositories;

import br.com.dasa.apideepwater.entities.Entidade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EntidadeRepository extends JpaRepository<Entidade,Long> {
}
