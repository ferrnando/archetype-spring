package br.com.dasa.apideepwater.exceptions;

import br.com.dasa.apideepwater.exceptions.dtos.ErrorResponseDTO;
import br.com.dasa.apideepwater.exceptions.enums.MessagesEnum;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public abstract class BaseException extends RuntimeException {

    private final HttpStatus status;
    private final MessagesEnum messagesEnum;

    public BaseException(HttpStatus status, MessagesEnum messagesEnum) {
        this.status = status;
        this.messagesEnum = messagesEnum;
    }

    public ErrorResponseDTO getErrorResponse() {
        return new ErrorResponseDTO(messagesEnum);
    }

}
