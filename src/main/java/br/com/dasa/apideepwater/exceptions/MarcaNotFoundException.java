package br.com.dasa.apideepwater.exceptions;

import br.com.dasa.apideepwater.exceptions.enums.MessagesEnum;
import org.springframework.http.HttpStatus;

public class MarcaNotFoundException extends BaseException {
    public MarcaNotFoundException() {
        super(HttpStatus.NOT_FOUND, MessagesEnum.MARCA_NOT_FOUND);
    }
}
