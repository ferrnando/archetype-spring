package br.com.dasa.apideepwater.exceptions;

import br.com.dasa.apideepwater.exceptions.enums.MessagesEnum;
import org.springframework.http.HttpStatus;

public class EntidadeNotFoundException extends BaseException {
    public EntidadeNotFoundException() {
        super(HttpStatus.NOT_FOUND, MessagesEnum.ENTIDADE_NOT_FOUND);
    }
}
