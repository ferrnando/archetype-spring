package br.com.dasa.apideepwater.exceptions.handlers;

import br.com.dasa.apideepwater.exceptions.BaseException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {BaseException.class})
    protected ResponseEntity<Object> handleCustomException(BaseException ex, WebRequest webRequest){
        return ResponseEntity.status(ex.getStatus()).body(ex.getErrorResponse());
    }

}
