package br.com.dasa.apideepwater.exceptions.dtos;

import br.com.dasa.apideepwater.exceptions.enums.MessagesEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(value = "Error Response", description = "Campos de retorno de erro")
public class ErrorResponseDTO {

    @ApiModelProperty(value = "Código do erro", example = "1")
    private Integer cod;

    @ApiModelProperty(value = "Mensagem do erro", example = "Houve um erro")
    private String message;

    public ErrorResponseDTO(MessagesEnum messagesEnum) {
        this.cod = messagesEnum.getCod();
        this.message = messagesEnum.getMessage();
    }

}
