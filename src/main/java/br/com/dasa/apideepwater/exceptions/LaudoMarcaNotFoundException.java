package br.com.dasa.apideepwater.exceptions;

import br.com.dasa.apideepwater.exceptions.enums.MessagesEnum;
import org.springframework.http.HttpStatus;

public class LaudoMarcaNotFoundException extends BaseException {
    public LaudoMarcaNotFoundException() {
        super(HttpStatus.NOT_FOUND, MessagesEnum.LAUDO_MARCA_NOT_FOUND);
    }
}
