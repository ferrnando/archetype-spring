package br.com.dasa.apideepwater.exceptions;

import br.com.dasa.apideepwater.exceptions.enums.MessagesEnum;
import org.springframework.http.HttpStatus;

public class ParametroConfigNotFoundException extends BaseException {
    public ParametroConfigNotFoundException() {
        super(HttpStatus.NOT_FOUND, MessagesEnum.PARAMETRO_CONFIG_NOT_FOUND);
    }
}
