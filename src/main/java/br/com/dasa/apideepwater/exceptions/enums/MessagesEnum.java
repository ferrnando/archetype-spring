package br.com.dasa.apideepwater.exceptions.enums;

import br.com.dasa.apideepwater.controllers.assembly.messages.ApiResponsesMessage;
import lombok.Getter;

@Getter
public enum MessagesEnum {

    DE_PARA_OS_NOT_FOUND(ApiResponsesMessage.getCode(ApiResponsesMessage.DE_PARA_OS_NOT_FOUND),
            ApiResponsesMessage.getMessage(ApiResponsesMessage.DE_PARA_OS_NOT_FOUND)),

    ENTIDADE_NOT_FOUND(ApiResponsesMessage.getCode(ApiResponsesMessage.ENTIDADE_NOT_FOUND),
            ApiResponsesMessage.getMessage(ApiResponsesMessage.ENTIDADE_NOT_FOUND)),

    LAUDO_MARCA_NOT_FOUND(ApiResponsesMessage.getCode(ApiResponsesMessage.LAUDO_MARCA_NOT_FOUND),
            ApiResponsesMessage.getMessage(ApiResponsesMessage.LAUDO_MARCA_NOT_FOUND)),

    MARCA_NOT_FOUND(ApiResponsesMessage.getCode(ApiResponsesMessage.MARCA_NOT_FOUND),
            ApiResponsesMessage.getMessage(ApiResponsesMessage.MARCA_NOT_FOUND)),

    PACIENTE_NOT_FOUND(ApiResponsesMessage.getCode(ApiResponsesMessage.PACIENTE_NOT_FOUND),
            ApiResponsesMessage.getMessage(ApiResponsesMessage.PACIENTE_NOT_FOUND)),

    PARAMETRO_CONFIG_NOT_FOUND(ApiResponsesMessage.getCode(ApiResponsesMessage.PARAMETRO_CONFIG_NOT_FOUND),
            ApiResponsesMessage.getMessage(ApiResponsesMessage.PARAMETRO_CONFIG_NOT_FOUND)),

    SOLICITACAO_NOT_FOUND(ApiResponsesMessage.getCode(ApiResponsesMessage.SOLICITACAO_NOT_FOUND),
            ApiResponsesMessage.getMessage(ApiResponsesMessage.SOLICITACAO_NOT_FOUND));

    private final int cod;
    private final String message;

    MessagesEnum(int cod, String message) {
        this.cod = cod;
        this.message = message;
    }

}
