package br.com.dasa.apideepwater.exceptions;

import br.com.dasa.apideepwater.exceptions.enums.MessagesEnum;
import org.springframework.http.HttpStatus;

public class SolicitacaoNotFoundException extends BaseException {
    public SolicitacaoNotFoundException() {
        super(HttpStatus.NOT_FOUND, MessagesEnum.SOLICITACAO_NOT_FOUND);
    }
}
