package br.com.dasa.apideepwater.exceptions;

import br.com.dasa.apideepwater.exceptions.enums.MessagesEnum;
import org.springframework.http.HttpStatus;

public class PacienteNotFoundException extends BaseException {
    public PacienteNotFoundException() {
        super(HttpStatus.NOT_FOUND, MessagesEnum.PACIENTE_NOT_FOUND);
    }
}
