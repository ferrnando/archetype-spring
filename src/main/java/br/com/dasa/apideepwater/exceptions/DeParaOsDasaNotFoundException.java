package br.com.dasa.apideepwater.exceptions;

import br.com.dasa.apideepwater.exceptions.enums.MessagesEnum;
import org.springframework.http.HttpStatus;

public class DeParaOsDasaNotFoundException extends BaseException {

    public DeParaOsDasaNotFoundException() {
        super(HttpStatus.NOT_FOUND, MessagesEnum.DE_PARA_OS_NOT_FOUND);
    }

}
