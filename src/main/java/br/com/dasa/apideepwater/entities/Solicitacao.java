package br.com.dasa.apideepwater.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "solicitacao", catalog = "aol")
public class Solicitacao implements Serializable{

    @Id
    @Column(name = "idSolicitacao")
    private Long id;

    @Column(name = "idEntidade")
    private Long idEntidade;

    @Column(name = "idPaciente")
    private Long idPaciente;

    @Column(name = "DataSolicitacao")
    private LocalDateTime dataSolicitacao;

    @Column(name = "idOrdemServico")
    private Long idOrdemServico;

}