package br.com.dasa.apideepwater.entities;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@Table(name = "marca",catalog = "labor")
public class Marca implements Serializable {

    @Id
    @Column(name = "idmarca")
    private Long id;

    @Column(name = "nomeMarca")
    private String nome;
}
