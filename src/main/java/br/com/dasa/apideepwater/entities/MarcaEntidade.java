package br.com.dasa.apideepwater.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@Table(name = "marca_entidade", catalog = "labor")
public class MarcaEntidade implements Serializable {

    @Id
    @Column(name = "idEntidade")
    private Long idEntidade;

    @Column(name = "idMarca")
    private Long idMarca;
}
