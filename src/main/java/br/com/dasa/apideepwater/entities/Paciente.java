package br.com.dasa.apideepwater.entities;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@Table(name = "paciente",catalog = "aol")
public class Paciente implements Serializable {

    @Id
    @Column(name = "idPaciente")
    private Long id;

    @Column(name = "idPacienteLabor")
    private Long idLabor;

    @Column(name = "idLocal")
    private String idLocal;

}
