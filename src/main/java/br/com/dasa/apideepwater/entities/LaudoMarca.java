package br.com.dasa.apideepwater.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name ="laudo_marca", catalog = "labor")
public class LaudoMarca implements Serializable {

    @Id
    @Column(name = "idMarca")
    private Long idMarca;

    @Column(name = "cabecalho")
    private String cabecalho;

    @Column(name = "versao_cabecalho")
    private Long versaoCabecalho;

    @Column(name = "rodape")
    private String rodape;

    @Column(name = "versao_rodape")
    private Long versaoRodape;

}
