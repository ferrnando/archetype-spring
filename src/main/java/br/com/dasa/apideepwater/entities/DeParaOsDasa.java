package br.com.dasa.apideepwater.entities;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@Table(name = "de_para_os_dasa",catalog = "aol")
public class DeParaOsDasa implements Serializable {

    @Id
    @Column(name = "idDeParaOsDasa")
    private Long id;

    @Column(name = "idOsDasa")
    private String idOs;

    @Column(name = "idOrdemServico")
    private Long idOrdemServico;

    @Column(name = "idAmostraDasa")
    private String idAmostra;
}
