package br.com.dasa.apideepwater.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "parametro_config", catalog = "labor")
public class ParametroConfig implements Serializable {

    @Id
    @Column(name = "CodChamada")
    private String codChamada;

    @Column(name = "parametro1")
    private String parametroUm;

    @Column(name = "Parametro2")
    private String parametroDois;

    @Column(name = "parametro3")
    private String parametroTres;

    @Lob
    @Column(name = "Imagem")
    private byte[] imagem;
}