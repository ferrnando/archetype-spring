package br.com.dasa.apideepwater.entities;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@Table(name = "entidades", catalog = "labor")
public class Entidade implements Serializable {

    @Id
    @Column(name = "entsid")
    private Long id;

    @Column(name = "entNome")
    private String nome;

    @Column(name = "entFantasia")
    private String fantasia;

    @Column(name = "entCidade")
    private String cidade;

}
