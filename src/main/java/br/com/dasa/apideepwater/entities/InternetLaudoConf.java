package br.com.dasa.apideepwater.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "internet_laudo_conf", catalog = "internet")
public class InternetLaudoConf {
    @Id
    @Column(name = "idinternet_conf")
    private Long id;

    @Column(name = "id_entidade")
    private Long idEntidade;

    @Column(name = "instituicaoImp")
    private String instituicaoImp;
}
