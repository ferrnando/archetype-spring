CREATE DATABASE IF NOT EXISTS internet;
USE internet;
SET FOREIGN_KEY_CHECKS=0;


DROP TABLE IF EXISTS `internet_laudo_conf`;
CREATE TABLE `internet_laudo_conf` (
  `idinternet_conf` int(11) NOT NULL,
  `id_entidade` int(11) NOT NULL,
  `instituicaoImp` varchar(40) DEFAULT NULL,
    `tipoLaudoImpressao`varchar(40) DEFAULT NULL,
    `qtd_laudo_pp` varchar(40) DEFAULT NULL,
    `logomarca`varchar(40) DEFAULT NULL,
    `textoassinatura` varchar(40) DEFAULT NULL,
    `assinatura`varchar(40) DEFAULT NULL,
    `tipo_laudo`varchar(40) DEFAULT NULL,
    `valor_referencia` varchar(40) DEFAULT NULL,
    `pesquisa` varchar(40) DEFAULT NULL,
    `busca_mostrar` varchar(40) DEFAULT NULL,
    `dataUltimaAtualizacao` varchar(40) DEFAULT NULL,
    `tipo_papel` varchar(40) DEFAULT NULL,
    `infAssinatura` varchar(40) DEFAULT NULL,
    `distanciaCabLau`varchar(40) DEFAULT NULL,
    `idadedata` varchar(40) DEFAULT NULL,
    `remSolicitacao` varchar(40) DEFAULT NULL,
    `remInstituicao`varchar(40) DEFAULT NULL,
    `sexoLaudo` varchar(40) DEFAULT NULL,
    `impResAnt` varchar(40) DEFAULT NULL,
     `instituicaoAssinatura` varchar(40) DEFAULT NULL,
     `impMatricula` varchar(40) DEFAULT NULL,
     `separador` varchar(40) DEFAULT NULL,
     `liberadoPor` varchar(40) DEFAULT NULL,
     `oslis_listagem` varchar(40) DEFAULT NULL,
     `tempo_refresh` varchar(40) DEFAULT NULL,
     `qtd_laudo_pp_maximo` varchar(40) DEFAULT NULL,
     `distanciaRodLau` varchar(40) DEFAULT NULL,
     `liberadoEm` varchar(40) DEFAULT NULL,
     `idadeExtenso` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`idinternet_conf`,`id_entidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE DATABASE IF NOT EXISTS aol;
USE aol;
SET FOREIGN_KEY_CHECKS=0;
--
-- Table structure for table `acessows`
--
DROP TABLE IF EXISTS `acessows`;
CREATE TABLE `acessows` (
  `idagente` int(11) NOT NULL,
  `identidade` int(11) NOT NULL,
  `ppla` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`idagente`,`identidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `agente`
--
DROP TABLE IF EXISTS `agente`;
CREATE TABLE `agente` (
  `idAgente` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Descricao` varchar(40) DEFAULT NULL,
  `Login` varchar(20) DEFAULT NULL,
  `Senha` varchar(20) DEFAULT NULL,
  `DtUltConexao` datetime DEFAULT NULL,
  `eMail` varchar(40) DEFAULT NULL,
  `Cidade` varchar(40) DEFAULT NULL,
  `Telefone` varchar(20) DEFAULT NULL,
  `VersaoDB` varchar(12) DEFAULT NULL,
  `VersaoAOL` varchar(19) DEFAULT NULL,
  `PodeSolicitarNumeracao` char(1) DEFAULT 'N',
  `IPServidor` varchar(15) DEFAULT NULL,
  `PermiteAlterarPaciente` enum('Sim','Não') DEFAULT 'Não',
  `Administrador` char(1) DEFAULT 'N',
  `versaoScript` int(10) unsigned DEFAULT NULL,
  `versaoComm` int(10) unsigned DEFAULT NULL,
  `logaComunicacao` char(1) DEFAULT 'S',
  `controla_lote` char(1) DEFAULT 'N',
  `atualiza` char(1) DEFAULT 'N',
  PRIMARY KEY (`idAgente`),
  KEY `DtUltConexao` (`DtUltConexao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `amostralote`
--
DROP TABLE IF EXISTS `amostralote`;
CREATE TABLE `amostralote` (
  `IdAmostraLote` int(11) NOT NULL AUTO_INCREMENT,
  `idlote` int(11) NOT NULL,
  `idOrdemServico` int(11) NOT NULL,
  `IdAmostra` int(11) NOT NULL,
  `idItemExame` int(11) DEFAULT NULL,
  PRIMARY KEY (`IdAmostraLote`),
  KEY `idLote` (`idlote`),
  KEY `idOrdemServico` (`idOrdemServico`),
  KEY `idAmostra` (`IdAmostra`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `amostraparticipantes`
--
DROP TABLE IF EXISTS `amostraparticipantes`;
CREATE TABLE `amostraparticipantes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idItemExame` int(11) NOT NULL,
  `nmNomeParticipante` varchar(255) NOT NULL,
  `idTipoParticipante` int(11) NOT NULL,
  `dtDataNascimento` date NOT NULL,
  `tpSexo` char(1) NOT NULL DEFAULT 'M',
  `mmObservacao` text,
  `nmIdentificador` varchar(255) NOT NULL,
  `nmColetor` varchar(255) NOT NULL,
  `nmJuiz` varchar(255) DEFAULT NULL,
  `qtEtiquetas` smallint(6) NOT NULL,
  `nrAmostras` smallint(6) NOT NULL,
  `boObrigatorio` char(1) NOT NULL DEFAULT 'N',
  `idTempo` int(11) NOT NULL,
  `idAmostra` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_amostra_participantes_iditemexame` (`idItemExame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `amostratransito`
--
DROP TABLE IF EXISTS `amostratransito`;
CREATE TABLE `amostratransito` (
  `idAmostraTransito` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `idOrdemServico` int(11) unsigned NOT NULL,
  `nroamostra` tinyint(3) NOT NULL,
  PRIMARY KEY (`idAmostraTransito`),
  KEY `amostratransitoinx` (`idOrdemServico`,`nroamostra`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `atualiza_aol`
--
DROP TABLE IF EXISTS `atualiza_aol`;
CREATE TABLE `atualiza_aol` (
  `Modulo` varchar(40) NOT NULL DEFAULT '0',
  `Descricao` varchar(80) NOT NULL DEFAULT '',
  `versao` datetime DEFAULT NULL,
  `caminho` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`Modulo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `atualiza_db`
--
DROP TABLE IF EXISTS `atualiza_db`;
CREATE TABLE `atualiza_db` (
  `VersaoDB` varchar(12) NOT NULL DEFAULT '',
  `Script` text NOT NULL,
  PRIMARY KEY (`VersaoDB`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `conexao`
--
DROP TABLE IF EXISTS `conexao`;
CREATE TABLE `conexao` (
  `idConexao` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idAgente` int(10) unsigned NOT NULL,
  `dataHora` datetime NOT NULL COMMENT 'Data e hora do login',
  `dataHoraFim` datetime DEFAULT NULL COMMENT 'Data e hora do logout',
  `AOL_Pacientes` mediumtext COMMENT 'Pacientes transferidos do AOL',
  `AOL_Solicitacoes` mediumtext COMMENT 'Solicitações transferidas do AOL',
  `resultados_AOL` mediumtext COMMENT 'OSs de resultados enviadas para o AOL',
  `status` text COMMENT 'Erro ocorrido na comunicação',
  PRIMARY KEY (`idConexao`),
  KEY `idAgente` (`idAgente`,`dataHora`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `conexaofalha`
--
DROP TABLE IF EXISTS `conexaofalha`;
CREATE TABLE `conexaofalha` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IP` char(15) DEFAULT NULL,
  `IdUsado` char(6) DEFAULT NULL,
  `SenhaUsada` char(10) DEFAULT NULL,
  `DataHora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `confirmacaopendente`
--
DROP TABLE IF EXISTS `confirmacaopendente`;
CREATE TABLE `confirmacaopendente` (
  `idOrdemServico` int(10) unsigned NOT NULL DEFAULT '0',
  `idAgente` int(10) unsigned DEFAULT NULL,
  `datahora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idOrdemServico`),
  KEY `idAgente` (`idAgente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `controle_lote`
--
DROP TABLE IF EXISTS `controle_lote`;
CREATE TABLE `controle_lote` (
  `idControleLote` int(11) NOT NULL AUTO_INCREMENT,
  `identificadorLote` varchar(50) NOT NULL,
  `dataInclusao` datetime NOT NULL,
  `dataEnvio` datetime DEFAULT NULL,
  `statusEnvio` varchar(20) NOT NULL,
  PRIMARY KEY (`idControleLote`),
  KEY `idx_identificadorLote` (`identificadorLote`),
  KEY `idx_status` (`statusEnvio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `controle_pendencia`
--
DROP TABLE IF EXISTS `controle_pendencia`;
CREATE TABLE `controle_pendencia` (
  `idControlePendencia` int(11) NOT NULL AUTO_INCREMENT,
  `idOrdemServico` int(11) DEFAULT NULL,
  `idAmostraDasa` varchar(20) NOT NULL,
  `dataCriacao` datetime DEFAULT NULL,
  `dataEntradaNTO` datetime DEFAULT NULL,
  `dataConfirmacao` datetime DEFAULT NULL,
  PRIMARY KEY (`idControlePendencia`),
  UNIQUE KEY `idAmostraDasa_UNIQUE` (`idAmostraDasa`),
  KEY `index2` (`idOrdemServico`),
  KEY `index3` (`dataCriacao`),
  KEY `index4` (`idAmostraDasa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `controle_semente_amostra`
--
DROP TABLE IF EXISTS `controle_semente_amostra`;
CREATE TABLE `controle_semente_amostra` (
  `id_controle_semente_amostra` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
  `numero_semente` varchar(10) NOT NULL COMMENT 'Número da semente disponibilizado pelo Sistema Motion',
  `qtd_amostras_disponiveis` int(11) NOT NULL COMMENT 'Quantidade total de amostras disponíveis para esse número de semente. \nEsse valor serve para controlar se deve ser usado uma nova semente para a geração das amostras.',
  `ordem` int(11) NOT NULL COMMENT 'Ordem em que será priorizado o uso das sementes. \nApós uma semente ser esgotada, será usada uma nova com base na ordem definida.',
  PRIMARY KEY (`id_controle_semente_amostra`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `controle_semente_requisicao`
--
DROP TABLE IF EXISTS `controle_semente_requisicao`;
CREATE TABLE `controle_semente_requisicao` (
  `id_controle_semente_requisicao` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador',
  `numero_semente` varchar(10) NOT NULL COMMENT 'Número da semente disponibilizado pelo Sistema Motion',
  `qtd_requisicoes_disponiveis` int(11) NOT NULL COMMENT 'Quantidade total de requisições disponíveis para esse número de semente. \nEsse valor serve para controlar se deve ser usado uma nova semente para a geração das requisições.',
  `ordem` int(11) NOT NULL COMMENT 'Ordem em que será priorizado o uso das sementes. \nApós uma semente ser esgotada, será usada uma nova com base na ordem definida.',
  PRIMARY KEY (`id_controle_semente_requisicao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `ctrl_mensagem`
--
DROP TABLE IF EXISTS `ctrl_mensagem`;
CREATE TABLE `ctrl_mensagem` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idMensagem` int(10) unsigned NOT NULL DEFAULT '0',
  `idAgente` int(10) unsigned NOT NULL DEFAULT '0',
  `Direcao` char(1) DEFAULT NULL,
  `MensagemTransferida` char(1) DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `idAgente` (`idAgente`,`Direcao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `dbversao`
--
DROP TABLE IF EXISTS `dbversao`;
CREATE TABLE `dbversao` (
  `dbvSid` int(11) NOT NULL,
  `dbvSequencia` int(11) NOT NULL,
  `dbvSQL` blob NOT NULL,
  `dbvObs` varchar(120) NOT NULL,
  `dbvResponsavel` varchar(32) NOT NULL,
  `dbvData` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`dbvSid`,`dbvSequencia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `de_para_antibiotico_dasa`
--
DROP TABLE IF EXISTS `de_para_antibiotico_dasa`;
CREATE TABLE `de_para_antibiotico_dasa` (
  `antisid` int(11) NOT NULL,
  `idExameDasa` int(11) NOT NULL,
  `idCompostoMotion` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`antisid`,`idExameDasa`,`idCompostoMotion`),
  KEY `index2` (`antisid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `de_para_dasa`
--
DROP TABLE IF EXISTS `de_para_dasa`;
CREATE TABLE `de_para_dasa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idExameDasa` int(10) unsigned DEFAULT NULL,
  `codExameDasa` varchar(15) DEFAULT NULL,
  `nomeExameDasa` varchar(100) DEFAULT NULL,
  `idExame` varchar(500) DEFAULT NULL,
  `idCritica` int(11) NOT NULL DEFAULT '0',
  `tempo` varchar(20) NOT NULL DEFAULT 'Basal',
  `codCompostoDasa` varchar(10) NOT NULL,
  `gerarPfora` char(1) DEFAULT 'N' COMMENT 'exames ti e psa nao podem gerar no pfora, pois isso geraria obs no laudo',
  `antibiotico` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `exame` (`codExameDasa`),
  KEY `idExameAlvaro` (`idExame`),
  KEY `idExa` (`idExameDasa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `de_para_dasa_alicotagem`
--
DROP TABLE IF EXISTS `de_para_dasa_alicotagem`;
CREATE TABLE `de_para_dasa_alicotagem` (
  `idAlicotagem` int(11) NOT NULL AUTO_INCREMENT,
  `grupoDasa` int(2) DEFAULT NULL,
  `equipamento` varchar(30) DEFAULT NULL,
  `codExame` varchar(15) NOT NULL,
  `nomeExame` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idAlicotagem`,`codExame`),
  UNIQUE KEY `codExame_UNIQUE` (`codExame`),
  UNIQUE KEY `idAlicotagem_UNIQUE` (`idAlicotagem`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `de_para_dasa_ligacao`
--
DROP TABLE IF EXISTS `de_para_dasa_ligacao`;
CREATE TABLE `de_para_dasa_ligacao` (
  `idExameMotion` int(11) NOT NULL,
  `idExameLabor` varchar(10) NOT NULL,
  `idCompostoMotion` int(11) NOT NULL DEFAULT '0',
  `idCritica` int(11) DEFAULT '0',
  `tempo` varchar(60) NOT NULL DEFAULT 'Basal',
  `idMaterial` int(10) unsigned NOT NULL DEFAULT '0',
  `idRecipiente` smallint(6) DEFAULT '0',
  `ImportaObservacao` varchar(1) DEFAULT 'S',
  `ConsideraComposto` char(1) DEFAULT 'N',
  `idConfiguracaoCurva` int(11) DEFAULT NULL,
  PRIMARY KEY (`idExameLabor`,`idExameMotion`,`idCompostoMotion`) USING BTREE,
  KEY `composto` (`idCompostoMotion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `de_para_descritivo_dasa`
--
DROP TABLE IF EXISTS `de_para_descritivo_dasa`;
CREATE TABLE `de_para_descritivo_dasa` (
  `idExameLabor` varchar(30) NOT NULL DEFAULT '',
  `idCompostoMotion` int(11) NOT NULL DEFAULT '0',
  `idSimplesMotion` int(11) NOT NULL DEFAULT '0',
  `Ordem` int(11) DEFAULT NULL,
  `ImportaObservacao` varchar(1) DEFAULT 'S',
  PRIMARY KEY (`idExameLabor`,`idCompostoMotion`,`idSimplesMotion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `de_para_material_dasa`
--
DROP TABLE IF EXISTS `de_para_material_dasa`;
CREATE TABLE `de_para_material_dasa` (
  `idMaterialDasa` int(11) NOT NULL,
  `descricaoDasa` varchar(50) DEFAULT NULL,
  `idAlvaro` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`idMaterialDasa`),
  KEY `idAlvaro` (`idAlvaro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `de_para_os_dasa`
--
DROP TABLE IF EXISTS `de_para_os_dasa`;
CREATE TABLE `de_para_os_dasa` (
  `idDeParaOsDasa` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idOrdemServico` int(11) unsigned DEFAULT NULL,
  `idOsDasa` varchar(20) DEFAULT NULL COMMENT 'numero da o.s. no dasa',
  `idRequisicaoDasa` varchar(30) DEFAULT NULL,
  `idAmostraDasa` char(20) DEFAULT NULL,
  `idExameDasa` int(11) unsigned DEFAULT NULL,
  `idExameSolicitado` int(10) unsigned DEFAULT NULL,
  `dtImportacao` datetime DEFAULT NULL,
  `dtExportacao` datetime DEFAULT NULL,
  `idPfora` int(11) NOT NULL DEFAULT '0' COMMENT 'indica que foi de cascavel para o dasa qdo maior que 0',
  `destinoTubo` char(45) NOT NULL DEFAULT 'SP' COMMENT 'Como e gerado o reg. e etiquetado com cod. motion e nao se tem certeza de que ira para sp, serve para indicar em caso de inclusao se envia mail ou nao',
  `container` char(20) DEFAULT '',
  `idItemExame` int(10) unsigned DEFAULT NULL,
  `nroamostra` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`idDeParaOsDasa`),
  KEY `idOrdemServico` (`idOrdemServico`),
  KEY `idExameSolicitado` (`idExameSolicitado`),
  KEY `idOsDasa` (`idOsDasa`),
  KEY `idAmostraDasa` (`idAmostraDasa`),
  KEY `iditemexame` (`idItemExame`),
  KEY `OrdemAmostraItem` (`idOrdemServico`,`nroamostra`,`idItemExame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `de_para_perfil_dasa`
--
DROP TABLE IF EXISTS `de_para_perfil_dasa`;
CREATE TABLE `de_para_perfil_dasa` (
  `idExameComposto` int(11) NOT NULL,
  `idExamePerfil` int(11) NOT NULL,
  `codigoExameLabor` varchar(20) NOT NULL,
  PRIMARY KEY (`idExameComposto`,`idExamePerfil`,`codigoExameLabor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `de_para_recipiente_dasa`
--
DROP TABLE IF EXISTS `de_para_recipiente_dasa`;
CREATE TABLE `de_para_recipiente_dasa` (
  `idRecipienteDasa` int(11) NOT NULL,
  `descricaoDasa` varchar(50) DEFAULT NULL,
  `idAlvaro` int(11) DEFAULT NULL,
  PRIMARY KEY (`idRecipienteDasa`),
  KEY `idAlvaro` (`idAlvaro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `de_para_relacionamento_material`
--
DROP TABLE IF EXISTS `de_para_relacionamento_material`;
CREATE TABLE `de_para_relacionamento_material` (
  `idMaterialAlvaro` int(11) NOT NULL,
  `idMaterialDasa` int(11) NOT NULL,
  PRIMARY KEY (`idMaterialAlvaro`,`idMaterialDasa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tab que liga aol.de_para_material_dasa\ncom labor.materiais\n';
--
-- Table structure for table `de_para_relacionamento_recipiente`
--
DROP TABLE IF EXISTS `de_para_relacionamento_recipiente`;
CREATE TABLE `de_para_relacionamento_recipiente` (
  `idRecipienteDasa` int(11) NOT NULL,
  `idFrascoAlvaro` int(11) NOT NULL,
  PRIMARY KEY (`idRecipienteDasa`,`idFrascoAlvaro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='ligação entre:\naol.de_para_recipiente_dasa\nlabor.frascos';
--
-- Table structure for table `de_para_tempo_dasa`
--
DROP TABLE IF EXISTS `de_para_tempo_dasa`;
CREATE TABLE `de_para_tempo_dasa` (
  `idde_para_tempo_dasa` int(11) NOT NULL AUTO_INCREMENT,
  `idConf_Curva` int(11) DEFAULT NULL,
  `codigoExaLabor` varchar(15) NOT NULL,
  `mnemonico_dasa` varchar(15) DEFAULT NULL,
  `CodExaMotion` int(11) NOT NULL,
  PRIMARY KEY (`idde_para_tempo_dasa`),
  KEY `index2` (`codigoExaLabor`),
  KEY `index3` (`CodExaMotion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `delete_itemexame`
--
DROP TABLE IF EXISTS `delete_itemexame`;
CREATE TABLE `delete_itemexame` (
  `idItemExame` int(10) unsigned NOT NULL,
  `idOrdemServico` int(10) unsigned NOT NULL DEFAULT '0',
  `CodExame` varchar(10) DEFAULT NULL,
  `idMaterial` int(10) unsigned NOT NULL DEFAULT '0',
  `DadosRecepcao` text,
  `MaterialColetado` char(1) DEFAULT 'S',
  `idLis` tinytext,
  `DataExclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `idSolicitacao` (`idOrdemServico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `delete_solicitacao`
--
DROP TABLE IF EXISTS `delete_solicitacao`;
CREATE TABLE `delete_solicitacao` (
  `idSolicitacao` int(10) unsigned NOT NULL,
  `idAgente` int(10) unsigned NOT NULL DEFAULT '0',
  `idPaciente` int(10) unsigned NOT NULL DEFAULT '0',
  `idEntidade` int(10) unsigned DEFAULT '0',
  `idMedico` int(10) unsigned DEFAULT '0',
  `DataSolicitacao` datetime DEFAULT NULL,
  `DataTransferencia` datetime DEFAULT NULL,
  `DataGeradoLabor` datetime DEFAULT NULL,
  `Medicamentos` text,
  `Observacoes` text,
  `idOrdemServico` int(10) unsigned DEFAULT '0',
  `idSolExterna` int(11) DEFAULT NULL,
  `DataResultadoBaixado` datetime DEFAULT NULL,
  `idOSLis` varchar(30) DEFAULT '',
  `dataColeta` datetime DEFAULT NULL,
  `dataLidoInova` datetime DEFAULT NULL,
  `solicitacaoOrigem` enum('Ws','Aol','Integracao') DEFAULT NULL,
  `DataExclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `DataGeradoLabor` (`DataGeradoLabor`),
  KEY `idOrdemServico` (`idOrdemServico`),
  KEY `idSolExterna` (`idAgente`,`idSolExterna`),
  KEY `idEntidade` (`DataTransferencia`,`idEntidade`),
  KEY `Index_6` (`idEntidade`,`DataTransferencia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `delete_tempo`
--
DROP TABLE IF EXISTS `delete_tempo`;
CREATE TABLE `delete_tempo` (
  `DescrAmostra` varchar(25) DEFAULT NULL,
  `idItemExame` int(10) unsigned NOT NULL DEFAULT '0',
  `idAmostra` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `idOrdemServico` int(10) unsigned NOT NULL DEFAULT '0',
  `Material` int(10) unsigned DEFAULT NULL,
  `Identificador` varchar(25) DEFAULT NULL,
  `alicotado` char(1) DEFAULT 'N',
  `restricaoAmostra` char(1) DEFAULT 'N',
  `DataExclusao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `iditemexame` (`idItemExame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `deparapaternidade`
--
DROP TABLE IF EXISTS `deparapaternidade`;
CREATE TABLE `deparapaternidade` (
  `IdExame` int(11) NOT NULL COMMENT 'Id do Exame no Labor',
  `IdCompostoMotion` int(11) NOT NULL COMMENT 'Id do Composto Motion',
  PRIMARY KEY (`IdExame`,`IdCompostoMotion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela de Configurao de De Para de Paternidade.';
--
-- Table structure for table `deparapaternidadeparticipanteexame`
--
DROP TABLE IF EXISTS `deparapaternidadeparticipanteexame`;
CREATE TABLE `deparapaternidadeparticipanteexame` (
  `IdExame` int(11) NOT NULL COMMENT 'Id do Exame no Labor',
  `IdCompostoMotion` int(11) NOT NULL COMMENT 'Id do Composto Motion',
  `IdTipoParticipante` int(11) NOT NULL DEFAULT '0' COMMENT 'Id do Tipo de Participante\nEx: 1 - Suposto Pai, 2 - Suposto Filho.',
  `IdTesteMotion` int(11) NOT NULL COMMENT 'Id do Teste Motion',
  `IdMaterialDasa` int(11) NOT NULL COMMENT 'Id do Material',
  `IdRecipienteDasa` int(11) NOT NULL COMMENT 'Id do Recipiente',
  PRIMARY KEY (`IdExame`,`IdCompostoMotion`,`IdTipoParticipante`,`IdTesteMotion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Configurao dos Testes Simples de cada participante inclu';
--
-- Table structure for table `destino`
--
DROP TABLE IF EXISTS `destino`;
CREATE TABLE `destino` (
  `desSid` int(11) NOT NULL AUTO_INCREMENT,
  `DesDescricao` varchar(22) DEFAULT NULL,
  `sementeRequisicao` varchar(4) DEFAULT NULL,
  `ultimaRequisicao` varchar(10) DEFAULT NULL,
  `sementeAmostra` varchar(10) DEFAULT NULL,
  `ultimaAmostra` varchar(10) DEFAULT NULL,
  `amostraFinal` varchar(10) DEFAULT NULL,
  `padraoCodBarras` varchar(12) DEFAULT NULL,
  `desAbv` char(1) DEFAULT 'A',
  `caixa` varchar(200) DEFAULT NULL,
  `desLocalDescricao` varchar(45) DEFAULT 'DASA',
  `desAtivo` char(1) DEFAULT 'N',
  `idempguia` int(11) DEFAULT NULL,
  `sigladestino` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`desSid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `destino_ftp`
--
DROP TABLE IF EXISTS `destino_ftp`;
CREATE TABLE `destino_ftp` (
  `idftp` int(11) NOT NULL AUTO_INCREMENT,
  `desSid` int(11) NOT NULL,
  `endereco` varchar(45) DEFAULT NULL,
  `porta` varchar(3) DEFAULT '22',
  `usuario` varchar(25) DEFAULT NULL,
  `senha` varchar(25) DEFAULT NULL,
  `entidades` varchar(45) DEFAULT NULL,
  `apelido` varchar(45) DEFAULT NULL,
  `idDestinoSituacao` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idftp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `destino_situacao`
--
DROP TABLE IF EXISTS `destino_situacao`;
CREATE TABLE `destino_situacao` (
  `idDestinoSituacao` int(11) NOT NULL AUTO_INCREMENT,
  `situacao` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idDestinoSituacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `ejb__timer__tbl`
--
DROP TABLE IF EXISTS `ejb__timer__tbl`;
CREATE TABLE `ejb__timer__tbl` (
  `CREATIONTIMERAW` bigint(20) NOT NULL,
  `BLOB` blob,
  `TIMERID` varchar(255) NOT NULL,
  `CONTAINERID` bigint(20) NOT NULL,
  `OWNERID` varchar(255) DEFAULT NULL,
  `STATE` int(11) NOT NULL,
  `PKHASHCODE` int(11) NOT NULL,
  `INTERVALDURATION` bigint(20) NOT NULL,
  `INITIALEXPIRATIONRAW` bigint(20) NOT NULL,
  `LASTEXPIRATIONRAW` bigint(20) NOT NULL,
  `SCHEDULE` varchar(255) DEFAULT NULL,
  `APPLICATIONID` bigint(20) NOT NULL,
  PRIMARY KEY (`TIMERID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `entidade`
--
DROP TABLE IF EXISTS `entidade`;
CREATE TABLE `entidade` (
  `idAgente` int(10) unsigned NOT NULL DEFAULT '0',
  `idEntidade` int(10) unsigned NOT NULL DEFAULT '0',
  `Descricao` char(40) DEFAULT NULL,
  `Cidade` char(40) DEFAULT NULL,
  `DtUltRecResultado` datetime DEFAULT NULL,
  `DataInsercao` datetime DEFAULT NULL,
  PRIMARY KEY (`idEntidade`),
  KEY `idAgente` (`idAgente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `entidade_controle_resultado`
--
DROP TABLE IF EXISTS `entidade_controle_resultado`;
CREATE TABLE `entidade_controle_resultado` (
  `idEntidade` int(11) NOT NULL,
  `dataUltimoResultado` datetime NOT NULL,
  `qtdeResultadosRequisicao` int(11) NOT NULL,
  PRIMARY KEY (`idEntidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `entidade_etiqueta`
--
DROP TABLE IF EXISTS `entidade_etiqueta`;
CREATE TABLE `entidade_etiqueta` (
  `idEntidade` int(11) NOT NULL,
  `idEtiqueta` int(11) NOT NULL,
  PRIMARY KEY (`idEntidade`,`idEtiqueta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `entidade_tubo_unico`
--
DROP TABLE IF EXISTS `entidade_tubo_unico`;
CREATE TABLE `entidade_tubo_unico` (
  `tbuEntSid` int(11) NOT NULL,
  PRIMARY KEY (`tbuEntSid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `fk_data_migration_2013`
--
DROP TABLE IF EXISTS `fk_data_migration_2013`;
CREATE TABLE `fk_data_migration_2013` (
  `id_field` varchar(20) NOT NULL COMMENT 'value of id from source table.',
  `id_field_name` varchar(60) NOT NULL COMMENT 'field''s name',
  `created_date` date NOT NULL COMMENT 'Date when id was created in source database',
  PRIMARY KEY (`id_field_name`,`id_field`),
  KEY `index_date_name` (`created_date`,`id_field_name`) USING BTREE,
  KEY `index_date` (`created_date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='TABLE TO STORE ID INFO TO MIGRATION PROCESS';
--
-- Table structure for table `fk_data_migration_2014`
--
DROP TABLE IF EXISTS `fk_data_migration_2014`;
CREATE TABLE `fk_data_migration_2014` (
  `id_field` varchar(20) NOT NULL COMMENT 'value of id from source table.',
  `id_field_name` varchar(60) NOT NULL COMMENT 'field''s name',
  `created_date` date NOT NULL COMMENT 'Date when id was created in source database',
  PRIMARY KEY (`id_field_name`,`id_field`),
  KEY `index_date_name` (`created_date`,`id_field_name`) USING BTREE,
  KEY `index_date` (`created_date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='TABLE TO STORE ID INFO TO MIGRATION PROCESS';
--
-- Table structure for table `fk_data_migration_logtransferencia_2013`
--
DROP TABLE IF EXISTS `fk_data_migration_logtransferencia_2013`;
CREATE TABLE `fk_data_migration_logtransferencia_2013` (
  `id_field` varchar(20) NOT NULL COMMENT 'value of id from source table.',
  `id_field_name` varchar(60) NOT NULL COMMENT 'field''s name',
  `created_date` date NOT NULL COMMENT 'Date when id was created in source database',
  PRIMARY KEY (`id_field_name`,`id_field`),
  KEY `index_date_name` (`created_date`,`id_field_name`) USING BTREE,
  KEY `index_date` (`created_date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='TABLE TO STORE ID INFO TO MIGRATION PROCESS';
--
-- Table structure for table `fk_data_migration_logtransferencia_2014`
--
DROP TABLE IF EXISTS `fk_data_migration_logtransferencia_2014`;
CREATE TABLE `fk_data_migration_logtransferencia_2014` (
  `id_field` varchar(20) NOT NULL COMMENT 'value of id from source table.',
  `id_field_name` varchar(60) NOT NULL COMMENT 'field''s name',
  `created_date` date NOT NULL COMMENT 'Date when id was created in source database',
  PRIMARY KEY (`id_field_name`,`id_field`),
  KEY `index_date_name` (`created_date`,`id_field_name`) USING BTREE,
  KEY `index_date` (`created_date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='TABLE TO STORE ID INFO TO MIGRATION PROCESS';
--
-- Table structure for table `ftp_arquivo`
--
DROP TABLE IF EXISTS `ftp_arquivo`;
CREATE TABLE `ftp_arquivo` (
  `idarquivo` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` enum('CadExportado','CadImportado','ResExportado','ResImportado','EnvioReconv','RecReconv') DEFAULT NULL,
  `arquivo` mediumtext NOT NULL,
  `os_alvaro` int(11) NOT NULL,
  `Nome_Arq` varchar(60) NOT NULL,
  `desSid_origem` varchar(20) NOT NULL,
  `desSid_destino` int(11) NOT NULL,
  `Erro` varchar(45) NOT NULL,
  `Data` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idarquivo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `ftp_path`
--
DROP TABLE IF EXISTS `ftp_path`;
CREATE TABLE `ftp_path` (
  `idpath` int(11) NOT NULL AUTO_INCREMENT,
  `idftp` int(11) NOT NULL,
  `pathExpCad` varchar(100) DEFAULT NULL,
  `pathImpCad` varchar(100) DEFAULT NULL,
  `pathExpResult` varchar(100) DEFAULT NULL,
  `pathImportResult` varchar(100) DEFAULT NULL,
  `PathLogExp` varchar(100) DEFAULT NULL,
  `PathLogImport` varchar(100) DEFAULT NULL,
  `PathResLido` varchar(100) DEFAULT NULL,
  `PathErro` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idpath`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `historico_etiqueta`
--
DROP TABLE IF EXISTS `historico_etiqueta`;
CREATE TABLE `historico_etiqueta` (
  `idOrdemServico` int(10) NOT NULL,
  `idAmostra` char(1) NOT NULL,
  `DataImpressao` datetime DEFAULT NULL,
  PRIMARY KEY (`idOrdemServico`,`idAmostra`),
  KEY `index_dtImpressao` (`DataImpressao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `inventarioequipamentos`
--
DROP TABLE IF EXISTS `inventarioequipamentos`;
CREATE TABLE `inventarioequipamentos` (
  `identidade` int(11) DEFAULT NULL,
  `nuSerie` varchar(20) DEFAULT NULL,
  `nmEquipamento` varchar(50) DEFAULT NULL,
  `dhInclusao` datetime DEFAULT NULL,
  `flUltimaIdentificacao` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `itemexame`
--
DROP TABLE IF EXISTS `itemexame`;
CREATE TABLE `itemexame` (
  `idItemExame` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idOrdemServico` int(10) unsigned NOT NULL DEFAULT '0',
  `CodExame` varchar(10) DEFAULT NULL,
  `idMaterial` int(10) unsigned NOT NULL DEFAULT '0',
  `DadosRecepcao` text,
  `MaterialColetado` char(1) DEFAULT 'S',
  `idLis` tinytext,
  `urgente` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idItemExame`),
  KEY `idSolicitacao` (`idOrdemServico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `itemexamealteracao`
--
DROP TABLE IF EXISTS `itemexamealteracao`;
CREATE TABLE `itemexamealteracao` (
  `idItemExameAlteracao` int(11) NOT NULL AUTO_INCREMENT,
  `idItemExame` int(10) DEFAULT NULL,
  `idOrdemServico` varchar(45) NOT NULL,
  `codExame` varchar(45) NOT NULL,
  `idAmostra` int(10) DEFAULT NULL,
  `idMaterial` int(10) DEFAULT NULL,
  `operacao` varchar(10) NOT NULL,
  `statusEnvio` varchar(45) DEFAULT NULL,
  `dataEnvio` datetime DEFAULT NULL,
  `dataRetorno` datetime DEFAULT NULL,
  `descricaoErro` varchar(200) DEFAULT NULL,
  `qtdeTentativas` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idItemExameAlteracao`),
  UNIQUE KEY `inx_iditemexamealteracao` (`idItemExameAlteracao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `itemexamecomposto`
--
DROP TABLE IF EXISTS `itemexamecomposto`;
CREATE TABLE `itemexamecomposto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idItemExame` int(11) NOT NULL,
  `idExameFilho` varchar(25) NOT NULL,
  `idMaterial` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `itemexameestimulo`
--
DROP TABLE IF EXISTS `itemexameestimulo`;
CREATE TABLE `itemexameestimulo` (
  `idSolicitacao` int(11) NOT NULL DEFAULT '0',
  `idItemExame` int(11) NOT NULL DEFAULT '0',
  `idEstimulos` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idSolicitacao`,`idItemExame`,`idEstimulos`),
  KEY `index2` (`idSolicitacao`),
  KEY `index3` (`idItemExame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `itemexameparticipantes`
--
DROP TABLE IF EXISTS `itemexameparticipantes`;
CREATE TABLE `itemexameparticipantes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idExameSolicitado` int(11) NOT NULL,
  `identificador` varchar(200) NOT NULL,
  `coletor` varchar(150) NOT NULL,
  `nomeJuiz` varchar(150) NOT NULL,
  `qtdeEtiqueta` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `itemlote`
--
DROP TABLE IF EXISTS `itemlote`;
CREATE TABLE `itemlote` (
  `IdItemLote` int(11) NOT NULL AUTO_INCREMENT,
  `idlote` int(11) NOT NULL,
  `idOrdemServico` int(10) NOT NULL,
  `IdAmostra` int(3) NOT NULL,
  `IdItemExame` int(11) DEFAULT NULL,
  PRIMARY KEY (`IdItemLote`),
  UNIQUE KEY `idx_itemlote_IdItemExame` (`IdItemExame`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `libera_exame_entidade`
--
DROP TABLE IF EXISTS `libera_exame_entidade`;
CREATE TABLE `libera_exame_entidade` (
  `libsid` int(11) NOT NULL DEFAULT '0',
  `libEntSid` int(11) NOT NULL DEFAULT '0',
  `libExaCodigo` varchar(10) NOT NULL DEFAULT '',
  `libDescricao` varchar(20) DEFAULT NULL,
  `libDtInclusao` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`libEntSid`,`libExaCodigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `lixo`
--
DROP TABLE IF EXISTS `lixo`;
CREATE TABLE `lixo` (
  `identidade` int(11) DEFAULT NULL,
  `idagente` int(11) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `requisicao` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `log`
--
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idConexao` bigint(20) unsigned DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `texto` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `log_de_para_dasa`
--
DROP TABLE IF EXISTS `log_de_para_dasa`;
CREATE TABLE `log_de_para_dasa` (
  `idLogDeParaDasa` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ConteudoAnterior` text,
  `Quem` varchar(45) DEFAULT NULL,
  `Data` datetime DEFAULT NULL,
  `Operacao` enum('Inserção','Edição','Exclusão') DEFAULT NULL,
  `idDeParaDasa` int(11) DEFAULT NULL,
  `tabela` enum('DeParaDasa','DeParaDasaLigação') DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idLogDeParaDasa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela para registrar o log das pendencias';
--
-- Table structure for table `log_libera_exame_entidade`
--
DROP TABLE IF EXISTS `log_libera_exame_entidade`;
CREATE TABLE `log_libera_exame_entidade` (
  `idLoglibera_exame_entidade` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ConteudoAnterior` text,
  `Quem` varchar(45) DEFAULT NULL,
  `Data` datetime DEFAULT NULL,
  `Operacao` enum('InserÃ§Ã£o','EdiÃ§Ã£o','ExclusÃ£o') DEFAULT NULL,
  `idlibera_exame_entidade` int(11) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `entsid` int(11) DEFAULT NULL,
  PRIMARY KEY (`idLoglibera_exame_entidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela para registrar o log das Alteracoes';
--
-- Table structure for table `log_origem_destino`
--
DROP TABLE IF EXISTS `log_origem_destino`;
CREATE TABLE `log_origem_destino` (
  `idLogOrigem_destino` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ConteudoAnterior` text,
  `Quem` varchar(45) DEFAULT NULL,
  `Data` datetime DEFAULT NULL,
  `Operacao` enum('Inserção','Edição','Exclusão') DEFAULT NULL,
  `idOrigem_destino` int(11) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idLogOrigem_destino`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela para registrar o log das Alteracoes';
--
-- Table structure for table `logpaciente`
--
DROP TABLE IF EXISTS `logpaciente`;
CREATE TABLE `logpaciente` (
  `idPacienteLabor` int(11) NOT NULL DEFAULT '0',
  `DataHora` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `NomeAnterior` varchar(60) DEFAULT NULL,
  `SexoAnterior` char(1) DEFAULT NULL,
  `DtNascAnterior` date DEFAULT NULL,
  `EnderecoAnterior` varchar(40) DEFAULT NULL,
  `CidadeAnterior` varchar(50) DEFAULT NULL,
  `UFAnterior` char(2) DEFAULT NULL,
  `CEPAnterior` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idPacienteLabor`,`DataHora`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `logtransferencia`
--
DROP TABLE IF EXISTS `logtransferencia`;
CREATE TABLE `logtransferencia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idAgente` int(10) unsigned NOT NULL,
  `sessao` varchar(32) DEFAULT '',
  `direcao` tinyint(4) NOT NULL,
  `DataHora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IP` varchar(15) DEFAULT NULL,
  `Detalhe` mediumtext,
  PRIMARY KEY (`id`),
  KEY `idAgente` (`idAgente`,`sessao`),
  KEY `logtransferencia_DataHora` (`DataHora`),
  KEY `logtransferencia_Agente_data` (`idAgente`,`DataHora`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT KEY_BLOCK_SIZE=4;
--
-- Table structure for table `logtransferencia1`
--
DROP TABLE IF EXISTS `logtransferencia1`;
CREATE TABLE `logtransferencia1` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idAgente` int(10) unsigned NOT NULL,
  `sessao` varchar(32) DEFAULT '',
  `direcao` tinyint(4) NOT NULL,
  `DataHora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IP` varchar(15) DEFAULT NULL,
  `Detalhe` mediumtext,
  PRIMARY KEY (`id`),
  KEY `idAgente` (`idAgente`,`sessao`),
  KEY `logtransferencia_DataHora` (`DataHora`),
  KEY `logtransferencia_Agente_data` (`idAgente`,`DataHora`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT KEY_BLOCK_SIZE=4;
--
-- Table structure for table `logtransferenciaarquivo`
--
DROP TABLE IF EXISTS `logtransferenciaarquivo`;
CREATE TABLE `logtransferenciaarquivo` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idAgente` int(10) unsigned NOT NULL,
  `dataTransferencia` datetime DEFAULT NULL,
  `idSessao` char(32) DEFAULT NULL,
  `dataFimTransferencia` datetime DEFAULT NULL,
  `fimNormal` char(1) DEFAULT '',
  `conteudo` mediumblob,
  PRIMARY KEY (`id`),
  KEY `idAgente` (`idAgente`,`dataTransferencia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `lote`
--
DROP TABLE IF EXISTS `lote`;
CREATE TABLE `lote` (
  `idlote` int(11) NOT NULL AUTO_INCREMENT,
  `idEntidade` int(10) DEFAULT NULL,
  `Qtd_amostras` int(11) DEFAULT NULL,
  `Destino` int(11) DEFAULT NULL,
  `acoid` int(11) DEFAULT NULL,
  `Data` datetime DEFAULT NULL,
  `idItemExame` int(11) DEFAULT NULL,
  PRIMARY KEY (`idlote`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `lotes`
--
DROP TABLE IF EXISTS `lotes`;
CREATE TABLE `lotes` (
  `idlote` int(11) NOT NULL AUTO_INCREMENT,
  `identificador` varchar(50) NOT NULL,
  `idTipoLote` varchar(10) NOT NULL,
  `dataCriacao` datetime NOT NULL,
  `qtdeAmostrasArmazenadas` int(11) NOT NULL,
  `qtdeMaximaPermitida` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `mnemonico` varchar(50) NOT NULL,
  `dataTransmissao` datetime DEFAULT NULL,
  `lotePaternidade` char(1) NOT NULL DEFAULT 'N',
  `idEntidade` int(11) NOT NULL,
  `loteExclusivo` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`idlote`),
  UNIQUE KEY `identificador` (`identificador`),
  KEY `dataCriacao` (`dataCriacao`),
  KEY `status` (`status`),
  KEY `dataTransmissao` (`dataTransmissao`),
  KEY `lotePaternidade` (`lotePaternidade`),
  KEY `idEntidade` (`idEntidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `lotesultimomnemonico`
--
DROP TABLE IF EXISTS `lotesultimomnemonico`;
CREATE TABLE `lotesultimomnemonico` (
  `idUltimoMnemonico` int(11) NOT NULL AUTO_INCREMENT,
  `dtCriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idTipoLote` int(11) NOT NULL,
  `dsMnemonico` varchar(5) NOT NULL DEFAULT ' ',
  `idEntidade` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idUltimoMnemonico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `material`
--
DROP TABLE IF EXISTS `material`;
CREATE TABLE `material` (
  `id` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `material_possivel_dasa`
--
DROP TABLE IF EXISTS `material_possivel_dasa`;
CREATE TABLE `material_possivel_dasa` (
  `idMaterialPossivelDasa` int(11) NOT NULL AUTO_INCREMENT,
  `idExameDasa` int(11) NOT NULL,
  `idMaterialDasa` int(11) NOT NULL,
  `idRecipiente` int(11) NOT NULL,
  PRIMARY KEY (`idMaterialPossivelDasa`),
  KEY `index2` (`idExameDasa`),
  KEY `index3` (`idMaterialDasa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `medico`
--
DROP TABLE IF EXISTS `medico`;
CREATE TABLE `medico` (
  `idAgente` int(10) unsigned NOT NULL DEFAULT '0',
  `Crm` int(10) unsigned NOT NULL DEFAULT '0',
  `CrmUF` char(2) NOT NULL DEFAULT '',
  `idAol` int(10) unsigned DEFAULT NULL,
  `idMedicoLabor` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`Crm`,`CrmUF`,`idAgente`),
  KEY `idAgente` (`idAgente`,`idAol`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `mensagem`
--
DROP TABLE IF EXISTS `mensagem`;
CREATE TABLE `mensagem` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Assunto` varchar(254) DEFAULT NULL,
  `TipoMsg` char(1) DEFAULT NULL,
  `Texto` text,
  `Data` datetime DEFAULT NULL,
  `VersaoMinimaDB` smallint(6) DEFAULT '0',
  `DataValidade` datetime DEFAULT '2031-12-31 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `origem`
--
DROP TABLE IF EXISTS `origem`;
CREATE TABLE `origem` (
  `oriSid` int(11) NOT NULL AUTO_INCREMENT,
  `oriDescricao` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`oriSid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `origem_destino`
--
DROP TABLE IF EXISTS `origem_destino`;
CREATE TABLE `origem_destino` (
  `oriSid` int(11) NOT NULL AUTO_INCREMENT,
  `oriEntSid` int(11) DEFAULT NULL,
  `oriExaCodigo` varchar(10) DEFAULT NULL,
  `oriDesSid` int(11) DEFAULT NULL,
  `oriDescricao` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`oriSid`),
  UNIQUE KEY `entexa` (`oriEntSid`,`oriExaCodigo`),
  KEY `entidade` (`oriEntSid`,`oriExaCodigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `paciente`
--
DROP TABLE IF EXISTS `paciente`;
CREATE TABLE `paciente` (
  `idPaciente` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idAgente` int(10) unsigned NOT NULL DEFAULT '0',
  `idPacienteLabor` int(10) unsigned DEFAULT NULL,
  `idAOL` int(11) NOT NULL DEFAULT '0',
  `idLocal` char(20) DEFAULT NULL,
  `Controle` char(1) DEFAULT 'N',
  PRIMARY KEY (`idPaciente`),
  KEY `idx2` (`idAgente`,`idAOL`),
  KEY `idAgente` (`idAgente`),
  KEY `idPacienteLabor` (`idPacienteLabor`),
  KEY `idLocal` (`idLocal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC KEY_BLOCK_SIZE=8;
--
-- Table structure for table `parametro_agente`
--
DROP TABLE IF EXISTS `parametro_agente`;
CREATE TABLE `parametro_agente` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idAgente` int(10) unsigned NOT NULL DEFAULT '0',
  `Acao` char(1) DEFAULT 'I',
  `codParametro` varchar(30) DEFAULT NULL,
  `conteudo` text,
  `excluirAposTransferencia` char(1) DEFAULT 'S',
  PRIMARY KEY (`id`),
  KEY `idAgente` (`idAgente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `participantes`
--
DROP TABLE IF EXISTS `participantes`;
CREATE TABLE `participantes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idItemExameParticipante` int(11) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `codigoParticipante` int(11) NOT NULL,
  `sexo` char(1) NOT NULL,
  `tipoParticipante` varchar(100) NOT NULL,
  `obs` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `php_funcoes`
--
DROP TABLE IF EXISTS `php_funcoes`;
CREATE TABLE `php_funcoes` (
  `acao` varchar(45) NOT NULL DEFAULT '',
  `arquivo` tinytext,
  `descricao` tinytext,
  `parametrosEntrada` text,
  `ParametrosSaida` text,
  `PatternSaida` text,
  `versao` int(11) DEFAULT '1',
  PRIMARY KEY (`acao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `quantidade_exame_entidade`
--
DROP TABLE IF EXISTS `quantidade_exame_entidade`;
CREATE TABLE `quantidade_exame_entidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idEntidade` int(11) NOT NULL,
  `codigoExame` varchar(45) NOT NULL,
  `quantidadePermitida` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_qt_exame_entidade` (`idEntidade`,`codigoExame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `resultadodisponivel`
--
DROP TABLE IF EXISTS `resultadodisponivel`;
CREATE TABLE `resultadodisponivel` (
  `idEntidade` int(10) unsigned NOT NULL,
  `dataDisponibilidade` datetime NOT NULL,
  `idOrdemServico` int(10) unsigned NOT NULL,
  KEY `idEntidade` (`idEntidade`,`dataDisponibilidade`),
  KEY `ix_exames_resultadoDisponivel_01` (`dataDisponibilidade`,`idEntidade`,`idOrdemServico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `solicitacao`
--
DROP TABLE IF EXISTS `solicitacao`;
CREATE TABLE `solicitacao` (
  `idSolicitacao` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idAgente` int(10) unsigned NOT NULL DEFAULT '0',
  `idPaciente` int(10) unsigned NOT NULL DEFAULT '0',
  `idEntidade` int(10) unsigned DEFAULT '0',
  `idMedico` int(10) unsigned DEFAULT '0',
  `DataSolicitacao` datetime DEFAULT NULL,
  `DataTransferencia` datetime DEFAULT NULL,
  `DataGeradoLabor` datetime DEFAULT NULL,
  `Medicamentos` text,
  `Observacoes` text,
  `idOrdemServico` int(10) unsigned DEFAULT '0',
  `idSolExterna` int(11) DEFAULT NULL,
  `DataResultadoBaixado` datetime DEFAULT NULL,
  `idOSLis` varchar(30) DEFAULT '',
  `dataColeta` datetime DEFAULT NULL,
  `dataLidoInova` datetime DEFAULT NULL,
  `solicitacaoOrigem` enum('Ws','Aol','Integracao','AOL2') DEFAULT NULL COMMENT '	',
  PRIMARY KEY (`idSolicitacao`),
  KEY `DataGeradoLabor` (`DataGeradoLabor`),
  KEY `idOrdemServico` (`idOrdemServico`),
  KEY `idSolExterna` (`idAgente`,`idSolExterna`),
  KEY `idEntidade` (`DataTransferencia`,`idEntidade`),
  KEY `Index_6` (`idEntidade`,`DataTransferencia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `solicitacao_controle_motion`
--
DROP TABLE IF EXISTS `solicitacao_controle_motion`;
CREATE TABLE `solicitacao_controle_motion` (
  `idSolicitacaoControleMotion` int(11) NOT NULL AUTO_INCREMENT,
  `idSolicitacao` int(10) unsigned NOT NULL,
  `statusEnvio` varchar(45) NOT NULL,
  `dataEnvio` datetime NOT NULL,
  `dataRetorno` datetime DEFAULT NULL,
  `descricaoErro` varchar(200) DEFAULT NULL,
  `qtdeTentativas` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idSolicitacaoControleMotion`),
  UNIQUE KEY `inx_idsolicitacaocontrolemotion` (`idSolicitacaoControleMotion`),
  KEY `solicitacao_fk_idx` (`idSolicitacao`),
  KEY `inx_statusEnvio` (`statusEnvio`),
  KEY `inx_dataEnvio` (`dataEnvio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `solicitacao_integracao`
--
DROP TABLE IF EXISTS `solicitacao_integracao`;
CREATE TABLE `solicitacao_integracao` (
  `idsolicitacao` int(10) NOT NULL,
  `sequencial` varchar(45) DEFAULT NULL,
  `lis` varchar(100) DEFAULT NULL,
  `operador` varchar(100) DEFAULT NULL,
  `versao` varchar(50) DEFAULT NULL,
  `identidade` int(10) DEFAULT NULL,
  `dataintegracao` datetime DEFAULT NULL,
  `idOSLis` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`idsolicitacao`),
  KEY `indxSolIntegracao` (`dataintegracao`,`identidade`,`idOSLis`,`sequencial`),
  KEY `indxDataIntegracao` (`dataintegracao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `template_etiqueta`
--
DROP TABLE IF EXISTS `template_etiqueta`;
CREATE TABLE `template_etiqueta` (
  `id` tinyint(3) unsigned NOT NULL,
  `descricao` char(40) DEFAULT NULL,
  `header` text,
  `body` text,
  `footer` text,
  `campos` text,
  `script` blob,
  `apelido` char(20) DEFAULT NULL,
  `linguagemImpressora` enum('Eltron','Zebra','Allegro') DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `apelido` (`apelido`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `tempo`
--
DROP TABLE IF EXISTS `tempo`;
CREATE TABLE `tempo` (
  `DescrAmostra` varchar(25) DEFAULT NULL,
  `idItemExame` int(10) unsigned NOT NULL DEFAULT '0',
  `idAmostra` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `idOrdemServico` int(10) unsigned NOT NULL DEFAULT '0',
  `Material` int(10) unsigned DEFAULT NULL,
  `Identificador` varchar(25) DEFAULT NULL,
  `alicotado` char(1) DEFAULT 'N',
  `restricaoAmostra` char(1) DEFAULT 'N',
  `Temperatura` varchar(6) DEFAULT NULL,
  `sequencia` int(3) DEFAULT NULL,
  PRIMARY KEY (`idOrdemServico`,`idItemExame`,`idAmostra`),
  KEY `iditemexame` (`idItemExame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
SET FOREIGN_KEY_CHECKS=1;

-- aol.solicitacao_controle definition

DROP TABLE IF EXISTS `solicitacao_controle`;
CREATE TABLE `solicitacao_controle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idOrdemServico` int(10) unsigned NOT NULL,
  `statusEnvio` varchar(40) NOT NULL,
  `dataCadastro` datetime DEFAULT NULL,
  `dataEnvio` datetime DEFAULT NULL,
  `dataRetorno` datetime DEFAULT NULL,
  `descricaoErro` varchar(250) DEFAULT NULL,
  `qtdeTentativas` int(10) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_sc_idOrdemServico` (`idOrdemServico`),
  KEY `idx_sc_statusEnvio` (`statusEnvio`),
  KEY `idx_sc_dataCadastro` (`dataCadastro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE DATABASE IF NOT EXISTS labor;
USE labor;
SET FOREIGN_KEY_CHECKS=0;
--
-- Table structure for table `erroaoltransferencia`
--
DROP TABLE IF EXISTS `erroaoltransferencia`;
CREATE TABLE `erroaoltransferencia` (
  `idErro` int(11) NOT NULL AUTO_INCREMENT,
  `Descricao` text NOT NULL,
  PRIMARY KEY (`idErro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `indicador_exame_atraso`
--
DROP TABLE IF EXISTS `indicador_exame_atraso`;
CREATE TABLE `indicador_exame_atraso` (
  `idindicador_exame_atraso` int(11) NOT NULL AUTO_INCREMENT,
  `idrepresentante` int(11) DEFAULT NULL,
  `datahora` datetime DEFAULT NULL,
  `exame` varchar(10) DEFAULT NULL,
  `qtd` int(11) DEFAULT NULL,
  PRIMARY KEY (`idindicador_exame_atraso`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `faq_pergunta`
--
DROP TABLE IF EXISTS `faq_pergunta`;
CREATE TABLE `faq_pergunta` (
  `perId` int(11) NOT NULL AUTO_INCREMENT,
  `perTexto` mediumtext,
  `perIdQuemCadastrou` int(11) DEFAULT NULL,
  `perDtCadastro` datetime DEFAULT NULL,
  PRIMARY KEY (`perId`),
  FULLTEXT KEY `perTexto` (`perTexto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `sms_celular`
--
DROP TABLE IF EXISTS `sms_celular`;
CREATE TABLE `sms_celular` (
  `idTabela` int(11) NOT NULL,
  `tipoTabela` char(1) DEFAULT NULL,
  `celular` char(10) NOT NULL,
  `obs` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idTabela`,`celular`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `ordemservico_guiaconvenio`
--
DROP TABLE IF EXISTS `ordemservico_guiaconvenio`;
CREATE TABLE `ordemservico_guiaconvenio` (
  `numeroGuia` bigint(20) unsigned NOT NULL,
  `ordsid` int(11) NOT NULL,
  `dataEmissao` date DEFAULT NULL,
  `senhaAutorizacao` char(20) DEFAULT NULL,
  `dataAutorizacao` date DEFAULT NULL,
  PRIMARY KEY (`numeroGuia`,`ordsid`),
  KEY `idx_OrdSid` (`ordsid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `exames_data_coleta`
--
DROP TABLE IF EXISTS `exames_data_coleta`;
CREATE TABLE `exames_data_coleta` (
  `edcNroAmostra` int(11) NOT NULL,
  `edcsolsid` int(11) NOT NULL,
  `edcDatacoleta` datetime DEFAULT NULL,
  PRIMARY KEY (`edcNroAmostra`,`edcsolsid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `entidade_sequencia_reservada`
--
DROP TABLE IF EXISTS `entidade_sequencia_reservada`;
CREATE TABLE `entidade_sequencia_reservada` (
  `idEntidade` int(10) unsigned NOT NULL DEFAULT '0',
  `Sequencia` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idEntidade`,`Sequencia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `entidade_cartao_saldo`
--
DROP TABLE IF EXISTS `entidade_cartao_saldo`;
CREATE TABLE `entidade_cartao_saldo` (
  `idEntidade` int(10) unsigned NOT NULL,
  `ano` smallint(5) unsigned NOT NULL,
  `pontos` int(10) unsigned DEFAULT NULL,
  `pontosRetirados` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`idEntidade`,`ano`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `exame_especie`
--
DROP TABLE IF EXISTS `exame_especie`;
CREATE TABLE `exame_especie` (
  `CodExame` varchar(10) NOT NULL DEFAULT '',
  `idEspecie` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CodExame`,`idEspecie`),
  KEY `CodExame` (`CodExame`),
  KEY `idEspecie` (`idEspecie`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `na_motivo_externo`
--
DROP TABLE IF EXISTS `na_motivo_externo`;
CREATE TABLE `na_motivo_externo` (
  `idMotivo_Externo` int(11) NOT NULL,
  `na_motivo_n1` varchar(100) DEFAULT NULL,
  `na_motivo_n2` varchar(100) DEFAULT NULL,
  `na_motivo_n3` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idMotivo_Externo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `observacoes`
--
DROP TABLE IF EXISTS `observacoes`;
CREATE TABLE `observacoes` (
  `obssid` int(11) NOT NULL AUTO_INCREMENT,
  `OBSNOME` blob,
  `OBSNOMEREFER` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`obssid`),
  KEY `OBSNOMEREFER` (`OBSNOMEREFER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `log_os_troca_entidade`
--
DROP TABLE IF EXISTS `log_os_troca_entidade`;
CREATE TABLE `log_os_troca_entidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alteracao` varchar(150) DEFAULT NULL,
  `quem` int(11) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `data` (`data`),
  KEY `quem` (`quem`),
  KEY `data-quem` (`quem`,`data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `celula_fila`
--
DROP TABLE IF EXISTS `celula_fila`;
CREATE TABLE `celula_fila` (
  `IdCelula` int(11) NOT NULL,
  `idFila_Atendimento` int(11) NOT NULL,
  `Ativo` char(1) DEFAULT NULL,
  PRIMARY KEY (`IdCelula`,`idFila_Atendimento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `log_container_conferencia`
--
DROP TABLE IF EXISTS `log_container_conferencia`;
CREATE TABLE `log_container_conferencia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idfunc` int(11) DEFAULT NULL,
  `codBarras` varchar(20) DEFAULT NULL,
  `descricao` varchar(300) DEFAULT NULL,
  `datahora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `idcaixa` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `validacao`
--
DROP TABLE IF EXISTS `validacao`;
CREATE TABLE `validacao` (
  `valsid` int(11) NOT NULL AUTO_INCREMENT,
  `VALREFERENCIA` int(11) DEFAULT NULL,
  `VALLINHA` int(11) DEFAULT NULL,
  `VALOPER` char(1) DEFAULT NULL,
  `VALLINHA2` int(11) DEFAULT NULL,
  `VALVALOR` float DEFAULT NULL,
  PRIMARY KEY (`valsid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `ocorrencia_tecnica`
--
DROP TABLE IF EXISTS `ocorrencia_tecnica`;
CREATE TABLE `ocorrencia_tecnica` (
  `idOcorrencia` int(11) NOT NULL AUTO_INCREMENT,
  `DtOcorrencia` datetime DEFAULT NULL,
  `idGrupo` int(10) unsigned DEFAULT '0',
  `idEquipamento` int(10) unsigned DEFAULT '0',
  `Descricao` text,
  `idResponsavel` int(10) unsigned DEFAULT '0',
  `QtdTotal` int(10) unsigned DEFAULT '0',
  `VlrCustoTotal` decimal(11,2) DEFAULT '0.00',
  PRIMARY KEY (`idOcorrencia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `aplicacao_produto`
--
DROP TABLE IF EXISTS `aplicacao_produto`;
CREATE TABLE `aplicacao_produto` (
  `CodigoExame` char(10) NOT NULL DEFAULT '',
  `idProduto` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`CodigoExame`,`idProduto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Produtos aplicados por exame';
--
-- Table structure for table `qc_externo`
--
DROP TABLE IF EXISTS `qc_externo`;
CREATE TABLE `qc_externo` (
  `idQC` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idExame` varchar(10) DEFAULT NULL,
  `idEquipamento` int(10) unsigned DEFAULT NULL,
  `Unidade` varchar(40) DEFAULT NULL,
  `Metodo_Interno` varchar(50) DEFAULT NULL,
  `Metodo_Externo` varchar(50) DEFAULT NULL,
  `VlrQualitativoInterno` varchar(50) DEFAULT NULL,
  `VlrQualitativoExterno` varchar(50) DEFAULT NULL,
  `VlrInterno` decimal(10,2) DEFAULT NULL,
  `Media` decimal(10,2) DEFAULT NULL,
  `DRM` decimal(10,2) DEFAULT NULL,
  `DP` decimal(10,2) DEFAULT NULL,
  `CV` decimal(10,2) DEFAULT NULL,
  `QGAV` decimal(10,2) DEFAULT NULL,
  `Conceito` enum('A','B','I','MC','QI','NA') DEFAULT NULL,
  `Mes` int(10) unsigned DEFAULT NULL,
  `Ano` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`idQC`),
  KEY `QC_Externo_idEquipamento` (`idEquipamento`),
  KEY `QC_Externo_idxExame` (`idExame`),
  KEY `QC_Externo_mesano` (`Mes`,`Ano`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `entidade_analisecredito`
--
DROP TABLE IF EXISTS `entidade_analisecredito`;
CREATE TABLE `entidade_analisecredito` (
  `identidade` int(11) NOT NULL,
  `dtUltimaRenovacaoCadastro` date DEFAULT NULL,
  `prazoRenovacao` int(11) DEFAULT '6',
  `fichaCadastro` enum('sim','não') DEFAULT 'não' COMMENT 'Ficha de solicitação de cadastro de cliente',
  `consultaSERASA` enum('sim','não') DEFAULT 'não' COMMENT 'Relatório de consulta da situação cadastral - SERASA',
  `fichaAnaliseCredito` enum('sim','não') DEFAULT 'não' COMMENT 'Ficha de análise de crédito do cliente (quando houver restrição cadastral)',
  `fichaCNPJ` enum('sim','não') DEFAULT 'não' COMMENT 'Ficha de inscrição no Cadastro Nacional de Pessoa Jurídica - CNPJ',
  `contratoAlvaro` enum('sim','não') DEFAULT 'não' COMMENT 'Contrato de prestação de serviços do Alvaro com o cliente',
  `copiaContratoSocial` enum('sim','não') DEFAULT 'não' COMMENT 'Cópia DO contrato social DO cliente',
  `copiaAlteracaoContratoSocial` enum('sim','não') DEFAULT 'não' COMMENT 'Cópia da última alteração do contrato social do cliente (quando houver)',
  `copiaProcuracaoRepresentante` enum('sim','não') DEFAULT 'não' COMMENT 'Cópia da procuração DO representante legal (quando houver)',
  `assinaturaRepresentante` enum('sim','não') DEFAULT 'não' COMMENT 'Assinatura do representante legal da empresa',
  `reconhecimentoFirma` enum('sim','não') DEFAULT 'não' COMMENT 'Reconhecimento da assinatura do cliente no contrato (reconhecimento de firma)',
  `vistoContrato` enum('sim','não') DEFAULT 'não' COMMENT 'Visto do cliente em todas as folhas do contrato',
  `idRespConferencia` int(11) DEFAULT NULL,
  PRIMARY KEY (`identidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `na_reconv`
--
DROP TABLE IF EXISTS `na_reconv`;
CREATE TABLE `na_reconv` (
  `idna_reconv` int(11) NOT NULL AUTO_INCREMENT,
  `idOrdemServico` int(11) NOT NULL,
  `codExameAlvaro` varchar(45) NOT NULL,
  `idExameDasa` int(11) NOT NULL,
  `motivo` varchar(255) NOT NULL,
  `obs` varchar(255) NOT NULL,
  `idArquivol` int(11) NOT NULL,
  `dtCriacao` datetime NOT NULL,
  PRIMARY KEY (`idna_reconv`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `stat_exame_processo`
--
DROP TABLE IF EXISTS `stat_exame_processo`;
CREATE TABLE `stat_exame_processo` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `exame` char(10) DEFAULT NULL,
  `entidade` int(11) DEFAULT NULL,
  `area` tinyint(4) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `medico` int(11) DEFAULT NULL,
  `valor` decimal(12,2) DEFAULT NULL,
  `qtd` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `monitoramentoconsulta`
--
DROP TABLE IF EXISTS `monitoramentoconsulta`;
CREATE TABLE `monitoramentoconsulta` (
  `idmonitoramentoconsulta` int(11) NOT NULL AUTO_INCREMENT,
  `datainicio` datetime DEFAULT NULL,
  `datafim` datetime DEFAULT NULL,
  `tempoexecucao` double DEFAULT NULL,
  `consulta` varchar(500) DEFAULT NULL,
  `excecao` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idmonitoramentoconsulta`),
  KEY `datainicio` (`datainicio`),
  KEY `tempoexecucao` (`tempoexecucao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `entidades_anexos`
--
DROP TABLE IF EXISTS `entidades_anexos`;
CREATE TABLE `entidades_anexos` (
  `aneIdEntidade` int(11) NOT NULL DEFAULT '0',
  `aneRecepcao` text,
  `aneFrente` longblob,
  `aneVerso` longblob,
  `aneTipoLiberacao` char(1) DEFAULT 'N',
  `aneCobranca` text,
  `aneDtInclusao` datetime DEFAULT NULL,
  `aneQuemIncluiu` int(11) DEFAULT NULL,
  `aneDtAlteracao` datetime DEFAULT NULL,
  `aneQuemAlterou` int(11) DEFAULT NULL,
  PRIMARY KEY (`aneIdEntidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `tabresultados`
--
DROP TABLE IF EXISTS `tabresultados`;
CREATE TABLE `tabresultados` (
  `TRENOME` varchar(50) DEFAULT NULL,
  `TREITALICO` char(1) DEFAULT 'N',
  `TRENEGRITO` char(1) DEFAULT 'N',
  `treCodigo` varchar(10) NOT NULL DEFAULT '',
  `treOBS` text,
  `trePreResultado` char(1) DEFAULT 'N',
  PRIMARY KEY (`treCodigo`),
  KEY `TRENOME` (`TRENOME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `notas_fiscais`
--
DROP TABLE IF EXISTS `notas_fiscais`;
CREATE TABLE `notas_fiscais` (
  `notsid` int(11) NOT NULL AUTO_INCREMENT,
  `notdtfatura` datetime DEFAULT NULL,
  `NOTPERIODOFATURA` varchar(50) DEFAULT NULL,
  `NOTFATURA` int(11) DEFAULT NULL,
  `NOTNOTA` int(11) DEFAULT NULL,
  `notValor` decimal(9,2) DEFAULT '0.00',
  `NOTQUEMCADASTROU` int(11) DEFAULT NULL,
  `NOTSIDENTIDADE` int(11) DEFAULT NULL,
  `NOTQTDEXAMES` int(11) DEFAULT NULL,
  `notdtnota` datetime DEFAULT NULL,
  `NOTMARCADO` char(1) DEFAULT 'N',
  `notQuemImprimiu` int(11) DEFAULT NULL,
  `NOTVLRIR` decimal(9,2) DEFAULT '0.00',
  `NOTVLRDESCONTO` decimal(9,2) DEFAULT '0.00',
  `notObs` varchar(150) DEFAULT NULL,
  `notDtVcto` date DEFAULT NULL,
  `notVlrParc1` decimal(11,2) DEFAULT '0.00',
  `notDtVcto2` date DEFAULT NULL,
  `notVlrParc2` decimal(11,2) DEFAULT '0.00',
  `notDtVcto3` date DEFAULT NULL,
  `notVlrParc3` decimal(11,2) DEFAULT '0.00',
  `notDtVcto4` date DEFAULT NULL,
  `notVlrParc4` decimal(11,2) DEFAULT '0.00',
  `notDtVcto5` date DEFAULT NULL,
  `notVlrParc5` decimal(11,2) DEFAULT '0.00',
  `notEmpresas` varchar(20) DEFAULT NULL,
  `NotVlrContribSocial` decimal(10,2) DEFAULT '0.00',
  `NotVlrPis` decimal(10,2) DEFAULT '0.00',
  `NotVlrISSQN` decimal(10,2) DEFAULT '0.00',
  `notVlrCofins` decimal(10,2) DEFAULT '0.00',
  `notCancelada` char(1) DEFAULT 'N',
  `notCodEspecie` varchar(4) DEFAULT NULL,
  `notVlrInss` decimal(9,2) NOT NULL DEFAULT '0.00',
  `notPctSid` int(11) DEFAULT NULL,
  `NotNroRPS` int(11) NOT NULL DEFAULT '0',
  `notnotaEMS` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`notsid`),
  KEY `notQuemImprimiu` (`NOTSIDENTIDADE`),
  KEY `NOTPERIODOFATURA` (`NOTPERIODOFATURA`),
  KEY `DtNota` (`notdtnota`),
  KEY `index_RPS` (`NotNroRPS`),
  KEY `index_dtFatura` (`notdtfatura`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC COMMENT='InnoDB free: 7648256 kB';
--
-- Table structure for table `remarcacao_motion`
--
DROP TABLE IF EXISTS `remarcacao_motion`;
CREATE TABLE `remarcacao_motion` (
  `idRemarcacao` int(11) NOT NULL AUTO_INCREMENT,
  `idRequisicaoMotion` varchar(20) NOT NULL,
  `idOrdemServico` int(11) NOT NULL,
  `codigoExame` varchar(45) NOT NULL,
  `dataPrometida` datetime NOT NULL,
  `remarcado` char(1) NOT NULL DEFAULT 'N',
  `motivo` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idRemarcacao`),
  KEY `idOrdemServico` (`idOrdemServico`),
  KEY `idRequisicaoMotion` (`idRequisicaoMotion`),
  KEY `idOrdReq` (`idRequisicaoMotion`,`idOrdemServico`),
  KEY `CodExame` (`codigoExame`),
  KEY `remarcado` (`remarcado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela utilizada para integração de remarcação de exames Alv';
--
-- Table structure for table `raca`
--
DROP TABLE IF EXISTS `raca`;
CREATE TABLE `raca` (
  `racId` int(11) NOT NULL AUTO_INCREMENT,
  `racNome` varchar(40) DEFAULT NULL,
  `racIdEspecie` int(11) DEFAULT NULL,
  PRIMARY KEY (`racId`),
  KEY `racIdEspecie` (`racIdEspecie`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `na_motivo_depara`
--
DROP TABLE IF EXISTS `na_motivo_depara`;
CREATE TABLE `na_motivo_depara` (
  `idna_motivo_depara` int(11) NOT NULL AUTO_INCREMENT,
  `idMotivoLabor` int(11) NOT NULL,
  `idMotivoExterno` int(11) NOT NULL,
  PRIMARY KEY (`idna_motivo_depara`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `pfora_movi`
--
DROP TABLE IF EXISTS `pfora_movi`;
CREATE TABLE `pfora_movi` (
  `pfmId` int(11) NOT NULL AUTO_INCREMENT,
  `pfmIdOs` int(11) DEFAULT NULL,
  `pfmIdEntidade` int(11) DEFAULT NULL,
  `pfmExame` varchar(10) DEFAULT NULL,
  `pfmNomeExame` varchar(50) DEFAULT NULL,
  `pfmPcoLab` decimal(12,2) DEFAULT NULL,
  `pfmPcoLabApoio` decimal(12,2) DEFAULT NULL,
  `pfmDtRemessa` datetime DEFAULT NULL,
  `pfmDtReceb` date DEFAULT NULL,
  `pfmObs` mediumtext,
  `pfmQuemCadastrou` int(11) DEFAULT NULL,
  `pfmQuemRecebeu` varchar(18) DEFAULT NULL,
  `pfmDtPrevisao` date DEFAULT NULL,
  `pfmNovaAmostra` char(1) DEFAULT 'N',
  `pfmNomePaciente` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`pfmId`),
  KEY `pfmDtRemessa` (`pfmDtRemessa`,`pfmIdEntidade`),
  KEY `pfmExame` (`pfmExame`,`pfmIdEntidade`),
  KEY `pfmIdOs` (`pfmIdOs`,`pfmExame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `visitante`
--
DROP TABLE IF EXISTS `visitante`;
CREATE TABLE `visitante` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NroCracha` int(10) unsigned DEFAULT NULL,
  `DataEntrada` datetime DEFAULT NULL,
  `DataSaida` datetime DEFAULT NULL,
  `Nome` varchar(100) DEFAULT NULL,
  `Documento` varchar(40) DEFAULT NULL,
  `TipoDocumento` varchar(20) DEFAULT NULL,
  `MotivoVisita` text,
  `Empresa` varchar(100) DEFAULT '',
  `Foto` blob,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Visitante_DtGracha` (`NroCracha`,`DataEntrada`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `entidade_glosa`
--
DROP TABLE IF EXISTS `entidade_glosa`;
CREATE TABLE `entidade_glosa` (
  `IdGlosa` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `DtLcto` datetime DEFAULT NULL,
  `QuemIncluiu` int(11) DEFAULT NULL,
  `DtParaFatura` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `idEntidade` int(10) unsigned NOT NULL DEFAULT '0',
  `idSolic` int(10) unsigned NOT NULL DEFAULT '0',
  `Situacao` enum('Pendente','Glosado','Cobrado') DEFAULT NULL,
  `NumFatura` int(10) unsigned DEFAULT NULL,
  `DtFatura` datetime DEFAULT NULL,
  `Obs` mediumtext,
  PRIMARY KEY (`IdGlosa`),
  KEY `convenio_glosa_dtFatura` (`DtParaFatura`,`idEntidade`),
  KEY `idxDtParaFatura` (`DtParaFatura`),
  KEY `entidade_glosa_idSolic` (`idSolic`,`Situacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `cbhpm_edicaoentidades`
--
DROP TABLE IF EXISTS `cbhpm_edicaoentidades`;
CREATE TABLE `cbhpm_edicaoentidades` (
  `EdicaoEntidadesid` int(11) NOT NULL AUTO_INCREMENT,
  `eenIdEdicaoCBHPM` int(11) NOT NULL,
  `eenIdEntidade` int(11) NOT NULL,
  `eenDeflatorPorte` decimal(9,3) DEFAULT NULL,
  `eenDeflatorCustoOper` decimal(9,3) DEFAULT NULL,
  PRIMARY KEY (`EdicaoEntidadesid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `script`
--
DROP TABLE IF EXISTS `script`;
CREATE TABLE `script` (
  `idScript` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Linguagem` enum('Pascal','Python','Lua') DEFAULT NULL,
  `UnitName` varchar(12) DEFAULT NULL,
  `Descricao` varchar(50) DEFAULT NULL,
  `Script` text,
  `ScriptCompilado` blob,
  PRIMARY KEY (`idScript`),
  KEY `UnitName` (`UnitName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `itemexameestimulo`
--
DROP TABLE IF EXISTS `itemexameestimulo`;
CREATE TABLE `itemexameestimulo` (
  `iditemExameEstimulo` int(11) NOT NULL AUTO_INCREMENT,
  `idOrdemServico` int(11) DEFAULT NULL,
  `idExamesSolicitados` int(11) DEFAULT NULL,
  `idEstimulos` int(11) DEFAULT NULL,
  PRIMARY KEY (`iditemExameEstimulo`),
  KEY `index2` (`idOrdemServico`),
  KEY `index3` (`idExamesSolicitados`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `remarcacao_motivo_depara`
--
DROP TABLE IF EXISTS `remarcacao_motivo_depara`;
CREATE TABLE `remarcacao_motivo_depara` (
  `idremarcacao_motivo_depara` int(11) NOT NULL AUTO_INCREMENT,
  `idMotivoLabor` int(11) NOT NULL,
  `idMotivoExterno` int(11) NOT NULL,
  PRIMARY KEY (`idremarcacao_motivo_depara`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `stat_exame_historico_producao_parcial_28_script_novo`
--
DROP TABLE IF EXISTS `stat_exame_historico_producao_parcial_28_script_novo`;
CREATE TABLE `stat_exame_historico_producao_parcial_28_script_novo` (
  `idstat_exame` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `area` char(1) DEFAULT NULL,
  `Entidade` int(10) unsigned DEFAULT NULL,
  `exame` varchar(10) DEFAULT NULL,
  `etapa` tinyint(4) DEFAULT NULL,
  `Data` varchar(8) DEFAULT NULL,
  `qtde` int(11) DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `idEmpresa` int(10) unsigned DEFAULT NULL,
  `idMedico` int(10) unsigned DEFAULT '0',
  `qtdePct` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`idstat_exame`),
  KEY `area` (`area`,`Entidade`,`exame`,`Data`),
  KEY `etapa` (`etapa`,`Data`),
  KEY `stat_exame_medico` (`area`,`idMedico`,`exame`,`Data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `dados_faturamento_exame`
--
DROP TABLE IF EXISTS `dados_faturamento_exame`;
CREATE TABLE `dados_faturamento_exame` (
  `dfeId` int(11) NOT NULL AUTO_INCREMENT,
  `dfeIdDadosFaturamento` int(11) NOT NULL,
  `dfeExame` varchar(25) NOT NULL,
  `dfeValor` float NOT NULL,
  `dfeOs` int(11) NOT NULL,
  `dfeDataEntr` datetime NOT NULL,
  `dfeQuantExame` int(11) NOT NULL,
  `dfeDessid` int(11) DEFAULT NULL,
  `dfeSolsid` int(11) DEFAULT NULL,
  PRIMARY KEY (`dfeId`),
  KEY `ck` (`dfeIdDadosFaturamento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `amb_custo_operacional`
--
DROP TABLE IF EXISTS `amb_custo_operacional`;
CREATE TABLE `amb_custo_operacional` (
  `Codigo` char(15) NOT NULL,
  `Exame` char(10) DEFAULT NULL,
  `Descricao` varchar(100) DEFAULT NULL,
  `porte` char(5) DEFAULT NULL,
  `pcoCusto` decimal(9,3) DEFAULT NULL,
  `pcoFinal` decimal(9,2) DEFAULT NULL,
  `perc` decimal(3,2) DEFAULT NULL,
  PRIMARY KEY (`Codigo`),
  KEY `exame` (`Exame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `rodada02`
--
DROP TABLE IF EXISTS `rodada02`;
CREATE TABLE `rodada02` (
  `ldosid` int(11) NOT NULL,
  `ldostatus` varchar(255) NOT NULL,
  `ldodetails` longtext,
  `ldodatasincronizacao` datetime DEFAULT NULL,
  `ldodatacriacao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `alerta_leitura`
--
DROP TABLE IF EXISTS `alerta_leitura`;
CREATE TABLE `alerta_leitura` (
  `alesid` int(11) NOT NULL AUTO_INCREMENT,
  `aleDescricao` varchar(80) DEFAULT NULL,
  `aleDataCadastro` datetime DEFAULT NULL,
  `aleQuemCadastrou` int(11) DEFAULT NULL,
  `aleAtivo` char(1) DEFAULT 'S',
  `alePerguntar` char(1) DEFAULT 'N',
  `aleDocumento` char(1) DEFAULT 'N',
  `aleDestino` int(11) DEFAULT NULL,
  PRIMARY KEY (`alesid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela para cadastra de alerta dos exames';
--
-- Table structure for table `empresa_guia`
--
DROP TABLE IF EXISTS `empresa_guia`;
CREATE TABLE `empresa_guia` (
  `idempguia` int(11) NOT NULL AUTO_INCREMENT,
  `empCNPJ` char(18) DEFAULT NULL,
  `empCNES` char(7) DEFAULT NULL,
  `empId` int(11) DEFAULT NULL,
  `empUnidade` int(11) DEFAULT '0',
  `empRazaoSocial` varchar(30) DEFAULT NULL,
  `empIM` varchar(15) DEFAULT NULL,
  `empSerieRps` varchar(3) DEFAULT NULL,
  `empTipoRps` varchar(1) DEFAULT NULL,
  `empNumeroLote` int(11) DEFAULT NULL,
  `empOSNacional` char(1) DEFAULT NULL,
  `empICultural` char(1) DEFAULT NULL,
  `empAlicota` decimal(9,2) DEFAULT NULL,
  `empCnae` varchar(10) DEFAULT NULL,
  `empCodCMunicipio` varchar(5) DEFAULT NULL,
  `empMPServico` varchar(7) DEFAULT NULL,
  `empEndereco` varchar(100) DEFAULT NULL,
  `empInsEst` varchar(20) DEFAULT NULL,
  `codEMSfaturar` varchar(4) DEFAULT NULL,
  `empFaturarPara` int(11) DEFAULT NULL,
  `empEnderecoEletronico` text,
  PRIMARY KEY (`idempguia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `qc_comutabilidade`
--
DROP TABLE IF EXISTS `qc_comutabilidade`;
CREATE TABLE `qc_comutabilidade` (
  `idcomutabilidade` int(11) NOT NULL AUTO_INCREMENT,
  `idequipamento` int(11) DEFAULT NULL,
  `idexame` char(20) DEFAULT NULL,
  `datahora` datetime DEFAULT NULL,
  `idResponsavel` int(11) DEFAULT NULL,
  `analise` text,
  `resultados` text,
  PRIMARY KEY (`idcomutabilidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `md_cadastro`
--
DROP TABLE IF EXISTS `md_cadastro`;
CREATE TABLE `md_cadastro` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(60) DEFAULT NULL,
  `Email` varchar(60) DEFAULT NULL,
  `Site` varchar(80) DEFAULT NULL,
  `Celular` varchar(16) DEFAULT NULL,
  `EnderecoPreferencial` char(1) DEFAULT NULL,
  `Endereco_1` varchar(60) DEFAULT NULL,
  `ComplEndereco_1` varchar(60) DEFAULT NULL,
  `Bairro_1` varchar(40) DEFAULT NULL,
  `Cidade_1` varchar(40) DEFAULT NULL,
  `Estado_1` char(2) DEFAULT NULL,
  `Cep_1` varchar(9) DEFAULT NULL,
  `Telefone_1` varchar(16) DEFAULT NULL,
  `Endereco_2` varchar(60) DEFAULT NULL,
  `ComplEndereco_2` varchar(60) DEFAULT NULL,
  `Bairro_2` varchar(40) DEFAULT NULL,
  `Cidade_2` varchar(40) DEFAULT NULL,
  `Estado_2` char(2) DEFAULT NULL,
  `Cep_2` varchar(9) DEFAULT NULL,
  `Telefone_2` varchar(16) DEFAULT NULL,
  `Tipo` char(1) DEFAULT NULL,
  `idAtividade` int(11) DEFAULT NULL,
  `DtCadastro` datetime DEFAULT NULL,
  `DtAlteracao` datetime DEFAULT NULL,
  `CodExterno` int(11) DEFAULT NULL,
  `Contato` varchar(40) DEFAULT NULL,
  `Obs` varchar(60) DEFAULT NULL,
  `DiaMesNasc` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `entidade_contrato`
--
DROP TABLE IF EXISTS `entidade_contrato`;
CREATE TABLE `entidade_contrato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idEntidade` int(11) NOT NULL DEFAULT '0',
  `QuemAssinou` int(11) DEFAULT NULL,
  `Data` datetime DEFAULT NULL,
  `Contrato` mediumtext,
  `idEmpresa` smallint(6) NOT NULL DEFAULT '0',
  `eComodato` char(1) DEFAULT 'N',
  `eRepresentante` char(1) DEFAULT 'N',
  `eConvenioPgLocal` enum('Sim','Não') DEFAULT 'Não',
  `eConvenioPgFatura` enum('Sim','Não') DEFAULT 'Não',
  `eDescricaoCarteiraFaturamento` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Index_2` (`idEntidade`,`idEmpresa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `paises`
--
DROP TABLE IF EXISTS `paises`;
CREATE TABLE `paises` (
  `iso` char(2) NOT NULL,
  `iso3` char(3) NOT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY (`iso`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `controle_interlaboratorial`
--
DROP TABLE IF EXISTS `controle_interlaboratorial`;
CREATE TABLE `controle_interlaboratorial` (
  `idEntidade` int(10) unsigned NOT NULL,
  `Data` char(6) NOT NULL,
  `idOrdem` int(10) unsigned DEFAULT NULL,
  `QtdExames` smallint(5) unsigned DEFAULT '0',
  PRIMARY KEY (`idEntidade`,`Data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `exames_solicitados_teste`
--
DROP TABLE IF EXISTS `exames_solicitados_teste`;
CREATE TABLE `exames_solicitados_teste` (
  `solsid` int(11) NOT NULL AUTO_INCREMENT,
  `SOLSIDORDEM` int(11) DEFAULT NULL,
  `solExame` varchar(10) DEFAULT NULL,
  `solControleAol` char(1) DEFAULT 'N',
  `solVlrExame` decimal(11,2) DEFAULT '0.00',
  `SOLFORMAEXECUCAO` char(1) DEFAULT NULL,
  `SolObsResultado` text,
  `soldttransmissao` datetime DEFAULT NULL,
  `soldtimpressao` datetime DEFAULT NULL,
  `SOLLIBERADO` char(1) DEFAULT NULL,
  `SOLQUEMLIBEROU` smallint(6) DEFAULT NULL,
  `soldtliberacao` datetime DEFAULT NULL,
  `SOLQUEMDIGITOU` smallint(6) DEFAULT NULL,
  `soldtdigitacao` datetime DEFAULT NULL,
  `SOLSETOR` char(3) DEFAULT NULL,
  `SOLBOLETOIMPRESSO` char(1) DEFAULT NULL,
  `SOLMATERIAL` int(11) DEFAULT NULL,
  `SOLESTANTE` varchar(10) DEFAULT NULL,
  `solMarcado` char(1) DEFAULT '',
  `solCodExComposto` varchar(10) DEFAULT NULL,
  `solInterfaceado` char(1) DEFAULT 'N',
  `SolDtGeradoInternet` datetime DEFAULT NULL,
  `SolDestGeradoInternet` varchar(5) DEFAULT NULL,
  `solBioqLiberou` int(11) DEFAULT NULL,
  `solDtEntr` datetime DEFAULT NULL,
  `solQuemCadastrou` smallint(6) unsigned DEFAULT '0',
  `solLimbo` char(1) DEFAULT 'N',
  `solCriterioLiberacao` text,
  `solQtdAmostras` smallint(6) DEFAULT '1',
  `solDtRedigitacao` datetime DEFAULT NULL,
  `solMotivoRedigitacao` tinytext,
  `solQuemRedigitou` smallint(6) DEFAULT NULL,
  `solDtLidoInternetPct` datetime DEFAULT NULL,
  `solMedico` int(10) unsigned DEFAULT NULL,
  `solEntidade` int(10) unsigned DEFAULT NULL,
  `solConvenio` int(10) unsigned DEFAULT NULL,
  `solvalordesconto` decimal(9,2) DEFAULT '0.00',
  `SolNroFatura` int(10) unsigned DEFAULT NULL,
  `solDtLidoInternetMed` datetime DEFAULT NULL,
  `solTuboUnico` char(1) DEFAULT 'N',
  `solPoucaAmostra` char(1) DEFAULT 'N',
  PRIMARY KEY (`solsid`),
  KEY `solMedico` (`solMedico`,`solDtEntr`),
  KEY `solConvenio` (`solConvenio`,`solDtEntr`),
  KEY `soldtdigitacao` (`soldtdigitacao`),
  KEY `SOLSIDORDEM` (`SOLSIDORDEM`),
  KEY `solDtEntr` (`solDtEntr`),
  KEY `solEntidade` (`solEntidade`,`solDtEntr`),
  KEY `solExame` (`solExame`,`solDtEntr`),
  KEY `SOLLIBERADO` (`SOLLIBERADO`),
  KEY `soldtliberacao` (`soldtliberacao`),
  KEY `solQuemCadastrou` (`solQuemCadastrou`),
  KEY `solQuemRedigitou` (`solQuemRedigitou`),
  KEY `SOLQUEMDIGITOU` (`SOLQUEMDIGITOU`),
  KEY `SOLQUEMLIBEROU` (`SOLQUEMLIBEROU`),
  KEY `index01_exames_sol` (`SOLSIDORDEM`,`solExame`,`solEntidade`,`solDtEntr`),
  KEY `idx_exames_solicitados_solDtRedigitacao` (`solDtRedigitacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `usuariodataserver`
--
DROP TABLE IF EXISTS `usuariodataserver`;
CREATE TABLE `usuariodataserver` (
  `idUsuario` int(11) DEFAULT NULL,
  `idDataServer` tinyint(3) unsigned DEFAULT NULL,
  UNIQUE KEY `idUsuario_2` (`idUsuario`,`idDataServer`),
  KEY `idUsuario` (`idUsuario`),
  KEY `idDataServer` (`idDataServer`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `apoio_exames_enviados`
--
DROP TABLE IF EXISTS `apoio_exames_enviados`;
CREATE TABLE `apoio_exames_enviados` (
  `solsid` int(11) NOT NULL DEFAULT '0',
  `idLabApoio` int(11) NOT NULL DEFAULT '0',
  `idExterno` varchar(30) DEFAULT '',
  `Manual` char(1) DEFAULT 'N',
  `CodBarras` varchar(20) DEFAULT '',
  `EtiquetaImpressa` char(1) DEFAULT 'N',
  `DataTransferencia` datetime DEFAULT NULL,
  `DataResultado` datetime DEFAULT NULL,
  `OSExterna` varchar(30) DEFAULT '',
  `Resultado` mediumtext,
  `ResultadoLancado` char(1) DEFAULT 'N',
  `suspenso` char(1) DEFAULT 'N',
  `DataProcesso` datetime DEFAULT NULL,
  PRIMARY KEY (`solsid`),
  KEY `idExterno` (`idExterno`),
  KEY `Transferidos` (`idLabApoio`,`DataTransferencia`),
  KEY `OSExterna` (`OSExterna`),
  KEY `idLabApoio` (`idLabApoio`,`ResultadoLancado`,`DataResultado`),
  KEY `DataProcesso` (`DataProcesso`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `remarcacao_exame`
--
DROP TABLE IF EXISTS `remarcacao_exame`;
CREATE TABLE `remarcacao_exame` (
  `idremarcacao_exame` int(11) NOT NULL AUTO_INCREMENT,
  `remExame` varchar(10) DEFAULT NULL,
  `remMotivo` varchar(45) DEFAULT NULL,
  `remData` datetime DEFAULT NULL,
  `remDataRemarcacao` datetime DEFAULT NULL,
  `motivo` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idremarcacao_exame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `examesapoiob2b`
--
DROP TABLE IF EXISTS `examesapoiob2b`;
CREATE TABLE `examesapoiob2b` (
  `id_material` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `id_packaging` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id_material`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `calibracao_medicao`
--
DROP TABLE IF EXISTS `calibracao_medicao`;
CREATE TABLE `calibracao_medicao` (
  `idCalibracaoMedicao` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idCalibracao` int(10) unsigned DEFAULT NULL,
  `ordem` smallint(3) unsigned DEFAULT NULL,
  `grandeza` decimal(9,4) DEFAULT NULL,
  `erro` decimal(9,4) DEFAULT NULL,
  PRIMARY KEY (`idCalibracaoMedicao`),
  KEY `calibracao` (`idCalibracao`,`ordem`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC COMMENT='resultado de cada medicao para o equipamento	';
--
-- Table structure for table `lotes`
--
DROP TABLE IF EXISTS `lotes`;
CREATE TABLE `lotes` (
  `idlote` int(11) NOT NULL AUTO_INCREMENT,
  `identificador` varchar(50) NOT NULL,
  `idTipoLote` int(11) NOT NULL,
  `dataCriacao` datetime NOT NULL,
  `qtdeAmostrasArmazenadas` int(11) NOT NULL,
  `qtdeMaximaPermitida` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `mnemonico` varchar(50) NOT NULL,
  `dataTransmissao` datetime DEFAULT NULL,
  `lotePaternidade` char(1) NOT NULL DEFAULT 'N',
  `idEntidade` int(11) NOT NULL,
  PRIMARY KEY (`idlote`),
  UNIQUE KEY `identificador` (`identificador`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `stat_exame_historico_producao_parcial_27_script_novo`
--
DROP TABLE IF EXISTS `stat_exame_historico_producao_parcial_27_script_novo`;
CREATE TABLE `stat_exame_historico_producao_parcial_27_script_novo` (
  `idstat_exame` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `area` char(1) DEFAULT NULL,
  `Entidade` int(10) unsigned DEFAULT NULL,
  `exame` varchar(10) DEFAULT NULL,
  `etapa` tinyint(4) DEFAULT NULL,
  `Data` varchar(8) DEFAULT NULL,
  `qtde` int(11) DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `idEmpresa` int(10) unsigned DEFAULT NULL,
  `idMedico` int(10) unsigned DEFAULT '0',
  `qtdePct` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`idstat_exame`),
  KEY `area` (`area`,`Entidade`,`exame`,`Data`),
  KEY `etapa` (`etapa`,`Data`),
  KEY `stat_exame_medico` (`area`,`idMedico`,`exame`,`Data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `nota_fiscal_exportacao_old`
--
DROP TABLE IF EXISTS `nota_fiscal_exportacao_old`;
CREATE TABLE `nota_fiscal_exportacao_old` (
  `idNotaFiscal` int(10) unsigned NOT NULL DEFAULT '0',
  `nota` int(10) unsigned DEFAULT NULL,
  `dtExportacao` datetime DEFAULT NULL,
  `idFunc` smallint(5) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `rep_mensal`
--
DROP TABLE IF EXISTS `rep_mensal`;
CREATE TABLE `rep_mensal` (
  `idRepresentante` int(10) unsigned NOT NULL DEFAULT '0',
  `AnoMesSemana` varchar(7) NOT NULL DEFAULT '',
  `Etapa` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `idSetor` int(10) unsigned NOT NULL DEFAULT '0',
  `idExame` varchar(10) DEFAULT NULL,
  `idEntidade` int(10) unsigned DEFAULT NULL,
  `EntidadesAtendidas` int(10) unsigned DEFAULT NULL,
  `EndidadesNaoAtendidas` int(10) unsigned DEFAULT NULL,
  `EntidadesNaoEnviaram` int(10) unsigned DEFAULT NULL,
  `Quilometragem` int(10) unsigned DEFAULT NULL,
  `Qtd` int(10) unsigned DEFAULT NULL,
  `Valor` decimal(11,2) DEFAULT NULL,
  `VlrDeconto` decimal(11,2) DEFAULT NULL,
  PRIMARY KEY (`idRepresentante`,`AnoMesSemana`,`Etapa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `notas_fiscais_agrupado`
--
DROP TABLE IF EXISTS `notas_fiscais_agrupado`;
CREATE TABLE `notas_fiscais_agrupado` (
  `notAgrup_id` int(11) NOT NULL AUTO_INCREMENT,
  `notAgrup_dataGeracao` datetime NOT NULL,
  `notAgrup_entsid` int(11) NOT NULL,
  `notAgrup_totalGeral` decimal(9,2) NOT NULL,
  PRIMARY KEY (`notAgrup_id`),
  KEY `index2` (`notAgrup_entsid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `refvalexclusao`
--
DROP TABLE IF EXISTS `refvalexclusao`;
CREATE TABLE `refvalexclusao` (
  `idExame` varchar(10) NOT NULL DEFAULT '',
  `RefVALPerfil_idPerfil` int(10) unsigned NOT NULL DEFAULT '0',
  `ValorInf` decimal(10,2) NOT NULL DEFAULT '0.00',
  `ValorSup` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`idExame`,`RefVALPerfil_idPerfil`),
  KEY `Exclusao_FKIndex1` (`RefVALPerfil_idPerfil`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `srt_capacidadesestante`
--
DROP TABLE IF EXISTS `srt_capacidadesestante`;
CREATE TABLE `srt_capacidadesestante` (
  `idCapacidadesEstante` int(11) NOT NULL AUTO_INCREMENT,
  `capQtdLinha` int(11) NOT NULL,
  `capQtdColuna` int(11) NOT NULL,
  PRIMARY KEY (`idCapacidadesEstante`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `autorizacao_envio_laudo`
--
DROP TABLE IF EXISTS `autorizacao_envio_laudo`;
CREATE TABLE `autorizacao_envio_laudo` (
  `codigo` varchar(15) NOT NULL DEFAULT '',
  `Destino` varchar(10) NOT NULL DEFAULT '',
  `DataAutorizacao` datetime DEFAULT NULL,
  `TipoDocumento` varchar(10) DEFAULT NULL,
  `Documento` varchar(14) DEFAULT NULL,
  `Autorizado` char(1) DEFAULT 'S',
  PRIMARY KEY (`codigo`,`Destino`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `exames_estimulos`
--
DROP TABLE IF EXISTS `exames_estimulos`;
CREATE TABLE `exames_estimulos` (
  `exaSid` int(11) NOT NULL,
  `idEstimulo` int(11) NOT NULL,
  PRIMARY KEY (`exaSid`,`idEstimulo`),
  KEY `index2` (`exaSid`),
  KEY `index3` (`idEstimulo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `pacientes_medicamentos`
--
DROP TABLE IF EXISTS `pacientes_medicamentos`;
CREATE TABLE `pacientes_medicamentos` (
  `idPct` int(11) NOT NULL DEFAULT '0',
  `IdMedicamentos` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idPct`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `assinaturalaudo`
--
DROP TABLE IF EXISTS `assinaturalaudo`;
CREATE TABLE `assinaturalaudo` (
  `idAssinaturaLaudo` int(11) NOT NULL AUTO_INCREMENT,
  `CodChamada` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`idAssinaturaLaudo`),
  UNIQUE KEY `codChamada` (`CodChamada`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `srt_solicitacao`
--
DROP TABLE IF EXISTS `srt_solicitacao`;
CREATE TABLE `srt_solicitacao` (
  `RTID` int(11) NOT NULL AUTO_INCREMENT,
  `RTSOLIC` varchar(30) DEFAULT NULL,
  `RTSETOR` varchar(35) DEFAULT NULL,
  `RTDATA` datetime DEFAULT NULL,
  `RTHORA` varchar(5) DEFAULT NULL,
  `RTLINKPOS` int(10) unsigned DEFAULT NULL COMMENT 'Qdo M e o num. da OS',
  `RTEXAME` varchar(500) DEFAULT NULL,
  `RTCONFIR` char(1) DEFAULT NULL,
  `RTOUTRO` varchar(45) DEFAULT NULL COMMENT 'Usar para identificar o cod. da relacao, se exisitir',
  `RTRETORNO` char(1) DEFAULT NULL,
  `RTC` char(1) DEFAULT NULL COMMENT 'M sera usado para registrar as leituras no mapa',
  `RTRESINI` varchar(45) DEFAULT NULL,
  `RTRESFIN` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`RTID`),
  KEY `os` (`RTLINKPOS`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `examessolicitados_autenticadorconvenio`
--
DROP TABLE IF EXISTS `examessolicitados_autenticadorconvenio`;
CREATE TABLE `examessolicitados_autenticadorconvenio` (
  `idExamesSolicitados` int(11) NOT NULL,
  `AutenticadorConvenio` varchar(45) DEFAULT '',
  PRIMARY KEY (`idExamesSolicitados`),
  UNIQUE KEY `idExamesSolicitados_UNIQUE` (`idExamesSolicitados`),
  KEY `indice_autenticador` (`AutenticadorConvenio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `log_inclusao_descentralizacao`
--
DROP TABLE IF EXISTS `log_inclusao_descentralizacao`;
CREATE TABLE `log_inclusao_descentralizacao` (
  `idlog_inclusao_descentralizacao` int(11) NOT NULL AUTO_INCREMENT,
  `Data` datetime DEFAULT NULL,
  `Entidade` varchar(10) DEFAULT NULL,
  `Exame` varchar(10) DEFAULT NULL,
  `Motivo` varchar(150) DEFAULT NULL,
  `Destino` int(11) DEFAULT NULL,
  `Quem` int(11) DEFAULT NULL,
  PRIMARY KEY (`idlog_inclusao_descentralizacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `entidadelote`
--
DROP TABLE IF EXISTS `entidadelote`;
CREATE TABLE `entidadelote` (
  `idEntidade` int(11) NOT NULL,
  `idTipoLote` int(11) NOT NULL,
  `capacidadeAmostra` int(11) NOT NULL,
  PRIMARY KEY (`idEntidade`,`idTipoLote`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `dna_pai`
--
DROP TABLE IF EXISTS `dna_pai`;
CREATE TABLE `dna_pai` (
  `paiId` int(11) NOT NULL AUTO_INCREMENT,
  `paiNome` varchar(50) DEFAULT NULL,
  `paiEnder` varchar(50) DEFAULT NULL,
  `paiBairro` varchar(20) DEFAULT NULL,
  `paiCidade` varchar(20) DEFAULT NULL,
  `paiEstado` char(2) DEFAULT NULL,
  `paiCep` varchar(9) DEFAULT NULL,
  `paiPai` varchar(50) DEFAULT NULL,
  `paiMae` varchar(50) DEFAULT NULL,
  `paiDtNasc` date DEFAULT NULL,
  `paiSexo` enum('Masculino','Feminino') DEFAULT 'Masculino',
  `paiCpf` varchar(20) DEFAULT '0',
  `paiRg` varchar(20) DEFAULT '0',
  `paiExpedidor` varchar(10) DEFAULT NULL,
  `paiCor` char(1) DEFAULT NULL,
  `paiTransplMedula` enum('Sim','Não') DEFAULT 'Não',
  `paiTransfusao` enum('Sim','Não') DEFAULT 'Não',
  `paiDtCadastro` date DEFAULT NULL,
  `paiQuemCadastrou` int(11) DEFAULT NULL,
  `paiFone` varchar(20) DEFAULT NULL,
  `paiEmail` varchar(40) DEFAULT NULL,
  `paiDtExpedicao` date DEFAULT NULL,
  `paiInfoObito` varchar(100) DEFAULT NULL,
  `paiOutroDoc` varchar(50) DEFAULT NULL,
  `paiDocComprovante` enum('Fornecido','Não Fornecido') DEFAULT 'Fornecido',
  `paiFalecido` enum('Sim','Não') DEFAULT 'Não',
  `paiDtObito` date DEFAULT NULL,
  PRIMARY KEY (`paiId`),
  UNIQUE KEY `XPKdna_pai` (`paiId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `log_bandeja`
--
DROP TABLE IF EXISTS `log_bandeja`;
CREATE TABLE `log_bandeja` (
  `idlog_bandeja` int(11) NOT NULL AUTO_INCREMENT,
  `data` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `motivo` text,
  `quem` varchar(45) DEFAULT NULL,
  `operacao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idlog_bandeja`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `grupoexames`
--
DROP TABLE IF EXISTS `grupoexames`;
CREATE TABLE `grupoexames` (
  `grusid` int(11) NOT NULL AUTO_INCREMENT,
  `GRUNOME` varchar(50) DEFAULT NULL,
  `idGrupo` int(11) DEFAULT NULL,
  PRIMARY KEY (`grusid`),
  KEY `idxIdGrupo` (`idGrupo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `multiregra_westgard`
--
DROP TABLE IF EXISTS `multiregra_westgard`;
CREATE TABLE `multiregra_westgard` (
  `idRegra` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Cdg_Regra` char(3) DEFAULT NULL,
  `Dsc_Regra` varchar(100) CHARACTER SET utf8 NOT NULL,
  `Dsc_Interpretacao` text,
  PRIMARY KEY (`idRegra`),
  UNIQUE KEY `idxMultiRegra_Westgard` (`Cdg_Regra`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `stat_exame_historico_producao_parcial_27_script_antigo_so_dia_27`
--
DROP TABLE IF EXISTS `stat_exame_historico_producao_parcial_27_script_antigo_so_dia_27`;
CREATE TABLE `stat_exame_historico_producao_parcial_27_script_antigo_so_dia_27` (
  `idstat_exame` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `area` char(1) DEFAULT NULL,
  `Entidade` int(10) unsigned DEFAULT NULL,
  `exame` varchar(10) DEFAULT NULL,
  `etapa` tinyint(4) DEFAULT NULL,
  `Data` varchar(8) DEFAULT NULL,
  `qtde` int(11) DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `idEmpresa` int(10) unsigned DEFAULT NULL,
  `idMedico` int(10) unsigned DEFAULT '0',
  `qtdePct` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`idstat_exame`),
  KEY `area` (`area`,`Entidade`,`exame`,`Data`),
  KEY `etapa` (`etapa`,`Data`),
  KEY `stat_exame_medico` (`area`,`idMedico`,`exame`,`Data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `tipoparticipante`
--
DROP TABLE IF EXISTS `tipoparticipante`;
CREATE TABLE `tipoparticipante` (
  `idTipoParticipante` int(11) NOT NULL AUTO_INCREMENT,
  `dsTipoParticipante` varchar(150) NOT NULL,
  PRIMARY KEY (`idTipoParticipante`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `fabricante`
--
DROP TABLE IF EXISTS `fabricante`;
CREATE TABLE `fabricante` (
  `idfabricante` int(11) NOT NULL AUTO_INCREMENT,
  `nomefabricante` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idfabricante`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `valorreferencia`
--
DROP TABLE IF EXISTS `valorreferencia`;
CREATE TABLE `valorreferencia` (
  `refsid` int(11) NOT NULL AUTO_INCREMENT,
  `refexame` varchar(10) DEFAULT NULL,
  `RefIdadeInicial` smallint(5) unsigned DEFAULT NULL,
  `refIdadeFinal` smallint(5) unsigned DEFAULT NULL,
  `REFSEXO` char(1) DEFAULT 'A',
  `REFVALORINFERIOR` float unsigned zerofill DEFAULT NULL,
  `REFVALORSUPERIOR` float DEFAULT NULL,
  `REFOBSAUTOINF` varchar(5) DEFAULT NULL,
  `REFOBSAUTOSUP` varchar(5) DEFAULT NULL,
  `REFVLRALFA` varchar(40) DEFAULT NULL,
  `REFNOME` varchar(50) NOT NULL DEFAULT '',
  `REFSIDCRITICA` int(11) DEFAULT NULL,
  `REFPOSICAO` smallint(2) unsigned DEFAULT NULL,
  `RefIdadeIniTexto` varchar(60) DEFAULT NULL,
  `RefIdadeFimTexto` varchar(60) DEFAULT NULL,
  `refVlrInfPerigoso` float DEFAULT '0',
  `refVlrSupPerigoso` float DEFAULT '0',
  `idMaterial` int(10) unsigned DEFAULT '0',
  `refAtributo` varchar(4) DEFAULT '',
  PRIMARY KEY (`refsid`),
  KEY `AK1ValorReferenciaCritica` (`REFSIDCRITICA`),
  KEY `AK1valorReferenciaExame` (`refexame`,`REFPOSICAO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `dna_resultadocontrole`
--
DROP TABLE IF EXISTS `dna_resultadocontrole`;
CREATE TABLE `dna_resultadocontrole` (
  `resid` int(11) NOT NULL AUTO_INCREMENT,
  `dosID` int(11) DEFAULT NULL,
  `locID` int(11) DEFAULT NULL,
  `resMaeAleloA` double DEFAULT '0',
  `resMaeAleloB` double DEFAULT '0',
  `resFilhoAleloA` double DEFAULT '0',
  `resFilhoAleloB` double DEFAULT '0',
  `resPaiAleloA` double DEFAULT '0',
  `resPaiAleloB` double DEFAULT '0',
  `resIP` double DEFAULT '0',
  `resIPC` double DEFAULT '0',
  `resIdUsrIncluiu` int(10) unsigned DEFAULT NULL,
  `resIdUsrAlterou` int(10) unsigned DEFAULT NULL,
  `resTpEntrada` enum('manual','automatizada') DEFAULT 'manual',
  PRIMARY KEY (`resid`),
  KEY `locID` (`locID`),
  KEY `dosID` (`dosID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `controlequalidade`
--
DROP TABLE IF EXISTS `controlequalidade`;
CREATE TABLE `controlequalidade` (
  `idSample` char(10) NOT NULL DEFAULT '',
  `idEquipamento` int(10) unsigned NOT NULL DEFAULT '0',
  `CodExame` char(10) NOT NULL DEFAULT '',
  `Data` datetime DEFAULT NULL,
  `Valor` decimal(6,3) DEFAULT NULL,
  PRIMARY KEY (`CodExame`,`idSample`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `motivonaoinclusao`
--
DROP TABLE IF EXISTS `motivonaoinclusao`;
CREATE TABLE `motivonaoinclusao` (
  `idMotivoNaoInclusao` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `motdescricao` varchar(150) DEFAULT NULL,
  `motAtivo` char(1) DEFAULT NULL,
  `motQuemCadastrou` int(11) DEFAULT NULL,
  PRIMARY KEY (`idMotivoNaoInclusao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `dmed_declarante`
--
DROP TABLE IF EXISTS `dmed_declarante`;
CREATE TABLE `dmed_declarante` (
  `idDeclarante` smallint(5) unsigned NOT NULL,
  `descricao` char(100) DEFAULT NULL,
  PRIMARY KEY (`idDeclarante`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='tipo de declarante para dmed';
--
-- Table structure for table `rastbandeja`
--
DROP TABLE IF EXISTS `rastbandeja`;
CREATE TABLE `rastbandeja` (
  `id_rastBandeja` int(11) NOT NULL AUTO_INCREMENT,
  `tbNome` varchar(10) DEFAULT NULL,
  `tbDataFechamento` datetime DEFAULT NULL,
  `tbQuant` int(11) DEFAULT NULL,
  `tbDataEntrega` datetime DEFAULT NULL,
  `tbObs` varchar(45) DEFAULT NULL,
  `tbFlag` int(11) DEFAULT '0',
  `tbCodigoBarra` varchar(45) DEFAULT NULL,
  `tbSequen` int(11) DEFAULT '0',
  `tbQuem` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_rastBandeja`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `logistica_pedidoentidade`
--
DROP TABLE IF EXISTS `logistica_pedidoentidade`;
CREATE TABLE `logistica_pedidoentidade` (
  `idPedido` int(11) NOT NULL AUTO_INCREMENT,
  `idEntidade` int(11) NOT NULL,
  `idResponsavel` int(11) DEFAULT NULL,
  `dtPedido` datetime DEFAULT NULL,
  `statusPedido` enum('aberto','recebido','enviado','cancelado') DEFAULT 'aberto',
  `dtProcessamento` datetime DEFAULT NULL,
  `dtFechamento` datetime DEFAULT NULL,
  `idRespProcessamento` int(11) DEFAULT NULL,
  `idRespFechamento` int(11) DEFAULT NULL,
  `idPedidoPai` int(11) DEFAULT NULL,
  PRIMARY KEY (`idPedido`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `ordem_servico_laudo_teste_05`
--
DROP TABLE IF EXISTS `ordem_servico_laudo_teste_05`;
CREATE TABLE `ordem_servico_laudo_teste_05` (
  `ldosid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `stat_exame_historico_producao`
--
DROP TABLE IF EXISTS `stat_exame_historico_producao`;
CREATE TABLE `stat_exame_historico_producao` (
  `idstat_exame` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `area` char(1) DEFAULT NULL,
  `Entidade` int(10) unsigned DEFAULT NULL,
  `exame` varchar(10) DEFAULT NULL,
  `etapa` tinyint(4) DEFAULT NULL,
  `Data` varchar(8) DEFAULT NULL,
  `qtde` int(11) DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `idEmpresa` int(10) unsigned DEFAULT NULL,
  `idMedico` int(10) unsigned DEFAULT '0',
  `qtdePct` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`idstat_exame`),
  KEY `area` (`area`,`Entidade`,`exame`,`Data`),
  KEY `etapa` (`etapa`,`Data`),
  KEY `stat_exame_medico` (`area`,`idMedico`,`exame`,`Data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `dna_mae`
--
DROP TABLE IF EXISTS `dna_mae`;
CREATE TABLE `dna_mae` (
  `maeId` int(11) NOT NULL AUTO_INCREMENT,
  `maeNome` varchar(50) DEFAULT NULL,
  `maeEnder` varchar(50) DEFAULT NULL,
  `maeBairro` varchar(20) DEFAULT NULL,
  `maeCidade` varchar(20) DEFAULT NULL,
  `maeEstado` char(2) DEFAULT NULL,
  `maeCep` varchar(9) DEFAULT NULL,
  `maePai` varchar(50) DEFAULT NULL,
  `maeMae` varchar(50) DEFAULT NULL,
  `maeDtNasc` date DEFAULT NULL,
  `maeSexo` enum('Masculino','Feminino') DEFAULT 'Feminino',
  `maeCpf` varchar(20) DEFAULT '0',
  `maeRg` varchar(20) DEFAULT '0',
  `maeExpedidor` varchar(10) DEFAULT NULL,
  `maeCor` char(1) DEFAULT NULL,
  `maeTransplMedula` enum('Sim','Não') DEFAULT 'Não',
  `maeTransfusao` enum('Sim','Não') DEFAULT 'Não',
  `maeDtCadastro` date DEFAULT NULL,
  `maeQuemCadastrou` int(11) DEFAULT NULL,
  `maeFone` varchar(20) DEFAULT NULL,
  `maeEMail` varchar(40) DEFAULT NULL,
  `maeDtExpedicao` date DEFAULT NULL,
  `maeOutroDoc` varchar(50) DEFAULT NULL,
  `maeDocComprovante` enum('Fornecido','Não Fornecido') DEFAULT 'Fornecido',
  PRIMARY KEY (`maeId`),
  UNIQUE KEY `XPKdna_mae` (`maeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `log_os_exclusao`
--
DROP TABLE IF EXISTS `log_os_exclusao`;
CREATE TABLE `log_os_exclusao` (
  `idLogOs` int(11) NOT NULL AUTO_INCREMENT,
  `idOrdem` int(11) DEFAULT NULL,
  `Data` datetime DEFAULT NULL,
  `Motivo` text,
  `Quem` int(11) DEFAULT NULL,
  `exame` varchar(10) DEFAULT NULL,
  `idSolic` int(11) DEFAULT NULL,
  `idEntCobranca` int(10) unsigned DEFAULT '0',
  `VlrExame` decimal(9,2) DEFAULT '0.00',
  PRIMARY KEY (`idLogOs`),
  KEY `logos_data` (`Data`),
  KEY `idOrdem` (`idOrdem`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `qc_if_analisecritica`
--
DROP TABLE IF EXISTS `qc_if_analisecritica`;
CREATE TABLE `qc_if_analisecritica` (
  `idQCAC` int(11) NOT NULL AUTO_INCREMENT,
  `idequipamento` int(11) DEFAULT NULL,
  `idexame` char(20) DEFAULT NULL,
  `idUsuario` int(11) DEFAULT NULL,
  `Analise` text,
  `DtHora` datetime DEFAULT NULL,
  PRIMARY KEY (`idQCAC`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `os_informacao`
--
DROP TABLE IF EXISTS `os_informacao`;
CREATE TABLE `os_informacao` (
  `DataAtendimento` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `QuemAtendeu` int(10) unsigned DEFAULT '0',
  `Informacao` text,
  `idOrdem` int(10) unsigned NOT NULL DEFAULT '9',
  `LiberadaInternet` char(1) NOT NULL DEFAULT 'N',
  `IdQuemLiberou` int(10) unsigned DEFAULT NULL,
  `DtLiberacao` datetime DEFAULT NULL,
  `idExame` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idOrdem`,`DataAtendimento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `sms_envio`
--
DROP TABLE IF EXISTS `sms_envio`;
CREATE TABLE `sms_envio` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `texto` varchar(250) DEFAULT NULL,
  `idTexto` smallint(5) unsigned NOT NULL,
  `destinatario` varchar(12) DEFAULT NULL,
  `dataGeracao` datetime DEFAULT NULL,
  `dataEnvio` datetime DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `idOrdemServico` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`,`dataEnvio`),
  KEY `idOrdemServico` (`idOrdemServico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `entidade_objecao`
--
DROP TABLE IF EXISTS `entidade_objecao`;
CREATE TABLE `entidade_objecao` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idEntidade` int(11) DEFAULT NULL,
  `DtObjecao` datetime DEFAULT NULL,
  `idFunc` smallint(5) unsigned DEFAULT NULL,
  `Objecao` text,
  `dtAcao` datetime DEFAULT NULL,
  `Acao` text,
  PRIMARY KEY (`id`),
  KEY `entidade` (`idEntidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `agrupamento_exame_gestor`
--
DROP TABLE IF EXISTS `agrupamento_exame_gestor`;
CREATE TABLE `agrupamento_exame_gestor` (
  `CodExame` char(10) NOT NULL DEFAULT '',
  `idGestor` int(11) DEFAULT NULL,
  PRIMARY KEY (`CodExame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `stat_exame_historico_producao_parcial_28_script_antigo`
--
DROP TABLE IF EXISTS `stat_exame_historico_producao_parcial_28_script_antigo`;
CREATE TABLE `stat_exame_historico_producao_parcial_28_script_antigo` (
  `idstat_exame` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `area` char(1) DEFAULT NULL,
  `Entidade` int(10) unsigned DEFAULT NULL,
  `exame` varchar(10) DEFAULT NULL,
  `etapa` tinyint(4) DEFAULT NULL,
  `Data` varchar(8) DEFAULT NULL,
  `qtde` int(11) DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `idEmpresa` int(10) unsigned DEFAULT NULL,
  `idMedico` int(10) unsigned DEFAULT '0',
  `qtdePct` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`idstat_exame`),
  KEY `area` (`area`,`Entidade`,`exame`,`Data`),
  KEY `etapa` (`etapa`,`Data`),
  KEY `stat_exame_medico` (`area`,`idMedico`,`exame`,`Data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `cbhpm_edicaoportes`
--
DROP TABLE IF EXISTS `cbhpm_edicaoportes`;
CREATE TABLE `cbhpm_edicaoportes` (
  `EdicaoPortesid` int(11) NOT NULL AUTO_INCREMENT,
  `epoIdEdicaoCBHPM` int(11) NOT NULL,
  `epoPorteEdicao` varchar(45) DEFAULT NULL,
  `epoVlrPorte` decimal(9,3) DEFAULT NULL,
  PRIMARY KEY (`EdicaoPortesid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `cadastro_faturamento`
--
DROP TABLE IF EXISTS `cadastro_faturamento`;
CREATE TABLE `cadastro_faturamento` (
  `idcadastro_faturamento` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `diaInicial` int(11) DEFAULT NULL,
  `diaFinal` varchar(2) DEFAULT NULL,
  `entidadesFilho` text NOT NULL,
  `area` int(11) NOT NULL,
  `ordem` varchar(10) NOT NULL,
  `quem_enviou` char(1) NOT NULL,
  `especie` int(11) NOT NULL,
  `empresa` varchar(15) NOT NULL,
  `cod_exame` int(11) NOT NULL,
  `endereco` varchar(5) NOT NULL,
  `encardenado` varchar(5) NOT NULL,
  `entidadeMae` int(11) NOT NULL,
  `periodo` varchar(15) NOT NULL,
  `entrada_laudo` char(1) DEFAULT 'E',
  PRIMARY KEY (`idcadastro_faturamento`),
  KEY `index2` (`entidadeMae`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `relatorio`
--
DROP TABLE IF EXISTS `relatorio`;
CREATE TABLE `relatorio` (
  `relId` int(11) NOT NULL AUTO_INCREMENT,
  `relDescricao` varchar(50) DEFAULT NULL,
  `relDtAtualizacao` datetime DEFAULT NULL,
  `relArquivo` blob,
  `relCodigo` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`relId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `tabela_precos_item`
--
DROP TABLE IF EXISTS `tabela_precos_item`;
CREATE TABLE `tabela_precos_item` (
  `tpiIdSrv` int(11) DEFAULT NULL,
  `tpiExame` varchar(10) DEFAULT NULL,
  `tpiPreco` decimal(9,2) DEFAULT NULL,
  UNIQUE KEY `XPKtabela_precos_item` (`tpiIdSrv`,`tpiExame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `almicro_dadosadicionais`
--
DROP TABLE IF EXISTS `almicro_dadosadicionais`;
CREATE TABLE `almicro_dadosadicionais` (
  `idOrdemServico` int(10) unsigned NOT NULL DEFAULT '0',
  `Produto` varchar(40) DEFAULT NULL,
  `Fabricante` varchar(40) DEFAULT NULL,
  `Lote` varchar(25) DEFAULT NULL,
  `DataProducao` varchar(13) DEFAULT NULL,
  `AspectoAmostra` varchar(30) DEFAULT NULL,
  `DescricaoEmbalagem` varchar(30) DEFAULT NULL,
  `DataValidade` varchar(13) DEFAULT NULL,
  `DataColeta` varchar(13) DEFAULT NULL,
  `HoraColeta` varchar(13) DEFAULT NULL,
  `TemperaturaColeta` varchar(13) DEFAULT NULL,
  `DataRemessa` varchar(13) DEFAULT NULL,
  `ResponsavelColeta` varchar(30) DEFAULT NULL,
  `TempAmostra` varchar(13) DEFAULT NULL,
  `resParmNorm` varchar(100) DEFAULT NULL,
  `resComentario` varchar(250) DEFAULT NULL,
  `resAmostra` varchar(100) DEFAULT NULL,
  `NroLaudoOficial` varchar(20) DEFAULT NULL,
  `COA` varchar(15) DEFAULT NULL,
  `ciclo` varchar(12) DEFAULT NULL,
  `amostra` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`idOrdemServico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `pool_reserva_numeracao`
--
DROP TABLE IF EXISTS `pool_reserva_numeracao`;
CREATE TABLE `pool_reserva_numeracao` (
  `idTabela` char(32) NOT NULL,
  `reserva` mediumtext,
  PRIMARY KEY (`idTabela`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `tipoparticipanteexame`
--
DROP TABLE IF EXISTS `tipoparticipanteexame`;
CREATE TABLE `tipoparticipanteexame` (
  `IdTipoParticipante` int(11) NOT NULL,
  `IdExameMotion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `flabfunc`
--
DROP TABLE IF EXISTS `flabfunc`;
CREATE TABLE `flabfunc` (
  `fncodigo` int(11) NOT NULL AUTO_INCREMENT,
  `FNNOME` varchar(50) DEFAULT NULL,
  `FNENDERECO` varchar(50) DEFAULT NULL,
  `FNBAIRRO` varchar(20) DEFAULT NULL,
  `FNCIDADE` varchar(20) DEFAULT NULL,
  `FNESTADO` char(2) DEFAULT NULL,
  `FNCEP` varchar(9) DEFAULT NULL,
  `FNCARTEIRA` varchar(10) DEFAULT NULL,
  `FNIDENTIDADE` varchar(15) DEFAULT NULL,
  `FNCPF` varchar(15) DEFAULT NULL,
  `fnDtNasc` date DEFAULT NULL,
  `fnCargo` varchar(25) DEFAULT NULL,
  `fndtadmissao` datetime DEFAULT NULL,
  `fnDtDemissao` datetime DEFAULT NULL,
  `FNMOTIVO` varchar(50) DEFAULT NULL,
  `FNFONE` varchar(15) DEFAULT NULL,
  `fnfoto` longblob,
  `FNESCOLARIDADE` varchar(20) DEFAULT NULL,
  `FNESTCIVIL` varchar(30) DEFAULT NULL,
  `FNNOMECONJ` varchar(40) DEFAULT NULL,
  `fnnascconj` datetime DEFAULT NULL,
  `FNESCOLARIDADECONJ` varchar(20) DEFAULT NULL,
  `FNSALARIO` double DEFAULT NULL,
  `FNEXTRA` double DEFAULT NULL,
  `FNPERCCOMIS` double DEFAULT NULL,
  `FNSENHA` varchar(10) DEFAULT NULL,
  `FNIDENT` varchar(10) DEFAULT NULL,
  `FNSETOR` char(3) DEFAULT NULL,
  `fndtsenha` datetime DEFAULT NULL,
  `fnLiberaResult` char(1) DEFAULT 'N',
  `fnFacilitador` char(1) DEFAULT 'N',
  `fnPegaTicket` char(1) DEFAULT 'N',
  `fnGestor` char(1) DEFAULT 'N',
  `fnEmpresaLiberada` varchar(20) DEFAULT NULL,
  `fnNivelSeguranca` tinyint(4) DEFAULT '0',
  `fnControleDoc` char(1) DEFAULT 'N',
  `fnAdminEmail` char(1) DEFAULT 'N',
  `fnAcessoRemoto` char(1) DEFAULT 'N',
  `fnAcessoInternet` char(1) DEFAULT NULL,
  `fnAssinatura` blob,
  `fnRubrica` blob,
  `fnNumConselho` varchar(15) DEFAULT NULL,
  `fnUfConselho` char(2) DEFAULT NULL,
  `fnMatricula` int(11) DEFAULT NULL,
  `fnPeriodoMaxPesq` int(11) DEFAULT '90',
  `fnIncExaOSComNovaAmostra` char(1) DEFAULT 'N',
  `fnLiberaDesconto` varchar(1) DEFAULT 'N',
  `fnAlteraRastreabilidade` varchar(1) DEFAULT 'N',
  PRIMARY KEY (`fncodigo`),
  KEY `FNIDENT` (`FNIDENT`),
  KEY `AK1FlabFuncNome` (`FNNOME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `log_cancelar_fatura`
--
DROP TABLE IF EXISTS `log_cancelar_fatura`;
CREATE TABLE `log_cancelar_fatura` (
  `idlog_cancelar_fatura` int(11) NOT NULL AUTO_INCREMENT,
  `notsid` varchar(100) NOT NULL,
  `notfatura` int(11) NOT NULL,
  `notnota` varchar(100) DEFAULT NULL,
  `entidades` varchar(100) NOT NULL,
  `data` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `quem` int(11) NOT NULL,
  `motivo` varchar(100) NOT NULL,
  PRIMARY KEY (`idlog_cancelar_fatura`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `os_busca`
--
DROP TABLE IF EXISTS `os_busca`;
CREATE TABLE `os_busca` (
  `idOrdem` int(11) NOT NULL,
  `idFunc` int(11) DEFAULT NULL,
  `Data` datetime DEFAULT NULL,
  `Mensagem` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idOrdem`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `ind_cadastro`
--
DROP TABLE IF EXISTS `ind_cadastro`;
CREATE TABLE `ind_cadastro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Descricao` varchar(60) DEFAULT NULL,
  `Meta` decimal(11,2) DEFAULT NULL,
  `TextoSql` text,
  `Observacao` text,
  `IncluiValor` char(1) DEFAULT 'N',
  `Unidade` varchar(5) DEFAULT NULL,
  `Acumula` char(1) DEFAULT 'N',
  `idProcesso` int(11) DEFAULT NULL,
  `TipoDado` enum('%','Acertos','Erros','Qtde') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `solicitacaoerro`
--
DROP TABLE IF EXISTS `solicitacaoerro`;
CREATE TABLE `solicitacaoerro` (
  `idSolErro` int(11) NOT NULL AUTO_INCREMENT,
  `idSolicitacao` int(11) NOT NULL,
  `idItemExame` int(11) NOT NULL,
  `idErro` int(11) NOT NULL,
  PRIMARY KEY (`idSolErro`),
  KEY `index2` (`idItemExame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `empassinatura`
--
DROP TABLE IF EXISTS `empassinatura`;
CREATE TABLE `empassinatura` (
  `idEmpresa` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `idSeq` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `Assinatura` blob
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `subcategorias`
--
DROP TABLE IF EXISTS `subcategorias`;
CREATE TABLE `subcategorias` (
  `idCategoria` int(11) NOT NULL,
  `idSubCategoria` int(11) NOT NULL,
  `dsCategoria` varchar(100) NOT NULL,
  PRIMARY KEY (`idCategoria`,`idSubCategoria`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `entidade_cartao_retirada`
--
DROP TABLE IF EXISTS `entidade_cartao_retirada`;
CREATE TABLE `entidade_cartao_retirada` (
  `idEntidade` int(10) unsigned NOT NULL,
  `dtRetirada` datetime DEFAULT NULL,
  `idQuem` int(10) unsigned DEFAULT NULL,
  `idPremio` int(10) unsigned NOT NULL DEFAULT '0',
  `quemSolicitou` varchar(50) DEFAULT NULL,
  `ano` smallint(6) NOT NULL DEFAULT '0',
  `voltagem` enum('110V','220V') DEFAULT NULL,
  PRIMARY KEY (`idEntidade`,`ano`,`idPremio`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `manutencao_centrocusto`
--
DROP TABLE IF EXISTS `manutencao_centrocusto`;
CREATE TABLE `manutencao_centrocusto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `centrocusto` varchar(5) NOT NULL,
  `UltimaOS` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `mensagem_email_faixa_panico`
--
DROP TABLE IF EXISTS `mensagem_email_faixa_panico`;
CREATE TABLE `mensagem_email_faixa_panico` (
  `idmsgPanico` int(11) NOT NULL AUTO_INCREMENT,
  `corpo_msg` text NOT NULL,
  `idDestino` int(11) NOT NULL,
  PRIMARY KEY (`idmsgPanico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `log_nfse_dmed`
--
DROP TABLE IF EXISTS `log_nfse_dmed`;
CREATE TABLE `log_nfse_dmed` (
  `idlog_NFSE_DMED` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ConteudoAnterior` text,
  `Quem` int(11) DEFAULT NULL,
  `Data` datetime DEFAULT NULL,
  `Operacao` enum('Inserção','Edição','Exclusão') DEFAULT NULL,
  `notsid` int(11) DEFAULT NULL,
  PRIMARY KEY (`idlog_NFSE_DMED`),
  KEY `notsid` (`notsid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela para registrar o log do dados NFSE/DMED';
--
-- Table structure for table `log_entidade_recebe_resultados`
--
DROP TABLE IF EXISTS `log_entidade_recebe_resultados`;
CREATE TABLE `log_entidade_recebe_resultados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idOrdem` int(11) NOT NULL,
  `entCadastro` int(11) NOT NULL,
  `entDestino` int(11) NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `rep_rota`
--
DROP TABLE IF EXISTS `rep_rota`;
CREATE TABLE `rep_rota` (
  `Id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Descricao` varchar(50) DEFAULT NULL,
  `Frequencia` varchar(7) DEFAULT NULL,
  `TamanhoTrajeto` int(11) DEFAULT NULL,
  `IdRepresentante` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `index_rep_setor_repres` (`IdRepresentante`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `exame_estabilidade`
--
DROP TABLE IF EXISTS `exame_estabilidade`;
CREATE TABLE `exame_estabilidade` (
  `codExame` varchar(10) NOT NULL,
  `ambiente` smallint(6) DEFAULT '0',
  `tpAmbiente` enum('Hora','Dia','Mes','Ano') DEFAULT NULL,
  `refrigerada` smallint(6) DEFAULT '0',
  `tpRefrigerada` enum('Hora','Dia','Mes','Ano') DEFAULT NULL,
  `freezer` smallint(6) DEFAULT '0',
  `tpFreezer` enum('Hora','Dia','Mes','Ano') DEFAULT NULL,
  PRIMARY KEY (`codExame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Identificar a estabilidade do exame. Criada por Sidnei em ou';
--
-- Table structure for table `exame_sequencia_resultado`
--
DROP TABLE IF EXISTS `exame_sequencia_resultado`;
CREATE TABLE `exame_sequencia_resultado` (
  `ID_EXAME_SOLICITADO` int(11) NOT NULL,
  `ID_EXAME_COMPLEMENTAR_ANTIBIOGRAMA` int(11) NOT NULL,
  `SEQUENCIA_RESULTADO` int(11) NOT NULL,
  `SEQUENCIA_AMOSTRA` int(11) NOT NULL,
  `SEQUENCIA_OCORRENCIA` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_EXAME_SOLICITADO`,`ID_EXAME_COMPLEMENTAR_ANTIBIOGRAMA`,`SEQUENCIA_RESULTADO`,`SEQUENCIA_AMOSTRA`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `controlenoticias`
--
DROP TABLE IF EXISTS `controlenoticias`;
CREATE TABLE `controlenoticias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_noticia` text,
  `ip` varchar(100) DEFAULT '0',
  `data` date DEFAULT '0000-00-00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `mapa`
--
DROP TABLE IF EXISTS `mapa`;
CREATE TABLE `mapa` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(20) DEFAULT NULL,
  `Data` datetime DEFAULT NULL,
  `idQuem` smallint(5) unsigned DEFAULT NULL,
  `xOss` text,
  `xExames` text,
  PRIMARY KEY (`Id`),
  KEY `Nome` (`Nome`,`Data`),
  KEY `mapa_data` (`Data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `log_exame_conferencia`
--
DROP TABLE IF EXISTS `log_exame_conferencia`;
CREATE TABLE `log_exame_conferencia` (
  `idlog_exame_conferencia` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordsid` int(10) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `cdBarra` varchar(45) DEFAULT NULL,
  `idExamesSolicitados` int(11) DEFAULT NULL,
  PRIMARY KEY (`idlog_exame_conferencia`),
  KEY `ordsid` (`ordsid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `qc_ponto`
--
DROP TABLE IF EXISTS `qc_ponto`;
CREATE TABLE `qc_ponto` (
  `idPonto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idLote` int(10) unsigned NOT NULL DEFAULT '0',
  `Dt_Avl` date DEFAULT NULL,
  `Hr_Avl` time DEFAULT NULL,
  `Valor` decimal(11,3) DEFAULT NULL,
  `Obs` text,
  `DscViolacao` text,
  `Descartado` char(1) DEFAULT 'F',
  `Flag` char(1) DEFAULT 'F',
  `NvlControle` char(1) DEFAULT 'A',
  `TipoLancamento` enum('Manual','Interfaceado') DEFAULT 'Manual',
  `idUsrAlterou` int(11) unsigned NOT NULL DEFAULT '0',
  `DtAlteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `idSample` varchar(15) DEFAULT NULL,
  `ObsEvento` text,
  PRIMARY KEY (`idPonto`),
  UNIQUE KEY `QC_Ponto_Chave` (`idPonto`,`idLote`),
  KEY `Descartado` (`Descartado`,`Dt_Avl`),
  KEY `idxDataHora` (`Dt_Avl`,`Hr_Avl`),
  KEY `IdxIdSample` (`idSample`),
  KEY `QC_Ponto_FKIndex1` (`idLote`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `exame_dicionario`
--
DROP TABLE IF EXISTS `exame_dicionario`;
CREATE TABLE `exame_dicionario` (
  `dicId` int(11) NOT NULL AUTO_INCREMENT,
  `dicExame` varchar(10) NOT NULL DEFAULT '',
  `dicNome` varchar(40) DEFAULT NULL,
  `dicVerificacao` mediumtext,
  PRIMARY KEY (`dicId`),
  KEY `exame_dicionario_exame` (`dicExame`),
  KEY `exame_dicionario_nome` (`dicNome`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `labcard`
--
DROP TABLE IF EXISTS `labcard`;
CREATE TABLE `labcard` (
  `crdsid` int(11) NOT NULL AUTO_INCREMENT,
  `CRDRG` varchar(15) DEFAULT NULL,
  `CRDCIC` varchar(15) DEFAULT NULL,
  `CRDESTADOCIVIL` varchar(10) DEFAULT NULL,
  `CRDCONJUGE` varchar(40) DEFAULT NULL,
  `CRDFILHOS` int(11) DEFAULT NULL,
  `CRDPROFISSAO` varchar(40) DEFAULT NULL,
  `CRDCARGO` varchar(20) DEFAULT NULL,
  `CRDENDERECO` varchar(40) DEFAULT NULL,
  `CRDCIDADE` varchar(20) DEFAULT NULL,
  `CRDESTADO` char(2) DEFAULT NULL,
  `CRDCEP` varchar(8) DEFAULT NULL,
  `CRDFONE` varchar(20) DEFAULT NULL,
  `CRDFAX` varchar(20) DEFAULT NULL,
  `CRDEMAIL` varchar(40) DEFAULT NULL,
  `CRDTEMPORESIDENCIA` double DEFAULT NULL,
  `crdconhecimento` varchar(30) DEFAULT NULL,
  `CRDQUALMEIO` varchar(40) DEFAULT NULL,
  `CRDPLANOSAUDE` varchar(30) DEFAULT NULL,
  `CRDFAZCHECKUP` char(1) DEFAULT NULL,
  `CRDCOMORETIRAR` char(2) DEFAULT NULL,
  `CRDPCT` int(11) DEFAULT NULL,
  `CRDEMPRESA` int(11) DEFAULT NULL,
  `CrdDtCadastro` datetime DEFAULT NULL,
  `CrdNome` varchar(60) DEFAULT NULL,
  `CrdDtNasc` datetime DEFAULT NULL,
  `CrdBairro` varchar(30) DEFAULT NULL,
  `CrdProfEndereco` varchar(60) DEFAULT NULL,
  `CrdProfCidade` varchar(30) DEFAULT NULL,
  `CrdProfBairro` varchar(30) DEFAULT NULL,
  `CrdProfEstado` char(2) DEFAULT NULL,
  `CrdProfCEP` varchar(9) DEFAULT NULL,
  `CrdProfFone` varchar(15) DEFAULT NULL,
  `CrdProfFax` varchar(15) DEFAULT NULL,
  `CrdProfEmail` varchar(60) DEFAULT NULL,
  `CrdClienteAntigo` char(1) DEFAULT NULL,
  `crdDtImpressao` datetime DEFAULT NULL,
  `crdQuemImprimiu` int(11) DEFAULT NULL,
  `crdEnvio` varchar(60) DEFAULT NULL,
  `crdDtEnvio` date DEFAULT NULL,
  `crdQuemEnviou` smallint(6) DEFAULT NULL,
  `profissionalSaude` char(1) DEFAULT 'N',
  `crdProfProfissao` char(60) DEFAULT NULL,
  `crdProfCargo` char(60) DEFAULT NULL,
  PRIMARY KEY (`crdsid`),
  KEY `indpct` (`CRDPCT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `campo_complementar`
--
DROP TABLE IF EXISTS `campo_complementar`;
CREATE TABLE `campo_complementar` (
  `ccoID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ccoExame` varchar(10) NOT NULL DEFAULT '',
  `ccoLabel` varchar(20) DEFAULT NULL,
  `ccoTipo` enum('Texto','Alfabético','Numérico') DEFAULT NULL,
  `ccoTamMinimo` int(10) unsigned DEFAULT NULL,
  `ccoListaLaudo` char(1) DEFAULT 'N',
  `ccoDtUltAlt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ccoID`),
  KEY `ccoExame` (`ccoExame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `ordem_servico_laudo_teste_03`
--
DROP TABLE IF EXISTS `ordem_servico_laudo_teste_03`;
CREATE TABLE `ordem_servico_laudo_teste_03` (
  `ldosid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `qc_lote`
--
DROP TABLE IF EXISTS `qc_lote`;
CREATE TABLE `qc_lote` (
  `idLote` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idEquipamento` int(10) unsigned NOT NULL DEFAULT '0',
  `idExame` varchar(30) NOT NULL,
  `idExameNoEquipamento` varchar(30) NOT NULL,
  `Lote` varchar(60) DEFAULT NULL,
  `LoteEquipamento` varchar(50) DEFAULT NULL,
  `Dsc_Calibrador` varchar(40) DEFAULT NULL,
  `Dt_Inclusao` datetime DEFAULT '0000-00-00 00:00:00',
  `Dt_Vencimento` date DEFAULT NULL,
  `CtrlAlto_CVReferencia` decimal(11,3) DEFAULT '0.000',
  `CtrlAlto_VlrSuperior` decimal(11,3) DEFAULT NULL,
  `CtrlAlto_VlrInferior` decimal(11,3) DEFAULT NULL,
  `CtrlAlto_Unidade` varchar(40) DEFAULT '',
  `CtrlMedio_CVReferencia` decimal(11,3) DEFAULT '0.000',
  `CtrlMedio_VlrSuperior` decimal(11,3) DEFAULT NULL,
  `CtrlMedio_VlrInferior` decimal(11,3) DEFAULT NULL,
  `CtrlMedio_Unidade` varchar(40) DEFAULT '',
  `CtrlBaixo_CVReferencia` decimal(11,3) DEFAULT '0.000',
  `CtrlBaixo_VlrSuperior` decimal(11,3) DEFAULT NULL,
  `CtrlBaixo_VlrInferior` decimal(11,3) DEFAULT NULL,
  `CtrlBaixo_Unidade` varchar(40) DEFAULT NULL,
  `Celula` int(11) NOT NULL DEFAULT '0',
  `LancaManual` enum('Sim','Não') NOT NULL DEFAULT 'Não',
  `FaixaDinamica` enum('Sim','Não') DEFAULT 'Não',
  `EmUso` enum('Sim','Não') DEFAULT NULL,
  `idUsrAlterou` int(11) DEFAULT NULL,
  `DtUltimaAlteracao` datetime DEFAULT NULL,
  `RotinaAlto` char(10) DEFAULT 'D111111000',
  `RotinaMedio` char(10) DEFAULT 'D111111000',
  `RotinaBaixo` char(10) DEFAULT 'D111111000',
  PRIMARY KEY (`idLote`),
  UNIQUE KEY `idx_Equip_Exame` (`idEquipamento`,`idExame`,`Lote`,`idExameNoEquipamento`,`Celula`) USING BTREE,
  KEY `idx_Data_Inclusao` (`idExame`,`Dt_Inclusao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `dna_resultado`
--
DROP TABLE IF EXISTS `dna_resultado`;
CREATE TABLE `dna_resultado` (
  `resid` int(11) NOT NULL AUTO_INCREMENT,
  `dosID` int(11) DEFAULT NULL,
  `locID` int(11) DEFAULT NULL,
  `resMaeAleloA` double DEFAULT '0',
  `resMaeAleloB` double DEFAULT '0',
  `resFilhoAleloA` double DEFAULT '0',
  `resFilhoAleloB` double DEFAULT '0',
  `resPaiAleloA` double DEFAULT '0',
  `resPaiAleloB` double DEFAULT '0',
  `resIP` double DEFAULT '0',
  `resIPC` double DEFAULT '0',
  `resIdUsrIncluiu` int(10) unsigned DEFAULT NULL,
  `resIdUsrAlterou` int(10) unsigned DEFAULT NULL,
  `resTpEntrada` enum('manual','automatizada') DEFAULT 'manual',
  PRIMARY KEY (`resid`),
  KEY `locID` (`locID`),
  KEY `dosID` (`dosID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `amb_exame`
--
DROP TABLE IF EXISTS `amb_exame`;
CREATE TABLE `amb_exame` (
  `CodAmb` int(11) NOT NULL DEFAULT '0',
  `Exame` varchar(100) DEFAULT NULL,
  `idExame` varchar(10) DEFAULT NULL,
  `Porte` varchar(5) DEFAULT NULL,
  `PercPorte` decimal(3,2) DEFAULT NULL,
  `CustoOper` decimal(9,3) DEFAULT NULL,
  PRIMARY KEY (`CodAmb`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `ctrlreag`
--
DROP TABLE IF EXISTS `ctrlreag`;
CREATE TABLE `ctrlreag` (
  `rgSid` int(11) NOT NULL AUTO_INCREMENT,
  `rgDescr` varchar(45) DEFAULT NULL,
  `rgVolume` varchar(7) DEFAULT NULL,
  `rgConcentracao` varchar(45) DEFAULT NULL,
  `rgArmazena1` varchar(10) DEFAULT NULL,
  `rgArmazena2` varchar(10) DEFAULT NULL,
  `rgDataPrep` datetime DEFAULT NULL,
  `rgDataVenc` date DEFAULT NULL,
  `rgTecnico` varchar(20) DEFAULT NULL,
  `rgQtde` int(11) DEFAULT NULL,
  `rgImp` char(1) DEFAULT NULL,
  `rgSetor` varchar(10) DEFAULT NULL,
  `rgAplicacao` text,
  `rgLote` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`rgSid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `setores`
--
DROP TABLE IF EXISTS `setores`;
CREATE TABLE `setores` (
  `SETCODIGO` char(3) DEFAULT NULL,
  `SETNOME` varchar(40) DEFAULT NULL,
  `setsid` int(11) NOT NULL AUTO_INCREMENT,
  `SETEMAIL` varchar(40) DEFAULT NULL,
  `SETRAMAL` varchar(16) DEFAULT NULL,
  `setGestor` int(11) DEFAULT '0',
  `setNvlPai` char(3) NOT NULL DEFAULT '',
  `setSequencia` int(10) unsigned DEFAULT NULL,
  `setposicao` int(6) DEFAULT '0',
  `SetGrupoListaContatos` tinytext,
  `setCoGestor1` smallint(6) DEFAULT NULL,
  `setCoGestor2` smallint(6) DEFAULT NULL,
  `setAcessor` smallint(6) DEFAULT NULL,
  `setRedigitacaoUnica` char(1) NOT NULL DEFAULT 'N' COMMENT 'Indica se a 2a digitacao pode ser pela mesma pessoa (S=dig. tem que ser diferentes, N = tanto faz)',
  `setQtdColaborador` smallint(4) DEFAULT '0',
  `setTemPreResultado` char(1) DEFAULT 'N',
  `boTemposAgrupadosAOL` varchar(1) DEFAULT 'N',
  PRIMARY KEY (`setsid`),
  UNIQUE KEY `SETCODIGO_UNIQUE` (`SETCODIGO`),
  KEY `setCod` (`SETCODIGO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `regiao_celula`
--
DROP TABLE IF EXISTS `regiao_celula`;
CREATE TABLE `regiao_celula` (
  `idRegiao_Celula` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(45) DEFAULT NULL,
  `Estado` varchar(45) DEFAULT NULL,
  `Representante` varchar(45) DEFAULT NULL,
  `IdCelula` int(11) DEFAULT NULL,
  PRIMARY KEY (`idRegiao_Celula`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `stat_exame_historico_producao_parcial_27_vazio_script_antigo`
--
DROP TABLE IF EXISTS `stat_exame_historico_producao_parcial_27_vazio_script_antigo`;
CREATE TABLE `stat_exame_historico_producao_parcial_27_vazio_script_antigo` (
  `idstat_exame` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `area` char(1) DEFAULT NULL,
  `Entidade` int(10) unsigned DEFAULT NULL,
  `exame` varchar(10) DEFAULT NULL,
  `etapa` tinyint(4) DEFAULT NULL,
  `Data` varchar(8) DEFAULT NULL,
  `qtde` int(11) DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `idEmpresa` int(10) unsigned DEFAULT NULL,
  `idMedico` int(10) unsigned DEFAULT '0',
  `qtdePct` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`idstat_exame`),
  KEY `area` (`area`,`Entidade`,`exame`,`Data`),
  KEY `etapa` (`etapa`,`Data`),
  KEY `stat_exame_medico` (`area`,`idMedico`,`exame`,`Data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `vacina_agenda`
--
DROP TABLE IF EXISTS `vacina_agenda`;
CREATE TABLE `vacina_agenda` (
  `ageid` int(11) NOT NULL AUTO_INCREMENT,
  `ageVaId` int(11) NOT NULL,
  `ageFunc` int(11) NOT NULL,
  `ageData` date DEFAULT NULL,
  `ageAplicado` date DEFAULT NULL,
  `observacao` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ageid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `mvto_produto`
--
DROP TABLE IF EXISTS `mvto_produto`;
CREATE TABLE `mvto_produto` (
  `idMvtoProduto` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idProduto` int(10) unsigned NOT NULL DEFAULT '0',
  `DtMvto` datetime DEFAULT NULL,
  `idOrdemServico` int(10) unsigned NOT NULL DEFAULT '0',
  `TipoMvto` enum('Entrada','Saida') DEFAULT NULL,
  `NroNotaFiscal` char(10) DEFAULT NULL,
  `Fornecedor` char(18) DEFAULT NULL,
  `Preco` decimal(9,2) DEFAULT NULL,
  PRIMARY KEY (`idMvtoProduto`),
  KEY `idProduto` (`idProduto`,`TipoMvto`,`DtMvto`),
  KEY `idOrdemServico` (`idOrdemServico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Movimento de estoque de produtos aplicados na coleta';
--
-- Table structure for table `resultado_imagem`
--
DROP TABLE IF EXISTS `resultado_imagem`;
CREATE TABLE `resultado_imagem` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idOs` int(11) NOT NULL DEFAULT '0',
  `Imagem` mediumblob,
  `TipoImagem` enum('Pequena','Media','Grande') DEFAULT NULL,
  `Comentario` text,
  `Ordem` char(1) DEFAULT NULL,
  `Exame` varchar(10) DEFAULT NULL,
  `ControleInspecaoEnsaio` char(1) DEFAULT 'N',
  `Arquivo` mediumblob,
  PRIMARY KEY (`id`),
  KEY `resultado_imagem_solic` (`idOs`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `exame_inclusao`
--
DROP TABLE IF EXISTS `exame_inclusao`;
CREATE TABLE `exame_inclusao` (
  `idExameSolicitado` int(11) NOT NULL DEFAULT '0',
  `dtImpressao` datetime DEFAULT NULL,
  KEY `idSolicitacao` (`idExameSolicitado`),
  KEY `dtImpressao` (`dtImpressao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `destinossf`
--
DROP TABLE IF EXISTS `destinossf`;
CREATE TABLE `destinossf` (
  `idDestinosSF` int(11) NOT NULL AUTO_INCREMENT,
  `idExameLabor` varchar(45) NOT NULL,
  `idDestino` int(11) NOT NULL,
  PRIMARY KEY (`idDestinosSF`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `qc_bloqueioexame_dependencia`
--
DROP TABLE IF EXISTS `qc_bloqueioexame_dependencia`;
CREATE TABLE `qc_bloqueioexame_dependencia` (
  `exacodigo` varchar(15) NOT NULL DEFAULT '',
  `exadependencia` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`exacodigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `treina_curso`
--
DROP TABLE IF EXISTS `treina_curso`;
CREATE TABLE `treina_curso` (
  `treId` int(11) NOT NULL AUTO_INCREMENT,
  `treInstrutor` varchar(40) DEFAULT '',
  `treDtIni` datetime DEFAULT NULL,
  `treDtFin` datetime DEFAULT NULL,
  `treMaterialDidatico` varchar(60) DEFAULT NULL,
  `treMaterialApoio` varchar(60) DEFAULT NULL,
  `treLocal` varchar(40) DEFAULT NULL,
  `treCurso` varchar(60) DEFAULT NULL,
  `treIdInstrutor` smallint(6) unsigned DEFAULT NULL,
  `treCarga` varchar(6) DEFAULT NULL,
  `treObjetivo` varchar(60) DEFAULT NULL,
  `treMinutosTreinados` smallint(6) unsigned DEFAULT '0',
  `treNovaAtividade` char(1) DEFAULT 'N',
  `treAtividade` text,
  PRIMARY KEY (`treId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Treinamento dos funcionarios - cursos';
--
-- Table structure for table `critica_exame`
--
DROP TABLE IF EXISTS `critica_exame`;
CREATE TABLE `critica_exame` (
  `idCritica` int(10) unsigned NOT NULL,
  `exameInterface` varchar(10) NOT NULL DEFAULT '',
  `idMaterial` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idCritica`,`exameInterface`,`idMaterial`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `loglaudotemplate`
--
DROP TABLE IF EXISTS `loglaudotemplate`;
CREATE TABLE `loglaudotemplate` (
  `idlog` bigint(20) NOT NULL AUTO_INCREMENT,
  `exaCodigo` varchar(10) NOT NULL,
  `cdtemplateanterior` varchar(255) NOT NULL,
  `nuversaoanterior` int(10) NOT NULL,
  `flreenvioosanterior` int(10) NOT NULL,
  `cdtemplateatual` varchar(255) NOT NULL,
  `nuversaoatual` int(10) NOT NULL,
  `flreenvioosatual` int(10) NOT NULL,
  `cdusuario` text,
  `nuip` varchar(15) DEFAULT NULL,
  `dtDataHora` datetime DEFAULT NULL,
  PRIMARY KEY (`idlog`),
  KEY `idx_loglaudotemplate_exacodigo` (`exaCodigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `interfacefullalteracao`
--
DROP TABLE IF EXISTS `interfacefullalteracao`;
CREATE TABLE `interfacefullalteracao` (
  `idinterfaceFullAlteracao` int(11) NOT NULL AUTO_INCREMENT,
  `intAltSolsid` int(11) NOT NULL,
  `intAltEnviado` varchar(1) DEFAULT 'N',
  PRIMARY KEY (`idinterfaceFullAlteracao`),
  KEY `index2` (`intAltSolsid`),
  KEY `index3` (`intAltEnviado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `logistica_pedidoentidadeitem`
--
DROP TABLE IF EXISTS `logistica_pedidoentidadeitem`;
CREATE TABLE `logistica_pedidoentidadeitem` (
  `idItemPedido` int(11) NOT NULL AUTO_INCREMENT,
  `idPedido` int(11) NOT NULL,
  `idMaterial` int(11) NOT NULL,
  `qtdSolicitada` int(11) DEFAULT '1',
  PRIMARY KEY (`idItemPedido`,`idPedido`),
  KEY `idPedido` (`idPedido`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `critica_digitacao`
--
DROP TABLE IF EXISTS `critica_digitacao`;
CREATE TABLE `critica_digitacao` (
  `idCritica` int(10) unsigned NOT NULL DEFAULT '0',
  `Unidade` varchar(15) DEFAULT NULL,
  `Mensagem` varchar(200) DEFAULT NULL,
  `VlrInferior` int(10) unsigned DEFAULT '0',
  `VlrSuperior` int(10) unsigned DEFAULT '99999999',
  PRIMARY KEY (`idCritica`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `dna_locos`
--
DROP TABLE IF EXISTS `dna_locos`;
CREATE TABLE `dna_locos` (
  `locId` int(11) NOT NULL AUTO_INCREMENT,
  `locOrdEquipamento` int(11) NOT NULL DEFAULT '0',
  `locNome` varchar(15) DEFAULT NULL,
  `locDescricao` varchar(250) NOT NULL DEFAULT '',
  `locDescricaoEspanhol` varchar(250) NOT NULL,
  PRIMARY KEY (`locId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC COMMENT='InnoDB free: 22528 kB';
--
-- Table structure for table `pvt_aliquota`
--
DROP TABLE IF EXISTS `pvt_aliquota`;
CREATE TABLE `pvt_aliquota` (
  `idSample` char(8) NOT NULL,
  `sequencia` tinyint(4) NOT NULL,
  `destino` char(20) DEFAULT NULL,
  `exames` varchar(60) DEFAULT NULL,
  `identificador` varchar(30) DEFAULT NULL,
  `dataHora` datetime DEFAULT NULL,
  `tipoTubo` enum('Primario','Aliquota') DEFAULT NULL,
  PRIMARY KEY (`idSample`,`sequencia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `notas_fiscais_sap_parcelas`
--
DROP TABLE IF EXISTS `notas_fiscais_sap_parcelas`;
CREATE TABLE `notas_fiscais_sap_parcelas` (
  `NotNroRPS` int(11) NOT NULL,
  `notnotaEMS` bigint(20) NOT NULL,
  `notDtVcto` date NOT NULL,
  `notsid` varchar(45) NOT NULL,
  PRIMARY KEY (`NotNroRPS`,`notnotaEMS`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `informe_capa_laudo`
--
DROP TABLE IF EXISTS `informe_capa_laudo`;
CREATE TABLE `informe_capa_laudo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `DataCriacao` datetime DEFAULT NULL,
  `DataValidade` datetime DEFAULT NULL,
  `Texto` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `resultados`
--
DROP TABLE IF EXISTS `resultados`;
CREATE TABLE `resultados` (
  `ressid` int(11) NOT NULL AUTO_INCREMENT,
  `resresultado` text,
  `resimagem` mediumblob,
  `RESSIDSOLIC` int(11) DEFAULT NULL,
  `RESSIDCRITICA` int(11) DEFAULT NULL,
  `RESSIDORDEM` int(11) DEFAULT NULL,
  `RESDESCRICAO` text,
  `resItalico` char(1) DEFAULT '',
  `resNormal` char(1) DEFAULT NULL,
  `residamostra` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`ressid`),
  KEY `AK1Resultados` (`RESSIDSOLIC`,`RESSIDCRITICA`),
  KEY `AK2Resultados` (`RESSIDORDEM`,`RESSIDCRITICA`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `stat_exame_historico`
--
DROP TABLE IF EXISTS `stat_exame_historico`;
CREATE TABLE `stat_exame_historico` (
  `idstat_exame` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `area` char(1) DEFAULT NULL,
  `Entidade` int(10) unsigned DEFAULT NULL,
  `exame` varchar(10) DEFAULT NULL,
  `etapa` tinyint(4) DEFAULT NULL,
  `Data` varchar(8) DEFAULT NULL,
  `qtde` int(11) DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `idEmpresa` int(10) unsigned DEFAULT NULL,
  `idMedico` int(10) unsigned DEFAULT '0',
  `qtdePct` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`idstat_exame`),
  KEY `area` (`area`,`Entidade`,`exame`,`Data`),
  KEY `etapa` (`etapa`,`Data`),
  KEY `stat_exame_medico` (`area`,`idMedico`,`exame`,`Data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `acondicionamento`
--
DROP TABLE IF EXISTS `acondicionamento`;
CREATE TABLE `acondicionamento` (
  `acoid` int(11) NOT NULL AUTO_INCREMENT,
  `acodescricao` char(255) DEFAULT NULL,
  `acotemperaturamax` decimal(16,2) DEFAULT NULL,
  `acotemperaturamin` decimal(16,2) DEFAULT NULL,
  `acoprioridade` tinyint(1) DEFAULT NULL,
  `acocontainerunico` tinyint(1) NOT NULL DEFAULT '0',
  `acoativo` tinyint(1) NOT NULL DEFAULT '1',
  `acopadrao` tinyint(1) NOT NULL DEFAULT '0',
  `acoTipoAcondicionamento` char(1) DEFAULT NULL,
  PRIMARY KEY (`acoid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Acondicionamentos (regrigerado, congelado, etc) para o Exame';
--
-- Table structure for table `equipamento_eventoproducao`
--
DROP TABLE IF EXISTS `equipamento_eventoproducao`;
CREATE TABLE `equipamento_eventoproducao` (
  `idevento` int(11) NOT NULL AUTO_INCREMENT,
  `idequipamento` int(11) NOT NULL,
  `evento` enum('calibração','reposição de reagente','manutenção') DEFAULT NULL,
  `dthora` datetime DEFAULT NULL,
  PRIMARY KEY (`idevento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `reagente_lote`
--
DROP TABLE IF EXISTS `reagente_lote`;
CREATE TABLE `reagente_lote` (
  `reagente_idReagente` int(11) NOT NULL DEFAULT '0',
  `Lote` varchar(15) NOT NULL DEFAULT '0',
  `Volume` varchar(50) DEFAULT NULL,
  `Qtde` int(11) DEFAULT '1',
  `Concentracao` varchar(50) DEFAULT NULL,
  `Embalagem` varchar(20) DEFAULT NULL,
  `DtPreparo` date DEFAULT NULL,
  `DtVcto` date DEFAULT NULL,
  `idQuemFez` int(11) DEFAULT NULL,
  `Setor` char(3) DEFAULT NULL,
  `Componentes` text,
  `nivel` char(4) NOT NULL DEFAULT '',
  `dtCriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`reagente_idReagente`,`Lote`,`nivel`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `representantes`
--
DROP TABLE IF EXISTS `representantes`;
CREATE TABLE `representantes` (
  `repsid` int(11) NOT NULL AUTO_INCREMENT,
  `REPNOME` varchar(40) DEFAULT NULL,
  `REPEMPRESA` varchar(40) DEFAULT NULL,
  `REPENDER` varchar(40) DEFAULT NULL,
  `REPBAIRRO` varchar(20) DEFAULT NULL,
  `REPCIDADE` varchar(20) DEFAULT NULL,
  `REPCEP` int(11) DEFAULT NULL,
  `REPCPF` varchar(14) DEFAULT NULL,
  `REPCGC` varchar(18) DEFAULT NULL,
  `REPCOMISSAO` float DEFAULT NULL,
  `REPABRANGENCIA` varchar(50) DEFAULT NULL,
  `REPOBS` varchar(50) DEFAULT NULL,
  `REPESTADO` char(2) DEFAULT NULL,
  `repEmail` varchar(50) DEFAULT NULL,
  `repIdentif` varchar(15) DEFAULT NULL,
  `repSenha` varchar(15) DEFAULT NULL,
  `repDtNasc` date DEFAULT NULL,
  `repFone` varchar(20) DEFAULT NULL,
  `repFax` varchar(20) DEFAULT NULL,
  `repCelular` varchar(20) DEFAULT NULL,
  `repLimite` varchar(60) DEFAULT NULL,
  `repRg` varchar(15) DEFAULT NULL,
  `repDesignacao` varchar(40) DEFAULT NULL,
  `repMeta` int(11) DEFAULT '0',
  `repSeqCxEtiqueta` int(10) unsigned DEFAULT '0',
  `repRegiao` varchar(30) DEFAULT NULL,
  `repAbreviacaoEMS` varchar(12) DEFAULT NULL,
  `repSupervisor` char(1) NOT NULL DEFAULT 'N',
  `repCentroCusto` varchar(45) DEFAULT NULL,
  `repNomeConsultor` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`repsid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `faixapanico`
--
DROP TABLE IF EXISTS `faixapanico`;
CREATE TABLE `faixapanico` (
  `idfaixapanico` int(11) NOT NULL AUTO_INCREMENT,
  `idExame` varchar(50) NOT NULL,
  `vlrMenor` decimal(7,4) NOT NULL,
  `vlrMaior` decimal(7,4) NOT NULL,
  `idadeInicial` int(11) NOT NULL,
  `idadeFinal` int(11) NOT NULL,
  `ativo` tinyint(1) NOT NULL,
  `idRefSid` int(11) NOT NULL,
  PRIMARY KEY (`idfaixapanico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `mensagem_email`
--
DROP TABLE IF EXISTS `mensagem_email`;
CREATE TABLE `mensagem_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iddestino` int(11) NOT NULL,
  `idEmail` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `layout_notafiscal`
--
DROP TABLE IF EXISTS `layout_notafiscal`;
CREATE TABLE `layout_notafiscal` (
  `idEmpguia` int(11) NOT NULL,
  `secretaria_financas` varchar(100) DEFAULT NULL,
  `logo_prefeitura` blob,
  `prefeitura_cidade` varchar(30) DEFAULT NULL,
  `logo_nto` blob,
  `nome_marca` varchar(30) DEFAULT NULL,
  `endereco` varchar(100) DEFAULT NULL,
  `InscMunicipal_cnpj` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idEmpguia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `entidade_cobranca`
--
DROP TABLE IF EXISTS `entidade_cobranca`;
CREATE TABLE `entidade_cobranca` (
  `idEntidade` int(11) NOT NULL,
  `data` datetime NOT NULL,
  `idFunc` smallint(6) DEFAULT NULL,
  `descricao` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idEntidade`,`data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Infomarcoes sobre cobranca';
--
-- Table structure for table `locais`
--
DROP TABLE IF EXISTS `locais`;
CREATE TABLE `locais` (
  `LCCODIGO` varchar(5) DEFAULT NULL,
  `LCDESCRICAO` varchar(40) DEFAULT NULL,
  `lcsid` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`lcsid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `log_os_sem_entrada`
--
DROP TABLE IF EXISTS `log_os_sem_entrada`;
CREATE TABLE `log_os_sem_entrada` (
  `seq` int(11) NOT NULL AUTO_INCREMENT,
  `idOrdemServico` int(11) NOT NULL COMMENT 'Id da OS -> aol.de_para_os_dasa.idOrdemServico',
  `idOsDasa` varchar(20) NOT NULL COMMENT 'FAP , id do Motion -> aol.de_para_os_dasa.isOsDasa',
  `data` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'Data em que o resultado do exame saiu, mas ele não deu entrada',
  PRIMARY KEY (`seq`),
  KEY `data` (`data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela para gerenciar exames que já tem resultados mas ainda não deram entrada no labor';
--
-- Table structure for table `tabela_convenio`
--
DROP TABLE IF EXISTS `tabela_convenio`;
CREATE TABLE `tabela_convenio` (
  `idtabela` int(11) NOT NULL AUTO_INCREMENT,
  `dsctabela` varchar(100) DEFAULT NULL,
  `cdgtabela` char(2) DEFAULT NULL,
  PRIMARY KEY (`idtabela`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `repeticao_exames`
--
DROP TABLE IF EXISTS `repeticao_exames`;
CREATE TABLE `repeticao_exames` (
  `rexsolsid` int(10) unsigned NOT NULL DEFAULT '0',
  `rexDataResultado` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `rexResultadoAtual` text,
  `rexQuemDigitou` int(10) unsigned NOT NULL DEFAULT '0',
  `rexDataPrevisao` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`rexsolsid`,`rexDataResultado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `rep_veiculo`
--
DROP TABLE IF EXISTS `rep_veiculo`;
CREATE TABLE `rep_veiculo` (
  `Placas` varchar(8) NOT NULL DEFAULT '',
  `Descricao` varchar(50) DEFAULT NULL,
  `Tipo` enum('Carro','Moto') DEFAULT NULL,
  `Propriedade` varchar(40) DEFAULT NULL,
  `Chassi` varchar(30) DEFAULT NULL,
  `Seguro` varchar(255) DEFAULT NULL,
  `idRepresentante` int(11) NOT NULL DEFAULT '0',
  `VctoSeguro` date DEFAULT NULL,
  PRIMARY KEY (`Placas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `md_atividade`
--
DROP TABLE IF EXISTS `md_atividade`;
CREATE TABLE `md_atividade` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Descricao` char(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `justificativa_sem_onus`
--
DROP TABLE IF EXISTS `justificativa_sem_onus`;
CREATE TABLE `justificativa_sem_onus` (
  `idOs` int(11) NOT NULL DEFAULT '0',
  `QuemAutorizou` varchar(30) DEFAULT NULL,
  `Motivo` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idOs`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `tabelaamb`
--
DROP TABLE IF EXISTS `tabelaamb`;
CREATE TABLE `tabelaamb` (
  `ambsid` int(11) NOT NULL AUTO_INCREMENT,
  `AMBANO` int(11) DEFAULT NULL,
  `AMBCODIGO` varchar(12) DEFAULT NULL,
  `ambExame` varchar(10) DEFAULT NULL,
  `AMBPCOCH` double DEFAULT NULL,
  `AMBPCOREAL` float DEFAULT NULL,
  `ambNovoCodigo` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`ambsid`),
  KEY `AMBANO` (`AMBANO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `log_patrimonio`
--
DROP TABLE IF EXISTS `log_patrimonio`;
CREATE TABLE `log_patrimonio` (
  `idLogPatrimonio` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ConteudoAnterior` text,
  `Quem` int(11) DEFAULT NULL,
  `Data` datetime DEFAULT NULL,
  `Operacao` enum('Inserção','Edição','Exclusão') DEFAULT NULL,
  `idPatrimonio` int(11) DEFAULT NULL,
  PRIMARY KEY (`idLogPatrimonio`),
  KEY `idPatrimonio` (`idPatrimonio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela para registrar o log do patrimonio';
--
-- Table structure for table `direitos`
--
DROP TABLE IF EXISTS `direitos`;
CREATE TABLE `direitos` (
  `DIRFUNC` double NOT NULL DEFAULT '0',
  `DIROBJETO` varchar(30) NOT NULL DEFAULT '',
  `DIRINCLUSAO` char(1) DEFAULT NULL,
  `DIRTIPO` char(1) NOT NULL DEFAULT '',
  `DIRALTERACAO` char(1) DEFAULT NULL,
  `DIREXCLUSAO` char(1) DEFAULT NULL,
  `DIRCONSULTA` char(1) DEFAULT NULL,
  `DIREXECUCAO` char(1) DEFAULT NULL,
  PRIMARY KEY (`DIRFUNC`,`DIROBJETO`,`DIRTIPO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `resultantib`
--
DROP TABLE IF EXISTS `resultantib`;
CREATE TABLE `resultantib` (
  `ranid` int(11) NOT NULL AUTO_INCREMENT,
  `RANIDANTIBIOTICO` int(11) DEFAULT NULL,
  `RANIDSOLIC` int(11) DEFAULT NULL,
  `RANHALO` int(11) DEFAULT NULL,
  `ranResistencia` char(4) DEFAULT NULL,
  `ranMic` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`ranid`),
  KEY `RANIDSOLIC` (`RANIDSOLIC`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `calibracao`
--
DROP TABLE IF EXISTS `calibracao`;
CREATE TABLE `calibracao` (
  `idCalibracao` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idEquipamento` int(10) unsigned DEFAULT NULL,
  `idCalibracaoFaixa` int(10) unsigned DEFAULT NULL,
  `dtCalibracao` datetime DEFAULT NULL,
  `idFunc` smallint(6) unsigned DEFAULT NULL,
  `grandezaUsada` decimal(9,4) DEFAULT NULL,
  `erroTotal` decimal(9,4) DEFAULT NULL,
  `media` decimal(9,4) DEFAULT NULL,
  `dtAprovacao` datetime DEFAULT NULL,
  `idFuncAprovacao` int(11) DEFAULT NULL,
  `Observacao` text,
  `idPadrao` smallint(6) unsigned DEFAULT '0',
  PRIMARY KEY (`idCalibracao`),
  KEY `equipamento` (`idEquipamento`,`idCalibracaoFaixa`,`dtCalibracao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC COMMENT='informacao sobre cada calibracao';
--
-- Table structure for table `entidade_relacao_config`
--
DROP TABLE IF EXISTS `entidade_relacao_config`;
CREATE TABLE `entidade_relacao_config` (
  `identidade_relacao_config` int(11) NOT NULL AUTO_INCREMENT,
  `id_entidade` int(11) DEFAULT NULL,
  `id_config` int(11) DEFAULT NULL,
  `flag` char(1) DEFAULT 'N',
  PRIMARY KEY (`identidade_relacao_config`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `apoio_formato_transferencia`
--
DROP TABLE IF EXISTS `apoio_formato_transferencia`;
CREATE TABLE `apoio_formato_transferencia` (
  `idLabApoio` int(11) NOT NULL DEFAULT '0',
  `ScriptGeracao` mediumtext,
  `ScriptRecepcao` mediumtext,
  `ParametrosComunicacao` text,
  PRIMARY KEY (`idLabApoio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `logistica_transportadora`
--
DROP TABLE IF EXISTS `logistica_transportadora`;
CREATE TABLE `logistica_transportadora` (
  `IdTransportadora` int(11) NOT NULL AUTO_INCREMENT,
  `NomeFantasia` varchar(100) DEFAULT NULL,
  `RazaoSocial` varchar(100) DEFAULT NULL,
  `telefone` char(10) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `contato` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`IdTransportadora`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `paciente_doenca`
--
DROP TABLE IF EXISTS `paciente_doenca`;
CREATE TABLE `paciente_doenca` (
  `idPaciente` int(10) unsigned NOT NULL,
  `idDoenca` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idPaciente`,`idDoenca`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `criticasequenciaparasitologia`
--
DROP TABLE IF EXISTS `criticasequenciaparasitologia`;
CREATE TABLE `criticasequenciaparasitologia` (
  `idCriticaPrincipal` int(11) NOT NULL,
  `idCriticaAuxiliar` int(11) NOT NULL,
  `sequenciaResultado` int(11) NOT NULL,
  PRIMARY KEY (`idCriticaPrincipal`,`idCriticaAuxiliar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `equipamento_periferico`
--
DROP TABLE IF EXISTS `equipamento_periferico`;
CREATE TABLE `equipamento_periferico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idEquipamento` int(11) DEFAULT NULL,
  `descricao` varchar(100) DEFAULT NULL,
  `numSerie` varchar(20) DEFAULT NULL,
  `status` enum('Normal','Danificado') DEFAULT 'Normal',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `refvalagrupamento`
--
DROP TABLE IF EXISTS `refvalagrupamento`;
CREATE TABLE `refvalagrupamento` (
  `idGrupo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Media` decimal(10,2) DEFAULT '0.00',
  `Mediana` decimal(10,2) DEFAULT '0.00',
  `Quartil1` decimal(10,2) DEFAULT '0.00',
  `Quartil3` decimal(10,2) DEFAULT '0.00',
  `QtdAmostras` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`idGrupo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `equivalencia`
--
DROP TABLE IF EXISTS `equivalencia`;
CREATE TABLE `equivalencia` (
  `Tabela` varchar(20) NOT NULL DEFAULT '',
  `Campo` varchar(20) NOT NULL DEFAULT '',
  `Cod_Original` varchar(15) NOT NULL DEFAULT '',
  `Cod_Substituto` varchar(15) DEFAULT NULL,
  `Descricao` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Tabela`,`Campo`,`Cod_Original`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `antibiograma`
--
DROP TABLE IF EXISTS `antibiograma`;
CREATE TABLE `antibiograma` (
  `ANTGRUPO` varchar(5) DEFAULT NULL,
  `ANTNOME` varchar(40) DEFAULT NULL,
  `ANTVLRINFERIOR` smallint(6) DEFAULT NULL,
  `ANTVLRSUPERIOR` smallint(6) DEFAULT NULL,
  `antsid` int(11) NOT NULL AUTO_INCREMENT,
  `antHalo` smallint(6) DEFAULT NULL,
  `antCodigo` char(8) DEFAULT NULL,
  `exporta` char(1) DEFAULT 'S',
  PRIMARY KEY (`antsid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `dna_responsavel`
--
DROP TABLE IF EXISTS `dna_responsavel`;
CREATE TABLE `dna_responsavel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(100) NOT NULL DEFAULT '',
  `Titulacao` varchar(100) NOT NULL DEFAULT '',
  `Documento` varchar(100) NOT NULL DEFAULT '',
  `Assinatura` blob,
  `TipoResponsabilidade` enum('Geral','Interpretação') NOT NULL DEFAULT 'Interpretação',
  `NomeSubstituto` varchar(100) NOT NULL DEFAULT '',
  `TitulacaoSubstituto` varchar(100) DEFAULT NULL,
  `DocumentoSubstituto` varchar(100) NOT NULL DEFAULT '',
  `AssinaturaSubstituto` blob,
  `Disponivel` enum('Sim','Não') NOT NULL DEFAULT 'Sim',
  `Rubrica` blob,
  `TipoAssinatura` enum('Punho','Auto') NOT NULL DEFAULT 'Auto',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `faixa_temperatura`
--
DROP TABLE IF EXISTS `faixa_temperatura`;
CREATE TABLE `faixa_temperatura` (
  `idfaixa_temperatura` int(11) NOT NULL AUTO_INCREMENT,
  `Descricao` varchar(45) DEFAULT NULL,
  `Minima` varchar(45) DEFAULT NULL,
  `Maxima` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idfaixa_temperatura`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `refvalperfil`
--
DROP TABLE IF EXISTS `refvalperfil`;
CREATE TABLE `refvalperfil` (
  `idPerfil` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Descricao` varchar(200) DEFAULT NULL,
  `idExame` varchar(10) DEFAULT NULL,
  `idExameComposto` varchar(100) DEFAULT '',
  `FxRst1` decimal(10,2) DEFAULT NULL,
  `FxRst2` decimal(10,2) DEFAULT NULL,
  `Sexo` enum('M','F','A') DEFAULT 'A',
  `FxEtariaInicial` int(10) unsigned DEFAULT NULL,
  `FxEtariaFinal` int(10) unsigned DEFAULT NULL,
  `FxEtariaOperador` enum('<=','>=','><') DEFAULT '>=',
  `ProcessaAutomatico` enum('Sim','Não') NOT NULL DEFAULT 'Não',
  `grupo` set('H','I','L','P') DEFAULT 'H,I,L,P',
  PRIMARY KEY (`idPerfil`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `telmovimento`
--
DROP TABLE IF EXISTS `telmovimento`;
CREATE TABLE `telmovimento` (
  `TelMovSid` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `TelMovSol` int(4) unsigned DEFAULT NULL,
  `TelMovDest` int(1) unsigned DEFAULT NULL,
  `TelMovAtend` varchar(40) DEFAULT NULL,
  `TelMovDate` datetime DEFAULT NULL,
  `TelMovDscSol` varchar(60) DEFAULT NULL,
  `TelMovDscDest` varchar(60) DEFAULT NULL,
  `TelMovCidade` varchar(60) DEFAULT NULL,
  `TelMovAssunto` varchar(60) DEFAULT NULL,
  `TelMovIdnRecSol` char(1) DEFAULT NULL,
  `TelMovLabGer` char(1) DEFAULT NULL,
  KEY `TelMovSid` (`TelMovSid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela de Movimentos de ligacoes telefonicas';
--
-- Table structure for table `valorref_dados`
--
DROP TABLE IF EXISTS `valorref_dados`;
CREATE TABLE `valorref_dados` (
  `idValorReferencia_dados` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idValorReferencia` int(10) unsigned NOT NULL DEFAULT '0',
  `Ordem` int(10) unsigned NOT NULL DEFAULT '0',
  `Sexo` char(1) DEFAULT NULL,
  `IdadeInicial` int(10) unsigned NOT NULL DEFAULT '0',
  `IdadeFinal` int(10) unsigned NOT NULL DEFAULT '0',
  `Descricao` char(50) DEFAULT NULL,
  `LimInferior` float DEFAULT NULL,
  `LimSuperior` float DEFAULT NULL,
  `LimInferiorPerigoso` float DEFAULT NULL,
  `LimSuperiorPerigoso` float DEFAULT NULL,
  `ObsAutoLimInferior` int(10) unsigned DEFAULT NULL,
  `ObsAutoLimSuperior` int(10) unsigned DEFAULT NULL,
  `ObsAutoLimInferiorPerigoso` int(10) unsigned DEFAULT NULL,
  `ObsAutoLimSuperiorPerigoso` int(10) unsigned DEFAULT NULL,
  `ValorRefAlfa` char(40) DEFAULT NULL,
  `Atributo` char(4) DEFAULT NULL,
  PRIMARY KEY (`idValorReferencia_dados`),
  KEY `idValorReferencia` (`idValorReferencia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `deparapaternidadeparticipanteexame`
--
DROP TABLE IF EXISTS `deparapaternidadeparticipanteexame`;
CREATE TABLE `deparapaternidadeparticipanteexame` (
  `IdExame` int(11) NOT NULL COMMENT 'Id do Exame no Labor',
  `IdCompostoMotion` int(11) NOT NULL COMMENT 'Id do Composto Motion',
  `IdTipoParticipante` int(11) NOT NULL DEFAULT '0' COMMENT 'Id do Tipo de Participante\nEx: 1 - Suposto Pai, 2 - Suposto Filho.',
  `IdTesteMotion` int(11) NOT NULL COMMENT 'Id do Teste Motion',
  `IdMaterialDasa` int(11) NOT NULL COMMENT 'Id do Material',
  `IdRecipienteDasa` int(11) NOT NULL COMMENT 'Id do Recipiente',
  PRIMARY KEY (`IdExame`,`IdCompostoMotion`,`IdTipoParticipante`,`IdTesteMotion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Configuração dos Testes Simples de cada participante inclu';
--
-- Table structure for table `exame_frase_padronizada`
--
DROP TABLE IF EXISTS `exame_frase_padronizada`;
CREATE TABLE `exame_frase_padronizada` (
  `idExame` char(10) NOT NULL,
  `operacao` char(2) NOT NULL,
  `valor` char(15) NOT NULL,
  `frase` tinytext NOT NULL,
  PRIMARY KEY (`idExame`,`operacao`,`valor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `log_exclusao_os`
--
DROP TABLE IF EXISTS `log_exclusao_os`;
CREATE TABLE `log_exclusao_os` (
  `idLogOsExc` int(11) NOT NULL AUTO_INCREMENT,
  `os` int(11) NOT NULL,
  `idMotivo` int(11) NOT NULL,
  `exames` text NOT NULL,
  `obs` varchar(255) DEFAULT NULL,
  `idEnt` int(11) NOT NULL,
  `idPct` int(11) NOT NULL,
  `dtExc` datetime NOT NULL,
  `idFunc` int(11) NOT NULL,
  PRIMARY KEY (`idLogOsExc`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `resultado_assinatura`
--
DROP TABLE IF EXISTS `resultado_assinatura`;
CREATE TABLE `resultado_assinatura` (
  `idExameSolicitado` int(10) unsigned NOT NULL,
  `crmMedico` varchar(12) NOT NULL,
  PRIMARY KEY (`idExameSolicitado`,`crmMedico`),
  KEY `resultado_assinatura_idexamesolicitado_idx` (`idExameSolicitado`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `entidade_imagem`
--
DROP TABLE IF EXISTS `entidade_imagem`;
CREATE TABLE `entidade_imagem` (
  `identidade` int(11) NOT NULL DEFAULT '0',
  `imagem` longblob,
  PRIMARY KEY (`identidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `exames_solicitados`
--
DROP TABLE IF EXISTS `exames_solicitados`;
CREATE TABLE `exames_solicitados` (
  `solsid` int(11) NOT NULL AUTO_INCREMENT,
  `SOLSIDORDEM` int(11) DEFAULT NULL,
  `solExame` varchar(10) DEFAULT NULL,
  `solControleAol` char(1) DEFAULT 'N',
  `solVlrExame` decimal(11,2) DEFAULT '0.00',
  `SOLFORMAEXECUCAO` char(1) DEFAULT NULL,
  `SolObsResultado` text,
  `soldttransmissao` datetime DEFAULT NULL,
  `soldtimpressao` datetime DEFAULT NULL,
  `SOLLIBERADO` char(1) DEFAULT NULL,
  `SOLQUEMLIBEROU` smallint(6) DEFAULT NULL,
  `soldtliberacao` datetime DEFAULT NULL,
  `SOLQUEMDIGITOU` smallint(6) DEFAULT NULL,
  `soldtdigitacao` datetime DEFAULT NULL,
  `SOLSETOR` char(3) DEFAULT NULL,
  `SOLBOLETOIMPRESSO` char(1) DEFAULT NULL,
  `SOLMATERIAL` int(11) DEFAULT NULL,
  `SOLESTANTE` varchar(10) DEFAULT NULL,
  `solMarcado` char(1) DEFAULT '',
  `solCodExComposto` varchar(10) DEFAULT NULL,
  `solInterfaceado` char(1) DEFAULT 'N',
  `SolDtGeradoInternet` datetime DEFAULT NULL,
  `SolDestGeradoInternet` varchar(5) DEFAULT NULL,
  `solBioqLiberou` int(11) DEFAULT NULL,
  `solDtEntr` datetime DEFAULT NULL,
  `solQuemCadastrou` smallint(6) unsigned DEFAULT '0',
  `solLimbo` char(1) DEFAULT 'N',
  `solCriterioLiberacao` text,
  `solQtdAmostras` smallint(6) DEFAULT '1',
  `solDtRedigitacao` datetime DEFAULT NULL,
  `solMotivoRedigitacao` tinytext,
  `solQuemRedigitou` smallint(6) DEFAULT NULL,
  `solDtLidoInternetPct` datetime DEFAULT NULL,
  `solMedico` int(10) unsigned DEFAULT NULL,
  `solEntidade` int(10) unsigned DEFAULT NULL,
  `solConvenio` int(10) unsigned DEFAULT NULL,
  `solvalordesconto` decimal(9,2) DEFAULT '0.00',
  `SolNroFatura` int(10) unsigned DEFAULT NULL,
  `solDtLidoInternetMed` datetime DEFAULT NULL,
  `solTuboUnico` char(1) DEFAULT 'N',
  `solPoucaAmostra` char(1) DEFAULT 'N',
  PRIMARY KEY (`solsid`),
  KEY `solMedico` (`solMedico`,`solDtEntr`),
  KEY `solConvenio` (`solConvenio`,`solDtEntr`),
  KEY `soldtdigitacao` (`soldtdigitacao`),
  KEY `SOLSIDORDEM` (`SOLSIDORDEM`),
  KEY `solDtEntr` (`solDtEntr`),
  KEY `solEntidade` (`solEntidade`,`solDtEntr`),
  KEY `solExame` (`solExame`,`solDtEntr`),
  KEY `SOLLIBERADO` (`SOLLIBERADO`),
  KEY `soldtliberacao` (`soldtliberacao`),
  KEY `solQuemCadastrou` (`solQuemCadastrou`),
  KEY `solQuemRedigitou` (`solQuemRedigitou`),
  KEY `SOLQUEMDIGITOU` (`SOLQUEMDIGITOU`),
  KEY `SOLQUEMLIBEROU` (`SOLQUEMLIBEROU`),
  KEY `index01_exames_sol` (`SOLSIDORDEM`,`solExame`,`solEntidade`,`solDtEntr`),
  KEY `idx_exames_solicitados_solDtRedigitacao` (`solDtRedigitacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `calibracao_tipo`
--
DROP TABLE IF EXISTS `calibracao_tipo`;
CREATE TABLE `calibracao_tipo` (
  `idCalibracaoTipo` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` varchar(45) DEFAULT NULL,
  `unidade` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`idCalibracaoTipo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC COMMENT='Define o tipo de medicao, volumetrica, rotacao....';
--
-- Table structure for table `log_paciente`
--
DROP TABLE IF EXISTS `log_paciente`;
CREATE TABLE `log_paciente` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Data` datetime DEFAULT NULL,
  `Quem` int(10) unsigned DEFAULT NULL,
  `Operacao` varchar(10) DEFAULT NULL,
  `Texto` text,
  `idPaciente` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `log_paciente_pct` (`idPaciente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `ordem_servico_laudo_exame`
--
DROP TABLE IF EXISTS `ordem_servico_laudo_exame`;
CREATE TABLE `ordem_servico_laudo_exame` (
  `solldosid` int(11) NOT NULL,
  `solldoCodigoTemplate` varchar(255) NOT NULL,
  `solldoVersaoTemplate` int(11) DEFAULT NULL,
  `solldoStatus` varchar(30) NOT NULL,
  `solldoDataSincronizacao` datetime DEFAULT NULL,
  `solldoDetails` text,
  PRIMARY KEY (`solldosid`),
  UNIQUE KEY `unique_solldosid` (`solldosid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `tuss_tabela`
--
DROP TABLE IF EXISTS `tuss_tabela`;
CREATE TABLE `tuss_tabela` (
  `codigo` char(8) NOT NULL,
  `descricao` tinytext,
  KEY `codigo` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `exame_alicotado`
--
DROP TABLE IF EXISTS `exame_alicotado`;
CREATE TABLE `exame_alicotado` (
  `idExame` char(10) NOT NULL,
  `idUnidade` char(5) DEFAULT NULL,
  `idGaveta` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`idExame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `pm_medida`
--
DROP TABLE IF EXISTS `pm_medida`;
CREATE TABLE `pm_medida` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `idCategoria` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `descricao` char(35) NOT NULL DEFAULT '',
  `unidade` char(10) DEFAULT NULL,
  `valor` double unsigned DEFAULT NULL,
  `valorFinal` double unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `paciente_matricula`
--
DROP TABLE IF EXISTS `paciente_matricula`;
CREATE TABLE `paciente_matricula` (
  `idPaciente` int(10) unsigned NOT NULL DEFAULT '0',
  `idEntidade` int(10) unsigned NOT NULL DEFAULT '0',
  `Matricula` varchar(20) DEFAULT NULL,
  `nomePlano` varchar(40) DEFAULT 'Não Informado',
  `tipoPlano` varchar(40) DEFAULT 'Não Informado',
  `dtValidadeCarteira` date DEFAULT NULL,
  `dtValidadeCartaoConvenio` date DEFAULT NULL,
  PRIMARY KEY (`idPaciente`,`idEntidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `especialidades`
--
DROP TABLE IF EXISTS `especialidades`;
CREATE TABLE `especialidades` (
  `espsid` int(11) NOT NULL AUTO_INCREMENT,
  `ESPNOME` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`espsid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `entidade_rota`
--
DROP TABLE IF EXISTS `entidade_rota`;
CREATE TABLE `entidade_rota` (
  `idEntidade` int(11) NOT NULL DEFAULT '0',
  `idRota` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idEntidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `logistica_envioentidadeitem`
--
DROP TABLE IF EXISTS `logistica_envioentidadeitem`;
CREATE TABLE `logistica_envioentidadeitem` (
  `idItemEnvio` int(11) NOT NULL AUTO_INCREMENT,
  `idEnvio` int(11) NOT NULL,
  `idItemPedido` int(11) DEFAULT NULL,
  `obs` varchar(200) DEFAULT NULL,
  `qtdEnviada` int(11) DEFAULT '1',
  `lote` varchar(50) DEFAULT NULL,
  `validade` char(7) DEFAULT NULL COMMENT 'dd/aaaa',
  `volume` int(2) DEFAULT NULL,
  PRIMARY KEY (`idItemEnvio`,`idEnvio`),
  KEY `idEnvio` (`idEnvio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `titulo_pagamento`
--
DROP TABLE IF EXISTS `titulo_pagamento`;
CREATE TABLE `titulo_pagamento` (
  `idTituloPagamento` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idTitulo` int(10) unsigned DEFAULT NULL,
  `Data` datetime DEFAULT NULL,
  `idQuem` int(10) unsigned DEFAULT NULL,
  `valor` decimal(11,2) DEFAULT NULL,
  `vlrDesconto` decimal(9,2) DEFAULT NULL,
  `vlrAcrescimo` decimal(9,2) DEFAULT NULL,
  PRIMARY KEY (`idTituloPagamento`),
  KEY `titulo` (`idTitulo`),
  KEY `data` (`Data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `especie`
--
DROP TABLE IF EXISTS `especie`;
CREATE TABLE `especie` (
  `espId` int(11) NOT NULL AUTO_INCREMENT,
  `espNome` varchar(40) DEFAULT NULL,
  `espAtivo` char(1) DEFAULT NULL,
  PRIMARY KEY (`espId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `manutencao_acao`
--
DROP TABLE IF EXISTS `manutencao_acao`;
CREATE TABLE `manutencao_acao` (
  `idAcao` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idManutencao` int(10) unsigned NOT NULL DEFAULT '0',
  `Descricao` varchar(200) DEFAULT NULL,
  `DtHoraInicio` datetime DEFAULT NULL,
  `DtHoraTermino` datetime DEFAULT NULL,
  `StatusAcao` enum('Aberta','Finalizada') DEFAULT 'Aberta',
  `idUsrRespRegistro` int(10) unsigned DEFAULT NULL,
  `SuporteContatado` varchar(100) DEFAULT NULL,
  `RespSuporte` varchar(100) DEFAULT NULL,
  `MedidaSuporte` text,
  `TipoSuporte` enum('Interno','Externo') NOT NULL DEFAULT 'Interno',
  PRIMARY KEY (`idAcao`,`idManutencao`),
  KEY `Acao_FKIndex1` (`idManutencao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `log_os_remarcacao`
--
DROP TABLE IF EXISTS `log_os_remarcacao`;
CREATE TABLE `log_os_remarcacao` (
  `idLogOs` int(11) NOT NULL AUTO_INCREMENT,
  `idOrdem` int(11) DEFAULT NULL,
  `Data` datetime DEFAULT NULL,
  `Motivo` text,
  `Quem` int(11) DEFAULT NULL,
  `exame` varchar(10) DEFAULT NULL,
  `Operacao` varchar(50) DEFAULT NULL,
  `idSolic` int(11) DEFAULT NULL,
  PRIMARY KEY (`idLogOs`),
  KEY `data_remarcacao` (`Data`),
  KEY `idordem_remarcacao` (`idOrdem`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `os_aliquotada`
--
DROP TABLE IF EXISTS `os_aliquotada`;
CREATE TABLE `os_aliquotada` (
  `idOrdem` int(11) NOT NULL,
  `Exames` varchar(200) DEFAULT NULL,
  `idFunc` int(11) DEFAULT NULL,
  `Data` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`idOrdem`,`Data`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `materiais`
--
DROP TABLE IF EXISTS `materiais`;
CREATE TABLE `materiais` (
  `matsid` int(11) NOT NULL AUTO_INCREMENT,
  `MATNOME` varchar(60) DEFAULT NULL,
  `matEquivalencia` int(10) unsigned DEFAULT NULL,
  `MatColetadoNoCadastro` char(1) DEFAULT 'S',
  `matDtUltAlteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `matComposicao` tinytext,
  `matVolumeMinimo` char(1) DEFAULT 'S',
  PRIMARY KEY (`matsid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `recepcao_guia_entregue`
--
DROP TABLE IF EXISTS `recepcao_guia_entregue`;
CREATE TABLE `recepcao_guia_entregue` (
  `idCaixa` int(10) unsigned NOT NULL,
  `DtEntrega` datetime DEFAULT NULL,
  `QuemLevou` varchar(30) DEFAULT NULL,
  `QuemGerou` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idCaixa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `restricao_cadastro`
--
DROP TABLE IF EXISTS `restricao_cadastro`;
CREATE TABLE `restricao_cadastro` (
  `idRestricao` int(11) NOT NULL AUTO_INCREMENT,
  `Descricao` varchar(250) DEFAULT NULL,
  `imprimirNoLaudo` char(1) DEFAULT 'N',
  PRIMARY KEY (`idRestricao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `bocat`
--
DROP TABLE IF EXISTS `bocat`;
CREATE TABLE `bocat` (
  `ctId` int(11) NOT NULL AUTO_INCREMENT,
  `ctNome` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ctId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `manutencao_erroequipamento`
--
DROP TABLE IF EXISTS `manutencao_erroequipamento`;
CREATE TABLE `manutencao_erroequipamento` (
  `idErro` int(11) NOT NULL AUTO_INCREMENT,
  `idEquipamento` int(11) NOT NULL DEFAULT '0',
  `cdgErro` int(11) NOT NULL DEFAULT '0',
  `Descricao` text,
  PRIMARY KEY (`idErro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `dna_parente`
--
DROP TABLE IF EXISTS `dna_parente`;
CREATE TABLE `dna_parente` (
  `parId` int(11) NOT NULL AUTO_INCREMENT,
  `parNome` varchar(50) DEFAULT NULL,
  `parEnder` varchar(50) DEFAULT NULL,
  `parBairro` varchar(20) DEFAULT NULL,
  `parCidade` varchar(20) DEFAULT NULL,
  `parEstado` char(2) DEFAULT NULL,
  `parCep` varchar(9) DEFAULT NULL,
  `parPai` varchar(50) DEFAULT NULL,
  `parMae` varchar(50) DEFAULT NULL,
  `parDtNasc` date DEFAULT NULL,
  `parSexo` enum('Masculino','Feminino') DEFAULT 'Masculino',
  `parCpf` int(11) DEFAULT NULL,
  `parRG` int(11) DEFAULT NULL,
  `parExpedidor` varchar(10) DEFAULT NULL,
  `parCor` char(1) DEFAULT NULL,
  `parTransplMedula` enum('Sim','Não') DEFAULT 'Não',
  `parTransfusao` enum('Sim','Não') DEFAULT 'Não',
  `parDtCadastro` date DEFAULT NULL,
  `parQuemCadastrou` int(11) DEFAULT NULL,
  `parFone` varchar(20) DEFAULT NULL,
  `parEmail` varchar(40) DEFAULT NULL,
  `parDtExpedicao` date DEFAULT NULL,
  `parGrauParentesco` varchar(50) DEFAULT NULL,
  `parIdpai` int(11) DEFAULT NULL,
  `parOutroDoc` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`parId`),
  KEY `parIdpai` (`parIdpai`),
  KEY `idxParNome` (`parNome`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `mapa_os`
--
DROP TABLE IF EXISTS `mapa_os`;
CREATE TABLE `mapa_os` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idmapa` int(10) NOT NULL,
  `ordsid` int(10) NOT NULL,
  `exaCodigo` varchar(10) NOT NULL,
  `idSample` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `NewIndex1` (`ordsid`,`exaCodigo`),
  KEY `NewIndex2` (`idmapa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `tmp_exames`
--
DROP TABLE IF EXISTS `tmp_exames`;
CREATE TABLE `tmp_exames` (
  `codigo` char(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `virtual`
--
DROP TABLE IF EXISTS `virtual`;
CREATE TABLE `virtual` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `origem` varchar(100) DEFAULT NULL,
  `destino` varchar(100) DEFAULT NULL,
  `site` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `origem` (`origem`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `qc_elisa_ponto`
--
DROP TABLE IF EXISTS `qc_elisa_ponto`;
CREATE TABLE `qc_elisa_ponto` (
  `idponto` int(11) NOT NULL AUTO_INCREMENT,
  `idEquipamento` int(11) DEFAULT NULL,
  `idExame` varchar(10) NOT NULL,
  `reagente` varchar(100) DEFAULT NULL,
  `Violado` enum('Sim','Não') DEFAULT 'Não',
  `VlrCutOff` decimal(10,3) DEFAULT NULL,
  `VlrPositivo` decimal(10,3) DEFAULT NULL,
  `VlrNegativo` decimal(10,3) DEFAULT NULL,
  `DtAvaliacao` date DEFAULT NULL,
  `idUsrIncluiu` int(11) DEFAULT NULL,
  `DtInclusao` datetime DEFAULT NULL,
  `idUsrAlterou` int(11) DEFAULT NULL,
  `DtUltimaAlteracao` datetime DEFAULT NULL,
  `HrAvaliacao` time NOT NULL,
  `lote` varchar(100) NOT NULL,
  `fabricante` varchar(100) NOT NULL,
  `Bloqueado` enum('Sim','Não') NOT NULL DEFAULT 'Sim',
  `idUsrLiberou` int(10) unsigned DEFAULT NULL,
  `DtLiberacao` datetime DEFAULT NULL,
  PRIMARY KEY (`idponto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `qc_lotecongelado`
--
DROP TABLE IF EXISTS `qc_lotecongelado`;
CREATE TABLE `qc_lotecongelado` (
  `idLote` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idEquipamento` int(10) unsigned NOT NULL DEFAULT '0',
  `idExame` varchar(30) NOT NULL,
  `idExameNoEquipamento` varchar(30) NOT NULL,
  `Lote` varchar(60) DEFAULT NULL,
  `LoteEquipamento` varchar(50) DEFAULT NULL,
  `Dsc_Calibrador` varchar(40) DEFAULT NULL,
  `Dt_Inclusao` datetime DEFAULT '0000-00-00 00:00:00',
  `Dt_Vencimento` date DEFAULT NULL,
  `CtrlAlto_CVReferencia` decimal(11,3) DEFAULT '0.000',
  `CtrlAlto_VlrSuperior` decimal(11,3) DEFAULT NULL,
  `CtrlAlto_VlrInferior` decimal(11,3) DEFAULT NULL,
  `CtrlAlto_Unidade` varchar(40) DEFAULT '',
  `CtrlMedio_CVReferencia` decimal(11,3) DEFAULT '0.000',
  `CtrlMedio_VlrSuperior` decimal(11,3) DEFAULT NULL,
  `CtrlMedio_VlrInferior` decimal(11,3) DEFAULT NULL,
  `CtrlMedio_Unidade` varchar(40) DEFAULT '',
  `CtrlBaixo_CVReferencia` decimal(11,3) DEFAULT '0.000',
  `CtrlBaixo_VlrSuperior` decimal(11,3) DEFAULT NULL,
  `CtrlBaixo_VlrInferior` decimal(11,3) DEFAULT NULL,
  `CtrlBaixo_Unidade` varchar(40) DEFAULT NULL,
  `Celula` int(11) NOT NULL DEFAULT '0',
  `LancaManual` enum('Sim','Não') NOT NULL DEFAULT 'Não',
  `FaixaDinamica` enum('Sim','Não') DEFAULT 'Não',
  `EmUso` enum('Sim','Não') DEFAULT NULL,
  `idUsrAlterou` int(11) DEFAULT NULL,
  `DtUltimaAlteracao` datetime DEFAULT NULL,
  `RotinaAlto` char(10) DEFAULT 'D111111000',
  `RotinaMedio` char(10) DEFAULT 'D111111000',
  `RotinaBaixo` char(10) DEFAULT 'D111111000',
  PRIMARY KEY (`idLote`),
  UNIQUE KEY `idx_Equip_Exame` (`idEquipamento`,`idExame`,`Lote`,`idExameNoEquipamento`,`Celula`) USING BTREE,
  KEY `idx_Data_Inclusao` (`idExame`,`Dt_Inclusao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `ocorrencia_exame`
--
DROP TABLE IF EXISTS `ocorrencia_exame`;
CREATE TABLE `ocorrencia_exame` (
  `Exame` char(10) NOT NULL DEFAULT '',
  `idOcorrencia` int(10) unsigned NOT NULL DEFAULT '0',
  `Qtd` int(11) DEFAULT '0',
  `VlrCusto` decimal(11,2) DEFAULT '0.00',
  PRIMARY KEY (`idOcorrencia`,`Exame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `srt_estoque`
--
DROP TABLE IF EXISTS `srt_estoque`;
CREATE TABLE `srt_estoque` (
  `LCEST` varchar(20) DEFAULT NULL,
  `LCDATA` datetime DEFAULT NULL,
  `LCPOSICAO` varchar(5) DEFAULT NULL,
  `LCARRAY` double DEFAULT NULL,
  `LCSTATE` int(11) DEFAULT NULL,
  `LCSID` int(11) NOT NULL AUTO_INCREMENT,
  `LCCODPAS` int(11) DEFAULT NULL,
  `lcIdQuem` int(11) NOT NULL DEFAULT '0',
  `lcObservacao` varchar(50) DEFAULT NULL,
  `lcIdSample` varchar(15) DEFAULT NULL,
  `lcIdMaterial` int(11) DEFAULT NULL,
  PRIMARY KEY (`LCSID`),
  KEY `LCCODPAS` (`LCCODPAS`),
  KEY `LCEST` (`LCEST`),
  KEY `lcIdSample` (`lcIdSample`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `amb_porte`
--
DROP TABLE IF EXISTS `amb_porte`;
CREATE TABLE `amb_porte` (
  `Porte` char(5) NOT NULL DEFAULT '',
  `Valor` decimal(11,2) DEFAULT NULL,
  PRIMARY KEY (`Porte`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `ordem_servico_laudo_teste_04`
--
DROP TABLE IF EXISTS `ordem_servico_laudo_teste_04`;
CREATE TABLE `ordem_servico_laudo_teste_04` (
  `ldosid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `log_pendencias`
--
DROP TABLE IF EXISTS `log_pendencias`;
CREATE TABLE `log_pendencias` (
  `idLogPendencias` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ConteudoAnterior` text,
  `Quem` varchar(45) DEFAULT NULL,
  `Data` datetime DEFAULT NULL,
  `Operacao` enum('Inserção','Edição','Exclusão') DEFAULT NULL,
  `idPendencia` int(11) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idLogPendencias`),
  KEY `idPendencia` (`idPendencia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela para registrar o log das pendencias';
--
-- Table structure for table `rodada03`
--
DROP TABLE IF EXISTS `rodada03`;
CREATE TABLE `rodada03` (
  `ldosid` int(11) NOT NULL,
  `ldostatus` varchar(255) NOT NULL,
  `ldodetails` longtext,
  `ldodatasincronizacao` datetime DEFAULT NULL,
  `ldodatacriacao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `internet_laudo_lido`
--
DROP TABLE IF EXISTS `internet_laudo_lido`;
CREATE TABLE `internet_laudo_lido` (
  `idLido` int(11) NOT NULL AUTO_INCREMENT,
  `os` int(10) unsigned DEFAULT NULL,
  `dataAcesso` datetime DEFAULT NULL,
  `login` varchar(20) DEFAULT NULL,
  `senha` varchar(20) DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`idLido`),
  KEY `os` (`os`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `vacina_vacinas`
--
DROP TABLE IF EXISTS `vacina_vacinas`;
CREATE TABLE `vacina_vacinas` (
  `vaid` int(11) NOT NULL AUTO_INCREMENT,
  `vaNome` varchar(50) DEFAULT NULL,
  `vaObservacao` varchar(100) DEFAULT NULL,
  `campo` varchar(10) DEFAULT NULL,
  `impObs` char(1) DEFAULT 'N',
  PRIMARY KEY (`vaid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `qc_agrupamento`
--
DROP TABLE IF EXISTS `qc_agrupamento`;
CREATE TABLE `qc_agrupamento` (
  `idGrupo` int(10) unsigned NOT NULL DEFAULT '0',
  `idLote` int(10) unsigned NOT NULL DEFAULT '0',
  `idusr` int(11) DEFAULT NULL,
  `dtalteracao` datetime DEFAULT NULL,
  PRIMARY KEY (`idGrupo`,`idLote`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `marca`
--
DROP TABLE IF EXISTS `marca`;
CREATE TABLE `marca` (
  `idmarca` int(11) NOT NULL AUTO_INCREMENT,
  `nomeMarca` varchar(100) NOT NULL,
  PRIMARY KEY (`idmarca`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `parametro_config`
--
DROP TABLE IF EXISTS `parametro_config`;
CREATE TABLE `parametro_config` (
  `CodChamada` varchar(30) NOT NULL DEFAULT '',
  `Descricao` varchar(250) DEFAULT NULL,
  `parametro1` text,
  `Parametro2` varchar(250) DEFAULT NULL,
  `Imagem` blob,
  `parametro3` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`CodChamada`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `exames_solicitados_informacao`
--
DROP TABLE IF EXISTS `exames_solicitados_informacao`;
CREATE TABLE `exames_solicitados_informacao` (
  `idexasol_informacao` int(11) NOT NULL,
  `exasolInf_ModoResultado` enum('Automatico','Intersite','Manual') NOT NULL DEFAULT 'Automatico',
  PRIMARY KEY (`idexasol_informacao`),
  CONSTRAINT `FK_solsid` FOREIGN KEY (`idexasol_informacao`) REFERENCES `exames_solicitados_historico` (`solsid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela de informações extras do exames_silicitados';
--
-- Table structure for table `informativos`
--
DROP TABLE IF EXISTS `informativos`;
CREATE TABLE `informativos` (
  `idinformativos` int(11) NOT NULL AUTO_INCREMENT,
  `tipoInformativo` enum('Tecnico','Comercial') DEFAULT NULL,
  `pdfInformativo` blob,
  `dataInicio` date DEFAULT NULL,
  `dataFim` date DEFAULT NULL,
  `quemCadastrou` int(11) DEFAULT NULL,
  `Descricao` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idinformativos`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `log_faturamentoautomatico`
--
DROP TABLE IF EXISTS `log_faturamentoautomatico`;
CREATE TABLE `log_faturamentoautomatico` (
  `idlogfaturamento` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datainicial` datetime DEFAULT NULL,
  `datafinal` datetime DEFAULT NULL,
  `periodo` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`idlogfaturamento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `nova_retirada_laudos`
--
DROP TABLE IF EXISTS `nova_retirada_laudos`;
CREATE TABLE `nova_retirada_laudos` (
  `idEntidade` int(10) unsigned NOT NULL,
  `escolha` enum('Internet','Impresso') NOT NULL,
  `opcao_antiga` varchar(12) NOT NULL,
  `datahora` datetime NOT NULL,
  PRIMARY KEY (`idEntidade`),
  UNIQUE KEY `idEntidade` (`idEntidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC COMMENT='Nova escolha de retirada de laudos';
--
-- Table structure for table `userauth`
--
DROP TABLE IF EXISTS `userauth`;
CREATE TABLE `userauth` (
  `sid` varchar(255) NOT NULL DEFAULT '',
  `user` varchar(32) NOT NULL DEFAULT '',
  `ip` varchar(16) NOT NULL DEFAULT '',
  `horario` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tipo_login` varchar(255) DEFAULT NULL,
  `id_instituicao` varchar(255) DEFAULT NULL,
  `login` varchar(255) DEFAULT NULL,
  `senha` varchar(255) DEFAULT NULL,
  `instituicao` varchar(255) DEFAULT NULL,
  `idEmpresa` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `dna_populacao`
--
DROP TABLE IF EXISTS `dna_populacao`;
CREATE TABLE `dna_populacao` (
  `popId` int(11) NOT NULL AUTO_INCREMENT,
  `popIdLocos` smallint(6) DEFAULT NULL,
  `popAlelo` double DEFAULT NULL,
  `popPerc` double DEFAULT NULL,
  PRIMARY KEY (`popId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `rever_entrada`
--
DROP TABLE IF EXISTS `rever_entrada`;
CREATE TABLE `rever_entrada` (
  `identidade` int(11) NOT NULL,
  `idExameSolicitado` int(11) NOT NULL,
  `dataVerificado` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`identidade`,`idExameSolicitado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `envia_email_destino`
--
DROP TABLE IF EXISTS `envia_email_destino`;
CREATE TABLE `envia_email_destino` (
  `idenvia_email_destino` int(11) NOT NULL AUTO_INCREMENT,
  `destino_idEmail` int(11) DEFAULT NULL,
  `destino_para` text,
  `destino_tipo` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`idenvia_email_destino`),
  KEY `destino_idEmail` (`destino_idEmail`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `qc_grupo`
--
DROP TABLE IF EXISTS `qc_grupo`;
CREATE TABLE `qc_grupo` (
  `idGrupo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `GrpDescricao` varchar(100) DEFAULT NULL,
  `idusr` int(11) DEFAULT NULL,
  `dtalteracao` datetime DEFAULT NULL,
  PRIMARY KEY (`idGrupo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `exames_mensagem`
--
DROP TABLE IF EXISTS `exames_mensagem`;
CREATE TABLE `exames_mensagem` (
  `Id_exammsg` int(11) NOT NULL AUTO_INCREMENT,
  `ExaCodigo` varchar(10) DEFAULT NULL,
  `Id_Msg` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id_exammsg`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `na_parametro`
--
DROP TABLE IF EXISTS `na_parametro`;
CREATE TABLE `na_parametro` (
  `TextoAposCabecalho` blob,
  `TextoAposExames` blob,
  `TextoAposMotivo` blob
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `parametros_expurgo`
--
DROP TABLE IF EXISTS `parametros_expurgo`;
CREATE TABLE `parametros_expurgo` (
  `tabela` varchar(45) NOT NULL,
  `pk` varchar(45) NOT NULL,
  `valor` varchar(30) NOT NULL,
  `limite` int(11) NOT NULL,
  PRIMARY KEY (`tabela`,`pk`,`valor`,`limite`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `descritivos`
--
DROP TABLE IF EXISTS `descritivos`;
CREATE TABLE `descritivos` (
  `dessid` int(11) NOT NULL AUTO_INCREMENT,
  `DESTEXTO` blob,
  `DESTITULO` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`dessid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `rep_rota_auxiliar`
--
DROP TABLE IF EXISTS `rep_rota_auxiliar`;
CREATE TABLE `rep_rota_auxiliar` (
  `IdRota` int(10) unsigned NOT NULL DEFAULT '0',
  `idAuxiliar` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idAuxiliar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `celula`
--
DROP TABLE IF EXISTS `celula`;
CREATE TABLE `celula` (
  `idCelula` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `Regiao` varchar(45) DEFAULT NULL,
  `Estado` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `Supervidor_regiao` varchar(45) DEFAULT NULL,
  `cidade` varchar(45) DEFAULT NULL,
  `telefone` varchar(45) DEFAULT NULL,
  `endereco` varchar(45) DEFAULT NULL,
  `cep` int(11) DEFAULT NULL,
  `Ativa` char(1) DEFAULT 'S',
  `idLocalInova` int(11) DEFAULT NULL,
  PRIMARY KEY (`idCelula`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `msg_popup`
--
DROP TABLE IF EXISTS `msg_popup`;
CREATE TABLE `msg_popup` (
  `idmsg_popup` int(11) NOT NULL,
  `msg_texto` text NOT NULL,
  `msg_tipo` varchar(100) DEFAULT NULL,
  `msg_exame` varchar(45) DEFAULT NULL,
  `msg_setor` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`idmsg_popup`),
  KEY `index2` (`msg_tipo`),
  KEY `index3` (`msg_exame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `sobreposicao_bloqueada`
--
DROP TABLE IF EXISTS `sobreposicao_bloqueada`;
CREATE TABLE `sobreposicao_bloqueada` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idOrdem` int(10) unsigned NOT NULL DEFAULT '0',
  `idUsuario` int(10) unsigned NOT NULL DEFAULT '0',
  `DataHora` datetime DEFAULT NULL,
  `Exame` varchar(10) NOT NULL DEFAULT '',
  `idCritica` int(10) unsigned NOT NULL DEFAULT '0',
  `ResultadoBloqueado` varchar(50) DEFAULT NULL,
  `ResultadoAtual` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `uf`
--
DROP TABLE IF EXISTS `uf`;
CREATE TABLE `uf` (
  `uf` char(2) NOT NULL DEFAULT '',
  `Descricao` varchar(40) DEFAULT NULL,
  `pais` char(3) DEFAULT 'BR',
  PRIMARY KEY (`uf`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `na_motivo`
--
DROP TABLE IF EXISTS `na_motivo`;
CREATE TABLE `na_motivo` (
  `idNA_Motivo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Descricao` varchar(255) DEFAULT NULL,
  `Texto` blob,
  `ativo` char(1) NOT NULL DEFAULT 'S',
  PRIMARY KEY (`idNA_Motivo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `exame_cod_movimento`
--
DROP TABLE IF EXISTS `exame_cod_movimento`;
CREATE TABLE `exame_cod_movimento` (
  `idMovimento` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idCadastro` int(10) unsigned NOT NULL DEFAULT '0',
  `idExame` varchar(10) DEFAULT NULL,
  `nome` varchar(60) DEFAULT NULL,
  `CodigoFornecido` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`idMovimento`),
  KEY `exame_cod_movimento_exame` (`idExame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `exame_material_possivel`
--
DROP TABLE IF EXISTS `exame_material_possivel`;
CREATE TABLE `exame_material_possivel` (
  `idExame` int(11) NOT NULL,
  `idMaterial` int(11) NOT NULL,
  PRIMARY KEY (`idExame`,`idMaterial`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `pdf_exames_questionario`
--
DROP TABLE IF EXISTS `pdf_exames_questionario`;
CREATE TABLE `pdf_exames_questionario` (
  `CodExame` varchar(10) NOT NULL,
  `CodRelatorio` int(10) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`CodRelatorio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `dnapopulacao`
--
DROP TABLE IF EXISTS `dnapopulacao`;
CREATE TABLE `dnapopulacao` (
  `PopId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PopLocos` varchar(20) DEFAULT NULL,
  `PopAlelo` varchar(10) NOT NULL DEFAULT '',
  `PopPerc` varchar(10) DEFAULT NULL,
  `PopOrdemList` char(2) DEFAULT NULL,
  PRIMARY KEY (`PopId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `log_os`
--
DROP TABLE IF EXISTS `log_os`;
CREATE TABLE `log_os` (
  `idLogOs` int(11) NOT NULL AUTO_INCREMENT,
  `idOrdem` int(11) DEFAULT NULL,
  `Data` datetime DEFAULT NULL,
  `Motivo` text,
  `Quem` int(11) DEFAULT NULL,
  `exame` varchar(10) DEFAULT NULL,
  `Operacao` varchar(50) DEFAULT NULL,
  `idSolic` int(11) DEFAULT NULL,
  `idEntCobranca` int(10) unsigned DEFAULT '0',
  `pcoExame` decimal(9,2) DEFAULT '0.00',
  PRIMARY KEY (`idLogOs`),
  KEY `logos_data` (`Data`),
  KEY `idOrdem` (`idOrdem`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `laudo_log_impressao`
--
DROP TABLE IF EXISTS `laudo_log_impressao`;
CREATE TABLE `laudo_log_impressao` (
  `id` int(11) NOT NULL,
  `session_id` varchar(128) NOT NULL,
  `ordsid` int(11) NOT NULL,
  `exames_solicitados` longtext NOT NULL COMMENT '(DC2Type:simple_array)',
  `data_geracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `ordsid` (`ordsid`),
  CONSTRAINT `laudo_log_impressao_ordem_servico_fk` FOREIGN KEY (`ordsid`) REFERENCES `ordem_servico` (`ordsid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `bandejascomposicao`
--
DROP TABLE IF EXISTS `bandejascomposicao`;
CREATE TABLE `bandejascomposicao` (
  `idbandejasComposicao` int(11) NOT NULL AUTO_INCREMENT,
  `bandejas_id` int(11) DEFAULT NULL,
  `exames_id` text,
  `exames_cod` text,
  PRIMARY KEY (`idbandejasComposicao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `entidade_cartao_premio`
--
DROP TABLE IF EXISTS `entidade_cartao_premio`;
CREATE TABLE `entidade_cartao_premio` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Descricao` varchar(80) DEFAULT NULL,
  `pontosInicial` int(10) unsigned DEFAULT NULL,
  `foto` longblob,
  `ano` smallint(5) unsigned DEFAULT NULL,
  `pontosFinal` int(10) unsigned NOT NULL,
  `eletronico` enum('S','N') NOT NULL DEFAULT 'N' COMMENT 'campo para verificar a necessidade de determinar a voltagem',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `rotest`
--
DROP TABLE IF EXISTS `rotest`;
CREATE TABLE `rotest` (
  `ETSID` double DEFAULT NULL,
  `ETCOL` smallint(6) DEFAULT NULL,
  `ETLINHA` smallint(6) DEFAULT NULL,
  `ETCOD` varchar(20) DEFAULT NULL,
  `ETULTDATE` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `exames_solicitados_inclusao`
--
DROP TABLE IF EXISTS `exames_solicitados_inclusao`;
CREATE TABLE `exames_solicitados_inclusao` (
  `incsid` int(11) NOT NULL AUTO_INCREMENT,
  `incsidordem` int(11) NOT NULL,
  `incExame` varchar(10) NOT NULL,
  `incControleAol` char(1) DEFAULT 'N',
  `incFormaExecucao` char(1) DEFAULT 'N',
  `incSetor` char(3) DEFAULT NULL,
  `inctempoDesejado` varchar(20) DEFAULT NULL,
  `incMedico` int(10) DEFAULT NULL,
  `incEntidade` int(10) DEFAULT NULL,
  `incConvenio` int(10) DEFAULT NULL,
  `incQuemSolicitou` int(11) DEFAULT NULL,
  `incDataSolicitou` datetime DEFAULT NULL,
  `incQuemVerificou` int(11) DEFAULT NULL,
  `incDataVerificou` datetime DEFAULT NULL,
  `incVerificado` enum('Nao','Sim') DEFAULT 'Nao',
  `incStatus` enum('Nao_Confirmado','Confirmado') DEFAULT 'Nao_Confirmado',
  `incObservacaoInclusao` text,
  `incIdMotivoNaoInclusao` int(11) DEFAULT NULL,
  `incSistemaUtilizado` enum('Labor','Inova') DEFAULT NULL,
  PRIMARY KEY (`incsid`),
  KEY `indice_idordemservico` (`incsidordem`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `material_pendente`
--
DROP TABLE IF EXISTS `material_pendente`;
CREATE TABLE `material_pendente` (
  `idSolic` int(11) NOT NULL DEFAULT '0',
  `Data` datetime DEFAULT NULL,
  `idQuemColocou` int(11) DEFAULT NULL,
  `idNroAmostra` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idSolic`,`idNroAmostra`),
  KEY `idsolic` (`idSolic`,`idNroAmostra`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `recepcao_caixa`
--
DROP TABLE IF EXISTS `recepcao_caixa`;
CREATE TABLE `recepcao_caixa` (
  `reccxsid` int(11) NOT NULL AUTO_INCREMENT,
  `RECSID` int(11) DEFAULT NULL,
  `PCTSID` int(11) DEFAULT NULL,
  `orddtent` date DEFAULT NULL,
  `ORDSID` int(11) DEFAULT NULL,
  `VLR_TOTAL` decimal(12,2) DEFAULT '0.00',
  `VLR_PAGO` decimal(12,2) DEFAULT '0.00',
  `VLR_DESC` decimal(12,2) DEFAULT '0.00',
  `DeveGuia` char(1) DEFAULT 'N',
  `idEntidade` int(11) DEFAULT NULL,
  `NotaFiscal` int(11) DEFAULT '0',
  `vlrNota1` decimal(11,2) DEFAULT '0.00',
  `NotaFiscal2` int(10) unsigned DEFAULT '0',
  `vlrNota2` decimal(11,2) DEFAULT '0.00',
  `RecNumeroRPS` int(11) DEFAULT '0',
  `dtSenha` datetime DEFAULT NULL,
  `dtImpEtiqueta` datetime DEFAULT NULL,
  `dtInicioCadastro` datetime DEFAULT NULL,
  PRIMARY KEY (`reccxsid`),
  KEY `ORDSID` (`ORDSID`),
  KEY `data` (`orddtent`),
  KEY `rps` (`RecNumeroRPS`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `conexao`
--
DROP TABLE IF EXISTS `conexao`;
CREATE TABLE `conexao` (
  `id` tinyint(4) NOT NULL DEFAULT '0',
  `ip` char(15) DEFAULT NULL,
  `conexao` int(10) unsigned DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `exame_dependencia`
--
DROP TABLE IF EXISTS `exame_dependencia`;
CREATE TABLE `exame_dependencia` (
  `exacodigo` varchar(15) NOT NULL DEFAULT '',
  `exadependencia` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`exacodigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `faq_resposta`
--
DROP TABLE IF EXISTS `faq_resposta`;
CREATE TABLE `faq_resposta` (
  `resId` int(11) NOT NULL AUTO_INCREMENT,
  `resIdPergunta` int(11) DEFAULT NULL,
  `resTexto` mediumtext,
  `resIdQuemCadastrou` int(11) DEFAULT NULL,
  `resDtCadastro` datetime DEFAULT NULL,
  PRIMARY KEY (`resId`),
  KEY `idPergunta` (`resIdPergunta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `na_exame`
--
DROP TABLE IF EXISTS `na_exame`;
CREATE TABLE `na_exame` (
  `idNA_Exame` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idNA_Requisicao` int(10) unsigned NOT NULL DEFAULT '0',
  `idExameSolicitado` int(10) unsigned NOT NULL DEFAULT '0',
  `idExame` varchar(10) DEFAULT NULL,
  `idMaterial` int(10) unsigned NOT NULL DEFAULT '0',
  `idEntidade` int(11) DEFAULT NULL,
  `idMedico` int(11) DEFAULT NULL,
  `idFuncLiberadoMaterial` int(11) DEFAULT NULL,
  PRIMARY KEY (`idNA_Exame`),
  KEY `idMaterial` (`idMaterial`),
  KEY `idExameSolicitado` (`idExameSolicitado`),
  KEY `idNA_Requisicao` (`idNA_Requisicao`),
  KEY `idExame` (`idExame`),
  KEY `idEntidade` (`idEntidade`),
  KEY `index01_na_exame` (`idNA_Requisicao`,`idEntidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `ejb__timer__tbl`
--
DROP TABLE IF EXISTS `ejb__timer__tbl`;
CREATE TABLE `ejb__timer__tbl` (
  `CREATIONTIMERAW` bigint(20) NOT NULL,
  `BLOB` blob,
  `TIMERID` varchar(255) NOT NULL,
  `CONTAINERID` bigint(20) NOT NULL,
  `OWNERID` varchar(255) DEFAULT NULL,
  `STATE` int(11) NOT NULL,
  `PKHASHCODE` int(11) NOT NULL,
  `INTERVALDURATION` bigint(20) NOT NULL,
  `INITIALEXPIRATIONRAW` bigint(20) NOT NULL,
  `LASTEXPIRATIONRAW` bigint(20) NOT NULL,
  `SCHEDULE` varchar(255) DEFAULT NULL,
  `APPLICATIONID` bigint(20) NOT NULL,
  PRIMARY KEY (`TIMERID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `equipamento`
--
DROP TABLE IF EXISTS `equipamento`;
CREATE TABLE `equipamento` (
  `idEquipamento` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Descricao` varchar(40) DEFAULT NULL,
  `idModeloEquip` int(10) unsigned NOT NULL DEFAULT '0',
  `CodEquipamento` varchar(15) DEFAULT NULL,
  `Batch` enum('S','N') DEFAULT NULL,
  `DirPool` varchar(60) DEFAULT NULL,
  `DirGeracao` varchar(60) DEFAULT NULL,
  `TempoGeracao` time DEFAULT NULL,
  `Modelo` varchar(20) DEFAULT NULL,
  `Serie` varchar(20) DEFAULT NULL,
  `Setor` varchar(5) DEFAULT NULL,
  `idResponsavel` int(10) unsigned DEFAULT '0',
  `DtInstalacao` date DEFAULT NULL,
  `NumDoc` varchar(20) DEFAULT NULL,
  `IdFornecedor` int(10) unsigned DEFAULT '0',
  `IdManutencao` int(10) unsigned DEFAULT '0',
  `idDocumento` int(11) DEFAULT '0',
  `Ativo` char(1) DEFAULT 'S',
  `idImobilizado` int(11) DEFAULT '0',
  `AutoExportacao` char(1) DEFAULT 'N',
  `ControlaArquivoProcessado` char(1) DEFAULT NULL,
  `StatusEquipamento` enum('Normal','Inativo','Parcial','Desuso','Devolvido') NOT NULL DEFAULT 'Normal',
  `TipoEquipamento` enum('Primário','Apoio') NOT NULL DEFAULT 'Primário',
  `Grupo` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Grupo do Equipamento para fins estatisticos',
  `ExibeEstatistica` enum('Sim','Não') NOT NULL DEFAULT 'Sim',
  `Propriedade` enum('Próprio','Comodato','Locado') NOT NULL DEFAULT 'Próprio',
  `Aquisicao` enum('Novo','Usado','Recondicionado') NOT NULL DEFAULT 'Novo',
  `PeriodicidadeManut` int(10) unsigned NOT NULL DEFAULT '4' COMMENT 'Em meses',
  `AssistenciaTerceirizada` enum('Sim','Não') NOT NULL DEFAULT 'Sim',
  `DtUltimaPreventiva` datetime DEFAULT NULL,
  `excluiResultadoAnterior` char(1) DEFAULT 'N',
  `processaResultadoNovaRotina` char(1) DEFAULT 'N',
  `rotina` char(30) DEFAULT '',
  `ProducaoHora` int(10) unsigned DEFAULT '0',
  `ProducaoNominal` int(10) unsigned DEFAULT '0' COMMENT 'Producao por modulo',
  `QtdModulos` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'Quantidade de Modulos',
  `Imagem` longblob,
  `SubstituidoPor` varchar(45) DEFAULT NULL COMMENT 'IDs dos equipamentos (separados por vírgula) que substituiram o equipamento',
  `idFabricante` int(10) unsigned DEFAULT NULL COMMENT 'Codigo do Fabricante do Equipamento',
  PRIMARY KEY (`idEquipamento`),
  KEY `idxGrupo` (`idEquipamento`,`Grupo`),
  KEY `idModeloEquip` (`idModeloEquip`),
  KEY `index01_equipamento` (`idEquipamento`,`idModeloEquip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `stat_exame_historico_producao_parcial_27_script_antigo`
--
DROP TABLE IF EXISTS `stat_exame_historico_producao_parcial_27_script_antigo`;
CREATE TABLE `stat_exame_historico_producao_parcial_27_script_antigo` (
  `idstat_exame` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `area` char(1) DEFAULT NULL,
  `Entidade` int(10) unsigned DEFAULT NULL,
  `exame` varchar(10) DEFAULT NULL,
  `etapa` tinyint(4) DEFAULT NULL,
  `Data` varchar(8) DEFAULT NULL,
  `qtde` int(11) DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `idEmpresa` int(10) unsigned DEFAULT NULL,
  `idMedico` int(10) unsigned DEFAULT '0',
  `qtdePct` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`idstat_exame`),
  KEY `area` (`area`,`Entidade`,`exame`,`Data`),
  KEY `etapa` (`etapa`,`Data`),
  KEY `stat_exame_medico` (`area`,`idMedico`,`exame`,`Data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `notas_fiscais_old`
--
DROP TABLE IF EXISTS `notas_fiscais_old`;
CREATE TABLE `notas_fiscais_old` (
  `notsid` int(11) NOT NULL DEFAULT '0',
  `notdtfatura` datetime DEFAULT NULL,
  `NOTPERIODOFATURA` varchar(50) DEFAULT NULL,
  `NOTFATURA` int(11) DEFAULT NULL,
  `NOTNOTA` int(11) DEFAULT NULL,
  `notValor` decimal(9,2) DEFAULT NULL,
  `NOTQUEMCADASTROU` int(11) DEFAULT NULL,
  `NOTSIDENTIDADE` int(11) DEFAULT NULL,
  `NOTQTDEXAMES` int(11) DEFAULT NULL,
  `notdtnota` datetime DEFAULT NULL,
  `NOTMARCADO` char(1) DEFAULT NULL,
  `notQuemImprimiu` int(11) DEFAULT NULL,
  `NOTVLRIR` decimal(9,2) DEFAULT NULL,
  `NOTVLRDESCONTO` decimal(9,2) DEFAULT NULL,
  `notObs` varchar(50) DEFAULT NULL,
  `notDtVcto` date DEFAULT NULL,
  `notVlrParc1` decimal(11,2) DEFAULT NULL,
  `notDtVcto2` date DEFAULT NULL,
  `notVlrParc2` decimal(11,2) DEFAULT NULL,
  `notDtVcto3` date DEFAULT NULL,
  `notVlrParc3` decimal(11,2) DEFAULT NULL,
  `notDtVcto4` date DEFAULT NULL,
  `notVlrParc4` decimal(11,2) DEFAULT NULL,
  `notDtVcto5` date DEFAULT NULL,
  `notVlrParc5` decimal(11,2) DEFAULT NULL,
  `notEmpresas` varchar(20) DEFAULT NULL,
  `NotVlrContribSocial` decimal(10,2) DEFAULT NULL,
  `NotVlrPis` decimal(10,2) DEFAULT NULL,
  `NotVlrISSQN` decimal(10,2) DEFAULT '0.00',
  `notVlrCofins` decimal(10,2) DEFAULT '0.00',
  `notCancelada` char(1) DEFAULT 'N',
  `notCodEspecie` varchar(4) DEFAULT NULL,
  `notVlrInss` decimal(9,2) NOT NULL DEFAULT '0.00',
  KEY `pk` (`notsid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `parametros`
--
DROP TABLE IF EXISTS `parametros`;
CREATE TABLE `parametros` (
  `idempguia` int(11) NOT NULL,
  `sequencia` int(11) DEFAULT NULL,
  `cooperate` int(11) NOT NULL,
  `PARNOME` varchar(60) DEFAULT NULL,
  `PARULTFATURA` int(11) DEFAULT NULL,
  `PARCONTASFORN` varchar(5) DEFAULT NULL,
  `PARCONTASCLIENTES` varchar(5) DEFAULT NULL,
  `PARCONTASPCT` varchar(5) DEFAULT NULL,
  `PARCONTASCONV` varchar(5) DEFAULT NULL,
  `PARCONTASLABEX` varchar(5) DEFAULT NULL,
  `PARULTNOTA` int(11) DEFAULT NULL,
  `PARMENSSAGEM` text,
  `PARMSGURGENTE` char(1) DEFAULT NULL,
  `parDtUltGeracaoInternet` datetime DEFAULT NULL,
  `parUltRecibo` smallint(6) DEFAULT NULL,
  `ParDtUltEmissaoLaudo` datetime DEFAULT NULL,
  `PARMENSAGEMREC` text,
  `PARMSGURGENTEREC` char(1) DEFAULT 'N',
  `PARNUMFECHACAIXA` mediumint(8) unsigned DEFAULT '0',
  `PARVLRMAXIMODESC` decimal(12,2) DEFAULT NULL,
  `parAssinatura` longblob,
  `parQuemAssina` varchar(50) DEFAULT NULL,
  `parCRFAssinante` varchar(15) DEFAULT NULL,
  `idScriptCabecalho` int(10) unsigned DEFAULT NULL,
  `idScriptRodape` int(10) unsigned DEFAULT NULL,
  `parDtRevisaoIndicadores` date DEFAULT NULL,
  `parMsgRepres` text,
  `PARULTIMONRONFE` int(11) DEFAULT NULL COMMENT 'Campo com o último numero de RPS utilizado na para NFE',
  `PARHomologacaoNRONFE` int(11) DEFAULT NULL,
  `PARPathNFSE` varchar(20) DEFAULT NULL,
  `serie` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idempguia`,`cooperate`),
  UNIQUE KEY `PKParametros` (`PARNOME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `amb_porte_excluir`
--
DROP TABLE IF EXISTS `amb_porte_excluir`;
CREATE TABLE `amb_porte_excluir` (
  `Porte` char(5) NOT NULL DEFAULT '',
  `Valor` decimal(11,2) DEFAULT NULL,
  PRIMARY KEY (`Porte`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `rep_desconto`
--
DROP TABLE IF EXISTS `rep_desconto`;
CREATE TABLE `rep_desconto` (
  `idRepresentante` int(11) NOT NULL DEFAULT '0',
  `idEntidade` int(11) NOT NULL DEFAULT '0',
  `Exame` varchar(10) DEFAULT NULL,
  `Perc` decimal(3,2) DEFAULT '0.00',
  `VlrPorExame` decimal(9,2) DEFAULT '0.00',
  UNIQUE KEY `rep_desconto_rep` (`idRepresentante`,`idEntidade`,`Exame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `template_etiqueta`
--
DROP TABLE IF EXISTS `template_etiqueta`;
CREATE TABLE `template_etiqueta` (
  `id` tinyint(3) unsigned NOT NULL,
  `descricao` char(40) DEFAULT NULL,
  `header` text,
  `body` text,
  `footer` text,
  `campos` text,
  `script` blob,
  `apelido` char(20) DEFAULT NULL,
  `linguagemImpressora` enum('Eltron','Zebra','Allegro') DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `apelido` (`apelido`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `na_requisicao`
--
DROP TABLE IF EXISTS `na_requisicao`;
CREATE TABLE `na_requisicao` (
  `idNA_Requisicao` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idOrdemServico` int(10) unsigned NOT NULL DEFAULT '0',
  `idFuncionario` int(10) unsigned NOT NULL DEFAULT '0',
  `Observacao` blob,
  `DataRequisicao` datetime DEFAULT NULL,
  `DataImpressao` datetime DEFAULT NULL,
  `Liberado` char(1) DEFAULT 'N',
  `DataLiberacao` datetime DEFAULT NULL,
  `datalidointernet` datetime DEFAULT NULL,
  `idFuncLiberou` int(11) DEFAULT NULL,
  `DataEnvioEmail` datetime DEFAULT NULL,
  PRIMARY KEY (`idNA_Requisicao`),
  KEY `idOrdemServico` (`idOrdemServico`),
  KEY `DataRequisicao` (`DataRequisicao`),
  KEY `LidoInternet` (`datalidointernet`),
  KEY `index01_na_requisicao` (`idNA_Requisicao`,`DataRequisicao`,`idOrdemServico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `rep_auxiliar`
--
DROP TABLE IF EXISTS `rep_auxiliar`;
CREATE TABLE `rep_auxiliar` (
  `Id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `idRepresentante` int(11) DEFAULT NULL,
  `Nome` varchar(50) DEFAULT NULL,
  `Ender` varchar(35) DEFAULT NULL,
  `Cidade` varchar(35) DEFAULT NULL,
  `Bairro` varchar(25) DEFAULT NULL,
  `UF` char(2) DEFAULT NULL,
  `CEP` int(11) DEFAULT NULL,
  `Fone` varchar(20) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `DtNasc` date DEFAULT NULL,
  `Rg` varchar(20) DEFAULT NULL,
  `Cpf` varchar(14) DEFAULT NULL,
  `Tipo` set('Coleta','Representação') DEFAULT NULL,
  `Celular` varchar(20) DEFAULT NULL,
  `identif` varchar(15) DEFAULT NULL,
  `senha` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `paciente_pesquisa`
--
DROP TABLE IF EXISTS `paciente_pesquisa`;
CREATE TABLE `paciente_pesquisa` (
  `pesid` int(11) NOT NULL AUTO_INCREMENT COMMENT '											',
  `pesPct` int(11) NOT NULL DEFAULT '0',
  `pesDataHora` datetime DEFAULT NULL,
  PRIMARY KEY (`pesid`,`pesPct`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `log_empresa`
--
DROP TABLE IF EXISTS `log_empresa`;
CREATE TABLE `log_empresa` (
  `idlog_empresa` int(11) NOT NULL AUTO_INCREMENT,
  `data` datetime NOT NULL,
  `quem` int(11) NOT NULL,
  `idempguia` int(11) NOT NULL,
  `operacao` varchar(10) NOT NULL,
  `conteudo` text,
  PRIMARY KEY (`idlog_empresa`),
  KEY `index2` (`data`),
  KEY `index3` (`idempguia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `stat_exame_original`
--
DROP TABLE IF EXISTS `stat_exame_original`;
CREATE TABLE `stat_exame_original` (
  `idstat_exame` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `area` char(1) DEFAULT NULL,
  `Entidade` int(10) unsigned DEFAULT NULL,
  `exame` varchar(10) DEFAULT NULL,
  `etapa` tinyint(4) DEFAULT NULL,
  `Data` varchar(8) DEFAULT NULL,
  `qtde` int(11) DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `idEmpresa` int(10) unsigned DEFAULT NULL,
  `idMedico` int(10) unsigned DEFAULT '0',
  `qtdePct` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`idstat_exame`),
  KEY `area` (`area`,`Entidade`,`exame`,`Data`),
  KEY `etapa` (`etapa`,`Data`),
  KEY `stat_exame_medico` (`area`,`idMedico`,`exame`,`Data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `exames_com_atraso`
--
DROP TABLE IF EXISTS `exames_com_atraso`;
CREATE TABLE `exames_com_atraso` (
  `ecaOs` int(11) NOT NULL DEFAULT '0',
  `ecaExames` varchar(50) DEFAULT NULL,
  `ecaData` datetime DEFAULT NULL,
  `ecaQuemCadastrou` int(11) DEFAULT NULL,
  `ecaMotivo` tinytext,
  `ecaObs` tinytext,
  PRIMARY KEY (`ecaOs`),
  KEY `data` (`ecaData`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `informe_impressao`
--
DROP TABLE IF EXISTS `informe_impressao`;
CREATE TABLE `informe_impressao` (
  `idEntidade` int(10) unsigned NOT NULL DEFAULT '0',
  `idInforme` int(10) unsigned NOT NULL DEFAULT '0',
  `Data` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`idEntidade`,`idInforme`,`Data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `celula_email`
--
DROP TABLE IF EXISTS `celula_email`;
CREATE TABLE `celula_email` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `idCelula` int(11) DEFAULT NULL,
  `tipoEmail` varchar(25) DEFAULT NULL,
  `email` varchar(125) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `idCelula_idx` (`idCelula`),
  CONSTRAINT `idCelula` FOREIGN KEY (`idCelula`) REFERENCES `celula` (`idCelula`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `paciente_especie`
--
DROP TABLE IF EXISTS `paciente_especie`;
CREATE TABLE `paciente_especie` (
  `idPct` int(10) unsigned NOT NULL DEFAULT '0',
  `idEspecie` int(10) unsigned DEFAULT NULL,
  `idRaca` int(10) unsigned DEFAULT NULL,
  `idProprietario` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`idPct`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `log_exclusao_descentralizacao`
--
DROP TABLE IF EXISTS `log_exclusao_descentralizacao`;
CREATE TABLE `log_exclusao_descentralizacao` (
  `idlog_exclusao_descentralizacao` int(11) NOT NULL AUTO_INCREMENT,
  `Data` datetime DEFAULT NULL,
  `Exame` varchar(10) DEFAULT NULL,
  `Entidade` varchar(10) DEFAULT NULL,
  `Motivo` varchar(150) DEFAULT NULL,
  `Destino` int(11) DEFAULT NULL,
  `Quem` int(11) DEFAULT NULL,
  PRIMARY KEY (`idlog_exclusao_descentralizacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `relatorioconfig`
--
DROP TABLE IF EXISTS `relatorioconfig`;
CREATE TABLE `relatorioconfig` (
  `rcfId` int(11) NOT NULL AUTO_INCREMENT,
  `relId` int(11) DEFAULT NULL,
  `rcfHora` varchar(5) DEFAULT NULL,
  `rcfHostProprio` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`rcfId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `bandejasregra`
--
DROP TABLE IF EXISTS `bandejasregra`;
CREATE TABLE `bandejasregra` (
  `idbandejasRegra` int(11) NOT NULL AUTO_INCREMENT,
  `idbandeja1` int(11) DEFAULT NULL,
  `idbandeja2` int(11) DEFAULT NULL,
  `idbandeja3` int(11) DEFAULT NULL,
  `resultado` int(11) NOT NULL,
  `prioridade` int(11) DEFAULT NULL,
  `congelado` int(2) DEFAULT NULL,
  PRIMARY KEY (`idbandejasRegra`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `localexecucaoimpostos`
--
DROP TABLE IF EXISTS `localexecucaoimpostos`;
CREATE TABLE `localexecucaoimpostos` (
  `idlocalExecucaoImpostos` int(11) NOT NULL AUTO_INCREMENT,
  `dessid` int(11) NOT NULL,
  `vlrIr` int(11) NOT NULL,
  `vlrContrib` int(11) NOT NULL,
  `vlrPis` int(11) NOT NULL,
  `vlrIssqn` int(11) NOT NULL,
  `vlrCofins` int(11) NOT NULL,
  PRIMARY KEY (`idlocalExecucaoImpostos`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `tp_acidente`
--
DROP TABLE IF EXISTS `tp_acidente`;
CREATE TABLE `tp_acidente` (
  `idTp_acidente` int(11) NOT NULL AUTO_INCREMENT,
  `TpcodAcidente` int(11) DEFAULT NULL,
  `TpDescricao` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idTp_acidente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `dnapaciente`
--
DROP TABLE IF EXISTS `dnapaciente`;
CREATE TABLE `dnapaciente` (
  `PacID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PacPai` varchar(60) DEFAULT NULL,
  `PacMae` varchar(60) DEFAULT NULL,
  `PacFilho` varchar(60) DEFAULT NULL,
  `PacDtNasc` date DEFAULT NULL,
  PRIMARY KEY (`PacID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `rotpos`
--
DROP TABLE IF EXISTS `rotpos`;
CREATE TABLE `rotpos` (
  `LCEST` varchar(20) DEFAULT NULL,
  `LCDATA` datetime DEFAULT NULL,
  `LCPOSICAO` varchar(5) DEFAULT NULL,
  `LCARRAY` double DEFAULT NULL,
  `LCSTATE` int(11) DEFAULT NULL,
  `LCSID` int(11) DEFAULT NULL,
  `LCCODPAS` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `srp_estantes`
--
DROP TABLE IF EXISTS `srp_estantes`;
CREATE TABLE `srp_estantes` (
  `estid` int(11) NOT NULL AUTO_INCREMENT,
  `ESTCOD` varchar(12) DEFAULT NULL,
  `ESTNOME` varchar(20) DEFAULT NULL,
  `ESTSETOR` varchar(40) DEFAULT NULL,
  `ESTFREEZER` smallint(6) DEFAULT NULL,
  `estvalidade` datetime DEFAULT NULL,
  `ESTOBS` varchar(200) DEFAULT NULL,
  `ESTV` smallint(6) DEFAULT NULL,
  `ESTH` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`estid`),
  KEY `AK1EstantesCod` (`ESTCOD`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `alqma_dadosadicionais`
--
DROP TABLE IF EXISTS `alqma_dadosadicionais`;
CREATE TABLE `alqma_dadosadicionais` (
  `idOrdem` int(11) NOT NULL DEFAULT '0',
  `RespColeta` varchar(30) DEFAULT NULL,
  `UtilizacaoAmostra` varchar(40) DEFAULT '',
  `OrigemAmostra` varchar(40) DEFAULT '',
  `LocalColeta` varchar(30) DEFAULT NULL,
  `PontoColeta` varchar(50) DEFAULT NULL,
  `DataColeta` varchar(13) DEFAULT NULL,
  `Horacoleta` varchar(13) DEFAULT NULL,
  `temperaturacoleta` varchar(13) DEFAULT NULL,
  `dataremessa` varchar(13) DEFAULT NULL,
  `ResponsavelColeta` varchar(30) DEFAULT NULL,
  `tempamostra` varchar(13) DEFAULT NULL,
  `ResParmNorm` varchar(100) DEFAULT NULL,
  `Rescomentario` text,
  `ResAmostra` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idOrdem`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `fornecedor`
--
DROP TABLE IF EXISTS `fornecedor`;
CREATE TABLE `fornecedor` (
  `idFornecedor` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(40) DEFAULT NULL,
  `Endereco` varchar(40) DEFAULT NULL,
  `Bairro` varchar(20) DEFAULT NULL,
  `Cidade` varchar(20) DEFAULT NULL,
  `estado` enum('RS','SC','PR','SP','MG','RJ','ES','MS','MT','GO','TO','PA','AM','RO','RR','AC','DF','BA','SE','CE','PI','PB','RN','AL','MA','PE','AP','XX') DEFAULT NULL,
  `Cep` varchar(9) DEFAULT NULL,
  `Fone` varchar(20) DEFAULT NULL,
  `Fax` varchar(20) DEFAULT NULL,
  `Email` varchar(60) DEFAULT NULL,
  `Contato` varchar(40) DEFAULT NULL,
  `CallCenter` varchar(20) DEFAULT NULL,
  `Fone2` varchar(20) DEFAULT NULL,
  `CNPJ` varchar(18) DEFAULT NULL,
  `inscricaoEstadual` varchar(14) DEFAULT NULL,
  `inscricaoMunicipal` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`idFornecedor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `logistica_envioentidade`
--
DROP TABLE IF EXISTS `logistica_envioentidade`;
CREATE TABLE `logistica_envioentidade` (
  `idEnvio` int(11) NOT NULL AUTO_INCREMENT,
  `idPedido` int(11) DEFAULT NULL,
  `dtEnvio` datetime DEFAULT NULL,
  `idResponsavel` int(11) DEFAULT NULL,
  `idRespConferencia` int(11) DEFAULT NULL,
  `QtdVolumes` int(11) DEFAULT '1',
  `idTransportadora` int(11) DEFAULT NULL,
  `destinatario` varchar(50) DEFAULT NULL,
  `CdgRastreioTransportadora` varchar(50) DEFAULT NULL,
  `statusEnvio` enum('aguardando','enviado','recebido') DEFAULT 'aguardando',
  `dtRecebimento` date DEFAULT NULL,
  PRIMARY KEY (`idEnvio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `marca_entidade`
--
DROP TABLE IF EXISTS `marca_entidade`;
CREATE TABLE `marca_entidade` (
  `idEntidade` int(11) NOT NULL,
  `idMarca` int(11) NOT NULL,
  PRIMARY KEY (`idEntidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `manutencao_ocorrencia`
--
DROP TABLE IF EXISTS `manutencao_ocorrencia`;
CREATE TABLE `manutencao_ocorrencia` (
  `idOcorrencia` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idManutencao` int(10) unsigned NOT NULL DEFAULT '0',
  `idTpOcorrEquip` int(10) unsigned NOT NULL DEFAULT '0',
  `idErro` int(10) unsigned DEFAULT NULL,
  `StatusEquipamento` enum('Normal','Inativo','Parcial') DEFAULT 'Normal',
  `Comentario` text,
  PRIMARY KEY (`idOcorrencia`,`idManutencao`),
  KEY `Ocorrencia_FKIndex1` (`idTpOcorrEquip`),
  KEY `Ocorrencia_FKIndex2` (`idManutencao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `medicamentos`
--
DROP TABLE IF EXISTS `medicamentos`;
CREATE TABLE `medicamentos` (
  `mdcsid` int(11) NOT NULL AUTO_INCREMENT,
  `MDCNOME` varchar(40) DEFAULT NULL,
  `MDCTRATAMENTO` varchar(50) DEFAULT NULL,
  `MDCINTERFERENCIA` varchar(100) DEFAULT NULL,
  `mdcDtUltAlteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`mdcsid`),
  KEY `AK1Medicamentosnome` (`MDCNOME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `textoliberacao_exames`
--
DROP TABLE IF EXISTS `textoliberacao_exames`;
CREATE TABLE `textoliberacao_exames` (
  `idTxtLiberacao` int(10) unsigned NOT NULL DEFAULT '0',
  `exaCodigo` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`idTxtLiberacao`,`exaCodigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `bkp_stat_exame`
--
DROP TABLE IF EXISTS `bkp_stat_exame`;
CREATE TABLE `bkp_stat_exame` (
  `idstat_exame` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `area` char(1) DEFAULT NULL,
  `Entidade` int(10) unsigned DEFAULT NULL,
  `exame` varchar(10) DEFAULT NULL,
  `etapa` tinyint(4) DEFAULT NULL,
  `Data` varchar(8) DEFAULT NULL,
  `qtde` int(11) DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `idEmpresa` int(10) unsigned DEFAULT NULL,
  `idMedico` int(10) unsigned DEFAULT '0',
  `qtdePct` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`idstat_exame`),
  KEY `area` (`area`,`Entidade`,`exame`,`Data`),
  KEY `etapa` (`etapa`,`Data`),
  KEY `stat_exame_medico` (`area`,`idMedico`,`exame`,`Data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `exames_solicitados_historico`
--
DROP TABLE IF EXISTS `exames_solicitados_historico`;
CREATE TABLE `exames_solicitados_historico` (
  `solsid` int(11) NOT NULL AUTO_INCREMENT,
  `SOLSIDORDEM` int(11) DEFAULT NULL,
  `solExame` varchar(10) DEFAULT NULL,
  `solControleAol` char(1) DEFAULT 'N',
  `solVlrExame` decimal(11,2) DEFAULT '0.00',
  `SOLFORMAEXECUCAO` char(1) DEFAULT NULL,
  `SolObsResultado` text,
  `soldttransmissao` datetime DEFAULT NULL,
  `soldtimpressao` datetime DEFAULT NULL,
  `SOLLIBERADO` char(1) DEFAULT NULL,
  `SOLQUEMLIBEROU` smallint(6) DEFAULT NULL,
  `soldtliberacao` datetime DEFAULT NULL,
  `SOLQUEMDIGITOU` smallint(6) DEFAULT NULL,
  `soldtdigitacao` datetime DEFAULT NULL,
  `SOLSETOR` char(3) DEFAULT NULL,
  `SOLBOLETOIMPRESSO` char(1) DEFAULT NULL,
  `SOLMATERIAL` int(11) DEFAULT NULL,
  `SOLESTANTE` varchar(10) DEFAULT NULL,
  `solMarcado` char(1) DEFAULT '',
  `solCodExComposto` varchar(10) DEFAULT NULL,
  `solInterfaceado` char(1) DEFAULT 'N',
  `SolDtGeradoInternet` datetime DEFAULT NULL,
  `SolDestGeradoInternet` varchar(5) DEFAULT NULL,
  `solBioqLiberou` int(11) DEFAULT NULL,
  `solDtEntr` datetime DEFAULT NULL,
  `solQuemCadastrou` smallint(6) unsigned DEFAULT '0',
  `solLimbo` char(1) DEFAULT 'N',
  `solCriterioLiberacao` text,
  `solQtdAmostras` smallint(6) DEFAULT '1',
  `solDtRedigitacao` datetime DEFAULT NULL,
  `solMotivoRedigitacao` tinytext,
  `solQuemRedigitou` smallint(6) DEFAULT NULL,
  `solDtLidoInternetPct` datetime DEFAULT NULL,
  `solMedico` int(10) unsigned DEFAULT NULL,
  `solEntidade` int(10) unsigned DEFAULT NULL,
  `solConvenio` int(10) unsigned DEFAULT NULL,
  `solvalordesconto` decimal(9,2) DEFAULT '0.00',
  `SolNroFatura` int(10) unsigned DEFAULT NULL,
  `solDtLidoInternetMed` datetime DEFAULT NULL,
  `solTuboUnico` char(1) DEFAULT 'N',
  `solPoucaAmostra` char(1) DEFAULT 'N',
  PRIMARY KEY (`solsid`),
  KEY `solMedico` (`solMedico`,`solDtEntr`),
  KEY `solConvenio` (`solConvenio`,`solDtEntr`),
  KEY `soldtdigitacao` (`soldtdigitacao`),
  KEY `SOLSIDORDEM` (`SOLSIDORDEM`),
  KEY `solDtEntr` (`solDtEntr`),
  KEY `solEntidade` (`solEntidade`,`solDtEntr`),
  KEY `solExame` (`solExame`,`solDtEntr`),
  KEY `SOLLIBERADO` (`SOLLIBERADO`),
  KEY `soldtliberacao` (`soldtliberacao`),
  KEY `solQuemCadastrou` (`solQuemCadastrou`),
  KEY `solQuemRedigitou` (`solQuemRedigitou`),
  KEY `SOLQUEMDIGITOU` (`SOLQUEMDIGITOU`),
  KEY `SOLQUEMLIBEROU` (`SOLQUEMLIBEROU`),
  KEY `index01_exames_sol` (`SOLSIDORDEM`,`solExame`,`solEntidade`,`solDtEntr`),
  KEY `idx_exames_solicitados_solDtRedigitacao` (`solDtRedigitacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `resultadocurva`
--
DROP TABLE IF EXISTS `resultadocurva`;
CREATE TABLE `resultadocurva` (
  `solsid` int(10) unsigned NOT NULL DEFAULT '0',
  `idCurva` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `pontos` mediumtext,
  PRIMARY KEY (`solsid`,`idCurva`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `processa_estatistica`
--
DROP TABLE IF EXISTS `processa_estatistica`;
CREATE TABLE `processa_estatistica` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idArea` tinyint(3) unsigned NOT NULL,
  `idEntidade` int(10) unsigned NOT NULL,
  `exame` char(10) NOT NULL,
  `data` date NOT NULL,
  `idMedico` int(10) unsigned NOT NULL,
  `quantidade` int(11) NOT NULL,
  `valor` decimal(10,4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `stat_exame_historico_producao_parcial_02_script_antigo`
--
DROP TABLE IF EXISTS `stat_exame_historico_producao_parcial_02_script_antigo`;
CREATE TABLE `stat_exame_historico_producao_parcial_02_script_antigo` (
  `idstat_exame` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `area` char(1) DEFAULT NULL,
  `Entidade` int(10) unsigned DEFAULT NULL,
  `exame` varchar(10) DEFAULT NULL,
  `etapa` tinyint(4) DEFAULT NULL,
  `Data` varchar(8) DEFAULT NULL,
  `qtde` int(11) DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `idEmpresa` int(10) unsigned DEFAULT NULL,
  `idMedico` int(10) unsigned DEFAULT '0',
  `qtdePct` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`idstat_exame`),
  KEY `area` (`area`,`Entidade`,`exame`,`Data`),
  KEY `etapa` (`etapa`,`Data`),
  KEY `stat_exame_medico` (`area`,`idMedico`,`exame`,`Data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `categorias`
--
DROP TABLE IF EXISTS `categorias`;
CREATE TABLE `categorias` (
  `catCodigo` varchar(6) DEFAULT NULL,
  `CATNOME` varchar(40) DEFAULT NULL,
  `catsid` int(11) NOT NULL AUTO_INCREMENT,
  `catGestor` int(11) DEFAULT NULL,
  `catRamal` varchar(20) DEFAULT NULL,
  `catEmail` varchar(50) DEFAULT NULL,
  `catIdUnidade` int(11) DEFAULT '0',
  PRIMARY KEY (`catsid`),
  KEY `Codigo` (`catCodigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `chegada_material_local`
--
DROP TABLE IF EXISTS `chegada_material_local`;
CREATE TABLE `chegada_material_local` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Data` datetime DEFAULT NULL,
  `Temperatura` decimal(4,1) DEFAULT NULL,
  `idQuemRegistrou` int(11) DEFAULT NULL,
  `QuemTransportou` varchar(30) DEFAULT NULL,
  `Observacao` varchar(100) DEFAULT NULL,
  `idcaixa` int(11) DEFAULT NULL,
  `origem` varchar(100) DEFAULT NULL,
  `tipoEntrada` enum('automatizada','manual') DEFAULT 'automatizada',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `logistica_transportadora_regiao`
--
DROP TABLE IF EXISTS `logistica_transportadora_regiao`;
CREATE TABLE `logistica_transportadora_regiao` (
  `idTransportadora` int(11) NOT NULL,
  `idRepresentante` int(11) NOT NULL,
  `idRota` int(11) NOT NULL,
  `diasUteis` int(11) NOT NULL DEFAULT '1',
  `ordem` int(2) DEFAULT NULL,
  PRIMARY KEY (`idTransportadora`,`idRepresentante`,`idRota`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `na_pergunta`
--
DROP TABLE IF EXISTS `na_pergunta`;
CREATE TABLE `na_pergunta` (
  `idPergunta` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `Descricao` text,
  `DtInicio` datetime DEFAULT NULL,
  `DtEncerramento` datetime DEFAULT NULL,
  PRIMARY KEY (`idPergunta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `na_motivorequisicao`
--
DROP TABLE IF EXISTS `na_motivorequisicao`;
CREATE TABLE `na_motivorequisicao` (
  `idNA_MotivoRequisicao` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idNA_Requisicao` int(10) unsigned NOT NULL DEFAULT '0',
  `idNA_Motivo` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idNA_MotivoRequisicao`),
  KEY `idNA_Motivo` (`idNA_Motivo`),
  KEY `idNA_Requisicao` (`idNA_Requisicao`),
  KEY `index01_na_motivorequisicao` (`idNA_Requisicao`,`idNA_Motivo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `tipolote`
--
DROP TABLE IF EXISTS `tipolote`;
CREATE TABLE `tipolote` (
  `id` varchar(10) NOT NULL,
  `idDestino` int(11) NOT NULL,
  `idAcondicionamento` int(11) NOT NULL,
  `corCaixa` varchar(150) NOT NULL,
  `qtdeAmostras` int(11) NOT NULL,
  `codigoDestino` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `idDestino` (`idDestino`),
  KEY `idAcondicionamento` (`idAcondicionamento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `logistica_celulaestoque`
--
DROP TABLE IF EXISTS `logistica_celulaestoque`;
CREATE TABLE `logistica_celulaestoque` (
  `idEstoque` int(11) NOT NULL AUTO_INCREMENT,
  `idCelula` int(11) NOT NULL,
  `idEnvio` int(11) NOT NULL,
  `idItemPedido` int(11) NOT NULL,
  `qtdRecebida` int(11) DEFAULT NULL,
  `qtdEstoque` int(11) DEFAULT NULL,
  PRIMARY KEY (`idEstoque`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `wl_llr_adminserver`
--
DROP TABLE IF EXISTS `wl_llr_adminserver`;
CREATE TABLE `wl_llr_adminserver` (
  `XIDSTR` varchar(40) NOT NULL,
  `POOLNAMESTR` varchar(64) DEFAULT NULL,
  `RECORDSTR` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`XIDSTR`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `exames_solicitados_producao`
--
DROP TABLE IF EXISTS `exames_solicitados_producao`;
CREATE TABLE `exames_solicitados_producao` (
  `solsid` int(11) NOT NULL AUTO_INCREMENT,
  `SOLSIDORDEM` int(11) DEFAULT NULL,
  `solExame` varchar(10) DEFAULT NULL,
  `solControleAol` char(1) DEFAULT 'N',
  `solVlrExame` decimal(11,2) DEFAULT '0.00',
  `SOLFORMAEXECUCAO` char(1) DEFAULT NULL,
  `SolObsResultado` text,
  `soldttransmissao` datetime DEFAULT NULL,
  `soldtimpressao` datetime DEFAULT NULL,
  `SOLLIBERADO` char(1) DEFAULT NULL,
  `SOLQUEMLIBEROU` smallint(6) DEFAULT NULL,
  `soldtliberacao` datetime DEFAULT NULL,
  `SOLQUEMDIGITOU` smallint(6) DEFAULT NULL,
  `soldtdigitacao` datetime DEFAULT NULL,
  `SOLSETOR` char(3) DEFAULT NULL,
  `SOLBOLETOIMPRESSO` char(1) DEFAULT NULL,
  `SOLMATERIAL` int(11) DEFAULT NULL,
  `SOLESTANTE` varchar(10) DEFAULT NULL,
  `solMarcado` char(1) DEFAULT '',
  `solCodExComposto` varchar(10) DEFAULT NULL,
  `solInterfaceado` char(1) DEFAULT 'N',
  `SolDtGeradoInternet` datetime DEFAULT NULL,
  `SolDestGeradoInternet` varchar(5) DEFAULT NULL,
  `solBioqLiberou` int(11) DEFAULT NULL,
  `solDtEntr` datetime DEFAULT NULL,
  `solQuemCadastrou` smallint(6) unsigned DEFAULT '0',
  `solLimbo` char(1) DEFAULT 'N',
  `solCriterioLiberacao` text,
  `solQtdAmostras` smallint(6) DEFAULT '1',
  `solDtRedigitacao` datetime DEFAULT NULL,
  `solMotivoRedigitacao` tinytext,
  `solQuemRedigitou` smallint(6) DEFAULT NULL,
  `solDtLidoInternetPct` datetime DEFAULT NULL,
  `solMedico` int(10) unsigned DEFAULT NULL,
  `solEntidade` int(10) unsigned DEFAULT NULL,
  `solConvenio` int(10) unsigned DEFAULT NULL,
  `solvalordesconto` decimal(9,2) DEFAULT '0.00',
  `SolNroFatura` int(10) unsigned DEFAULT NULL,
  `solDtLidoInternetMed` datetime DEFAULT NULL,
  `solTuboUnico` char(1) DEFAULT 'N',
  `solPoucaAmostra` char(1) DEFAULT 'N',
  PRIMARY KEY (`solsid`),
  KEY `solMedico` (`solMedico`,`solDtEntr`),
  KEY `solConvenio` (`solConvenio`,`solDtEntr`),
  KEY `soldtdigitacao` (`soldtdigitacao`),
  KEY `SOLSIDORDEM` (`SOLSIDORDEM`),
  KEY `solDtEntr` (`solDtEntr`),
  KEY `solEntidade` (`solEntidade`,`solDtEntr`),
  KEY `solExame` (`solExame`,`solDtEntr`),
  KEY `SOLLIBERADO` (`SOLLIBERADO`),
  KEY `soldtliberacao` (`soldtliberacao`),
  KEY `solQuemCadastrou` (`solQuemCadastrou`),
  KEY `solQuemRedigitou` (`solQuemRedigitou`),
  KEY `SOLQUEMDIGITOU` (`SOLQUEMDIGITOU`),
  KEY `SOLQUEMLIBEROU` (`SOLQUEMLIBEROU`),
  KEY `index01_exames_sol` (`SOLSIDORDEM`,`solExame`,`solEntidade`,`solDtEntr`),
  KEY `idx_exames_solicitados_solDtRedigitacao` (`solDtRedigitacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `log_tabrecepcao`
--
DROP TABLE IF EXISTS `log_tabrecepcao`;
CREATE TABLE `log_tabrecepcao` (
  `idlogtabRecepcao` int(11) NOT NULL AUTO_INCREMENT,
  `idTabRecepcao` int(11) DEFAULT NULL,
  `idflabfunc` int(11) DEFAULT NULL,
  `operacao` enum('Inclusao','Alteracao','Exclusao') DEFAULT NULL,
  `Motivo` varchar(250) DEFAULT NULL,
  `Alteracao` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idlogtabRecepcao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `prefeituras`
--
DROP TABLE IF EXISTS `prefeituras`;
CREATE TABLE `prefeituras` (
  `preIdPref` int(11) NOT NULL AUTO_INCREMENT,
  `preNomePref` varchar(45) DEFAULT NULL,
  `preNomeSecretariaPref` varchar(45) DEFAULT NULL,
  `preEnderecoPref` varchar(45) DEFAULT NULL,
  `preNumeroPref` varchar(6) DEFAULT NULL,
  `preBairroPref` varchar(45) DEFAULT NULL,
  `preCidadePref` varchar(45) DEFAULT NULL,
  `preUFPref` varchar(2) DEFAULT NULL,
  `preFonePref` varchar(45) DEFAULT NULL,
  `preCEPPref` varchar(10) DEFAULT NULL,
  `preLogoPref` blob,
  PRIMARY KEY (`preIdPref`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `campo_complementar_resultado`
--
DROP TABLE IF EXISTS `campo_complementar_resultado`;
CREATE TABLE `campo_complementar_resultado` (
  `ccrID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ccrSolSid` int(11) DEFAULT NULL,
  `ccrIdCCO` int(10) unsigned NOT NULL DEFAULT '0',
  `ccrResultado` text,
  PRIMARY KEY (`ccrID`),
  KEY `ccrSolSid` (`ccrSolSid`),
  KEY `ccrIdCCO` (`ccrIdCCO`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `refvalperfilsumario`
--
DROP TABLE IF EXISTS `refvalperfilsumario`;
CREATE TABLE `refvalperfilsumario` (
  `idPerfil` int(11) NOT NULL DEFAULT '0',
  `Equipamento` varchar(100) NOT NULL DEFAULT '',
  `Data` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Media` decimal(10,2) NOT NULL DEFAULT '0.00',
  `Mediana` decimal(10,2) NOT NULL DEFAULT '0.00',
  `Quartil1` decimal(10,2) NOT NULL DEFAULT '0.00',
  `Quartil3` decimal(10,2) NOT NULL DEFAULT '0.00',
  `QtdAmostras` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Data`,`Equipamento`,`idPerfil`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `log_exames_template`
--
DROP TABLE IF EXISTS `log_exames_template`;
CREATE TABLE `log_exames_template` (
  `idlog_exames_template` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_exame` varchar(45) DEFAULT NULL,
  `template` varchar(80) DEFAULT NULL,
  `versao` int(11) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `usuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`idlog_exames_template`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `pacientes`
--
DROP TABLE IF EXISTS `pacientes`;
CREATE TABLE `pacientes` (
  `PctSid` int(11) NOT NULL AUTO_INCREMENT,
  `PctNome` varchar(60) DEFAULT NULL,
  `PctNomeSocial` varchar(60) DEFAULT NULL,
  `PctCidade` varchar(50) DEFAULT NULL,
  `PctEnder` varchar(40) DEFAULT NULL,
  `PctEstado` char(2) DEFAULT NULL,
  `PctCep` varchar(8) DEFAULT NULL,
  `PctFone` varchar(60) DEFAULT NULL,
  `PctFax` varchar(20) DEFAULT NULL,
  `PctEmail` varchar(40) DEFAULT NULL,
  `PctSexo` char(1) DEFAULT NULL,
  `PctDtNasc` date DEFAULT '1900-01-01',
  `PctObs` varchar(50) DEFAULT NULL,
  `PctRetirar` varchar(10) DEFAULT NULL,
  `PctIdentif` varchar(10) DEFAULT NULL,
  `PctSenha` varchar(10) DEFAULT NULL,
  `PctArea` smallint(6) DEFAULT NULL,
  `PctDtCadastro` datetime DEFAULT NULL,
  `PctPrimeiraVez` char(1) DEFAULT NULL,
  `PctBairro` varchar(20) DEFAULT NULL,
  `PctDtCartao` datetime DEFAULT NULL,
  `PctConvenio` int(11) DEFAULT NULL,
  `PctEAnimal` char(1) DEFAULT NULL,
  `PctDtUltExame` datetime DEFAULT NULL,
  `PctQuemAlterou` int(11) DEFAULT NULL,
  `PctCodConvenio` varchar(20) DEFAULT NULL,
  `PctDtValidadeConvenio` date DEFAULT NULL,
  `PctCelular` varchar(12) DEFAULT NULL,
  `PctTipoDoc` varchar(10) DEFAULT NULL,
  `PctNumDoc` varchar(20) DEFAULT NULL,
  `pctDtUltAlteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pctrua` varchar(40) DEFAULT NULL,
  `pctnumero` varchar(10) DEFAULT NULL,
  `pctcomplemento` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`PctSid`),
  KEY `AK2PacientesNome` (`PctNome`),
  KEY `pctDtUltAlteracao` (`pctDtUltAlteracao`),
  KEY `PctIdentif` (`PctIdentif`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `pfora_exames`
--
DROP TABLE IF EXISTS `pfora_exames`;
CREATE TABLE `pfora_exames` (
  `pfeExame` varchar(10) DEFAULT NULL,
  `pfeNomeExame` varchar(60) DEFAULT NULL,
  `pfeIdEntidade` int(11) DEFAULT NULL,
  `pfeTempo` smallint(6) DEFAULT '0',
  `pfePreco` decimal(12,2) DEFAULT NULL,
  `pfeSid` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`pfeSid`),
  UNIQUE KEY `pfeNomeExame` (`pfeNomeExame`,`pfeIdEntidade`),
  KEY `idExame` (`pfeExame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `tempo`
--
DROP TABLE IF EXISTS `tempo`;
CREATE TABLE `tempo` (
  `tmpsolsid` int(10) unsigned NOT NULL DEFAULT '0',
  `tmpNroAmostra` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `tmpComplDescricao` varchar(20) DEFAULT NULL,
  `tmpOrdem` int(11) DEFAULT NULL,
  `tmpMaterial` int(10) unsigned DEFAULT NULL,
  `temperatura` varchar(6) DEFAULT NULL,
  `tmpNtoExecucao` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`tmpsolsid`,`tmpNroAmostra`),
  KEY `tmpsolsidordem` (`tmpOrdem`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `entidade_licitacao`
--
DROP TABLE IF EXISTS `entidade_licitacao`;
CREATE TABLE `entidade_licitacao` (
  `ident_lic` int(11) NOT NULL AUTO_INCREMENT,
  `idEntidade` int(11) NOT NULL,
  `possuiLic` char(1) DEFAULT NULL,
  `termo` varchar(30) NOT NULL,
  `dtIniLic` datetime NOT NULL,
  `dtFimLic` datetime NOT NULL,
  `prazoRen` varchar(45) DEFAULT NULL,
  `obs` varchar(255) DEFAULT NULL,
  `idFunc` int(11) NOT NULL,
  `dtIncLic` datetime NOT NULL,
  PRIMARY KEY (`ident_lic`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `envia_email_anexo`
--
DROP TABLE IF EXISTS `envia_email_anexo`;
CREATE TABLE `envia_email_anexo` (
  `idenvia_email_anexo` int(11) NOT NULL AUTO_INCREMENT,
  `anexo_idEnviaEmail` int(11) DEFAULT NULL,
  `anexo_arquivo` longblob,
  `anexo_fileName` text,
  PRIMARY KEY (`idenvia_email_anexo`),
  KEY `anexo_idEnviaEmail` (`anexo_idEnviaEmail`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `agenda_coleta`
--
DROP TABLE IF EXISTS `agenda_coleta`;
CREATE TABLE `agenda_coleta` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idOrdem` int(10) unsigned DEFAULT NULL,
  `idPaciente` int(10) unsigned DEFAULT NULL,
  `DtParaColeta` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ordem` (`idOrdem`),
  KEY `data` (`DtParaColeta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `restricao_os`
--
DROP TABLE IF EXISTS `restricao_os`;
CREATE TABLE `restricao_os` (
  `idRestricaoOs` int(11) NOT NULL AUTO_INCREMENT,
  `idOrdemServico` int(11) NOT NULL,
  `textoRestricao` text,
  `idQuemCadastrou` int(11) DEFAULT NULL,
  `dtCadastro` datetime DEFAULT NULL,
  `idQuemLiberou` int(11) DEFAULT NULL,
  `dtLiberacao` datetime DEFAULT NULL,
  `imprimirNoLaudo` char(1) DEFAULT 'N',
  `idExameSolicitado` int(11) DEFAULT NULL,
  PRIMARY KEY (`idRestricaoOs`),
  KEY `dtCad` (`dtCadastro`),
  KEY `index01_idordemservico` (`idOrdemServico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `qc_analiseresultados`
--
DROP TABLE IF EXISTS `qc_analiseresultados`;
CREATE TABLE `qc_analiseresultados` (
  `idQCAnalise` int(11) NOT NULL AUTO_INCREMENT,
  `idExame` varchar(10) DEFAULT NULL,
  `idCritica` int(11) DEFAULT NULL,
  `dtBase` date DEFAULT NULL,
  `dtAnalise` datetime DEFAULT NULL,
  `qtd` int(11) DEFAULT '0',
  `media` decimal(10,3) DEFAULT NULL,
  `quartil1` decimal(10,3) DEFAULT NULL,
  `quartil3` decimal(10,3) DEFAULT NULL,
  `desvPadrao` decimal(10,3) DEFAULT NULL,
  `minimo` decimal(10,3) DEFAULT NULL,
  `maximo` decimal(10,3) DEFAULT NULL,
  `grupo` enum('1','2','3') DEFAULT '1',
  `notificacaoEnviada` enum('sim','não') DEFAULT 'não',
  PRIMARY KEY (`idQCAnalise`),
  KEY `idxData` (`dtBase`),
  KEY `idxExame` (`idExame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `texto_padrao`
--
DROP TABLE IF EXISTS `texto_padrao`;
CREATE TABLE `texto_padrao` (
  `Codigo` varchar(15) NOT NULL DEFAULT '',
  `Texto` text,
  PRIMARY KEY (`Codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `pm_categoria`
--
DROP TABLE IF EXISTS `pm_categoria`;
CREATE TABLE `pm_categoria` (
  `id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `descricao` char(25) NOT NULL DEFAULT '',
  `tipo` enum('Escala','Coeficiente') NOT NULL DEFAULT 'Coeficiente',
  `medidaBase` smallint(5) unsigned DEFAULT NULL,
  `dtUltAlteracao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `ultimodiaferiadofaturamento`
--
DROP TABLE IF EXISTS `ultimodiaferiadofaturamento`;
CREATE TABLE `ultimodiaferiadofaturamento` (
  `ultimoDiaUtil` date NOT NULL,
  PRIMARY KEY (`ultimoDiaUtil`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `estimulos`
--
DROP TABLE IF EXISTS `estimulos`;
CREATE TABLE `estimulos` (
  `estimulo` varchar(50) NOT NULL,
  `ativo` varchar(1) NOT NULL DEFAULT 'S',
  `flExcluido` varchar(1) DEFAULT 'N',
  `idEstimulos` int(3) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idEstimulos`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `container`
--
DROP TABLE IF EXISTS `container`;
CREATE TABLE `container` (
  `idCaixa` bigint(20) unsigned NOT NULL,
  `idOrdemServico` int(15) NOT NULL DEFAULT '0',
  `idFunc` int(11) DEFAULT NULL,
  `dtCadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `idSample` varchar(15) NOT NULL,
  `digitado` char(1) DEFAULT 'N',
  `Destino` char(1) DEFAULT 'A',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idCaixaDasa` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idordemservico` (`idOrdemServico`),
  KEY `idsample` (`idSample`),
  KEY `idcaixa` (`idCaixa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `imagem`
--
DROP TABLE IF EXISTS `imagem`;
CREATE TABLE `imagem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` text,
  `grupo` varchar(200) DEFAULT NULL,
  `img` longblob,
  `Data` date DEFAULT NULL,
  `Controle` char(1) DEFAULT 'N',
  `idExame` varchar(10) DEFAULT NULL,
  `Thumb` blob,
  PRIMARY KEY (`id`),
  KEY `data` (`Data`),
  KEY `exame` (`idExame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `entidade_cartao_fidelidade`
--
DROP TABLE IF EXISTS `entidade_cartao_fidelidade`;
CREATE TABLE `entidade_cartao_fidelidade` (
  `idEntidade` int(10) unsigned NOT NULL,
  `codCartao` varchar(22) DEFAULT NULL,
  `categoria` enum('Branco','Prata','Bordo') DEFAULT NULL,
  `dtEmissao` datetime DEFAULT NULL,
  `idQuemEmitiu` int(10) unsigned DEFAULT NULL,
  `diretor` varchar(50) DEFAULT NULL,
  `contato` varchar(50) DEFAULT NULL,
  `cargo` varchar(50) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idEntidade`),
  KEY `codCartao` (`codCartao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `sorotecaseparacaodescarte`
--
DROP TABLE IF EXISTS `sorotecaseparacaodescarte`;
CREATE TABLE `sorotecaseparacaodescarte` (
  `idExame` char(10) NOT NULL,
  `resultados` text,
  PRIMARY KEY (`idExame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `critica_parear`
--
DROP TABLE IF EXISTS `critica_parear`;
CREATE TABLE `critica_parear` (
  `idCriticaPrincipal` int(11) NOT NULL,
  `idCriticaAuxiliar` int(11) NOT NULL,
  PRIMARY KEY (`idCriticaPrincipal`,`idCriticaAuxiliar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `reccaixa_pagamento`
--
DROP TABLE IF EXISTS `reccaixa_pagamento`;
CREATE TABLE `reccaixa_pagamento` (
  `reccxsid` int(11) NOT NULL DEFAULT '0',
  `reccxpgmsid` int(11) NOT NULL AUTO_INCREMENT,
  `dta_pgmto` date DEFAULT NULL,
  `VLR_PAGO` decimal(12,2) DEFAULT NULL,
  `RESP_RECEBIMENTO` int(6) DEFAULT NULL,
  `RESP_FECHAMENTO` int(6) DEFAULT NULL,
  `GAVETA` varchar(40) DEFAULT NULL,
  `Tipo_Oper` enum('Pgt','Inc','Ext','Guia') DEFAULT NULL,
  `VLR_DESCONTO` decimal(12,2) DEFAULT NULL,
  `SEQCAIXA` int(10) unsigned DEFAULT '0',
  `MOTIVO_DESC` varchar(200) DEFAULT NULL,
  `dta_fechamento` datetime DEFAULT NULL,
  `notaFiscal` int(11) DEFAULT '0',
  `reccxpgNroRPS` int(11) DEFAULT '0',
  PRIMARY KEY (`reccxpgmsid`),
  KEY `dta_pgmto` (`dta_pgmto`),
  KEY `reccxsid` (`reccxsid`),
  KEY `index_RPS` (`reccxpgNroRPS`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `dna_resultado_alelo`
--
DROP TABLE IF EXISTS `dna_resultado_alelo`;
CREATE TABLE `dna_resultado_alelo` (
  `reaIdRes` int(11) NOT NULL DEFAULT '0',
  `reaTabela` enum('S','M','F','P') NOT NULL DEFAULT 'S',
  `reaIdDeQuem` int(11) NOT NULL DEFAULT '0',
  `reaAlelo1` varchar(10) DEFAULT NULL,
  `reaAlelo2` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`reaIdRes`,`reaTabela`,`reaIdDeQuem`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `ordem_servico_laudo_teste_06`
--
DROP TABLE IF EXISTS `ordem_servico_laudo_teste_06`;
CREATE TABLE `ordem_servico_laudo_teste_06` (
  `ldosid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `examespaternidade`
--
DROP TABLE IF EXISTS `examespaternidade`;
CREATE TABLE `examespaternidade` (
  `idExamePaternidade` tinyint(4) NOT NULL AUTO_INCREMENT,
  `exaCodigo` varchar(10) NOT NULL DEFAULT '',
  `idTipoParticipante` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`idExamePaternidade`,`exaCodigo`),
  KEY `exaCodigo` (`exaCodigo`),
  KEY `idTipoParticipante` (`idTipoParticipante`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `mensagens`
--
DROP TABLE IF EXISTS `mensagens`;
CREATE TABLE `mensagens` (
  `Id_Msg` int(11) NOT NULL AUTO_INCREMENT,
  `Descricao` varchar(150) NOT NULL,
  `Obs` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id_Msg`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `log_container`
--
DROP TABLE IF EXISTS `log_container`;
CREATE TABLE `log_container` (
  `idCaixa` int(10) NOT NULL,
  `idOrdemServico` varchar(12) NOT NULL DEFAULT '0',
  `idFunc` int(11) NOT NULL,
  `dtCadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `idSample` varchar(15) DEFAULT NULL,
  `digitado` char(1) DEFAULT 'N',
  `Destino` char(1) DEFAULT 'A',
  `id` int(11) DEFAULT NULL,
  `idCaixaDasa` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `critica_parear_fora`
--
DROP TABLE IF EXISTS `critica_parear_fora`;
CREATE TABLE `critica_parear_fora` (
  `idCriticaPrincipal` int(11) NOT NULL,
  `idCriticaAuxiliar` int(11) NOT NULL,
  PRIMARY KEY (`idCriticaPrincipal`,`idCriticaAuxiliar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `integracao_intersite`
--
DROP TABLE IF EXISTS `integracao_intersite`;
CREATE TABLE `integracao_intersite` (
  `solsid` int(10) unsigned NOT NULL,
  `solsidMatriz` int(10) unsigned NOT NULL DEFAULT '0',
  `dataProcesso` datetime NOT NULL,
  `dataLiberacao` datetime DEFAULT NULL,
  `dataTransferencia` datetime DEFAULT NULL,
  `dataResultado` datetime DEFAULT NULL,
  PRIMARY KEY (`solsid`),
  KEY `dataProcesso` (`dataProcesso`),
  KEY `dataResultado` (`dataResultado`),
  KEY `DataTransferencia` (`dataTransferencia`),
  KEY `dataLiberacao` (`dataLiberacao`),
  KEY `solsidMatriz` (`solsidMatriz`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `dnaresultado`
--
DROP TABLE IF EXISTS `dnaresultado`;
CREATE TABLE `dnaresultado` (
  `ResId` int(6) NOT NULL AUTO_INCREMENT,
  `ResData` date DEFAULT NULL,
  `PacId` int(11) unsigned DEFAULT NULL,
  `ResResultado` float DEFAULT NULL,
  `ResAleloFilho1` float DEFAULT NULL,
  `ResAleloFilho2` float DEFAULT NULL,
  `ResAleloMae1` float DEFAULT NULL,
  `ResAleloMae2` float DEFAULT NULL,
  `ResAleloPai1` float DEFAULT NULL,
  `ResAleloPai2` float DEFAULT NULL,
  `ResLocos` char(20) DEFAULT NULL,
  `ResNumero` char(15) DEFAULT NULL,
  PRIMARY KEY (`ResId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `treina_usuario`
--
DROP TABLE IF EXISTS `treina_usuario`;
CREATE TABLE `treina_usuario` (
  `truId` int(11) NOT NULL AUTO_INCREMENT,
  `truIdCurso` smallint(8) DEFAULT NULL,
  `truIdFunc` smallint(6) unsigned DEFAULT NULL,
  `truAtividade` text,
  `truSetor` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`truId`),
  KEY `idFunc` (`truIdFunc`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='treinamento - funcionarios treinados';
--
-- Table structure for table `fila_impressao`
--
DROP TABLE IF EXISTS `fila_impressao`;
CREATE TABLE `fila_impressao` (
  `filsid` int(11) NOT NULL AUTO_INCREMENT,
  `idOrdemServico` int(11) NOT NULL,
  `recsid` int(11) NOT NULL,
  PRIMARY KEY (`filsid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `exame_falta`
--
DROP TABLE IF EXISTS `exame_falta`;
CREATE TABLE `exame_falta` (
  `idFalta` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CodExame` varchar(10) DEFAULT NULL,
  `DtCadastro` datetime DEFAULT NULL,
  `QuemCadastrou` int(10) unsigned DEFAULT NULL,
  `Descricao` text,
  `DtPrevista` date DEFAULT NULL,
  `DtChegada` datetime DEFAULT NULL,
  PRIMARY KEY (`idFalta`),
  KEY `exame_falta_exame` (`CodExame`,`DtCadastro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `dna_ordem_servico`
--
DROP TABLE IF EXISTS `dna_ordem_servico`;
CREATE TABLE `dna_ordem_servico` (
  `dosId` int(11) NOT NULL AUTO_INCREMENT,
  `dosIdPai` int(11) DEFAULT NULL,
  `dosIdOrdem` int(11) DEFAULT NULL,
  `dosResponsavel` varchar(100) DEFAULT NULL,
  `dosNtNome` varchar(50) DEFAULT NULL,
  `dosNtEnder` varchar(50) DEFAULT NULL,
  `dosNtCidade` varchar(20) DEFAULT NULL,
  `dosNtCep` varchar(9) DEFAULT NULL,
  `dosNtFone` varchar(20) DEFAULT NULL,
  `dosNtFax` varchar(20) DEFAULT NULL,
  `dosNtDcto` varchar(20) DEFAULT NULL,
  `dosNtInscr` varchar(20) DEFAULT NULL,
  `dosCCM` varchar(20) DEFAULT NULL,
  `dosResultTexto` mediumblob,
  `dosFoto` mediumblob,
  `dosNumLaudo` varchar(20) DEFAULT NULL,
  `dosIdFilho` int(11) DEFAULT NULL,
  `dosIdMae` int(11) DEFAULT NULL,
  `dosDocResp` varchar(100) DEFAULT NULL,
  `dosNtEstado` char(2) DEFAULT NULL,
  `dosTipoColeta` enum('Externa','Interna') DEFAULT 'Externa',
  `dosDtColetaPai` datetime DEFAULT NULL,
  `dosDtColetaMae` datetime DEFAULT NULL,
  `dosDtColetaFilho` datetime DEFAULT NULL,
  `dosQuemColheuPai` int(11) DEFAULT NULL,
  `dosQuemColheuMae` int(11) DEFAULT NULL,
  `dosQuemColheuFilho` int(11) DEFAULT NULL,
  `dosDocQuemColheuPai` varchar(20) DEFAULT NULL,
  `dosDocQuemColheuMae` varchar(20) DEFAULT NULL,
  `dosDocQuemColheuFilho` varchar(20) DEFAULT NULL,
  `dosNomeColheuPai` varchar(100) DEFAULT NULL,
  `dosNomeColheuMae` varchar(100) DEFAULT NULL,
  `dosNomeColheuFilho` varchar(100) DEFAULT NULL,
  `dosRespInterpretacao01` int(11) DEFAULT '1',
  `dosRespInterpretacao02` int(11) DEFAULT '2',
  `dosRespInterpretacao03` int(11) DEFAULT '3',
  `dosQuemSolicitou` varchar(50) DEFAULT NULL,
  `dosCobrarDe` char(1) DEFAULT 'E',
  `dosTipoSolicitacao` char(1) DEFAULT 'P',
  `dosSequencial` int(10) unsigned DEFAULT NULL,
  `dosNroVias` int(10) unsigned NOT NULL DEFAULT '1',
  `dosEntregarPara` varchar(100) DEFAULT NULL,
  `dosRunFolder01` varchar(50) DEFAULT NULL,
  `dosRunFolder02` varchar(50) DEFAULT NULL,
  `dosNroBackup01` int(10) unsigned DEFAULT NULL,
  `dosNroBackup02` int(10) unsigned DEFAULT NULL,
  `dosJuntoCom01` varchar(50) DEFAULT NULL,
  `dosJuntoCom02` varchar(50) DEFAULT NULL,
  `dosObsResultado` varchar(100) DEFAULT NULL,
  `dosEntregue` enum('S','N') NOT NULL DEFAULT 'N',
  `dosLiberado` enum('S','N') NOT NULL DEFAULT 'N',
  `dosImpresso` enum('S','N') NOT NULL DEFAULT 'N',
  `dosDtCadastro` datetime DEFAULT '0000-00-00 00:00:00',
  `dosDtEntregue` datetime DEFAULT NULL,
  `dosDtLiberado` datetime DEFAULT NULL,
  `dosDtImpresso` datetime DEFAULT NULL,
  `dosDtEntradaResultado` date DEFAULT NULL,
  `dosTipoEntrega` enum('SEDEX','Malote','Motorista','Mãos') NOT NULL DEFAULT 'Mãos',
  `dosLote` varchar(20) DEFAULT NULL,
  `dosDtValidadeLote` date DEFAULT NULL,
  `dosTipoInvestigacao` enum('PaiMãeFilho','PaiFilho','MãeFilho') NOT NULL DEFAULT 'PaiMãeFilho',
  `dosEletroesferograma` mediumblob,
  `dosEntSolicitante` varchar(100) NOT NULL,
  `dosIdiomaLaudo` enum('Português','Espanhol') NOT NULL DEFAULT 'Português',
  `dosResultado` enum('Não Avaliado','Inconclusivo','Inclusão','Exclusão') NOT NULL DEFAULT 'Não Avaliado',
  PRIMARY KEY (`dosId`),
  UNIQUE KEY `XPKdna_ordem_servico` (`dosIdOrdem`),
  KEY `idxdosNumLaudo` (`dosNumLaudo`),
  KEY `idxDtCadastro` (`dosDtCadastro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC COMMENT='InnoDB free: 8192 kB';
--
-- Table structure for table `qc_if_parametro`
--
DROP TABLE IF EXISTS `qc_if_parametro`;
CREATE TABLE `qc_if_parametro` (
  `idparametro` int(11) NOT NULL AUTO_INCREMENT,
  `dscparametro` varchar(50) DEFAULT NULL,
  `vlrparametro` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idparametro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `stat_equipamento`
--
DROP TABLE IF EXISTS `stat_equipamento`;
CREATE TABLE `stat_equipamento` (
  `idEquipamento` int(10) unsigned NOT NULL DEFAULT '0',
  `Exame` char(10) NOT NULL DEFAULT '',
  `tipoExame` set('Normal','Repetição','Controle') NOT NULL DEFAULT 'Normal',
  `Data` date NOT NULL DEFAULT '0000-00-00',
  `Quantidade` int(10) unsigned NOT NULL DEFAULT '0',
  `Hora` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`Data`,`idEquipamento`,`Exame`,`tipoExame`,`Hora`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `log_exclusao_resultados_duplicados`
--
DROP TABLE IF EXISTS `log_exclusao_resultados_duplicados`;
CREATE TABLE `log_exclusao_resultados_duplicados` (
  `ressid` int(11) NOT NULL AUTO_INCREMENT,
  `RESRESULTADO` varchar(50) DEFAULT NULL,
  `resimagem` mediumblob,
  `RESSIDSOLIC` int(11) DEFAULT NULL,
  `RESSIDCRITICA` int(11) DEFAULT NULL,
  `RESSIDORDEM` int(11) DEFAULT NULL,
  `RESDESCRICAO` text,
  `resItalico` char(1) DEFAULT '',
  `resNormal` char(1) DEFAULT NULL,
  `residamostra` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`ressid`),
  KEY `AK1Resultados` (`RESSIDSOLIC`,`RESSIDCRITICA`),
  KEY `AK2Resultados` (`RESSIDORDEM`,`RESSIDCRITICA`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `calibracao_faixa`
--
DROP TABLE IF EXISTS `calibracao_faixa`;
CREATE TABLE `calibracao_faixa` (
  `idCalibracaoFaixa` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idCalibracaoTipo` int(10) unsigned DEFAULT NULL,
  `faixaInicial` decimal(9,4) DEFAULT '0.0000',
  `faixaFinal` decimal(9,4) DEFAULT '0.0000',
  `valorAceitacao` decimal(5,2) DEFAULT '0.00',
  `qtdMedicao` smallint(2) unsigned DEFAULT '0',
  PRIMARY KEY (`idCalibracaoFaixa`),
  KEY `equipamento` (`idCalibracaoTipo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `desp_financeira`
--
DROP TABLE IF EXISTS `desp_financeira`;
CREATE TABLE `desp_financeira` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idEntidade` int(11) DEFAULT NULL,
  `Data` date DEFAULT NULL,
  `Descricao` text,
  `valor` decimal(11,2) DEFAULT NULL,
  `QuemLancou` int(11) DEFAULT NULL,
  `NumFatura` int(11) DEFAULT NULL,
  `nota_fiscal` int(11) DEFAULT '0',
  `Sinal` char(1) DEFAULT '+',
  `Tipo` enum('Desp. Financ.','Ajuste Fatura') DEFAULT 'Desp. Financ.',
  `setor` char(5) DEFAULT NULL,
  `idEmpresaGuia` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idEntidade` (`idEntidade`,`Data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `stat_exame_historico_producao_parcial_27_vazio_script_novo`
--
DROP TABLE IF EXISTS `stat_exame_historico_producao_parcial_27_vazio_script_novo`;
CREATE TABLE `stat_exame_historico_producao_parcial_27_vazio_script_novo` (
  `idstat_exame` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `area` char(1) DEFAULT NULL,
  `Entidade` int(10) unsigned DEFAULT NULL,
  `exame` varchar(10) DEFAULT NULL,
  `etapa` tinyint(4) DEFAULT NULL,
  `Data` varchar(8) DEFAULT NULL,
  `qtde` int(11) DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `idEmpresa` int(10) unsigned DEFAULT NULL,
  `idMedico` int(10) unsigned DEFAULT '0',
  `qtdePct` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`idstat_exame`),
  KEY `area` (`area`,`Entidade`,`exame`,`Data`),
  KEY `etapa` (`etapa`,`Data`),
  KEY `stat_exame_medico` (`area`,`idMedico`,`exame`,`Data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `reagente_cadastro`
--
DROP TABLE IF EXISTS `reagente_cadastro`;
CREATE TABLE `reagente_cadastro` (
  `idReagente` int(11) NOT NULL AUTO_INCREMENT,
  `Reativo` varchar(50) DEFAULT NULL,
  `Procedencia` varchar(50) DEFAULT NULL,
  `Aplicacao` varchar(100) DEFAULT NULL,
  `Procedimento` text,
  `Armazenamento` varchar(50) DEFAULT NULL,
  `Estabilidade` varchar(50) DEFAULT NULL,
  `Validade` varchar(50) DEFAULT NULL,
  `Regras_Uso` text,
  `Riscos` text,
  `UltimoLote` varchar(15) DEFAULT '0',
  `idQuemAlterou` int(11) DEFAULT NULL,
  `dtAlteracao` datetime DEFAULT NULL,
  `SimboloRisco` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idReagente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `dados_faturamento`
--
DROP TABLE IF EXISTS `dados_faturamento`;
CREATE TABLE `dados_faturamento` (
  `dfId` int(11) NOT NULL AUTO_INCREMENT,
  `dfEntidade` int(11) NOT NULL,
  `dfNotaFiscal` int(11) NOT NULL,
  `dfData` datetime NOT NULL,
  `dtValor` double NOT NULL,
  PRIMARY KEY (`dfId`,`dfEntidade`),
  KEY `data` (`dfData`),
  KEY `entidade` (`dfEntidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `exames_integracao`
--
DROP TABLE IF EXISTS `exames_integracao`;
CREATE TABLE `exames_integracao` (
  `exintid` int(11) NOT NULL AUTO_INCREMENT,
  `identidade` int(11) DEFAULT NULL,
  `codexamelabor` varchar(10) DEFAULT NULL,
  `codexameexterno` varchar(40) DEFAULT NULL,
  `prioridade` int(11) DEFAULT '1',
  `crisid` int(10) unsigned DEFAULT NULL,
  `Tempo` varchar(45) NOT NULL DEFAULT 'Basal',
  PRIMARY KEY (`exintid`),
  KEY `idxEntidadeExame` (`identidade`,`codexamelabor`),
  KEY `idxExame` (`codexamelabor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC COMMENT='InnoDB free: 4101120 kB';
--
-- Table structure for table `rep_supervisao`
--
DROP TABLE IF EXISTS `rep_supervisao`;
CREATE TABLE `rep_supervisao` (
  `idSupervisor` int(10) unsigned NOT NULL,
  `idRepresentante` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idSupervisor`,`idRepresentante`),
  KEY `representante` (`idRepresentante`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `entidades_prioridade`
--
DROP TABLE IF EXISTS `entidades_prioridade`;
CREATE TABLE `entidades_prioridade` (
  `identidadePrioridade` int(11) NOT NULL,
  PRIMARY KEY (`identidadePrioridade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `medicos`
--
DROP TABLE IF EXISTS `medicos`;
CREATE TABLE `medicos` (
  `medsid` int(11) NOT NULL AUTO_INCREMENT,
  `MEDNOME` varchar(40) DEFAULT NULL,
  `MEDENDER` varchar(40) DEFAULT NULL,
  `MEDBAIRRO` varchar(20) DEFAULT NULL,
  `MEDCIDADE` varchar(20) DEFAULT NULL,
  `MEDESTADO` char(2) DEFAULT NULL,
  `MEDCEP` varchar(8) DEFAULT NULL,
  `MEDFONE` varchar(20) DEFAULT NULL,
  `MEDFAX` varchar(20) DEFAULT NULL,
  `MEDEMAIL` varchar(40) DEFAULT NULL,
  `MEDSEXO` char(1) DEFAULT NULL,
  `meddtnasc` datetime DEFAULT NULL,
  `MEDESPEC` int(11) DEFAULT NULL,
  `medRetirar` varchar(10) DEFAULT NULL,
  `MEDIDENTIF` varchar(10) DEFAULT NULL,
  `MEDSENHA` varchar(10) DEFAULT NULL,
  `MEDTITULO` varchar(5) DEFAULT NULL,
  `MEDCODUNIMED` varchar(20) DEFAULT NULL,
  `medCrm` int(11) DEFAULT NULL,
  `medCrmUf` char(2) DEFAULT NULL,
  `MedParticipaCRM` char(1) DEFAULT 'S',
  `medCPF` char(11) DEFAULT NULL,
  `medCBOS` char(5) DEFAULT NULL,
  PRIMARY KEY (`medsid`),
  KEY `MEDNOME` (`MEDNOME`),
  KEY `medCrm` (`medCrm`,`medCrmUf`),
  KEY `MEDIDENTIF` (`MEDIDENTIF`,`MEDSENHA`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `deparapaternidade`
--
DROP TABLE IF EXISTS `deparapaternidade`;
CREATE TABLE `deparapaternidade` (
  `IdExame` int(11) NOT NULL COMMENT 'Id do Exame no Labor',
  `IdCompostoMotion` int(11) NOT NULL COMMENT 'Id do Composto Motion',
  PRIMARY KEY (`IdExame`,`IdCompostoMotion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela de Configuração de De Para de Paternidade.';
--
-- Table structure for table `empresa`
--
DROP TABLE IF EXISTS `empresa`;
CREATE TABLE `empresa` (
  `empID` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `empNome` varchar(40) DEFAULT NULL,
  `empAssinatura` blob,
  `empQuemAssina` varchar(30) DEFAULT NULL,
  `empCRFAssinante` varchar(15) DEFAULT NULL,
  `empModeloContrato` mediumtext,
  `empTitulo` varchar(20) DEFAULT NULL,
  `empLogo` blob,
  `empMensagem` text,
  `empModeloContratoComodato` mediumtext,
  `empModeloContratoConvenioPgLocal` mediumtext,
  `empModeloContratoConvenioPgFatura` mediumtext,
  `empCPFQuemAssina` varchar(11) DEFAULT NULL,
  `empAtiva` varchar(5) DEFAULT NULL,
  `empCidade` varchar(45) DEFAULT NULL,
  `empEstado` varchar(2) DEFAULT NULL,
  `empCEP` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`empID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `log_entidade`
--
DROP TABLE IF EXISTS `log_entidade`;
CREATE TABLE `log_entidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ConteudoAnterior` text,
  `Quem` int(11) DEFAULT NULL,
  `Data` datetime DEFAULT NULL,
  `Operacao` varchar(50) DEFAULT NULL,
  `idEntidade` int(11) DEFAULT NULL,
  `Maquina` varchar(50) DEFAULT NULL,
  `IP` varchar(50) DEFAULT NULL,
  `Chamada` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idEntidade` (`idEntidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `monitoramento_resultado`
--
DROP TABLE IF EXISTS `monitoramento_resultado`;
CREATE TABLE `monitoramento_resultado` (
  `idMonitoramentoResultado` int(11) NOT NULL AUTO_INCREMENT,
  `idAmostraDasa` varchar(20) NOT NULL,
  `idExameSolicitado` int(11) NOT NULL,
  `dataLaudoRastreabilidade` datetime DEFAULT NULL,
  `dataLaudoRecebeResultado` datetime DEFAULT NULL,
  PRIMARY KEY (`idMonitoramentoResultado`),
  KEY `index2` (`dataLaudoRastreabilidade`),
  KEY `index3` (`dataLaudoRecebeResultado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `rodada01`
--
DROP TABLE IF EXISTS `rodada01`;
CREATE TABLE `rodada01` (
  `ldosid` int(11) NOT NULL,
  `ldostatus` varchar(255) NOT NULL,
  `ldodetails` longtext,
  `ldodatasincronizacao` datetime DEFAULT NULL,
  `ldodatacriacao` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `exameinterface`
--
DROP TABLE IF EXISTS `exameinterface`;
CREATE TABLE `exameinterface` (
  `exisid` int(11) NOT NULL AUTO_INCREMENT,
  `exiNossoExame` varchar(10) DEFAULT NULL,
  `EXIEXAME1` varchar(15) DEFAULT NULL,
  `EXIEXAME2` varchar(15) DEFAULT NULL,
  `EXIEXAME3` varchar(15) DEFAULT NULL,
  `EXIEXAME4` varchar(15) DEFAULT NULL,
  `EXIEQUIPA` int(11) DEFAULT NULL,
  `EXICOMPL1` enum('S','N') DEFAULT 'N',
  `EXICOMPL2` enum('S','N') DEFAULT 'N',
  `EXICOMPL3` enum('S','N') DEFAULT 'N',
  `EXICOMPL4` enum('S','N') DEFAULT 'N',
  `EXISIDCRITICA1` int(11) DEFAULT '0',
  `EXISIDCRITICA2` int(11) DEFAULT '0',
  `EXISIDCRITICA3` int(11) DEFAULT '0',
  `EXISIDCRITICA4` int(11) DEFAULT '0',
  PRIMARY KEY (`exisid`),
  KEY `IEexamesinterface` (`exiNossoExame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `equiv_tempo`
--
DROP TABLE IF EXISTS `equiv_tempo`;
CREATE TABLE `equiv_tempo` (
  `Tempo` char(20) NOT NULL DEFAULT '',
  `Equivalente` char(20) DEFAULT NULL,
  PRIMARY KEY (`Tempo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `entidades_exames`
--
DROP TABLE IF EXISTS `entidades_exames`;
CREATE TABLE `entidades_exames` (
  `idEntidade` int(11) NOT NULL DEFAULT '0',
  `Exame` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`idEntidade`,`Exame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `na_resposta`
--
DROP TABLE IF EXISTS `na_resposta`;
CREATE TABLE `na_resposta` (
  `idna_Requisicao` int(10) unsigned NOT NULL,
  `idPergunta` smallint(5) unsigned NOT NULL,
  `Resposta` text,
  PRIMARY KEY (`idna_Requisicao`,`idPergunta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `qc_elisa_parametro`
--
DROP TABLE IF EXISTS `qc_elisa_parametro`;
CREATE TABLE `qc_elisa_parametro` (
  `idparametro` int(11) NOT NULL AUTO_INCREMENT,
  `dscparametro` varchar(50) NOT NULL,
  `vlrparametro` varchar(100) NOT NULL,
  PRIMARY KEY (`idparametro`),
  KEY `idxdscparametro` (`dscparametro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='InnoDB free: 10240 kB';
--
-- Table structure for table `dna_titulos`
--
DROP TABLE IF EXISTS `dna_titulos`;
CREATE TABLE `dna_titulos` (
  `titId` int(11) NOT NULL AUTO_INCREMENT,
  `titIdOs` int(11) DEFAULT NULL,
  `titObservacao` text,
  `titSituacao` enum('Pago','Pendente') NOT NULL DEFAULT 'Pendente',
  `titDtVcto` date DEFAULT NULL,
  `titDtPgto` date DEFAULT '0000-00-00',
  `titFaturar` enum('Sim','Não') NOT NULL DEFAULT 'Sim',
  `titTpPagamento` enum('Dinheiro','Cheque','Depósito Bancário','Cartão de Crédito','Fatura') DEFAULT 'Dinheiro',
  `titNmrDocPgto` varchar(50) DEFAULT '',
  `titRspPgto` varchar(100) DEFAULT '',
  `idUsrUltAlteracao` int(11) DEFAULT '0',
  `titVlrParc` decimal(12,2) DEFAULT NULL,
  `titSequencia` int(10) unsigned DEFAULT '1',
  PRIMARY KEY (`titId`),
  KEY `dtVcto` (`titDtVcto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `controlelaudo`
--
DROP TABLE IF EXISTS `controlelaudo`;
CREATE TABLE `controlelaudo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordens` int(10) unsigned DEFAULT '0',
  `exames` int(10) unsigned DEFAULT '0',
  `nome` varchar(50) DEFAULT NULL,
  `tipo` varchar(10) DEFAULT '0',
  `data` date DEFAULT '0000-00-00',
  PRIMARY KEY (`id`),
  KEY `nome` (`nome`,`data`,`tipo`),
  KEY `controle_data` (`data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `ordem_servico_producao`
--
DROP TABLE IF EXISTS `ordem_servico_producao`;
CREATE TABLE `ordem_servico_producao` (
  `ordsid` int(11) NOT NULL AUTO_INCREMENT,
  `ORDPCT` int(11) DEFAULT NULL,
  `orddtentr` datetime DEFAULT NULL,
  `ORDQUEMCADASTROU` smallint(6) DEFAULT NULL,
  `ORDQUEMCOLHEU` smallint(6) DEFAULT NULL,
  `ORDAREA` char(1) DEFAULT NULL,
  `ORDOBSPCT` varchar(200) DEFAULT NULL,
  `ORDVLRTOTAL` decimal(12,2) DEFAULT '0.00',
  `ORDVLRDESCONTO` decimal(12,2) DEFAULT '0.00',
  `ORDVLRTAX` decimal(12,2) DEFAULT '0.00',
  `ORDVLRTROCO` decimal(12,2) DEFAULT NULL,
  `ORDRECEPCAO` smallint(6) DEFAULT NULL,
  `ORDQTDEXAMES` smallint(6) DEFAULT NULL,
  `orddtprometida` datetime DEFAULT NULL,
  `ORDMEDICAMENTO` varchar(100) DEFAULT NULL,
  `orddtcoleta` datetime DEFAULT NULL,
  `ORDGUIA` varchar(35) DEFAULT NULL,
  `ordreal` decimal(12,2) DEFAULT NULL,
  `ORDOUTRO` decimal(12,2) DEFAULT NULL,
  `ORDVLRPAGO` decimal(12,2) DEFAULT NULL,
  `ORDCONVENIO` smallint(6) DEFAULT NULL,
  `ORDLOCALCOLETA` varchar(40) DEFAULT NULL,
  `OrdDtLancamento` datetime DEFAULT NULL,
  `ORDVLRBRUTO` decimal(12,2) DEFAULT NULL,
  `ordLocalEntrega` varchar(100) DEFAULT NULL,
  `ordDtAlteracao` datetime DEFAULT NULL,
  `ordQuemAlterou` int(11) DEFAULT NULL,
  `orddtprometida_cliente` datetime DEFAULT NULL,
  PRIMARY KEY (`ordsid`),
  KEY `orddtentr` (`orddtentr`),
  KEY `ORDPCT` (`ORDPCT`),
  KEY `ORDQUEMCOLHEU` (`ORDQUEMCOLHEU`,`orddtcoleta`),
  KEY `index01_ordem_servico` (`ordsid`,`ORDPCT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `qc_perfilanaliseresultados`
--
DROP TABLE IF EXISTS `qc_perfilanaliseresultados`;
CREATE TABLE `qc_perfilanaliseresultados` (
  `idPerfilAnalise` int(11) NOT NULL AUTO_INCREMENT,
  `idExame` varchar(30) DEFAULT NULL,
  `processoAtivado` enum('sim','não') DEFAULT 'sim',
  `idCritica` int(11) DEFAULT NULL,
  `email` varchar(200) DEFAULT 'relacionamento_1@alvaro.com.br',
  PRIMARY KEY (`idPerfilAnalise`),
  KEY `idCritica` (`idCritica`),
  KEY `idExame` (`idExame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `controleacesso`
--
DROP TABLE IF EXISTS `controleacesso`;
CREATE TABLE `controleacesso` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sessao` varchar(200) NOT NULL DEFAULT '0',
  `unicos` int(10) unsigned NOT NULL DEFAULT '0',
  `retornos` int(10) unsigned NOT NULL DEFAULT '0',
  `area` varchar(200) NOT NULL DEFAULT '',
  `data` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `doenca`
--
DROP TABLE IF EXISTS `doenca`;
CREATE TABLE `doenca` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Descricao` varchar(50) DEFAULT NULL,
  `Referencia` varchar(250) DEFAULT '',
  `Sobre` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `logistica_caixa`
--
DROP TABLE IF EXISTS `logistica_caixa`;
CREATE TABLE `logistica_caixa` (
  `idCaixa` int(11) NOT NULL,
  `capacidade` int(11) DEFAULT NULL,
  `cor` enum('branca','vermelha','azul') DEFAULT 'branca',
  `idRota` int(11) DEFAULT NULL,
  `idResponsavel` int(11) DEFAULT NULL,
  `dtAtualizacao` datetime DEFAULT NULL,
  PRIMARY KEY (`idCaixa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `rep_movimentacao`
--
DROP TABLE IF EXISTS `rep_movimentacao`;
CREATE TABLE `rep_movimentacao` (
  `idrep_movimentacao` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idSetor` int(10) unsigned DEFAULT NULL,
  `idAux` int(10) unsigned DEFAULT NULL,
  `Placas` varchar(8) DEFAULT NULL,
  `Data` date DEFAULT NULL,
  `Quilometragem` int(10) unsigned DEFAULT NULL,
  `Odometro` int(10) unsigned DEFAULT NULL,
  `idEntidade` int(10) unsigned DEFAULT NULL,
  `TipoVisita` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idrep_movimentacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `conv_unidade`
--
DROP TABLE IF EXISTS `conv_unidade`;
CREATE TABLE `conv_unidade` (
  `idCategoria` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `Unidade` char(15) NOT NULL DEFAULT '',
  `Descricao` char(60) DEFAULT NULL,
  PRIMARY KEY (`idCategoria`,`Unidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `parametrosimpressaoautomatica`
--
DROP TABLE IF EXISTS `parametrosimpressaoautomatica`;
CREATE TABLE `parametrosimpressaoautomatica` (
  `idParametrosImpressaoAutomatica` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `EnderecoImpressora` varchar(45) DEFAULT NULL,
  `recIdentificador` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`idParametrosImpressaoAutomatica`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `qc_if_ponto`
--
DROP TABLE IF EXISTS `qc_if_ponto`;
CREATE TABLE `qc_if_ponto` (
  `idponto` int(11) NOT NULL AUTO_INCREMENT,
  `idequipamento` int(11) DEFAULT NULL,
  `idexame` varchar(20) DEFAULT NULL,
  `reagente` varchar(20) DEFAULT NULL,
  `violado` enum('Sim','Não') DEFAULT NULL,
  `bloqueado` enum('Sim','Não') DEFAULT NULL,
  `DtAvaliacao` date DEFAULT NULL,
  `HrAvaliacao` time DEFAULT NULL,
  `idUsrIncluiu` int(11) DEFAULT NULL,
  `DtInclusao` datetime DEFAULT NULL,
  `idUsrAlterou` int(11) DEFAULT NULL,
  `DtUltimaAlteracao` datetime DEFAULT NULL,
  `fabricante` varchar(100) DEFAULT NULL,
  `lotekit` varchar(100) DEFAULT NULL,
  `loteControlePositivo` varchar(100) DEFAULT NULL,
  `loteControleNegativo` varchar(100) DEFAULT NULL,
  `ValidadeKit` char(5) DEFAULT NULL,
  `ValidadeControlePositivo` char(5) DEFAULT NULL,
  `ValidadeControleNegativo` char(5) DEFAULT NULL,
  `TituloEsperado` varchar(20) DEFAULT NULL,
  `TituloObtido` varchar(20) DEFAULT NULL,
  `ControleNegativo` varchar(20) DEFAULT NULL,
  `idUsrLiberou` int(11) DEFAULT NULL,
  `DtLiberacao` datetime DEFAULT NULL,
  PRIMARY KEY (`idponto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `manutencao_equipamento`
--
DROP TABLE IF EXISTS `manutencao_equipamento`;
CREATE TABLE `manutencao_equipamento` (
  `idManutencao` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idEquipamento` int(10) unsigned DEFAULT NULL,
  `tipoManutencao` enum('Preventiva','Corretiva') DEFAULT 'Corretiva',
  `DtHoraAgendada` datetime DEFAULT NULL,
  `DtHoraInicio` datetime DEFAULT NULL,
  `DtHoraAceite` datetime DEFAULT NULL,
  `DtHoraTermino` datetime DEFAULT NULL,
  `StatusManutencao` enum('Aberta','Finalizada','Agendada','Aceita') DEFAULT 'Aberta',
  `idUsrRespRegistro` int(10) unsigned DEFAULT NULL,
  `idUsrRespLiberacao` int(10) unsigned DEFAULT NULL,
  `idUsrRespAlteracao` int(10) unsigned DEFAULT NULL,
  `idUsrRespAceite` int(10) unsigned DEFAULT NULL,
  `DtHoraAlteracao` datetime DEFAULT NULL,
  `NroRegistroTecnico` varchar(45) DEFAULT NULL,
  `OsManutencao` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idManutencao`),
  KEY `idxOS` (`OsManutencao`,`idEquipamento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `exame_alerta`
--
DROP TABLE IF EXISTS `exame_alerta`;
CREATE TABLE `exame_alerta` (
  `CodExame` varchar(10) NOT NULL,
  `idAlerta` int(11) NOT NULL,
  PRIMARY KEY (`CodExame`,`idAlerta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela de ligação entra alertas e exame';
--
-- Table structure for table `remarcacao_motivo_externo`
--
DROP TABLE IF EXISTS `remarcacao_motivo_externo`;
CREATE TABLE `remarcacao_motivo_externo` (
  `idremarcacao_motivo_externo` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(100) DEFAULT NULL,
  `nivel` int(11) NOT NULL,
  PRIMARY KEY (`idremarcacao_motivo_externo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `equipamento_calibracao`
--
DROP TABLE IF EXISTS `equipamento_calibracao`;
CREATE TABLE `equipamento_calibracao` (
  `idEquipamento` int(10) unsigned NOT NULL,
  `numCertificado` varchar(45) DEFAULT NULL,
  `dtVcto` date DEFAULT NULL,
  `tempoValidade` smallint(3) unsigned DEFAULT '0',
  `formaUtilizacao` enum('Padrão','Rotina') DEFAULT NULL,
  `orgaoCertificador` varchar(45) DEFAULT NULL,
  `qtdMinimaTeste` smallint(3) unsigned DEFAULT '0',
  `idCalibracaoTipo` smallint(3) unsigned DEFAULT '0',
  `identificacaoAux` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idEquipamento`),
  KEY `identificacaoAux` (`identificacaoAux`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `notas_fiscais_composicao`
--
DROP TABLE IF EXISTS `notas_fiscais_composicao`;
CREATE TABLE `notas_fiscais_composicao` (
  `notAgrup_id` int(11) NOT NULL,
  `notfatura` int(11) NOT NULL,
  `notsid` int(11) NOT NULL,
  `idempguia` int(11) NOT NULL,
  PRIMARY KEY (`notAgrup_id`,`notsid`),
  KEY `index2` (`notfatura`),
  KEY `index3` (`notsid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `produto`
--
DROP TABLE IF EXISTS `produto`;
CREATE TABLE `produto` (
  `idProduto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Descricao` char(30) DEFAULT NULL,
  `preco` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`idProduto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Produtos aplicados na coleta';
--
-- Table structure for table `conv_tabela`
--
DROP TABLE IF EXISTS `conv_tabela`;
CREATE TABLE `conv_tabela` (
  `idCategoria` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `Unidade` char(10) NOT NULL DEFAULT '',
  `Peso` double DEFAULT NULL,
  `IntervaloInf` float DEFAULT NULL,
  `IntervaloSup` float DEFAULT NULL,
  PRIMARY KEY (`idCategoria`,`Unidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `cbhpm_edicoes`
--
DROP TABLE IF EXISTS `cbhpm_edicoes`;
CREATE TABLE `cbhpm_edicoes` (
  `EdicaoCBHPMsid` int(11) NOT NULL AUTO_INCREMENT,
  `ediNomeEdicaoCBHPM` varchar(45) DEFAULT NULL,
  `ediAnoEdicaoCBHPM` int(11) DEFAULT NULL,
  `ediCustoOperEdicao` decimal(9,3) DEFAULT NULL,
  PRIMARY KEY (`EdicaoCBHPMsid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `critica`
--
DROP TABLE IF EXISTS `critica`;
CREATE TABLE `critica` (
  `crisid` int(11) NOT NULL AUTO_INCREMENT,
  `criexame` varchar(10) DEFAULT NULL,
  `CRILINHA` int(11) DEFAULT NULL,
  `CRINOME` varchar(55) DEFAULT NULL,
  `CRINUMEROLETRA` char(1) DEFAULT NULL,
  `CRIDECIMAIS` smallint(6) DEFAULT NULL,
  `CRIDESCRGRAFICO` varchar(8) DEFAULT NULL,
  `CRIUNIDADE` varchar(15) DEFAULT NULL,
  `CRICOMRESULT` char(1) DEFAULT 'S',
  `CRIMENSAGEMALERTA` varchar(200) DEFAULT NULL,
  `CRIENTRARPELARECEPCAO` char(1) DEFAULT 'N',
  `CRISOLICITARSENHA` char(1) DEFAULT NULL,
  `CRIFORMULACALCULO` varchar(200) DEFAULT NULL,
  `CRIALERTARESULTADO` varchar(200) DEFAULT NULL,
  `CRIIDPOSICIONAR` int(11) DEFAULT NULL,
  `CriCodigoGrafico` tinyint(4) DEFAULT NULL,
  `criVlrInfPerigoso` int(11) DEFAULT NULL,
  `criVlrSupPerigoso` int(11) DEFAULT NULL,
  `criImpResultAnterior` char(1) DEFAULT 'N',
  `criDeltaCheck` int(11) DEFAULT '0',
  `criDeltaPositividade` decimal(9,2) DEFAULT '0.00',
  `criInformePeriodico` char(1) DEFAULT 'S',
  `criMnemonico` enum('Volume','Peso','Altura','Outro','IdadeGestacionalSemanas','IdadeMaterna','DUM','Leucocitos','Hemacias','Hemoglobina','Hematocrito','Plaquetas','TipoAmostra','NovoVirusRNA','CPF','RG','cadeia','NumeroCadeiaCustodia','Email','Material','Data de coleta','Microorganismo isolado') DEFAULT NULL,
  `criLinhaAuxiliar` char(1) DEFAULT 'N' COMMENT 'Indica se esta linha é invisível a aplicações externas - laudos, exportações, etc...',
  `flprincipal` char(1) DEFAULT 'N',
  `criAgrupador` char(1) NOT NULL DEFAULT 'N',
  `criVazia` char(1) DEFAULT 'N',
  `CRIRESULTADOAGRUPADO` varchar(55) DEFAULT NULL,
  `CRIRESULTADOAGRUPADOSEQUENCIA` int(11) DEFAULT NULL,
  `criMnemonicoOutro` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`crisid`),
  KEY `IECritica` (`criexame`,`CRILINHA`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `almicro_relacaodfa`
--
DROP TABLE IF EXISTS `almicro_relacaodfa`;
CREATE TABLE `almicro_relacaodfa` (
  `Estado` char(2) NOT NULL DEFAULT '',
  `descricao` char(40) DEFAULT NULL,
  PRIMARY KEY (`Estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `amb_exame_excluir`
--
DROP TABLE IF EXISTS `amb_exame_excluir`;
CREATE TABLE `amb_exame_excluir` (
  `CodAmb` int(11) NOT NULL DEFAULT '0',
  `Exame` varchar(100) DEFAULT NULL,
  `idExame` varchar(10) DEFAULT NULL,
  `Porte` varchar(5) DEFAULT NULL,
  `PercPorte` decimal(3,2) DEFAULT NULL,
  `CustoOper` decimal(9,3) DEFAULT NULL,
  PRIMARY KEY (`CodAmb`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `nfse`
--
DROP TABLE IF EXISTS `nfse`;
CREATE TABLE `nfse` (
  `NFSEID` int(11) NOT NULL AUTO_INCREMENT,
  `NotSid` int(11) NOT NULL,
  `NFSENumeroRPS` int(11) DEFAULT NULL,
  `NFSLote` int(11) DEFAULT NULL,
  `NfseSeriePRS` varchar(10) DEFAULT NULL,
  `NFSEDataRebebimento` varchar(45) DEFAULT NULL,
  `NFSEProtocolo` varchar(60) DEFAULT NULL,
  `NFSENumeroNotaFiscal` varchar(45) DEFAULT NULL,
  `NFSECodigoVerificacao` varchar(45) DEFAULT NULL,
  `NFSENarturezaOperacao` int(11) DEFAULT NULL,
  `NFSEXmlEnvio` blob,
  `NFSEXmlRetorno` blob,
  `NFSEXmlRetornoCancelamento` blob,
  `NFSEPedidoCancelamento` varchar(255) DEFAULT NULL,
  `NFSECodigoCancelamento` varchar(45) DEFAULT NULL,
  `NFSEDataCancelamento` datetime DEFAULT NULL,
  `NfseMensagem` varchar(500) DEFAULT NULL,
  `NFSEDataEmissao` varchar(45) DEFAULT NULL,
  `NFSEDescricaoServico` varchar(2000) DEFAULT NULL,
  `NFSEISSRetido` int(11) DEFAULT '2',
  `nfseStatus` enum('Não Recebido','Em Processamento','Nota Enviada','Situação RPS','Aguardando','Processado com Erro','Processado com Sucesso','Cancelar','Em Cancelamento','Cancelada') DEFAULT 'Não Recebido',
  `nfseRazaoSocial` varchar(500) DEFAULT NULL,
  `nfseCnpjCPF` varchar(20) NOT NULL,
  `nfseInscricaoMunicipal` varchar(20) DEFAULT NULL,
  `nfseRua` varchar(45) DEFAULT NULL,
  `nfseNumeroRua` varchar(10) DEFAULT NULL,
  `nfseComplemento` varchar(100) DEFAULT NULL,
  `nfseBairro` varchar(45) DEFAULT NULL,
  `nfsecep` varchar(14) DEFAULT NULL,
  `nfseIdCidade` varchar(45) DEFAULT NULL,
  `nfseTipoPessoa` varchar(45) DEFAULT NULL,
  `nfseObservacao` varchar(255) DEFAULT NULL,
  `nfseEmail` varchar(60) DEFAULT NULL,
  `nfseStatusEmail` int(11) DEFAULT NULL,
  PRIMARY KEY (`NFSEID`),
  KEY `FK_NotSid` (`NotSid`),
  KEY `index_nRPS` (`NFSENumeroRPS`),
  CONSTRAINT `FK_NotSid` FOREIGN KEY (`NotSid`) REFERENCES `notas_fiscais` (`notsid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Dados referentes as notas fiscais eletrônicas de serviços \n';
--
-- Table structure for table `entidades`
--
DROP TABLE IF EXISTS `entidades`;
CREATE TABLE `entidades` (
  `entsid` int(11) NOT NULL AUTO_INCREMENT,
  `entNome` varchar(100) NOT NULL,
  `ENTENDER` varchar(40) DEFAULT NULL,
  `ENTBAIRRO` varchar(20) DEFAULT NULL,
  `entCidade` varchar(60) DEFAULT '',
  `ENTCEP` varchar(8) DEFAULT NULL,
  `ENTEMAIL` varchar(60) DEFAULT NULL,
  `ENTCGC` varchar(18) DEFAULT NULL,
  `ENTINSCR` varchar(15) DEFAULT NULL,
  `ENTMEDRESP` varchar(30) DEFAULT NULL,
  `ENTFINANCRESP` varchar(30) DEFAULT NULL,
  `ENTFORMACOBR` varchar(60) DEFAULT NULL,
  `ENTOBS` varchar(500) DEFAULT NULL,
  `ENTIDENTIF` varchar(10) DEFAULT NULL,
  `ENTSENHA` varchar(10) DEFAULT NULL,
  `ENTTABAMB` smallint(4) DEFAULT NULL,
  `ENTPERCCH` double DEFAULT NULL,
  `ENTPRAZO` smallint(6) DEFAULT NULL,
  `ENTENTRANOCAIXA` char(1) DEFAULT NULL,
  `ENTESTADO` char(2) DEFAULT NULL,
  `entPais` char(2) DEFAULT 'BR',
  `ENTFONE` varchar(20) DEFAULT NULL,
  `ENTFAX` varchar(20) DEFAULT NULL,
  `ENTENDERCOB` varchar(40) DEFAULT NULL,
  `ENTBAIRROCOB` varchar(20) DEFAULT NULL,
  `ENTCIDADECOB` varchar(20) DEFAULT NULL,
  `ENTESTADOCOB` char(2) DEFAULT '',
  `ENTCEPCOB` varchar(8) DEFAULT NULL,
  `entDtUltAlteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `entdtcadastro` datetime DEFAULT NULL,
  `entdta_nascmedresp` datetime DEFAULT NULL,
  `ENTFAXPROPRIO` char(1) DEFAULT NULL,
  `ENTLOCALFAX` varchar(30) DEFAULT NULL,
  `ENTRESPFAX` varchar(30) DEFAULT NULL,
  `ENTTIPO` char(1) DEFAULT NULL,
  `ENTCONTATO` varchar(40) DEFAULT NULL,
  `ENTDIACOBRANCA` smallint(6) DEFAULT NULL,
  `ENTFANTASIA` varchar(40) DEFAULT NULL,
  `ENTEMITEFATURA` char(1) DEFAULT NULL,
  `ENTINADIMPLENTE` char(1) DEFAULT NULL,
  `EntMtvInadimplente` text,
  `ENTVLRPAGONOBALCAO` float DEFAULT NULL,
  `entPercVlrPagoBalcao` decimal(4,2) DEFAULT '0.00',
  `ENTQUEMALTEROU` int(11) DEFAULT NULL,
  `entTabPreco` smallint(6) DEFAULT '0',
  `ENTPERCDESC` float DEFAULT NULL,
  `entDescSobrePcoEspecial` char(1) DEFAULT 'N' COMMENT 'Se for S aplicar o desconto sobre o total da fatura',
  `entrepresentante` int(10) unsigned zerofill DEFAULT NULL,
  `entretirar` varchar(10) DEFAULT NULL,
  `ENTPERCACRESCIMO` decimal(4,2) DEFAULT '0.00',
  `entHorasAtrasoLaudo` tinyint(3) unsigned DEFAULT '0',
  `entPcoInps` char(1) DEFAULT 'N',
  `entEmiteNota` char(1) DEFAULT 'S',
  `entDiaVcto1` smallint(2) DEFAULT '0',
  `entDiaVcto2` smallint(2) DEFAULT '0',
  `entDiaVcto3` smallint(2) DEFAULT '0',
  `entDiaVcto4` smallint(2) DEFAULT '0',
  `entDiaVcto5` smallint(2) DEFAULT '0',
  `EntMostraDescItem` char(1) DEFAULT NULL,
  `entDiasMinimoFaturamento` int(11) DEFAULT '15',
  `entFaxAutomatico` char(1) DEFAULT 'N',
  `entContratoOK` char(1) DEFAULT 'N',
  `entArea` smallint(6) DEFAULT NULL,
  `entAtiva` char(1) DEFAULT 'S',
  `EntLaudoPorEmail` char(1) DEFAULT 'N',
  `EntLaudoEmail` varchar(60) DEFAULT '',
  `EntSenhaPDF` varchar(15) DEFAULT '',
  `entFaixaPco` varchar(30) DEFAULT '',
  `entDescontaISSQN` char(1) DEFAULT 'N',
  `entultsequenciagerada` int(10) unsigned DEFAULT '0',
  `entmaxsequenciasreserva` int(10) unsigned DEFAULT '0',
  `entImprComplSorologico` char(1) DEFAULT 'N',
  `entPercIR` decimal(3,2) DEFAULT '1.50',
  `entTipoDocumento` enum('CNPJ','CPF','OUTROS') DEFAULT 'CNPJ',
  `entVlrMeta` int(10) unsigned DEFAULT '0',
  `entDescontaContrSocial` char(1) DEFAULT 'S',
  `entDescontaPIS` char(1) DEFAULT 'S',
  `entDescontaCofins` char(1) DEFAULT 'S',
  `entRegistroANS` char(6) DEFAULT NULL,
  `entGravarFatura` char(1) DEFAULT 'N',
  `entDtLimiteTrocaSenha` date DEFAULT NULL,
  `entClassificacao` smallint(6) DEFAULT NULL COMMENT 'Grau de importancia do lab por valor de exames',
  `entEntidadePontosCartaoFidelidade` int(10) unsigned DEFAULT NULL,
  `entIssqnSempre` char(1) DEFAULT 'N' COMMENT 'Descontar sempre esse imposto',
  `entIrSempre` char(1) DEFAULT 'N',
  `entPisSempre` char(1) DEFAULT 'N',
  `entCofinsSempre` char(1) DEFAULT 'N',
  `entDescricaoNotaFiscal` varchar(100) DEFAULT NULL COMMENT 'Substitui a descricao da nota fiscal',
  `entContribSocialSempre` char(1) DEFAULT 'N',
  `entFaturarPara` int(10) unsigned DEFAULT '0' COMMENT 'id da entidade que devera faturar',
  `entPercINSS` decimal(6,2) NOT NULL DEFAULT '0.00',
  `entSeqCxEtiqueta` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Numero da ultima etiqueta de caixa emitida',
  `entAdiantaVcto` char(1) DEFAULT 'S' COMMENT 'Qdo cai em um sab ou dom. o sistema coloca o vcto para sex, se for N nao fara isso',
  `entNomePlano` varchar(40) DEFAULT NULL,
  `entMailFatura` varchar(150) DEFAULT NULL COMMENT 'mail para envio de fatura',
  `entMailNovaAmostra` varchar(350) DEFAULT NULL,
  `entUsaNroGuia` enum('Sim','Não') DEFAULT 'Não',
  `entUltimaGuia` int(10) unsigned NOT NULL DEFAULT '0',
  `entEmiteEtiquetaCxTemp` enum('Sim','Não') DEFAULT 'Não',
  `entComprvanteImpSenha` char(1) NOT NULL DEFAULT 'S',
  `entCodigoPrestadorNaOperadora` int(11) DEFAULT NULL,
  `entQtParcelas` int(10) unsigned DEFAULT '0',
  `entidCidade` int(11) DEFAULT NULL,
  `entEmiteGuiaHospital` enum('Sim','Não') DEFAULT 'Não',
  `entFaturamentoNormal` char(1) NOT NULL DEFAULT 'S',
  `entIdEmpresaGuia` int(11) DEFAULT '1',
  `entIdEMS` int(11) DEFAULT '0',
  `entImprimeLaudoAuto` enum('Sim','Não') DEFAULT 'Não',
  `entAbreviacaoEMS` varchar(12) DEFAULT NULL,
  `entPermiteInclusaoAposFaturamento` char(1) NOT NULL DEFAULT 'S',
  `entAlterarSenhaColigadas` char(1) NOT NULL DEFAULT 'N' COMMENT 'Qdo tem faturarPara, se altera senha para eles tbem',
  `entGuiaSus` char(1) DEFAULT 'N',
  `entEmiteGuiaSus` enum('Sim','Não') DEFAULT 'Não',
  `entEmiteDMED` char(1) DEFAULT 'N',
  `entCnpjDasa` char(1) DEFAULT 'N',
  `entEnviaSP` char(1) DEFAULT 'N',
  `entRua` varchar(125) DEFAULT NULL,
  `entNumero` varchar(10) DEFAULT NULL,
  `entComplemento` varchar(60) DEFAULT NULL,
  `entInscrMunicipal` varchar(20) DEFAULT NULL,
  `entBloqueioAutomatico` char(1) DEFAULT 'N',
  `entMailStatusExames` varchar(50) DEFAULT NULL,
  `entSolicitaDtaColeta` varchar(1) DEFAULT 'N',
  `entSolicitaDataValidadeCartao` varchar(1) DEFAULT 'N',
  `entSolicitaAutenticadorGuiaConvenio` varchar(1) DEFAULT 'N',
  `entTemPreResultado` char(1) DEFAULT 'N',
  `EntBloqTransfAOL` char(1) DEFAULT 'N',
  `entQtExamesTubo` int(11) DEFAULT '0',
  `entMedicinaOcupacional` char(1) DEFAULT 'N',
  `entExamesExclusivos` char(1) DEFAULT 'N',
  `entGeraLote` varchar(1) DEFAULT 'N',
  `entbloqtransfaol2` char(1) DEFAULT 'N',
  `entWhatsApp` varchar(20) DEFAULT NULL,
  `EntTipo2` enum('Laboratorio','Hospital') DEFAULT 'Laboratorio',
  PRIMARY KEY (`entsid`),
  KEY `fantasia` (`ENTFANTASIA`),
  KEY `ENTNOME` (`entNome`),
  KEY `indicematar` (`ENTIDENTIF`,`ENTSENHA`),
  KEY `ENTINADIMPLENTE` (`ENTINADIMPLENTE`),
  KEY `idEms` (`entIdEMS`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC COMMENT='InnoDB free: 1988608 kB; InnoDB free: 15580160 kB';
--
-- Table structure for table `resultados_new`
--
DROP TABLE IF EXISTS `resultados_new`;
CREATE TABLE `resultados_new` (
  `ressid` int(11) NOT NULL AUTO_INCREMENT,
  `resresultado` text,
  `resimagem` mediumblob,
  `RESSIDSOLIC` int(11) DEFAULT NULL,
  `RESSIDCRITICA` int(11) DEFAULT NULL,
  `RESSIDORDEM` int(11) DEFAULT NULL,
  `RESDESCRICAO` text,
  `resItalico` char(1) DEFAULT '',
  `resNormal` char(1) DEFAULT NULL,
  `residamostra` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`ressid`),
  KEY `AK1Resultados` (`RESSIDSOLIC`,`RESSIDCRITICA`),
  KEY `AK2Resultados` (`RESSIDORDEM`,`RESSIDCRITICA`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `separacao_tubo`
--
DROP TABLE IF EXISTS `separacao_tubo`;
CREATE TABLE `separacao_tubo` (
  `Id` char(2) NOT NULL DEFAULT '',
  `Ordem` smallint(5) unsigned DEFAULT NULL,
  `regra` text,
  `tipo` enum('Quantidade','Exames','QtdeSetores','Setores','QtdeCategorias','Categorias','Join','Script') DEFAULT NULL,
  `Descricao` varchar(60) NOT NULL DEFAULT '',
  `dtUltAlteracao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sepEnviaSP` char(1) DEFAULT 'N',
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `intresultados`
--
DROP TABLE IF EXISTS `intresultados`;
CREATE TABLE `intresultados` (
  `RESUSUARIO` varchar(10) DEFAULT NULL,
  `RESSENHA` varchar(10) DEFAULT NULL,
  `resdataenvio` datetime DEFAULT NULL,
  `resdataleitura` datetime DEFAULT NULL,
  `RESPACIENTE` varchar(40) DEFAULT NULL,
  `resid` int(11) NOT NULL AUTO_INCREMENT,
  `rescompleto` text,
  `resresumo` text,
  `FAXUSUARIO` varchar(4) DEFAULT NULL,
  `FAXSENHA` varchar(6) DEFAULT NULL,
  `faxresultado` text,
  `RESINST` varchar(45) DEFAULT NULL,
  `CODPAC` varchar(15) NOT NULL DEFAULT '',
  `CODORD` varchar(15) DEFAULT NULL,
  `MARK` int(11) DEFAULT NULL,
  PRIMARY KEY (`resid`),
  KEY `AK1IntResultadosUsuario` (`RESUSUARIO`,`RESSENHA`),
  KEY `AK1IntResultadosCodPac` (`CODPAC`,`CODORD`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `exames`
--
DROP TABLE IF EXISTS `exames`;
CREATE TABLE `exames` (
  `exasid` int(11) NOT NULL AUTO_INCREMENT,
  `ExaCodigo` varchar(10) NOT NULL DEFAULT '',
  `ExaNome` varchar(60) NOT NULL DEFAULT '',
  `EXASINONIMO` varchar(50) DEFAULT NULL,
  `EXAMATERIAL` int(11) DEFAULT NULL,
  `EXAVOLUME` varchar(50) DEFAULT NULL,
  `exacoleta` text,
  `EXATEMPERATURA` varchar(50) DEFAULT NULL,
  `EXAMETODO` text,
  `EXAROTINA` varchar(50) DEFAULT NULL,
  `EXARESULTADO` varchar(50) DEFAULT NULL,
  `exaLocalExecucao` varchar(50) DEFAULT 'Lab. Alvaro',
  `exadtinicio` datetime DEFAULT NULL,
  `EXASETOR` char(3) DEFAULT NULL,
  `EXATIPOFRASCO` int(11) DEFAULT NULL,
  `exaCategoria` varchar(6) DEFAULT NULL,
  `exaCodAmb` varchar(250) DEFAULT NULL,
  `exaCodSus` varchar(250) DEFAULT NULL,
  `EXACODHOSPITAL` varchar(15) DEFAULT NULL,
  `EXAVOLUMELABEX` varchar(100) DEFAULT NULL,
  `EXACOLETALABEX` varchar(100) DEFAULT NULL,
  `EXALISTARNOLABEX` char(1) DEFAULT NULL,
  `exaDtUltAlt` datetime NOT NULL,
  `EXASELECIONADO` char(1) DEFAULT NULL,
  `EXAINTERFACEAMENTO` char(1) DEFAULT NULL,
  `EXAGRUPOMAPA` varchar(5) DEFAULT NULL,
  `EXAGRUPOIMPR` varchar(5) DEFAULT NULL,
  `EXACOMPOSTO` char(1) DEFAULT NULL,
  `EXATEMANTIBIOGRAMA` char(1) DEFAULT NULL,
  `EXASUSPENSO` char(1) DEFAULT NULL,
  `EXAMOTIVOSUSPENSAO` varchar(100) DEFAULT NULL,
  `EXAQUEMSUSPENDEU` int(11) DEFAULT NULL,
  `exadtsuspensao` datetime DEFAULT NULL,
  `EXADESCRITIVO` char(1) DEFAULT NULL,
  `exaImpResultAnterior` char(1) DEFAULT 'N',
  `EXAPCOLABEX` double DEFAULT NULL,
  `EXAPCOPARTI` double DEFAULT NULL,
  `EXAPORFAX` char(1) DEFAULT NULL,
  `EXAPORINTERNET` char(1) DEFAULT NULL,
  `EXAUSAGRAFICO` char(1) DEFAULT NULL,
  `EXAHORAENTRADAMAXIMA` varchar(5) DEFAULT NULL,
  `EXADIASREALIZAR` int(11) DEFAULT NULL,
  `exaexamesrelacionados` text,
  `exainterferentes` text,
  `EXAOCORRENCIA` smallint(6) DEFAULT NULL,
  `EXAQUEMCADASTROU` int(11) DEFAULT NULL,
  `EXAQUEMALTEROU` int(11) DEFAULT NULL,
  `exainterpretacao` text,
  `EXADIASEMANAROTINA` varchar(10) DEFAULT NULL,
  `EXAPCOINPS` double DEFAULT NULL,
  `EXACODMETODO` varchar(5) DEFAULT NULL,
  `EXARESULTADOLABEX` varchar(50) DEFAULT '0',
  `exaPcoCusto` decimal(11,2) DEFAULT '0.00',
  `ExaTermoResp` text,
  `exaExameSubstituto` varchar(10) DEFAULT '',
  `ExaDtVigencia` datetime DEFAULT NULL,
  `exaMsgVigencia` tinytext,
  `exaAuxiliar` char(1) DEFAULT 'N',
  `exaAvisoAtendente` tinytext,
  `exaVolumeMinimo` varchar(20) DEFAULT NULL,
  `exaGrupoLista` varchar(50) DEFAULT NULL,
  `exaRejeicaoAmostra` varchar(20) DEFAULT NULL,
  `exaNovoExame` char(1) DEFAULT 'N',
  `exaCodigoEstoque` varchar(50) DEFAULT NULL,
  `exaTempoPadraoCurva` varchar(250) DEFAULT NULL,
  `exaIdEmpresa` smallint(5) unsigned DEFAULT '1',
  `exaCodigoInterfaceamento` varchar(12) DEFAULT NULL,
  `exaImprimeLaudoRecepcao` char(1) DEFAULT 'S',
  `exaQtdEtiquetas` smallint(6) DEFAULT '1',
  `ExaLaudoEmPlanilha` char(1) DEFAULT 'N',
  `exaEsoterico` char(1) DEFAULT 'N',
  `ExaBioqMonitor` int(10) unsigned DEFAULT '0',
  `exaIdQuemLiberaResultado` int(11) DEFAULT '0',
  `exaCodSusAntigo` varchar(250) DEFAULT NULL,
  `exaCVMaxPermitido` decimal(10,3) DEFAULT NULL,
  `exaRevisado` char(1) DEFAULT 'N',
  `exaDtRevisado` datetime DEFAULT NULL,
  `exaQuemRevisou` int(11) DEFAULT NULL,
  `exaCBHPM` char(12) DEFAULT '0.00.00.00-0' COMMENT 'Classificação Brasileira Hierarquizada de Procedimentos Médicos',
  `exaDtUltimoCusto` datetime DEFAULT NULL COMMENT 'Data da ultima alteracao do custo',
  `exaAceitaInclusao` varchar(1) DEFAULT 'S',
  `exaTempoInclusao` int(10) unsigned DEFAULT '0',
  `exaSolicitaDtColeta` char(1) DEFAULT 'N',
  `exaTempoViabilidade` tinyint(3) DEFAULT '0',
  `exaTempoPadraoRemarcacao` tinyint(3) unsigned DEFAULT '0',
  `exaDtUltAltAOL` datetime NOT NULL,
  `exaListaMateriaisAceitaveis` varchar(2000) DEFAULT NULL,
  `exaSolicitaOSAnterior` char(1) DEFAULT 'N',
  `exaCurva` char(1) DEFAULT 'N',
  `exaTemPreResultado` char(1) DEFAULT 'N',
  `exaRemarcacao` int(11) DEFAULT NULL,
  `exaRedigitacaoUnica` varchar(1) NOT NULL DEFAULT 'N',
  `exaIdAcondicionamento` int(11) DEFAULT NULL,
  `exaQuebrarMaterial` int(11) DEFAULT NULL,
  `exaInterface` int(11) DEFAULT NULL,
  `exaEtiquetaUnica` char(1) DEFAULT 'N',
  `exaLaudoTemplate` varchar(100) DEFAULT NULL,
  `exaBloqueiaLaudo` char(1) DEFAULT NULL,
  `exaLaudoTemplateVersao` int(11) DEFAULT NULL,
  `exaPaternidade` char(1) DEFAULT 'N',
  `exaSubcategoria` int(11) NOT NULL DEFAULT '0',
  `exaLoteExclusivo` char(1) DEFAULT 'N',
  `exaLabexEspecial` char(1) DEFAULT 'N',
  `exahemograma` varchar(45) DEFAULT NULL,
  `exaPermiteDuplicar` char(1) DEFAULT 'N',
  `exaIdDasa` varchar(15) DEFAULT NULL,
  `exaIdSap` int(15) DEFAULT NULL,
  `exaSetorSap` varchar(10) DEFAULT NULL,
  `exaSegmentacaoSap` varchar(10) DEFAULT NULL,
  `exaLipidograma` char(1) DEFAULT 'N',
  PRIMARY KEY (`exasid`),
  UNIQUE KEY `ExaCodigo` (`ExaCodigo`),
  KEY `AK2ExamesNome` (`ExaNome`),
  KEY `exaCodigoEstoque` (`exaCodigoEstoque`),
  KEY `exadtultalt` (`exaDtUltAlt`),
  KEY `ExaDtVigencia` (`ExaDtVigencia`),
  KEY `EXASETOR` (`EXASETOR`),
  KEY `exaCategoria` (`exaCategoria`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `resultadografico`
--
DROP TABLE IF EXISTS `resultadografico`;
CREATE TABLE `resultadografico` (
  `idresultadoGrafico` int(11) NOT NULL AUTO_INCREMENT,
  `idordemServico` int(11) NOT NULL,
  `codExame` varchar(45) NOT NULL,
  `conteudo` longtext NOT NULL,
  PRIMARY KEY (`idresultadoGrafico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `apoio_paciente`
--
DROP TABLE IF EXISTS `apoio_paciente`;
CREATE TABLE `apoio_paciente` (
  `idPaciente` int(11) NOT NULL DEFAULT '0',
  `idLabApoio` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idPacienteExterno` char(30) DEFAULT NULL,
  `DataTransferencia` datetime DEFAULT NULL,
  PRIMARY KEY (`idPaciente`,`idLabApoio`),
  KEY `idPaciente` (`idPaciente`),
  KEY `idPacienteExterno` (`idLabApoio`,`idPacienteExterno`),
  KEY `Transferidos` (`DataTransferencia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `entidade_destino`
--
DROP TABLE IF EXISTS `entidade_destino`;
CREATE TABLE `entidade_destino` (
  `idEntidade` int(11) NOT NULL,
  `idDestino` varchar(255) NOT NULL,
  PRIMARY KEY (`idEntidade`,`idDestino`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Ligação entre a tabela labor.entidades e aol.destino';
--
-- Table structure for table `srp_estoque`
--
DROP TABLE IF EXISTS `srp_estoque`;
CREATE TABLE `srp_estoque` (
  `locid` int(11) NOT NULL AUTO_INCREMENT,
  `LOCEST` smallint(6) DEFAULT NULL,
  `LOCPOSICAO` char(3) DEFAULT NULL,
  `LOCCOD` int(11) DEFAULT NULL,
  `LOCNOME` varchar(25) DEFAULT NULL,
  `LOCEXAME` varchar(35) DEFAULT NULL,
  `LOCRESULTADO` varchar(25) DEFAULT NULL,
  `LOCARRAY` int(11) DEFAULT NULL,
  `locdata` datetime DEFAULT NULL,
  PRIMARY KEY (`locid`),
  KEY `AK1LocalSoroCodigo` (`LOCCOD`),
  KEY `AK2LocalSoroEst` (`LOCEST`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `envia_email`
--
DROP TABLE IF EXISTS `envia_email`;
CREATE TABLE `envia_email` (
  `idenvia_email` int(11) NOT NULL AUTO_INCREMENT,
  `emailDe` varchar(45) DEFAULT NULL,
  `emailAssunto` text,
  `emailMensagem` text,
  `emailTela` varchar(45) DEFAULT NULL,
  `emailEnviado` varchar(1) DEFAULT 'N',
  `emailDataSolicitado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `emailDataEnviado` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idenvia_email`),
  KEY `index` (`idenvia_email`,`emailEnviado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `conf_curva`
--
DROP TABLE IF EXISTS `conf_curva`;
CREATE TABLE `conf_curva` (
  `idconf_Curva` int(11) NOT NULL AUTO_INCREMENT,
  `conf_tempo_numerico` int(11) DEFAULT NULL,
  `conf_descritivo` varchar(25) DEFAULT NULL,
  `conf_ativo` varchar(1) NOT NULL DEFAULT 'S',
  `conf_limite_inferior` int(11) DEFAULT NULL,
  `conf_limite_superior` int(11) DEFAULT NULL,
  `conf_ordem` int(11) DEFAULT NULL,
  `idTipoConfCurva` int(2) DEFAULT NULL,
  `flExcluido` varchar(1) DEFAULT 'N',
  `conf_ordem_resultado` int(11) DEFAULT NULL,
  PRIMARY KEY (`idconf_Curva`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `destino_faixa_panico`
--
DROP TABLE IF EXISTS `destino_faixa_panico`;
CREATE TABLE `destino_faixa_panico` (
  `idDestino` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(50) NOT NULL,
  PRIMARY KEY (`idDestino`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `stat_exame_execucao`
--
DROP TABLE IF EXISTS `stat_exame_execucao`;
CREATE TABLE `stat_exame_execucao` (
  `controleID` int(16) NOT NULL AUTO_INCREMENT,
  `sistemaOrigem` varchar(15) DEFAULT NULL,
  `sistemaOrigemRequisicao` varchar(15) DEFAULT NULL,
  `codigoRequisicao` varchar(18) DEFAULT NULL,
  `codigoRequisicaoExterna` varchar(15) DEFAULT NULL,
  `codigoAmostra` varchar(16) DEFAULT NULL,
  `sequenciaAmostra` int(16) DEFAULT NULL,
  `examePerfil` varchar(15) DEFAULT NULL,
  `exameComposto` varchar(15) DEFAULT NULL,
  `exameTeste` varchar(15) DEFAULT NULL,
  `sequenciaRepeticao` int(16) DEFAULT NULL,
  `marcaOrigem` varchar(15) DEFAULT NULL,
  `tabelaDepara` varchar(15) DEFAULT NULL,
  `unidadeOrigem` varchar(30) DEFAULT NULL,
  `localExecucao` varchar(40) DEFAULT NULL,
  `plataformaExecucao` varchar(40) DEFAULT NULL,
  `dataExecucao` datetime DEFAULT NULL,
  PRIMARY KEY (`controleID`),
  KEY `idx_stat_exame_execucao_codigoRequisicao` (`codigoRequisicao`),
  KEY `idx_stat_exame_execucao_dataExecucao` (`dataExecucao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Estatísticas de produção do dia anterior.';
--
-- Table structure for table `log_cadexamesfora`
--
DROP TABLE IF EXISTS `log_cadexamesfora`;
CREATE TABLE `log_cadexamesfora` (
  `idlog_cadExamesFora` int(11) NOT NULL AUTO_INCREMENT,
  `Operacao` varchar(45) DEFAULT NULL,
  `Quem` int(11) DEFAULT NULL,
  `Data` datetime DEFAULT NULL,
  `Exame` varchar(45) DEFAULT NULL,
  `Entidade` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idlog_cadExamesFora`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela que gravará os logs de quando alguém inclui ou exclui';
--
-- Table structure for table `os_pronta`
--
DROP TABLE IF EXISTS `os_pronta`;
CREATE TABLE `os_pronta` (
  `idOrdemServico` int(10) unsigned NOT NULL DEFAULT '0',
  `DataProcesso` datetime DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  PRIMARY KEY (`idOrdemServico`),
  KEY `DataProcesso` (`data`,`DataProcesso`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `examesagrupados`
--
DROP TABLE IF EXISTS `examesagrupados`;
CREATE TABLE `examesagrupados` (
  `exgsid` int(11) NOT NULL AUTO_INCREMENT,
  `EXGGRUPO` int(11) DEFAULT NULL,
  `exgexame` varchar(10) DEFAULT NULL,
  `EXGCOMPGRUPO` char(1) DEFAULT NULL,
  PRIMARY KEY (`exgsid`),
  KEY `AK1ExamesAgrupadosGrupo` (`EXGGRUPO`),
  KEY `IdxExame` (`exgexame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `entidade_exameautorizacao`
--
DROP TABLE IF EXISTS `entidade_exameautorizacao`;
CREATE TABLE `entidade_exameautorizacao` (
  `entsid` int(11) NOT NULL,
  `exacodigo` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`entsid`,`exacodigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `alvet_dadosadicionais`
--
DROP TABLE IF EXISTS `alvet_dadosadicionais`;
CREATE TABLE `alvet_dadosadicionais` (
  `idPaciente` int(10) unsigned NOT NULL DEFAULT '0',
  `idProprietario` int(10) unsigned NOT NULL DEFAULT '0',
  `idRaca` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idPaciente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `agrupamento`
--
DROP TABLE IF EXISTS `agrupamento`;
CREATE TABLE `agrupamento` (
  `idAgrupamento` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CodAgrupamento` varchar(10) DEFAULT NULL,
  `codExame` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idAgrupamento`),
  KEY `CodExame` (`codExame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `ordem_servico_laudo`
--
DROP TABLE IF EXISTS `ordem_servico_laudo`;
CREATE TABLE `ordem_servico_laudo` (
  `ldosid` int(11) NOT NULL,
  `ldostatus` varchar(255) NOT NULL,
  `ldodetails` longtext,
  `ldodatasincronizacao` datetime DEFAULT NULL,
  `ldodatacriacao` datetime DEFAULT NULL,
  PRIMARY KEY (`ldosid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `convvalref`
--
DROP TABLE IF EXISTS `convvalref`;
CREATE TABLE `convvalref` (
  `idConvValRef` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idEquipamento` smallint(5) unsigned DEFAULT NULL,
  `CodExame` varchar(10) DEFAULT NULL,
  `ExameEquip` varchar(10) DEFAULT NULL,
  `VlrInfNormal` float DEFAULT NULL,
  `VlrSupNormal` float DEFAULT NULL,
  `VlrInfEquip` float DEFAULT NULL,
  `VlrSupEquip` float DEFAULT NULL,
  `PtoFx1` float DEFAULT NULL,
  `PtoFx2` float DEFAULT NULL,
  `FatorConv1` float DEFAULT NULL,
  `FatorConv2` float DEFAULT NULL,
  `FatorConv3` float DEFAULT NULL,
  PRIMARY KEY (`idConvValRef`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `protocolo_cpd`
--
DROP TABLE IF EXISTS `protocolo_cpd`;
CREATE TABLE `protocolo_cpd` (
  `idProtocolo` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `ordsid` int(3) unsigned DEFAULT NULL,
  `respProtocolo` int(3) unsigned DEFAULT NULL,
  `obsProtocolo` varchar(200) DEFAULT NULL,
  `dtaProtocolo` datetime DEFAULT NULL,
  `pctsid` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`idProtocolo`),
  KEY `procolo_index_data` (`dtaProtocolo`),
  KEY `idProtocolo` (`ordsid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `qc_analiseresultados_ordemservico`
--
DROP TABLE IF EXISTS `qc_analiseresultados_ordemservico`;
CREATE TABLE `qc_analiseresultados_ordemservico` (
  `idQCAnalise` int(11) NOT NULL,
  `listaOS` text,
  PRIMARY KEY (`idQCAnalise`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `rotret`
--
DROP TABLE IF EXISTS `rotret`;
CREATE TABLE `rotret` (
  `RTID` int(11) DEFAULT NULL,
  `RTSOLIC` varchar(30) DEFAULT NULL,
  `RTSETOR` varchar(35) DEFAULT NULL,
  `RTDATA` datetime DEFAULT NULL,
  `RTHORA` varchar(5) DEFAULT NULL,
  `RTLINKPOS` int(11) DEFAULT NULL,
  `RTEXAME` varchar(45) DEFAULT NULL,
  `RTCONFIR` char(1) DEFAULT NULL,
  `RTOUTRO` varchar(45) DEFAULT NULL,
  `RTRETORNO` char(1) DEFAULT NULL,
  `RTC` char(1) DEFAULT NULL,
  `RTRESINI` varchar(45) DEFAULT NULL,
  `RTRESFIN` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `tpocorrenciaequipamento`
--
DROP TABLE IF EXISTS `tpocorrenciaequipamento`;
CREATE TABLE `tpocorrenciaequipamento` (
  `idTpOcorrEquip` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Descricao` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idTpOcorrEquip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `modeloequip`
--
DROP TABLE IF EXISTS `modeloequip`;
CREATE TABLE `modeloequip` (
  `idModeloEquip` int(11) NOT NULL AUTO_INCREMENT,
  `Descricao` char(40) DEFAULT NULL,
  `driver` varchar(60) DEFAULT '',
  `scriptPreResultado` mediumtext,
  PRIMARY KEY (`idModeloEquip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `ordem_servico_laudo_teste_02`
--
DROP TABLE IF EXISTS `ordem_servico_laudo_teste_02`;
CREATE TABLE `ordem_servico_laudo_teste_02` (
  `ldosid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `ordem_servico_laudo_teste_01`
--
DROP TABLE IF EXISTS `ordem_servico_laudo_teste_01`;
CREATE TABLE `ordem_servico_laudo_teste_01` (
  `ldosid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `valorref`
--
DROP TABLE IF EXISTS `valorref`;
CREATE TABLE `valorref` (
  `idValorReferencia` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idEspecie` int(11) NOT NULL DEFAULT '0',
  `idMaterial` int(11) NOT NULL DEFAULT '0',
  `idCritica` int(11) NOT NULL DEFAULT '0',
  `DataValidade` datetime DEFAULT '9999-12-31 00:00:00',
  PRIMARY KEY (`idValorReferencia`),
  KEY `idMaterial` (`idMaterial`),
  KEY `idEspecie` (`idEspecie`),
  KEY `idCritica` (`idCritica`),
  KEY `Exame_2` (`idEspecie`,`idMaterial`,`idCritica`,`DataValidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `os_busca_relaciona`
--
DROP TABLE IF EXISTS `os_busca_relaciona`;
CREATE TABLE `os_busca_relaciona` (
  `idOrdem` int(11) NOT NULL,
  `idFunc` int(11) DEFAULT NULL,
  `Data` datetime DEFAULT NULL,
  `Mensagem` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idOrdem`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `pre_stat_exame`
--
DROP TABLE IF EXISTS `pre_stat_exame`;
CREATE TABLE `pre_stat_exame` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idSolsid` int(10) unsigned NOT NULL,
  `area` char(1) NOT NULL,
  `entidade` int(10) unsigned NOT NULL,
  `data` date NOT NULL,
  `exame` char(10) NOT NULL,
  `medico` int(10) unsigned NOT NULL,
  `qtde` int(11) DEFAULT NULL,
  `valor` decimal(12,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `logistica_celulaenvio`
--
DROP TABLE IF EXISTS `logistica_celulaenvio`;
CREATE TABLE `logistica_celulaenvio` (
  `idEnvio` int(11) NOT NULL AUTO_INCREMENT,
  `idEstoque` int(11) NOT NULL,
  `dtEnvio` date DEFAULT NULL,
  `idEntidadeEnviado` int(11) NOT NULL,
  `qtdEnviada` int(11) DEFAULT '1',
  `idTransf` int(11) DEFAULT NULL,
  PRIMARY KEY (`idEnvio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `pendencia`
--
DROP TABLE IF EXISTS `pendencia`;
CREATE TABLE `pendencia` (
  `idPendencia` int(11) NOT NULL AUTO_INCREMENT,
  `idPaciente` int(10) unsigned DEFAULT NULL,
  `NomePaciente` varchar(60) DEFAULT '',
  `idEntidade` int(10) unsigned DEFAULT NULL,
  `idOrdem` int(10) unsigned DEFAULT NULL,
  `Exames` varchar(200) DEFAULT NULL,
  `DataCadastro` datetime DEFAULT NULL,
  `idFunc` smallint(5) unsigned DEFAULT NULL,
  `Aberta` char(1) NOT NULL DEFAULT 'S' COMMENT 'O campo "Aberta" recebe os seguintes valores: S : Aberta;  N : Fechada; R : Recobrada sem retorno; M : Recobrada material devolvido; D : Recobrada descartado; E : Recobrada exame realizado. ',
  `Descricao` mediumtext,
  `resolucao` text,
  `idFuncAceite` int(11) DEFAULT '0',
  `idFuncResolucao` int(11) DEFAULT '0',
  `idMaterial` int(11) unsigned DEFAULT NULL,
  `idAmostra` int(10) unsigned DEFAULT NULL,
  `enviadoemail` char(1) DEFAULT 'N',
  `penRepSid` int(11) DEFAULT NULL,
  `DataUltimaAlteracao` datetime DEFAULT NULL,
  `LidoSite` char(1) DEFAULT 'N',
  `emailPendenciaManual` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idPendencia`),
  KEY `os` (`idOrdem`),
  KEY `data` (`DataCadastro`),
  KEY `datacadastro` (`DataCadastro`),
  KEY `identidade` (`idEntidade`),
  KEY `LidoSite` (`LidoSite`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `proprietario`
--
DROP TABLE IF EXISTS `proprietario`;
CREATE TABLE `proprietario` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` char(40) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `entidade_config`
--
DROP TABLE IF EXISTS `entidade_config`;
CREATE TABLE `entidade_config` (
  `identidade_config` int(11) NOT NULL AUTO_INCREMENT,
  `entconfig_descricao` text,
  `entconfig_dataCriacao` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`identidade_config`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `rep_atuacao`
--
DROP TABLE IF EXISTS `rep_atuacao`;
CREATE TABLE `rep_atuacao` (
  `idRepresentante` int(11) unsigned NOT NULL DEFAULT '0',
  `idSetor` int(11) NOT NULL DEFAULT '0',
  `idCidade` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idRepresentante`,`idSetor`,`idCidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `itemexameparticipantes`
--
DROP TABLE IF EXISTS `itemexameparticipantes`;
CREATE TABLE `itemexameparticipantes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idExameSolicitado` int(11) NOT NULL,
  `identificador` varchar(200) NOT NULL,
  `coletor` varchar(150) NOT NULL,
  `nomeJuiz` varchar(150) NOT NULL,
  `qtdeEtiqueta` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `equipamento_ocorrencia`
--
DROP TABLE IF EXISTS `equipamento_ocorrencia`;
CREATE TABLE `equipamento_ocorrencia` (
  `idOcorrencia` int(11) NOT NULL AUTO_INCREMENT,
  `idEquipamento` int(11) NOT NULL DEFAULT '0',
  `idUsuario` int(11) NOT NULL DEFAULT '0',
  `dscOcorrencia` text,
  `dtOcorrencia` date DEFAULT NULL,
  `hrOcorrencia` time DEFAULT NULL,
  PRIMARY KEY (`idOcorrencia`),
  KEY `idEquipamento` (`idEquipamento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `log_dna`
--
DROP TABLE IF EXISTS `log_dna`;
CREATE TABLE `log_dna` (
  `idlogdna` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ConteudoAnterior` text,
  `Quem` varchar(45) DEFAULT NULL,
  `Data` datetime DEFAULT NULL,
  `Operacao` enum('Inserção','Edição','Exclusão') DEFAULT NULL,
  `idDNA` int(11) DEFAULT NULL,
  `tabela` enum('dna_pai','dna_filho','dna_mae','dna_ordem_servico','dna_titulos','dna_parente') DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idlogdna`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `laudo_marca`
--
DROP TABLE IF EXISTS `laudo_marca`;
CREATE TABLE `laudo_marca` (
  `idmarca` int(11) NOT NULL,
  `cabecalho` varchar(100) DEFAULT NULL,
  `versao_cabecalho` int(11) DEFAULT NULL,
  `rodape` varchar(100) DEFAULT NULL,
  `versao_rodape` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmarca`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `qc_elisa_analisecritica`
--
DROP TABLE IF EXISTS `qc_elisa_analisecritica`;
CREATE TABLE `qc_elisa_analisecritica` (
  `idQCAC` int(11) NOT NULL AUTO_INCREMENT,
  `idequipamento` int(11) DEFAULT NULL,
  `idexame` char(20) DEFAULT NULL,
  `idUsuario` int(11) DEFAULT NULL,
  `Analise` text,
  `DtHora` datetime DEFAULT NULL,
  PRIMARY KEY (`idQCAC`),
  KEY `idxDtHora` (`DtHora`),
  KEY `idxequipamento` (`idequipamento`),
  KEY `idxexame` (`idexame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `exame_falta_aviso`
--
DROP TABLE IF EXISTS `exame_falta_aviso`;
CREATE TABLE `exame_falta_aviso` (
  `idAviso` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idFalta` int(10) unsigned NOT NULL DEFAULT '0',
  `idSolic` int(11) DEFAULT NULL,
  `QuemAvisou` int(10) unsigned DEFAULT NULL,
  `DataAviso` datetime DEFAULT NULL,
  `Forma` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idAviso`,`idFalta`),
  KEY `exame_falta_aviso_falta` (`idFalta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `exame_controleaol`
--
DROP TABLE IF EXISTS `exame_controleaol`;
CREATE TABLE `exame_controleaol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exacodigo` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `exame` (`exacodigo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `atendimento`
--
DROP TABLE IF EXISTS `atendimento`;
CREATE TABLE `atendimento` (
  `idAtendimento` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idSolicitante` int(10) unsigned DEFAULT NULL,
  `TipoIdSolicitante` enum('Lab','Med','Pct','Est','Pro','Int','Out','Rep','Cel') DEFAULT NULL,
  `Contato` varchar(40) DEFAULT NULL,
  `Fone` varchar(40) DEFAULT NULL,
  `Fax` varchar(40) DEFAULT '',
  `Email` varchar(50) DEFAULT NULL,
  `Gravidade` enum('Sem gravidade','Pequena','Média','Grande','Risco de perder o cliente') DEFAULT 'Sem gravidade',
  `Exames` text,
  `Os` text,
  `idAgenda` int(10) unsigned DEFAULT NULL,
  `atuacao` enum('Apoio','Privado','Não definido') DEFAULT 'Não definido',
  PRIMARY KEY (`idAtendimento`),
  KEY `Index_tipo` (`TipoIdSolicitante`,`idSolicitante`),
  KEY `idx_labor_atendimento_idagenda` (`idAgenda`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `de_para_os_dasa`
--
DROP TABLE IF EXISTS `de_para_os_dasa`;
CREATE TABLE `de_para_os_dasa` (
  `idDeParaOsDasa` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idOrdemServico` int(11) unsigned DEFAULT NULL,
  `idOsDasa` varchar(20) DEFAULT NULL COMMENT 'numero da o.s. no dasa',
  `idRequisicaoDasa` varchar(20) DEFAULT NULL,
  `idAmostraDasa` char(20) DEFAULT NULL,
  `idExameDasa` int(11) unsigned DEFAULT NULL,
  `idExameSolicitado` int(10) unsigned DEFAULT NULL,
  `dtImportacao` datetime DEFAULT NULL,
  `dtExportacao` datetime DEFAULT NULL,
  `idPfora` int(11) NOT NULL DEFAULT '0' COMMENT 'indica que foi de cascavel para o dasa qdo maior que 0',
  `destinoTubo` char(3) NOT NULL DEFAULT 'SP' COMMENT 'Como e gerado o reg. e etiquetado com cod. motion e nao se tem certeza de que ira para sp, serve para indicar em caso de inclusao se envia mail ou nao',
  `container` char(20) DEFAULT '',
  PRIMARY KEY (`idDeParaOsDasa`),
  KEY `idOrdemServico` (`idOrdemServico`),
  KEY `idOsDasa` (`idOsDasa`,`idAmostraDasa`),
  KEY `idSolicitacao` (`idAmostraDasa`),
  KEY `idExameSolicitado` (`idExameSolicitado`),
  KEY `idAmostraDasa` (`idAmostraDasa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `stat_exame_old`
--
DROP TABLE IF EXISTS `stat_exame_old`;
CREATE TABLE `stat_exame_old` (
  `idstat_exame` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `area` char(1) DEFAULT NULL,
  `Entidade` int(10) unsigned DEFAULT NULL,
  `exame` varchar(10) DEFAULT NULL,
  `etapa` tinyint(4) DEFAULT NULL,
  `Data` varchar(8) DEFAULT NULL,
  `qtde` int(11) DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `idEmpresa` int(10) unsigned DEFAULT NULL,
  `idMedico` int(10) unsigned DEFAULT '0',
  `qtdePct` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`idstat_exame`),
  KEY `area` (`area`,`Entidade`,`exame`,`Data`),
  KEY `etapa` (`etapa`,`Data`),
  KEY `stat_exame_medico` (`area`,`idMedico`,`exame`,`Data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `controle_laudo`
--
DROP TABLE IF EXISTS `controle_laudo`;
CREATE TABLE `controle_laudo` (
  `id` int(10) NOT NULL DEFAULT '0',
  `ordens` int(10) unsigned NOT NULL DEFAULT '0',
  `exames` int(10) unsigned NOT NULL DEFAULT '0',
  `tipo` varchar(10) NOT NULL DEFAULT '0',
  `data` date NOT NULL DEFAULT '0000-00-00',
  `origem` enum('HTML','PDF','Touch') NOT NULL DEFAULT 'HTML',
  PRIMARY KEY (`id`,`tipo`,`data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `reagente_imagem`
--
DROP TABLE IF EXISTS `reagente_imagem`;
CREATE TABLE `reagente_imagem` (
  `idImagem` tinyint(3) unsigned NOT NULL,
  `imagemOriginal` blob,
  `imagemTratada` blob,
  `imagemEltron` blob,
  `label` char(8) DEFAULT NULL,
  PRIMARY KEY (`idImagem`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `separacao_tubo1`
--
DROP TABLE IF EXISTS `separacao_tubo1`;
CREATE TABLE `separacao_tubo1` (
  `Id` char(2) NOT NULL DEFAULT '',
  `Ordem` smallint(5) unsigned DEFAULT NULL,
  `regra` text,
  `tipo` enum('Quantidade','Exames','QtdeSetores','Setores','QtdeCategorias','Categorias','Join','Script') DEFAULT NULL,
  `Descricao` varchar(60) NOT NULL DEFAULT '',
  `dtUltAlteracao` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sepEnviaSP` char(1) DEFAULT 'N',
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `exame_cod_entidade`
--
DROP TABLE IF EXISTS `exame_cod_entidade`;
CREATE TABLE `exame_cod_entidade` (
  `idEntidade` int(10) unsigned NOT NULL DEFAULT '0',
  `idCadastro` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idEntidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `srt_os_pendente`
--
DROP TABLE IF EXISTS `srt_os_pendente`;
CREATE TABLE `srt_os_pendente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idOrdem` int(10) unsigned DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `examesPendentes` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idOrdem` (`idOrdem`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `refvalinclusao`
--
DROP TABLE IF EXISTS `refvalinclusao`;
CREATE TABLE `refvalinclusao` (
  `idExame` varchar(10) NOT NULL DEFAULT '',
  `RefVALPerfil_idPerfil` int(10) unsigned NOT NULL DEFAULT '0',
  `ValorInf` decimal(10,2) NOT NULL DEFAULT '0.00',
  `ValorSup` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`idExame`,`RefVALPerfil_idPerfil`),
  KEY `Inclusao_FKIndex1` (`RefVALPerfil_idPerfil`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC COMMENT='InnoDB free: 721920 kB; (`RefVALPerfil_idPerfil`) REFER `lab';
--
-- Table structure for table `dataserver`
--
DROP TABLE IF EXISTS `dataserver`;
CREATE TABLE `dataserver` (
  `idDataServer` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Descricao` char(40) DEFAULT NULL,
  `porta` smallint(5) unsigned NOT NULL DEFAULT '0',
  `NomeServidor` char(20) DEFAULT NULL,
  `Compartilhamento` char(20) DEFAULT NULL,
  `Padrao` enum('S','N') DEFAULT NULL,
  `Banco` char(12) NOT NULL DEFAULT '',
  `Host` char(40) NOT NULL DEFAULT '',
  PRIMARY KEY (`idDataServer`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `ind_movimento`
--
DROP TABLE IF EXISTS `ind_movimento`;
CREATE TABLE `ind_movimento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idCad` int(11) DEFAULT NULL,
  `data` char(8) DEFAULT NULL,
  `Valor` decimal(11,2) DEFAULT NULL,
  `Melhoria` text,
  `Meta` decimal(11,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `jobenviohybris`
--
DROP TABLE IF EXISTS `jobenviohybris`;
CREATE TABLE `jobenviohybris` (
  `jobId` int(11) NOT NULL AUTO_INCREMENT,
  `jobNome` varchar(30) NOT NULL DEFAULT '',
  `jobDtExec` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'Data de inicio da execucao do job',
  PRIMARY KEY (`jobId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `titulo`
--
DROP TABLE IF EXISTS `titulo`;
CREATE TABLE `titulo` (
  `idTitulo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idEntidade` int(10) unsigned DEFAULT NULL,
  `numero` int(10) unsigned DEFAULT NULL,
  `parcela` smallint(2) unsigned DEFAULT NULL,
  `dtVencimento` date DEFAULT NULL,
  `dtGeracao` datetime DEFAULT NULL,
  `valor` decimal(11,2) unsigned DEFAULT NULL,
  `vlrDesconto` decimal(9,2) unsigned DEFAULT '0.00',
  `vlrAcrescimo` decimal(9,2) unsigned DEFAULT '0.00',
  `vlrPago` decimal(11,2) unsigned DEFAULT '0.00',
  `dtPagamento` datetime DEFAULT NULL,
  `observacao` varchar(100) DEFAULT NULL,
  `situacao` enum('Aberto','Pago Parcial','Pago Total') NOT NULL DEFAULT 'Aberto',
  `tipoDoc` char(2) DEFAULT 'NF',
  PRIMARY KEY (`idTitulo`),
  KEY `entidade` (`idEntidade`),
  KEY `nota` (`numero`,`parcela`),
  KEY `dtGeracao` (`dtGeracao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `laudo_template`
--
DROP TABLE IF EXISTS `laudo_template`;
CREATE TABLE `laudo_template` (
  `idlaudotemplate` int(11) NOT NULL AUTO_INCREMENT,
  `cdtemplate` varchar(255) NOT NULL,
  `detemplate` varchar(255) NOT NULL,
  `nuversao` int(10) NOT NULL,
  `boAtivo` varchar(1) NOT NULL DEFAULT 'S',
  `dtCriacao` datetime NOT NULL,
  `dtAlteracao` datetime DEFAULT NULL,
  `idUsuario` int(11) NOT NULL,
  PRIMARY KEY (`idlaudotemplate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `sms_recepcao`
--
DROP TABLE IF EXISTS `sms_recepcao`;
CREATE TABLE `sms_recepcao` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `origem` varchar(12) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  `mensagem` varchar(250) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `valor_vitamina_d`
--
DROP TABLE IF EXISTS `valor_vitamina_d`;
CREATE TABLE `valor_vitamina_d` (
  `idValor` int(11) NOT NULL AUTO_INCREMENT,
  `resultado` decimal(7,2) NOT NULL,
  `qtde` int(11) NOT NULL,
  `dtInicial` datetime NOT NULL,
  `dtFinal` datetime NOT NULL,
  PRIMARY KEY (`idValor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `log_exclusao_exames_duplicados`
--
DROP TABLE IF EXISTS `log_exclusao_exames_duplicados`;
CREATE TABLE `log_exclusao_exames_duplicados` (
  `solsid` int(11) NOT NULL AUTO_INCREMENT,
  `SOLSIDORDEM` int(11) DEFAULT NULL,
  `solExame` varchar(10) DEFAULT NULL,
  `solControleAol` char(1) DEFAULT 'N',
  `solVlrExame` decimal(11,2) DEFAULT '0.00',
  `SOLFORMAEXECUCAO` char(1) DEFAULT NULL,
  `SolObsResultado` text,
  `soldttransmissao` datetime DEFAULT NULL,
  `soldtimpressao` datetime DEFAULT NULL,
  `SOLLIBERADO` char(1) DEFAULT NULL,
  `SOLQUEMLIBEROU` smallint(6) DEFAULT NULL,
  `soldtliberacao` datetime DEFAULT NULL,
  `SOLQUEMDIGITOU` smallint(6) DEFAULT NULL,
  `soldtdigitacao` datetime DEFAULT NULL,
  `SOLSETOR` char(3) DEFAULT NULL,
  `SOLBOLETOIMPRESSO` char(1) DEFAULT NULL,
  `SOLMATERIAL` int(11) DEFAULT NULL,
  `SOLESTANTE` varchar(10) DEFAULT NULL,
  `solMarcado` char(1) DEFAULT '',
  `solCodExComposto` varchar(10) DEFAULT NULL,
  `solInterfaceado` char(1) DEFAULT 'N',
  `SolDtGeradoInternet` datetime DEFAULT NULL,
  `SolDestGeradoInternet` varchar(5) DEFAULT NULL,
  `solBioqLiberou` int(11) DEFAULT NULL,
  `solDtEntr` datetime DEFAULT NULL,
  `solQuemCadastrou` smallint(6) unsigned DEFAULT '0',
  `solLimbo` char(1) DEFAULT 'N',
  `solCriterioLiberacao` text,
  `solQtdAmostras` smallint(6) DEFAULT '1',
  `solDtRedigitacao` datetime DEFAULT NULL,
  `solMotivoRedigitacao` tinytext,
  `solQuemRedigitou` smallint(6) DEFAULT NULL,
  `solDtLidoInternetPct` datetime DEFAULT NULL,
  `solMedico` int(10) unsigned DEFAULT NULL,
  `solEntidade` int(10) unsigned DEFAULT NULL,
  `solConvenio` int(10) unsigned DEFAULT NULL,
  `solvalordesconto` decimal(9,2) DEFAULT '0.00',
  `SolNroFatura` int(10) unsigned DEFAULT NULL,
  `solDtLidoInternetMed` datetime DEFAULT NULL,
  `solTuboUnico` char(1) DEFAULT 'N',
  `solPoucaAmostra` char(1) DEFAULT 'N',
  PRIMARY KEY (`solsid`),
  KEY `solMedico` (`solMedico`,`solDtEntr`),
  KEY `solConvenio` (`solConvenio`,`solDtEntr`),
  KEY `soldtdigitacao` (`soldtdigitacao`),
  KEY `SOLSIDORDEM` (`SOLSIDORDEM`),
  KEY `soldttransmissao` (`soldttransmissao`),
  KEY `solDtEntr` (`solDtEntr`),
  KEY `solEntidade` (`solEntidade`,`solDtEntr`),
  KEY `solExame` (`solExame`,`solDtEntr`),
  KEY `SOLLIBERADO` (`SOLLIBERADO`),
  KEY `soldtliberacao` (`soldtliberacao`),
  KEY `solQuemCadastrou` (`solQuemCadastrou`),
  KEY `solQuemRedigitou` (`solQuemRedigitou`),
  KEY `SOLQUEMDIGITOU` (`SOLQUEMDIGITOU`),
  KEY `SOLQUEMLIBEROU` (`SOLQUEMLIBEROU`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `nota_fiscal_cobranca`
--
DROP TABLE IF EXISTS `nota_fiscal_cobranca`;
CREATE TABLE `nota_fiscal_cobranca` (
  `notaFiscal` int(11) NOT NULL COMMENT 'Numor da nota fiscal do EMS',
  `idEntidade` int(11) NOT NULL,
  `nossoNumero` varchar(25) NOT NULL DEFAULT '',
  `mesAnoNota` char(7) DEFAULT NULL,
  `dtVcto` date NOT NULL,
  PRIMARY KEY (`idEntidade`,`nossoNumero`) USING BTREE,
  KEY `nota` (`notaFiscal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `controle_acessos`
--
DROP TABLE IF EXISTS `controle_acessos`;
CREATE TABLE `controle_acessos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IdEmpresa` int(11) DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL,
  `url` varchar(150) DEFAULT NULL,
  `dataAcesso` datetime DEFAULT NULL,
  `tipoEntidade` enum('LA','ME','PA','RE') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dataAcesso` (`dataAcesso`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `treina_instrutor`
--
DROP TABLE IF EXISTS `treina_instrutor`;
CREATE TABLE `treina_instrutor` (
  `triId` int(11) NOT NULL AUTO_INCREMENT,
  `triIdCurso` int(11) DEFAULT NULL,
  `triIdInstrutor` int(11) DEFAULT '0',
  `triInstrutor` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`triId`),
  KEY `triIdCurso` (`triIdCurso`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `tipoconfcurva`
--
DROP TABLE IF EXISTS `tipoconfcurva`;
CREATE TABLE `tipoconfcurva` (
  `idTipoConfCurva` int(2) NOT NULL,
  `dsTipoConfCurva` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`idTipoConfCurva`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `teste`
--
DROP TABLE IF EXISTS `teste`;
CREATE TABLE `teste` (
  `name` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `exame_cod_cadastro`
--
DROP TABLE IF EXISTS `exame_cod_cadastro`;
CREATE TABLE `exame_cod_cadastro` (
  `idCadastro` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`idCadastro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `log_os_insercao`
--
DROP TABLE IF EXISTS `log_os_insercao`;
CREATE TABLE `log_os_insercao` (
  `idLogOs` int(11) NOT NULL AUTO_INCREMENT,
  `idOrdem` int(11) DEFAULT NULL,
  `Data` datetime DEFAULT NULL,
  `Motivo` text,
  `Quem` int(11) DEFAULT NULL,
  `exame` varchar(10) DEFAULT NULL,
  `Operacao` varchar(50) DEFAULT NULL,
  `idSolic` int(11) DEFAULT NULL,
  `idEntCobranca` int(10) unsigned DEFAULT '0',
  `VlrExame` decimal(9,2) DEFAULT '0.00',
  PRIMARY KEY (`idLogOs`),
  KEY `logos_data` (`Data`),
  KEY `idOrdem` (`idOrdem`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `parametros2renomeada`
--
DROP TABLE IF EXISTS `parametros2renomeada`;
CREATE TABLE `parametros2renomeada` (
  `idempguia` int(11) NOT NULL,
  `sequencia` int(11) NOT NULL,
  `cooperate` int(11) NOT NULL,
  `PARNOME` varchar(60) DEFAULT NULL,
  `PARULTFATURA` int(11) DEFAULT NULL,
  `PARCONTASFORN` varchar(5) DEFAULT NULL,
  `PARCONTASCLIENTES` varchar(5) DEFAULT NULL,
  `PARCONTASPCT` varchar(5) DEFAULT NULL,
  `PARCONTASCONV` varchar(5) DEFAULT NULL,
  `PARCONTASLABEX` varchar(5) DEFAULT NULL,
  `PARULTNOTA` int(11) DEFAULT NULL,
  `PARMENSSAGEM` text,
  `PARMSGURGENTE` char(1) DEFAULT NULL,
  `parDtUltGeracaoInternet` datetime DEFAULT NULL,
  `parUltRecibo` smallint(6) DEFAULT NULL,
  `ParDtUltEmissaoLaudo` datetime DEFAULT NULL,
  `PARMENSAGEMREC` text,
  `PARMSGURGENTEREC` char(1) DEFAULT 'N',
  `PARNUMFECHACAIXA` mediumint(8) unsigned DEFAULT '0',
  `PARVLRMAXIMODESC` decimal(12,2) DEFAULT NULL,
  `parAssinatura` longblob,
  `parQuemAssina` varchar(50) DEFAULT NULL,
  `parCRFAssinante` varchar(15) DEFAULT NULL,
  `idScriptCabecalho` int(10) unsigned DEFAULT NULL,
  `idScriptRodape` int(10) unsigned DEFAULT NULL,
  `parDtRevisaoIndicadores` date DEFAULT NULL,
  `parMsgRepres` text,
  `PARULTIMONRONFE` int(11) DEFAULT NULL COMMENT 'Campo com o último numero de RPS utilizado na para NFE',
  `PARHomologacaoNRONFE` int(11) DEFAULT NULL,
  `PARPathNFSE` varchar(20) DEFAULT NULL,
  `serie` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idempguia`,`cooperate`),
  UNIQUE KEY `PKParametros` (`PARNOME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `os_pronta_destino`
--
DROP TABLE IF EXISTS `os_pronta_destino`;
CREATE TABLE `os_pronta_destino` (
  `idOrdemServico` int(10) unsigned NOT NULL DEFAULT '0',
  `Destino` char(10) NOT NULL DEFAULT '',
  `DataProcesso` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idOrdemServico`,`Destino`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `de_para_recipiente_dasa`
--
DROP TABLE IF EXISTS `de_para_recipiente_dasa`;
CREATE TABLE `de_para_recipiente_dasa` (
  `idRecipienteDasa` int(11) NOT NULL,
  `descricaoDasa` varchar(50) DEFAULT NULL,
  `idAlvaro` int(11) DEFAULT NULL,
  PRIMARY KEY (`idRecipienteDasa`),
  KEY `idAlvaro` (`idAlvaro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `lotesultimomnemonico`
--
DROP TABLE IF EXISTS `lotesultimomnemonico`;
CREATE TABLE `lotesultimomnemonico` (
  `idUltimoMnemonico` int(11) NOT NULL AUTO_INCREMENT,
  `dtCriacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idTipoLote` int(11) NOT NULL,
  `dsMnemonico` varchar(5) NOT NULL DEFAULT ' ',
  PRIMARY KEY (`idUltimoMnemonico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `exames_acoplados`
--
DROP TABLE IF EXISTS `exames_acoplados`;
CREATE TABLE `exames_acoplados` (
  `Perfil` varchar(12) NOT NULL DEFAULT '',
  `Exames` mediumtext,
  PRIMARY KEY (`Perfil`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `precos_regionais`
--
DROP TABLE IF EXISTS `precos_regionais`;
CREATE TABLE `precos_regionais` (
  `idRepresentante` int(11) NOT NULL,
  `codigoExame` varchar(45) NOT NULL,
  `preco` varchar(45) DEFAULT NULL,
  `data` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idFunc` int(11) DEFAULT NULL,
  PRIMARY KEY (`idRepresentante`,`codigoExame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `reserva_numeracao`
--
DROP TABLE IF EXISTS `reserva_numeracao`;
CREATE TABLE `reserva_numeracao` (
  `Tabela` varchar(64) NOT NULL DEFAULT '',
  `Numeracao` mediumtext,
  PRIMARY KEY (`Tabela`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `nota_fiscal_exportacao`
--
DROP TABLE IF EXISTS `nota_fiscal_exportacao`;
CREATE TABLE `nota_fiscal_exportacao` (
  `idNotaFiscal` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nota` int(10) unsigned DEFAULT NULL,
  `dtExportacao` datetime DEFAULT NULL,
  `idFunc` smallint(5) unsigned DEFAULT NULL,
  `codEspecie` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`idNotaFiscal`),
  KEY `nota` (`nota`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `patrimonio_item`
--
DROP TABLE IF EXISTS `patrimonio_item`;
CREATE TABLE `patrimonio_item` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `DataCompra` date DEFAULT NULL,
  `Fornecedor` varchar(60) DEFAULT NULL,
  `NotaFiscal` varchar(10) DEFAULT NULL,
  `Descricao` varchar(50) DEFAULT NULL,
  `Detalhes` text,
  `idEntidadeComodato` int(10) unsigned DEFAULT NULL,
  `eRepresentante` char(1) DEFAULT 'N',
  `dtEnvioContrato` datetime DEFAULT NULL,
  `dtRetornoContrato` datetime DEFAULT NULL,
  `ValorCompra` decimal(9,2) DEFAULT NULL,
  `codFornecedor` int(10) unsigned NOT NULL,
  `nuSerie` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `entidades_email`
--
DROP TABLE IF EXISTS `entidades_email`;
CREATE TABLE `entidades_email` (
  `idEntidades_Email` int(11) NOT NULL AUTO_INCREMENT,
  `idEntidade` int(11) DEFAULT NULL,
  `Tipo` varchar(60) DEFAULT NULL,
  `Email` varchar(60) DEFAULT NULL,
  `nomeResponsavel` varchar(60) DEFAULT NULL,
  `Telefone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`idEntidades_Email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `motivo_exclusao_os`
--
DROP TABLE IF EXISTS `motivo_exclusao_os`;
CREATE TABLE `motivo_exclusao_os` (
  `idMotivo_exclusao_os` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) NOT NULL,
  `ativo` char(1) NOT NULL,
  PRIMARY KEY (`idMotivo_exclusao_os`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `envio_item`
--
DROP TABLE IF EXISTS `envio_item`;
CREATE TABLE `envio_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idCadastro` int(10) unsigned DEFAULT NULL,
  `Descricao` varchar(50) DEFAULT NULL,
  `Qtde` int(10) unsigned DEFAULT NULL,
  `idProduto` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `env_item_index3593` (`idCadastro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `integracao_intersite_paciente`
--
DROP TABLE IF EXISTS `integracao_intersite_paciente`;
CREATE TABLE `integracao_intersite_paciente` (
  `idPacienteLocal` int(10) unsigned NOT NULL,
  `idPacienteMatriz` int(10) unsigned NOT NULL DEFAULT '0',
  `dataTransferencia` datetime DEFAULT NULL,
  PRIMARY KEY (`idPacienteLocal`),
  KEY `dataTransferencia` (`dataTransferencia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `qc_ausencia`
--
DROP TABLE IF EXISTS `qc_ausencia`;
CREATE TABLE `qc_ausencia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idLote` int(11) NOT NULL,
  `DtRegistro` datetime DEFAULT NULL,
  `DtAusencia` date DEFAULT NULL,
  `Descricao` varchar(100) DEFAULT NULL,
  `NvlControle` char(1) NOT NULL,
  `Liberado` enum('S','N') DEFAULT 'N',
  `idUsrLiberou` int(11) DEFAULT NULL,
  `DtLiberacao` datetime DEFAULT NULL,
  `MotivoLiberacao` text,
  PRIMARY KEY (`id`),
  KEY `Liberado` (`Liberado`,`DtLiberacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `medicinaocupacionalos`
--
DROP TABLE IF EXISTS `medicinaocupacionalos`;
CREATE TABLE `medicinaocupacionalos` (
  `idMedicinaOcupacionalOS` int(11) NOT NULL AUTO_INCREMENT,
  `medOcu_os` int(11) NOT NULL,
  `medOcu_tipo` varchar(50) NOT NULL,
  PRIMARY KEY (`idMedicinaOcupacionalOS`),
  KEY `index2` (`medOcu_os`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `destino_exames`
--
DROP TABLE IF EXISTS `destino_exames`;
CREATE TABLE `destino_exames` (
  `idDestino` int(11) NOT NULL,
  `codExame` varchar(10) NOT NULL,
  `exclusivo` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`idDestino`,`codExame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `resultado_grafico_imagem`
--
DROP TABLE IF EXISTS `resultado_grafico_imagem`;
CREATE TABLE `resultado_grafico_imagem` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `solsid` int(11) NOT NULL,
  `svg` mediumblob NOT NULL,
  `imagem` mediumblob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_res_gra_img_solsid` (`solsid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `relatorios`
--
DROP TABLE IF EXISTS `relatorios`;
CREATE TABLE `relatorios` (
  `relId` int(11) NOT NULL AUTO_INCREMENT,
  `relQuery` blob NOT NULL,
  `reldescricao` blob,
  `reldtUltAcesso` datetime DEFAULT NULL,
  `relUsrUltAcesso` int(11) DEFAULT NULL,
  `relTitulo` varchar(45) NOT NULL,
  `relQuemSolicitou` int(11) NOT NULL,
  `relMaxPeriodo` int(11) NOT NULL DEFAULT '30',
  `relDtSolicitado` datetime NOT NULL,
  `relQtdUtilizado` int(11) DEFAULT '0',
  `relUltErro` varchar(100) DEFAULT NULL,
  `relAutomatizar` varchar(1) DEFAULT 'N',
  PRIMARY KEY (`relId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `dna_filho`
--
DROP TABLE IF EXISTS `dna_filho`;
CREATE TABLE `dna_filho` (
  `filId` int(11) NOT NULL AUTO_INCREMENT,
  `filNome` varchar(50) DEFAULT NULL,
  `filEnder` varchar(50) DEFAULT NULL,
  `filBairro` varchar(20) DEFAULT NULL,
  `filCidade` varchar(20) DEFAULT NULL,
  `filEstado` char(2) DEFAULT NULL,
  `filCep` varchar(9) DEFAULT NULL,
  `filPai` varchar(50) DEFAULT NULL,
  `filMae` varchar(50) DEFAULT NULL,
  `filDtNasc` date DEFAULT NULL,
  `filSexo` enum('Masculino','Feminino') DEFAULT 'Masculino',
  `filCpf` varchar(20) DEFAULT '0',
  `filRg` varchar(20) DEFAULT '0',
  `filExpedidor` varchar(10) DEFAULT NULL,
  `filCor` char(1) DEFAULT NULL,
  `filTransplMedula` enum('Sim','Não') DEFAULT 'Não',
  `filTransfusao` enum('Sim','Não') DEFAULT 'Não',
  `filDtCadastro` date DEFAULT NULL,
  `filQuemCadastrou` int(11) DEFAULT NULL,
  `filFone` varchar(20) DEFAULT NULL,
  `filEMail` varchar(40) DEFAULT NULL,
  `filDtExpedicao` date DEFAULT NULL,
  `filOutroDoc` varchar(50) DEFAULT '0',
  `filDocComprovante` enum('Fornecido','Não Fornecido') DEFAULT 'Fornecido',
  PRIMARY KEY (`filId`),
  UNIQUE KEY `XPKdna_filho` (`filId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `stat_producao`
--
DROP TABLE IF EXISTS `stat_producao`;
CREATE TABLE `stat_producao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` datetime DEFAULT NULL,
  `ttlentrada` int(11) DEFAULT '0',
  `ttlpendente` int(11) DEFAULT '0',
  `ttlpronto` int(11) DEFAULT '0',
  `ttlproduzido` int(11) DEFAULT '0',
  `grupo` enum('END','ELI','BIO','IF','COA','ESO','ALL') NOT NULL DEFAULT 'ALL',
  PRIMARY KEY (`id`),
  KEY `data` (`data`),
  KEY `IdxGrupo` (`grupo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `documento`
--
DROP TABLE IF EXISTS `documento`;
CREATE TABLE `documento` (
  `docid` int(11) NOT NULL AUTO_INCREMENT,
  `docIdEntidade` int(11) NOT NULL,
  `docData` datetime NOT NULL,
  `docPdf` longblob NOT NULL,
  `docDescricao` varchar(45) DEFAULT NULL,
  `docXml` longblob,
  `docCsv` longblob,
  PRIMARY KEY (`docid`),
  KEY `index2` (`docData`),
  KEY `index3` (`docIdEntidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Documentos em blob';
--
-- Table structure for table `tabrecepcoes`
--
DROP TABLE IF EXISTS `tabrecepcoes`;
CREATE TABLE `tabrecepcoes` (
  `recsid` int(11) NOT NULL AUTO_INCREMENT,
  `RECNOME` varchar(40) DEFAULT NULL,
  `RECAREA` int(11) DEFAULT NULL,
  `recIdentificador` smallint(6) DEFAULT '0',
  `recSubUnidade` varchar(6) DEFAULT NULL,
  `SolicitaDtSenha` char(1) DEFAULT NULL,
  `recDtInicioAtendimento` date DEFAULT NULL,
  `recRua` varchar(45) DEFAULT NULL,
  `recNumero` int(11) DEFAULT NULL,
  `recComplemento` varchar(45) DEFAULT NULL,
  `recBairro` varchar(45) DEFAULT NULL,
  `recCidade` varchar(45) DEFAULT NULL,
  `recEstado` varchar(2) DEFAULT NULL,
  `recCep` varchar(9) DEFAULT NULL,
  `recFone` varchar(20) DEFAULT NULL,
  `recFax` varchar(20) DEFAULT NULL,
  `recResponsavel` varchar(45) DEFAULT NULL,
  `recCNPJ` varchar(45) DEFAULT NULL,
  `recEstFiscal` varchar(45) DEFAULT NULL,
  `recPorte` varchar(10) DEFAULT NULL,
  `recDiasAtendimento` varchar(20) DEFAULT NULL,
  `recEmiteRPS` char(1) DEFAULT 'S',
  `recFormaPagto` varchar(80) DEFAULT NULL,
  `recRealizaTesteAgendado` varchar(1) DEFAULT 'N',
  `recRamal` varchar(10) DEFAULT NULL,
  `recCentroCusto` varchar(20) DEFAULT NULL,
  `recListaTestes` varchar(200) DEFAULT NULL,
  `recIniAtendSemana` time DEFAULT NULL,
  `recFimAtendSemana` time DEFAULT NULL,
  `recIniAtendSabado` time DEFAULT NULL,
  `recFimAtendSabado` time DEFAULT NULL,
  `recIniAtendDomingo` time DEFAULT NULL,
  `recFimAtendDomingo` time DEFAULT NULL,
  `recObservacoes` varchar(100) DEFAULT NULL,
  `recAtiva` varchar(1) DEFAULT 'S',
  `RecIdEmpresa` int(11) DEFAULT NULL,
  PRIMARY KEY (`recsid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `qc_analisecritica`
--
DROP TABLE IF EXISTS `qc_analisecritica`;
CREATE TABLE `qc_analisecritica` (
  `idQCAC` int(11) NOT NULL AUTO_INCREMENT,
  `idLote` int(11) DEFAULT NULL,
  `idUsuario` int(11) DEFAULT NULL,
  `Analise` text,
  `DtHora` datetime DEFAULT NULL,
  PRIMARY KEY (`idQCAC`),
  KEY `idxDtHora` (`DtHora`),
  KEY `idxLote` (`idLote`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `stat_exame_teste`
--
DROP TABLE IF EXISTS `stat_exame_teste`;
CREATE TABLE `stat_exame_teste` (
  `idstat_exame` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `area` char(1) DEFAULT NULL,
  `Entidade` int(10) unsigned DEFAULT NULL,
  `exame` varchar(10) DEFAULT NULL,
  `etapa` tinyint(4) DEFAULT NULL,
  `Data` varchar(8) DEFAULT NULL,
  `qtde` int(11) DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `idEmpresa` int(10) unsigned DEFAULT NULL,
  `idMedico` int(10) unsigned DEFAULT '0',
  `qtdePct` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`idstat_exame`),
  KEY `area` (`area`,`Entidade`,`exame`,`Data`),
  KEY `etapa` (`etapa`,`Data`),
  KEY `stat_exame_medico` (`area`,`idMedico`,`exame`,`Data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `rejeicao_amostra`
--
DROP TABLE IF EXISTS `rejeicao_amostra`;
CREATE TABLE `rejeicao_amostra` (
  `rejSid` int(11) NOT NULL AUTO_INCREMENT,
  `rejDescricao` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`rejSid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `entidade_tabconvenio`
--
DROP TABLE IF EXISTS `entidade_tabconvenio`;
CREATE TABLE `entidade_tabconvenio` (
  `entsid` int(11) NOT NULL DEFAULT '0',
  `idtabela` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`entsid`,`idtabela`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `notafiscal_ordemservico`
--
DROP TABLE IF EXISTS `notafiscal_ordemservico`;
CREATE TABLE `notafiscal_ordemservico` (
  `NFOSordSid` int(11) NOT NULL,
  `NFOSnotsid` int(11) NOT NULL,
  PRIMARY KEY (`NFOSordSid`,`NFOSnotsid`),
  KEY `FK_NFOSordSid` (`NFOSordSid`),
  KEY `FK_NFOSnotSid` (`NFOSnotsid`),
  CONSTRAINT `FK_NFOSnotSid` FOREIGN KEY (`NFOSnotsid`) REFERENCES `notas_fiscais` (`notsid`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_NFOSordSid` FOREIGN KEY (`NFOSordSid`) REFERENCES `ordem_servico` (`ordsid`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela de ligação entre as Ordem de Serviços e notas fiscais';
--
-- Table structure for table `envio_cadastro`
--
DROP TABLE IF EXISTS `envio_cadastro`;
CREATE TABLE `envio_cadastro` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `DtEnvio` datetime DEFAULT NULL,
  `idDe` int(10) unsigned DEFAULT NULL,
  `idPara` int(10) unsigned DEFAULT NULL,
  `Transportadora` varchar(40) DEFAULT NULL,
  `Conhecimento` varchar(20) DEFAULT NULL,
  `LocalEmbarque` varchar(30) DEFAULT NULL,
  `QtdVolume` smallint(5) unsigned DEFAULT NULL,
  `Tipo` char(1) DEFAULT NULL,
  `Obs` varchar(250) DEFAULT NULL,
  `idFunc` int(10) unsigned DEFAULT NULL,
  `dtCadastro` datetime DEFAULT NULL,
  `TipoCadDe` char(1) DEFAULT NULL,
  `TipoCadPara` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `env_cadastro_index3595` (`DtEnvio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `log_medico`
--
DROP TABLE IF EXISTS `log_medico`;
CREATE TABLE `log_medico` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Data` datetime DEFAULT NULL,
  `Quem` smallint(5) unsigned DEFAULT NULL,
  `Operacao` varchar(10) DEFAULT NULL,
  `Texto` text,
  `idMedico` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idMedico` (`idMedico`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `dmed_entidade`
--
DROP TABLE IF EXISTS `dmed_entidade`;
CREATE TABLE `dmed_entidade` (
  `idEntidade` int(10) unsigned NOT NULL,
  `nomeResponsavel` char(50) DEFAULT '',
  `cpfResponsavel` bigint(11) unsigned DEFAULT '0',
  `tipoDeclarante` smallint(5) unsigned DEFAULT '0',
  `oppas` char(5) DEFAULT NULL COMMENT 'Operadora Plano Privado Assist Saude',
  `cnes` int(10) unsigned DEFAULT '0' COMMENT 'Cad. Nacional de Estab. Saude',
  PRIMARY KEY (`idEntidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `qc_logviolacao`
--
DROP TABLE IF EXISTS `qc_logviolacao`;
CREATE TABLE `qc_logviolacao` (
  `idSequencia` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idLote` int(10) unsigned NOT NULL DEFAULT '0',
  `idPonto` int(10) unsigned NOT NULL DEFAULT '0',
  `Liberado` enum('S','N') NOT NULL DEFAULT 'N',
  `idUsrLiberou` int(11) NOT NULL DEFAULT '0',
  `DtLiberacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `MotivoLiberacao` text,
  PRIMARY KEY (`idSequencia`),
  KEY `lote` (`idLote`),
  KEY `ponto` (`idPonto`),
  KEY `Liberado` (`Liberado`,`DtLiberacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `examesolicitadoparticipantes`
--
DROP TABLE IF EXISTS `examesolicitadoparticipantes`;
CREATE TABLE `examesolicitadoparticipantes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idExameSolicitado` int(11) NOT NULL,
  `identificador` varchar(200) NOT NULL,
  `coletor` varchar(150) NOT NULL,
  `nomeJuiz` varchar(150) NOT NULL,
  `qtdeEtiqueta` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `materialexamapoiob2b`
--
DROP TABLE IF EXISTS `materialexamapoiob2b`;
CREATE TABLE `materialexamapoiob2b` (
  `id_exam` int(11) NOT NULL,
  `id_material` varchar(45) NOT NULL,
  PRIMARY KEY (`id_exam`,`id_material`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `bandejas`
--
DROP TABLE IF EXISTS `bandejas`;
CREATE TABLE `bandejas` (
  `idbandejas` int(11) NOT NULL AUTO_INCREMENT,
  `bandNome` varchar(45) DEFAULT NULL,
  `bandAbreviatura` varchar(2) DEFAULT NULL,
  `bandQuantidade` int(11) DEFAULT NULL,
  `bandSetor` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`idbandejas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `os_atendimento`
--
DROP TABLE IF EXISTS `os_atendimento`;
CREATE TABLE `os_atendimento` (
  `idatendimento` int(11) NOT NULL AUTO_INCREMENT,
  `idordem` int(11) DEFAULT NULL,
  `pct` int(11) DEFAULT NULL,
  `tipo_acidente` varchar(50) DEFAULT NULL,
  `RN` char(1) DEFAULT 'N',
  PRIMARY KEY (`idatendimento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `stat_exame`
--
DROP TABLE IF EXISTS `stat_exame`;
CREATE TABLE `stat_exame` (
  `idstat_exame` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `area` char(1) DEFAULT NULL,
  `Entidade` int(10) unsigned DEFAULT NULL,
  `exame` varchar(10) DEFAULT NULL,
  `etapa` tinyint(4) DEFAULT NULL,
  `Data` varchar(8) DEFAULT NULL,
  `qtde` int(11) DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `idEmpresa` int(10) unsigned DEFAULT NULL,
  `idMedico` int(10) unsigned DEFAULT '0',
  `qtdePct` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`idstat_exame`),
  KEY `area` (`area`,`Entidade`,`exame`,`Data`),
  KEY `etapa` (`etapa`,`Data`),
  KEY `stat_exame_medico` (`area`,`idMedico`,`exame`,`Data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `cidade`
--
DROP TABLE IF EXISTS `cidade`;
CREATE TABLE `cidade` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Uf` char(2) DEFAULT NULL,
  `Descricao` char(40) DEFAULT NULL,
  `Categoria` enum('Vila','Cidade','Capital') DEFAULT NULL,
  `Fonte` char(16) DEFAULT NULL,
  `Latitude` int(11) DEFAULT NULL,
  `Longitude` int(11) DEFAULT NULL,
  `Altitude` int(11) DEFAULT NULL,
  `Area` decimal(10,2) DEFAULT NULL,
  `AnoInstalacao` year(4) DEFAULT NULL,
  `Municipio` char(40) DEFAULT NULL,
  `CodigoUf` char(2) DEFAULT NULL,
  `CodigoMunicipio` int(11) DEFAULT NULL,
  `Digito` tinyint(4) DEFAULT NULL,
  `Distrito` tinyint(4) DEFAULT NULL,
  `indiceIBGE` int(11) DEFAULT '0',
  `regiao` char(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `estado` (`Uf`),
  KEY `cidade` (`Descricao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `pdf_questionario`
--
DROP TABLE IF EXISTS `pdf_questionario`;
CREATE TABLE `pdf_questionario` (
  `CodRelatorio` int(10) NOT NULL AUTO_INCREMENT,
  `nome` varchar(60) DEFAULT NULL,
  `RelImportado` blob,
  PRIMARY KEY (`CodRelatorio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `rastbandejatemp`
--
DROP TABLE IF EXISTS `rastbandejatemp`;
CREATE TABLE `rastbandejatemp` (
  `idrastBandejaTEMP` int(11) NOT NULL AUTO_INCREMENT,
  `seq` varchar(45) DEFAULT NULL,
  `os` varchar(45) DEFAULT NULL,
  `exames` varchar(45) DEFAULT NULL,
  `bandejas` varchar(45) DEFAULT NULL,
  `paciente` varchar(45) DEFAULT NULL,
  `dt_cadastro` varchar(45) DEFAULT NULL,
  `entidade` varchar(45) DEFAULT NULL,
  `observacao` varchar(45) DEFAULT NULL,
  `gavetas` varchar(45) DEFAULT NULL,
  `alicotas` varchar(45) DEFAULT NULL,
  `quant_bandeja` varchar(45) DEFAULT NULL,
  `data_entr_bandeja` varchar(45) DEFAULT NULL,
  `codigo_amostra` varchar(45) DEFAULT NULL,
  `funcionario` int(11) DEFAULT NULL,
  PRIMARY KEY (`idrastBandejaTEMP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `doenca_exame`
--
DROP TABLE IF EXISTS `doenca_exame`;
CREATE TABLE `doenca_exame` (
  `idDoenca` int(11) DEFAULT NULL,
  `Exame` varchar(10) DEFAULT NULL,
  KEY `Exame` (`Exame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `stat_exame_laudo`
--
DROP TABLE IF EXISTS `stat_exame_laudo`;
CREATE TABLE `stat_exame_laudo` (
  `idstat_exame` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `area` char(1) DEFAULT NULL,
  `Entidade` int(10) unsigned DEFAULT NULL,
  `exame` varchar(10) DEFAULT NULL,
  `etapa` tinyint(4) DEFAULT NULL,
  `Data` varchar(8) DEFAULT NULL,
  `qtde` int(11) DEFAULT NULL,
  `valor` decimal(11,2) DEFAULT NULL,
  `qtdePct` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`idstat_exame`),
  KEY `area` (`area`,`Entidade`,`exame`,`Data`),
  KEY `etapa` (`etapa`,`Data`),
  KEY `stat_exame_medico` (`area`,`exame`,`Data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `rep_concorrencia`
--
DROP TABLE IF EXISTS `rep_concorrencia`;
CREATE TABLE `rep_concorrencia` (
  `idRepresentante` int(10) unsigned NOT NULL DEFAULT '0',
  `AnoMes` char(6) NOT NULL DEFAULT '',
  `idConcorrencia` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idRepresentante`,`AnoMes`,`idConcorrencia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `srt_estante`
--
DROP TABLE IF EXISTS `srt_estante`;
CREATE TABLE `srt_estante` (
  `ETSID` int(11) NOT NULL AUTO_INCREMENT,
  `ETCOL` smallint(6) DEFAULT NULL,
  `ETLINHA` smallint(6) DEFAULT NULL,
  `ETCOD` varchar(20) DEFAULT NULL,
  `ETULTDATE` datetime DEFAULT NULL,
  `ETNTOEXECUCAO` text,
  PRIMARY KEY (`ETSID`),
  UNIQUE KEY `ETCOD` (`ETCOD`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `sms_mensagem`
--
DROP TABLE IF EXISTS `sms_mensagem`;
CREATE TABLE `sms_mensagem` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `texto` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `cri_normaliza_resultado`
--
DROP TABLE IF EXISTS `cri_normaliza_resultado`;
CREATE TABLE `cri_normaliza_resultado` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Comparacao` char(1) DEFAULT '=',
  `idCritica` int(10) unsigned NOT NULL DEFAULT '0',
  `ResultadoOriginal` varchar(40) DEFAULT NULL,
  `ResultadoSubstituto` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idCritica` (`idCritica`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `bo`
--
DROP TABLE IF EXISTS `bo`;
CREATE TABLE `bo` (
  `boId` int(11) NOT NULL AUTO_INCREMENT,
  `boDeSetor` char(3) DEFAULT NULL,
  `boData` datetime DEFAULT NULL,
  `boTipo` char(1) DEFAULT NULL,
  `boTpSolic` char(1) DEFAULT NULL,
  `boFunc` int(11) DEFAULT NULL,
  `boNome` varchar(40) DEFAULT NULL,
  `boIdEnt` int(11) DEFAULT NULL,
  `boNomeEnt` varchar(40) DEFAULT NULL,
  `boFoneEnt` varchar(20) DEFAULT NULL,
  `boDescr` tinytext,
  `boAcao` tinytext,
  `boConclusao` tinytext,
  `boIdFuncConcl` int(11) DEFAULT NULL,
  `boDtConcl` datetime DEFAULT NULL,
  `boIdFuncAprovacao` int(11) DEFAULT NULL,
  `boDtAprovacao` datetime DEFAULT NULL,
  `boIdCategoria` int(11) DEFAULT NULL,
  `boTpGrupo` char(1) DEFAULT NULL,
  `boParaSetor` char(3) DEFAULT NULL,
  `boDtAcao` datetime DEFAULT NULL,
  `boIdFuncAcao` int(11) DEFAULT NULL,
  PRIMARY KEY (`boId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `ordem_servico`
--
DROP TABLE IF EXISTS `ordem_servico`;
CREATE TABLE `ordem_servico` (
  `ordsid` int(11) NOT NULL AUTO_INCREMENT,
  `ORDPCT` int(11) DEFAULT NULL,
  `orddtentr` datetime DEFAULT NULL,
  `ORDQUEMCADASTROU` smallint(6) DEFAULT NULL,
  `ORDQUEMCOLHEU` smallint(6) DEFAULT NULL,
  `ORDAREA` char(1) DEFAULT NULL,
  `ORDOBSPCT` varchar(200) DEFAULT NULL,
  `ORDVLRTOTAL` decimal(12,2) DEFAULT '0.00',
  `ORDVLRDESCONTO` decimal(12,2) DEFAULT '0.00',
  `ORDVLRTAX` decimal(12,2) DEFAULT '0.00',
  `ORDVLRTROCO` decimal(12,2) DEFAULT NULL,
  `ORDRECEPCAO` smallint(6) DEFAULT NULL,
  `ORDQTDEXAMES` smallint(6) DEFAULT NULL,
  `orddtprometida` datetime DEFAULT NULL,
  `ORDMEDICAMENTO` varchar(100) DEFAULT NULL,
  `orddtcoleta` datetime DEFAULT NULL,
  `ORDGUIA` varchar(35) DEFAULT NULL,
  `ordreal` decimal(12,2) DEFAULT NULL,
  `ORDOUTRO` decimal(12,2) DEFAULT NULL,
  `ORDVLRPAGO` decimal(12,2) DEFAULT NULL,
  `ORDCONVENIO` smallint(6) DEFAULT NULL,
  `ORDLOCALCOLETA` varchar(40) DEFAULT NULL,
  `OrdDtLancamento` datetime DEFAULT NULL,
  `ORDVLRBRUTO` decimal(12,2) DEFAULT NULL,
  `ordLocalEntrega` varchar(100) DEFAULT NULL,
  `ordDtAlteracao` datetime DEFAULT NULL,
  `ordQuemAlterou` int(11) DEFAULT NULL,
  `orddtprometida_cliente` datetime DEFAULT NULL,
  PRIMARY KEY (`ordsid`),
  KEY `orddtentr` (`orddtentr`),
  KEY `ORDPCT` (`ORDPCT`),
  KEY `ORDQUEMCOLHEU` (`ORDQUEMCOLHEU`,`orddtcoleta`),
  KEY `index01_ordem_servico` (`ordsid`,`ORDPCT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `chegada_material`
--
DROP TABLE IF EXISTS `chegada_material`;
CREATE TABLE `chegada_material` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idRegiao` smallint(5) unsigned DEFAULT NULL,
  `Data` datetime DEFAULT NULL,
  `Temperatura` decimal(4,1) DEFAULT NULL,
  `idQuem` smallint(5) unsigned DEFAULT NULL,
  `Observacao` varchar(100) DEFAULT NULL,
  `idCaixa` int(11) DEFAULT NULL,
  `idCaixaTemporaria` char(8) DEFAULT NULL,
  `idEntidade` int(11) DEFAULT NULL,
  `tipoEntrada` enum('automatizada','manual','indefinida') DEFAULT 'indefinida',
  `cxAtrasada` varchar(1) DEFAULT 'N',
  `faixaTemperatura` int(1) DEFAULT NULL,
  `recond` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `data` (`Data`,`idRegiao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `importacao_inadimplentes`
--
DROP TABLE IF EXISTS `importacao_inadimplentes`;
CREATE TABLE `importacao_inadimplentes` (
  `idImportacaoInadimplentes` varchar(20) NOT NULL,
  `dataArquivo` date DEFAULT NULL,
  `estabelecimento` varchar(3) DEFAULT NULL,
  `especie` varchar(2) DEFAULT NULL,
  `titulo` varchar(15) DEFAULT NULL,
  `parcela` varchar(3) DEFAULT NULL,
  `serie` varchar(3) DEFAULT NULL,
  `clienteEMS` int(11) DEFAULT NULL,
  `clienteAlvaro` int(11) DEFAULT NULL,
  `grupoClienteCarteira` varchar(3) DEFAULT NULL,
  `dataEmissaoNota` date DEFAULT NULL,
  `dataVencimentoNota` date DEFAULT NULL,
  `valorOriginal` decimal(10,2) DEFAULT NULL,
  `saldo` decimal(10,2) DEFAULT NULL,
  `dias` int(11) DEFAULT NULL,
  `mercado` varchar(10) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `sinonimia` varchar(50) DEFAULT NULL,
  `repsid` int(11) DEFAULT NULL,
  PRIMARY KEY (`idImportacaoInadimplentes`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `log_exame`
--
DROP TABLE IF EXISTS `log_exame`;
CREATE TABLE `log_exame` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Tabela` varchar(30) DEFAULT NULL,
  `ConteudoAnterior` text,
  `Quem` int(11) DEFAULT NULL,
  `Data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Operacao` varchar(10) DEFAULT NULL,
  `Exame` varchar(10) DEFAULT NULL,
  `idTabela` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `log_data` (`Data`),
  KEY `log_exame_data` (`Exame`,`Data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `participantes`
--
DROP TABLE IF EXISTS `participantes`;
CREATE TABLE `participantes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idItemExameParticipante` int(11) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `codigoParticipante` int(11) NOT NULL,
  `sexo` char(1) NOT NULL,
  `tipoParticipante` varchar(100) NOT NULL,
  `obs` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `precos`
--
DROP TABLE IF EXISTS `precos`;
CREATE TABLE `precos` (
  `PCOENTIDADE` int(11) NOT NULL DEFAULT '0',
  `pcoExame` varchar(10) NOT NULL DEFAULT '',
  `PCOVALOR` float DEFAULT NULL,
  `pcoQtdExamesMes` int(10) unsigned DEFAULT '0',
  `pcoDataValidade` datetime DEFAULT NULL,
  PRIMARY KEY (`PCOENTIDADE`,`pcoExame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `entidade_cartao_agrupamento`
--
DROP TABLE IF EXISTS `entidade_cartao_agrupamento`;
CREATE TABLE `entidade_cartao_agrupamento` (
  `idAgrupado` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idEntidadePrincipal` int(11) NOT NULL,
  `idEntidadeAgrupada` int(11) NOT NULL,
  `dataagrupamento` datetime NOT NULL,
  `quemagrupou` int(11) NOT NULL,
  PRIMARY KEY (`idAgrupado`),
  UNIQUE KEY `index_entidades` (`idEntidadePrincipal`,`idEntidadeAgrupada`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `logistica_celulatransferencia`
--
DROP TABLE IF EXISTS `logistica_celulatransferencia`;
CREATE TABLE `logistica_celulatransferencia` (
  `idtransf` int(11) NOT NULL AUTO_INCREMENT,
  `datahora` datetime NOT NULL,
  `entsid` int(10) unsigned NOT NULL DEFAULT '0',
  `repsid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idtransf`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC COMMENT='transferencias feitas da celula para cliente ou consumo';
--
-- Table structure for table `dna_genitores`
--
DROP TABLE IF EXISTS `dna_genitores`;
CREATE TABLE `dna_genitores` (
  `idosfilho` int(11) NOT NULL DEFAULT '0',
  `idosgenitor` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idosfilho`,`idosgenitor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `envia_fax`
--
DROP TABLE IF EXISTS `envia_fax`;
CREATE TABLE `envia_fax` (
  `idEnvFax` int(11) NOT NULL AUTO_INCREMENT,
  `envCodGerado` varchar(40) DEFAULT NULL,
  `EnvidEntidade` int(11) DEFAULT NULL,
  `EnvPdf` longblob,
  `EnvDataGerado` datetime DEFAULT NULL,
  `EnvDataEnviado` datetime DEFAULT NULL,
  `envListOs` varchar(100) DEFAULT NULL,
  `EnvEnviado` char(10) NOT NULL DEFAULT 'N',
  `envFunc` int(11) NOT NULL,
  `envTel` varchar(45) NOT NULL,
  PRIMARY KEY (`idEnvFax`),
  UNIQUE KEY `envCodGerado_UNIQUE` (`envCodGerado`),
  KEY `index_EnvidEntidade` (`EnvidEntidade`),
  KEY `index_Entidade_data` (`EnvidEntidade`,`EnvDataGerado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `stat_equipamento_bloqueio`
--
DROP TABLE IF EXISTS `stat_equipamento_bloqueio`;
CREATE TABLE `stat_equipamento_bloqueio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idequipamento` int(11) DEFAULT NULL,
  `idexame` varchar(20) DEFAULT NULL,
  `idviolacaoqc` int(11) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idxEquipamento` (`idequipamento`),
  KEY `IdxExameEquipamento` (`idexame`,`idequipamento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `entidade_guiaconvenio`
--
DROP TABLE IF EXISTS `entidade_guiaconvenio`;
CREATE TABLE `entidade_guiaconvenio` (
  `entsid` int(11) NOT NULL,
  `ordsid` int(11) NOT NULL,
  `nroguia` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entsid`,`ordsid`,`nroguia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `refvalamostra`
--
DROP TABLE IF EXISTS `refvalamostra`;
CREATE TABLE `refvalamostra` (
  `RefVALPerfil_idPerfil` int(10) unsigned NOT NULL DEFAULT '0',
  `idAmostra` int(10) unsigned NOT NULL DEFAULT '0',
  `DtAmostra` datetime DEFAULT NULL,
  `VlrAmostra` decimal(10,2) DEFAULT NULL,
  `Sexo` enum('M','F') DEFAULT NULL,
  `Equipamento` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`RefVALPerfil_idPerfil`,`idAmostra`),
  KEY `RefVALAmostra_FKIndex1` (`RefVALPerfil_idPerfil`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `remarcacao_motivo`
--
DROP TABLE IF EXISTS `remarcacao_motivo`;
CREATE TABLE `remarcacao_motivo` (
  `idremarcacao_motivo` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`idremarcacao_motivo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `wl_llr_applaudos01`
--
DROP TABLE IF EXISTS `wl_llr_applaudos01`;
CREATE TABLE `wl_llr_applaudos01` (
  `XIDSTR` varchar(40) NOT NULL,
  `POOLNAMESTR` varchar(64) DEFAULT NULL,
  `RECORDSTR` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`XIDSTR`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `cbhpm_edicaoexames`
--
DROP TABLE IF EXISTS `cbhpm_edicaoexames`;
CREATE TABLE `cbhpm_edicaoexames` (
  `EdicaoExamesid` int(11) NOT NULL AUTO_INCREMENT,
  `eexIdEdicaoCBHPM` int(11) NOT NULL,
  `eexIdEdicaoPorte` int(11) NOT NULL,
  `eexCodExameCBHPM` varchar(45) DEFAULT NULL,
  `eexDscExameCBHPM` varchar(120) DEFAULT NULL,
  `eexCodExameLabor` varchar(10) DEFAULT NULL,
  `eexCustoOperExame` decimal(9,3) DEFAULT NULL,
  `eexVlrPorteExame` decimal(9,3) DEFAULT NULL,
  PRIMARY KEY (`EdicaoExamesid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `logatualizacaosistema`
--
DROP TABLE IF EXISTS `logatualizacaosistema`;
CREATE TABLE `logatualizacaosistema` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sistema` varchar(100) DEFAULT 'Labor',
  `Solicitante` varchar(50) DEFAULT NULL,
  `Responsavel` varchar(50) DEFAULT NULL,
  `DataSolicitacao` date DEFAULT NULL,
  `HoraSolicitacao` time DEFAULT NULL,
  `DataLiberacao` date DEFAULT NULL,
  `HoraLiberacao` time DEFAULT NULL,
  `DscAtualizacao` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `logistica_dadosentrega`
--
DROP TABLE IF EXISTS `logistica_dadosentrega`;
CREATE TABLE `logistica_dadosentrega` (
  `dadSid` int(11) NOT NULL AUTO_INCREMENT,
  `dadNome` varchar(40) DEFAULT NULL,
  `dadEndereco` varchar(40) DEFAULT NULL,
  `dadCpfResponsavel` varchar(14) DEFAULT NULL,
  `dadCidade` varchar(20) DEFAULT NULL,
  `dadCep` varchar(11) DEFAULT NULL,
  `dadfone` varchar(30) DEFAULT NULL,
  `dadCelular` varchar(20) DEFAULT NULL,
  `dadBairro` varchar(20) DEFAULT NULL,
  `dadRepSid` int(11) DEFAULT NULL,
  `daduf` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`dadSid`),
  UNIQUE KEY `dadSid_UNIQUE` (`dadSid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `ips`
--
DROP TABLE IF EXISTS `ips`;
CREATE TABLE `ips` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(200) NOT NULL DEFAULT '0',
  `data` date NOT NULL DEFAULT '0000-00-00',
  `tempo` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `diferenca`
--
DROP TABLE IF EXISTS `diferenca`;
CREATE TABLE `diferenca` (
  `entSid` int(11) DEFAULT NULL,
  `entNome` char(100) DEFAULT NULL,
  `solExame` char(10) DEFAULT NULL,
  `solQtdAmostras` int(11) DEFAULT NULL,
  `vlrCalc` decimal(11,2) DEFAULT NULL,
  `vlrNormal` decimal(11,2) DEFAULT NULL,
  `tipo` char(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `dmed_paciente`
--
DROP TABLE IF EXISTS `dmed_paciente`;
CREATE TABLE `dmed_paciente` (
  `idPaciente` int(11) NOT NULL,
  `cpfResponsavel` char(14) DEFAULT NULL,
  `nomeResponsavel` char(50) DEFAULT NULL,
  `idParentesco` char(2) DEFAULT '00',
  `DmedRua` varchar(45) DEFAULT '',
  `Dmednumero` varchar(10) DEFAULT '0',
  `Dmedcomplemento` varchar(100) DEFAULT '',
  `DmedBairro` varchar(45) DEFAULT '',
  `DmedCep` varchar(14) DEFAULT '',
  `DmedIDCidade` int(11) DEFAULT '0',
  `DmedEstado` varchar(2) DEFAULT '',
  `DmedTipoPessoa` varchar(2) DEFAULT 'PF',
  `DmedObservacao` varchar(255) DEFAULT '',
  `dmedInscrMunicipal` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`idPaciente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `exames_repetidos`
--
DROP TABLE IF EXISTS `exames_repetidos`;
CREATE TABLE `exames_repetidos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idSolic` int(11) DEFAULT NULL,
  `DataCadastro` datetime DEFAULT NULL,
  `DataPrevisao` datetime DEFAULT NULL,
  `Obs` varchar(250) DEFAULT NULL,
  `idResponsavel` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Solsid` (`idSolic`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `faixa_desconto`
--
DROP TABLE IF EXISTS `faixa_desconto`;
CREATE TABLE `faixa_desconto` (
  `fdid` int(11) NOT NULL AUTO_INCREMENT,
  `fdCodUsr` int(11) NOT NULL,
  `fdMaxDesc` decimal(5,2) DEFAULT '0.00',
  PRIMARY KEY (`fdid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `rastamostrabandeja`
--
DROP TABLE IF EXISTS `rastamostrabandeja`;
CREATE TABLE `rastamostrabandeja` (
  `id_rastAmostraBandeja` int(11) NOT NULL AUTO_INCREMENT,
  `tabOS` int(11) DEFAULT NULL,
  `tabIdBandeja` int(11) DEFAULT NULL,
  `tabCodigoBarra` varchar(12) DEFAULT NULL,
  `tabData` datetime DEFAULT NULL,
  PRIMARY KEY (`id_rastAmostraBandeja`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `logistica_regiaorepresentante`
--
DROP TABLE IF EXISTS `logistica_regiaorepresentante`;
CREATE TABLE `logistica_regiaorepresentante` (
  `idRepresentante` int(11) NOT NULL,
  `idRota` int(11) NOT NULL,
  `qtdReposicaoMensal` int(11) DEFAULT '2',
  PRIMARY KEY (`idRepresentante`,`idRota`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `documento_notas_fiscais`
--
DROP TABLE IF EXISTS `documento_notas_fiscais`;
CREATE TABLE `documento_notas_fiscais` (
  `docid` int(11) NOT NULL,
  `notsid` int(11) NOT NULL,
  PRIMARY KEY (`docid`,`notsid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `log_entidades_exames`
--
DROP TABLE IF EXISTS `log_entidades_exames`;
CREATE TABLE `log_entidades_exames` (
  `idLogentidades_exames` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ConteudoAnterior` text,
  `Quem` varchar(45) DEFAULT NULL,
  `Data` datetime DEFAULT NULL,
  `Operacao` enum('Inserção','Edição','Exclusão') DEFAULT NULL,
  `identidades_exames` int(11) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idLogentidades_exames`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabela para registrar o log das Alteracoes';
--
-- Table structure for table `log_etiquetas`
--
DROP TABLE IF EXISTS `log_etiquetas`;
CREATE TABLE `log_etiquetas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idOrdemServico` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `idNTO` int(11) NOT NULL,
  `dataHoraImpressao` datetime NOT NULL,
  `qtdeEtiquetasImpressas` int(11) NOT NULL,
  `localImpressao` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `textoliberacao`
--
DROP TABLE IF EXISTS `textoliberacao`;
CREATE TABLE `textoliberacao` (
  `idTxtLiberacao` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `DscTxtLiberacao` text,
  PRIMARY KEY (`idTxtLiberacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `apoio_exames_realizados`
--
DROP TABLE IF EXISTS `apoio_exames_realizados`;
CREATE TABLE `apoio_exames_realizados` (
  `CodExame` varchar(10) NOT NULL DEFAULT '',
  `idLabApoio` int(11) NOT NULL DEFAULT '0',
  `CodExameExterno` varchar(30) DEFAULT NULL,
  `LigacoesResultado` text,
  `idEntidade` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`idLabApoio`,`CodExame`,`idEntidade`),
  KEY `FK_Exames` (`CodExame`),
  KEY `FK_Entidades` (`idLabApoio`),
  KEY `CodExameExterno` (`CodExameExterno`),
  KEY `idEntidade` (`idEntidade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `dna_parametro`
--
DROP TABLE IF EXISTS `dna_parametro`;
CREATE TABLE `dna_parametro` (
  `id` int(11) NOT NULL DEFAULT '0',
  `ParametroImg` blob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `conv_categoria`
--
DROP TABLE IF EXISTS `conv_categoria`;
CREATE TABLE `conv_categoria` (
  `idCategoria` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `Descricao` char(40) DEFAULT NULL,
  `tipo` enum('Peso','Intervalo','Formula') DEFAULT NULL,
  `UnidBase` char(10) DEFAULT NULL,
  PRIMARY KEY (`idCategoria`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `logistica_celulaconsumo`
--
DROP TABLE IF EXISTS `logistica_celulaconsumo`;
CREATE TABLE `logistica_celulaconsumo` (
  `idConsumo` int(11) NOT NULL AUTO_INCREMENT,
  `idEstoque` int(11) NOT NULL,
  `dtEnvio` datetime NOT NULL,
  `idRepresentante` int(11) NOT NULL,
  `qtdConsumida` int(11) NOT NULL,
  `idTransf` int(11) DEFAULT NULL,
  PRIMARY KEY (`idConsumo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `refvalmedia`
--
DROP TABLE IF EXISTS `refvalmedia`;
CREATE TABLE `refvalmedia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idexame` varchar(20) DEFAULT NULL,
  `sexo` enum('M','F','A') DEFAULT 'A',
  `dia` int(11) DEFAULT '0',
  `mes` int(11) DEFAULT '1',
  `ano` int(11) DEFAULT '0',
  `media` decimal(10,2) DEFAULT NULL,
  `dsvpadrao` decimal(10,2) DEFAULT NULL,
  `qtdAmostras` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `defcurva`
--
DROP TABLE IF EXISTS `defcurva`;
CREATE TABLE `defcurva` (
  `idCurva` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `dfcexame` varchar(10) DEFAULT NULL,
  `vhMin` decimal(10,4) DEFAULT NULL,
  `vhMax` decimal(10,4) DEFAULT NULL,
  `vvMin` decimal(10,4) DEFAULT NULL,
  `vvMax` decimal(10,4) DEFAULT NULL,
  `LabelSup` varchar(25) DEFAULT NULL,
  `LabelInf` varchar(25) DEFAULT NULL,
  `Largura` smallint(6) DEFAULT NULL,
  `Altura` smallint(6) DEFAULT NULL,
  `LarguraPonto` tinyint(4) DEFAULT NULL,
  `EstiloGrafico` enum('Pontilhado','Solido','Linha') DEFAULT NULL,
  `Definicoes` set('PontoValor','DesenhaGrade','DesenhaQuadro') DEFAULT NULL,
  PRIMARY KEY (`idCurva`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `tabela_precos_srv`
--
DROP TABLE IF EXISTS `tabela_precos_srv`;
CREATE TABLE `tabela_precos_srv` (
  `tpsId` int(11) NOT NULL AUTO_INCREMENT,
  `tpsDescricao` varchar(50) DEFAULT NULL,
  `tpsDtCriacao` date DEFAULT NULL,
  `tpsDtUltAlteracao` date DEFAULT NULL,
  `tpsResponsavel` varchar(20) DEFAULT NULL,
  `tpsTabelaExterna` char(1) DEFAULT 'N',
  `tpsFaixaSuperior` int(11) DEFAULT NULL,
  PRIMARY KEY (`tpsId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `concorrencia`
--
DROP TABLE IF EXISTS `concorrencia`;
CREATE TABLE `concorrencia` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `Nome` varchar(40) DEFAULT NULL,
  `idCidade` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `frascos`
--
DROP TABLE IF EXISTS `frascos`;
CREATE TABLE `frascos` (
  `FRACODIGO` char(5) DEFAULT NULL,
  `FRANOME` char(50) DEFAULT NULL,
  `frasid` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`frasid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `titulos_vencidos`
--
DROP TABLE IF EXISTS `titulos_vencidos`;
CREATE TABLE `titulos_vencidos` (
  `idTitulosVencidos` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Est` varchar(10) DEFAULT NULL,
  `Esp` varchar(10) DEFAULT NULL,
  `Serie` varchar(10) DEFAULT NULL,
  `Titulo` int(11) DEFAULT NULL,
  `Parcela` int(11) DEFAULT NULL,
  `Cliente` int(11) DEFAULT NULL,
  `NomeAbreviado` varchar(80) DEFAULT NULL,
  `GrupoCliente` varchar(10) DEFAULT NULL,
  `Carteira` varchar(10) DEFAULT NULL,
  `ValorOriginal` decimal(10,2) DEFAULT NULL,
  `Saldo` decimal(10,2) DEFAULT NULL,
  `Dias` int(11) DEFAULT NULL,
  `Status` varchar(20) DEFAULT NULL,
  `CartaCobrancaEnviada` enum('Enviada','Não Enviada','Erro ao enviar') DEFAULT 'Não Enviada',
  `vencimento` date DEFAULT NULL,
  `DataCriacao` date DEFAULT NULL,
  PRIMARY KEY (`idTitulosVencidos`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `tipo_pagamento`
--
DROP TABLE IF EXISTS `tipo_pagamento`;
CREATE TABLE `tipo_pagamento` (
  `TpSid` int(11) NOT NULL AUTO_INCREMENT,
  `TpOrdemServico` int(15) DEFAULT NULL,
  `TpEntsid` int(11) DEFAULT NULL,
  `tpTipo` enum('Dinheiro','Cheque','Cartao','Guia') DEFAULT NULL,
  `valor` float DEFAULT NULL,
  PRIMARY KEY (`TpSid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `metodo`
--
DROP TABLE IF EXISTS `metodo`;
CREATE TABLE `metodo` (
  `MetSid` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `MetNome` varchar(200) DEFAULT NULL,
  `MetSigla` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`MetSid`),
  KEY `MetSigla` (`MetSigla`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `provisao`
--
DROP TABLE IF EXISTS `provisao`;
CREATE TABLE `provisao` (
  `idExameSolicitado` int(11) NOT NULL,
  `idOrdemServico` int(11) NOT NULL,
  `exame` varchar(45) DEFAULT NULL,
  `preco` float DEFAULT NULL,
  `quantidade` int(11) DEFAULT NULL,
  `dataEntradaOS` datetime DEFAULT NULL,
  `idEntidade` int(11) DEFAULT NULL,
  PRIMARY KEY (`idExameSolicitado`,`idOrdemServico`),
  KEY `index2` (`dataEntradaOS`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `fila_atendimento`
--
DROP TABLE IF EXISTS `fila_atendimento`;
CREATE TABLE `fila_atendimento` (
  `idFila_Atendimento` int(11) NOT NULL AUTO_INCREMENT,
  `Nome_dept` varchar(45) DEFAULT NULL,
  `link_Atendimento` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idFila_Atendimento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `grupo`
--
DROP TABLE IF EXISTS `grupo`;
CREATE TABLE `grupo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `log_representantes`
--
DROP TABLE IF EXISTS `log_representantes`;
CREATE TABLE `log_representantes` (
  `idLogRepresentante` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ConteudoAnterior` text,
  `Quem` int(11) DEFAULT NULL,
  `Data` datetime DEFAULT NULL,
  `Operacao` varchar(10) DEFAULT NULL,
  `idRepresentante` int(11) DEFAULT NULL,
  PRIMARY KEY (`idLogRepresentante`),
  KEY `idRepresentante` (`idRepresentante`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='log para a tabela representantes';
--
-- Table structure for table `logistica_celulactrlestoque`
--
DROP TABLE IF EXISTS `logistica_celulactrlestoque`;
CREATE TABLE `logistica_celulactrlestoque` (
  `idctrlestoque` int(11) NOT NULL AUTO_INCREMENT,
  `idCelula` int(11) DEFAULT NULL,
  `idProduto` int(11) NOT NULL,
  `estoqueMinimo` int(11) NOT NULL DEFAULT '0',
  `estoqueMaximo` int(11) NOT NULL DEFAULT '0',
  `estoqueMedio` int(11) DEFAULT '0',
  `dataLibera` datetime DEFAULT NULL,
  PRIMARY KEY (`idctrlestoque`),
  KEY `index_produto` (`idProduto`),
  KEY `index_celula` (`idCelula`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
--
-- Table structure for table `entidade_recebe_resultados`
--
DROP TABLE IF EXISTS `entidade_recebe_resultados`;
CREATE TABLE `entidade_recebe_resultados` (
  `EntCadastro` int(11) NOT NULL,
  `EntDestino` int(11) NOT NULL,
  PRIMARY KEY (`EntCadastro`,`EntDestino`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `cep`
--
DROP TABLE IF EXISTS `cep`;
CREATE TABLE `cep` (
  `CPCODIGO` int(11) DEFAULT NULL,
  `CPESTADO` char(2) DEFAULT NULL,
  `CPCIDADE` varchar(30) DEFAULT NULL,
  `CPDESCR` varchar(60) DEFAULT NULL,
  `CPTITULO` varchar(10) DEFAULT NULL,
  `CPBAIRRO` varchar(30) DEFAULT NULL,
  `CPCEPANT` varchar(5) DEFAULT NULL,
  `CPCEP` varchar(9) DEFAULT NULL,
  `CPDDD` varchar(4) DEFAULT NULL,
  `CPTIPO` varchar(10) DEFAULT NULL,
  KEY `PACCEP_CODIGO` (`CPCODIGO`),
  KEY `PACCEP_DESCR` (`CPDESCR`),
  KEY `PACCEP_CIDADE` (`CPCIDADE`),
  KEY `CPESTADO` (`CPESTADO`,`CPCIDADE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `entidades_exames_alerta`
--
DROP TABLE IF EXISTS `entidades_exames_alerta`;
CREATE TABLE `entidades_exames_alerta` (
  `idEntidade` int(11) NOT NULL,
  `Exame` varchar(10) NOT NULL,
  `Alerta` text,
  PRIMARY KEY (`idEntidade`,`Exame`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `pendencia_celula_email`
--
DROP TABLE IF EXISTS `pendencia_celula_email`;
CREATE TABLE `pendencia_celula_email` (
  `mailid` int(11) NOT NULL AUTO_INCREMENT,
  `mailRepSid` int(11) DEFAULT NULL,
  `mailemail` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`mailid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET FOREIGN_KEY_CHECKS=1;